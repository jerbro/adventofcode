

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <chrono>

std::vector<long> readFile(const std::string & filename);
long getStepsToFinish(std::vector<long>& instructions, long newValueForInstruction(long));

int main(int argc, char *argv[]) {
    auto start = std::chrono::steady_clock::now();
    auto instructions = readFile("input.txt");
    auto origInstructions = instructions;


    std::cout << "Advent of code 2017, day 05" << std::endl;
    std::cout << "A: "<<getStepsToFinish(instructions,[](long currVal){return currVal+1;})<<std::endl;
    std::cout << "B: "<<getStepsToFinish(origInstructions,[](long currVal){return currVal < 3?currVal+1:currVal-1;})<<std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took "<<std::chrono::duration <double, std::milli> (end-start).count() << " ms"<<std::endl;

    return 0;
}

long getStepsToFinish(std::vector<long>& instructions, long newValueForInstruction(long)){
    long instrCount = 0;
    int currInstr = 0;
    int nrOfInstr = instructions.size();
    while (currInstr >= 0 && currInstr < nrOfInstr){
        int nextInstr =instructions.at(currInstr)+currInstr;
        instrCount++;
        instructions.at(currInstr) = newValueForInstruction(instructions.at(currInstr));
        currInstr = nextInstr;
    }
    return instrCount;
}


std::vector<long> readFile(const std::string & filename){
    std::vector<long> result;

    std::fstream fs;
    long currVal = 0;
    fs.open(filename, std::ios::in);
    while(fs >> currVal){
        result.push_back(currVal);
    }
    fs.close();

    return result;
}
