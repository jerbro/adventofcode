#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <chrono>
#include <iomanip>
#include <boost/algorithm/string.hpp>
#include <regex>
#include <set>

typedef std::vector<std::pair<int,int>> BRIDGEPARTS;
BRIDGEPARTS readFile(const std::string &filename);
int getMaxStrength(const BRIDGEPARTS &parts, int prevPart);
std::set<int> getIdsOfFittingParts(const BRIDGEPARTS &parts, int prevPart);
std::pair<int,int> getMaxLength(const BRIDGEPARTS &parts, int prevPart, int currLength);

int main(int argc, char *argv[]) {

    auto start = std::chrono::steady_clock::now();


    auto bridgeParts = readFile("input.txt");


    auto answerForA = getMaxStrength(bridgeParts,0);

    auto answerForB = getMaxLength(bridgeParts,0,0).second;


    std::cout << "Advent of code 2017, day 24" << std::endl;
    std::cout << "A: " << answerForA << std::endl;
    std::cout << "B: " << answerForB << std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}


int getMaxStrength(const BRIDGEPARTS &parts, int prevPart){
    auto fittingParts = getIdsOfFittingParts(parts,prevPart);
    if (fittingParts.empty())
        return 0;

    int maxStrength = 0;
    for (auto id:fittingParts){
        auto currPart = parts.at(id);
        int currStrength = currPart.first + currPart.second;
        int otherSideId = (currPart.first == prevPart)?currPart.second:currPart.first;
        BRIDGEPARTS restOfParts = parts;
        restOfParts.erase(restOfParts.begin()+id);
        currStrength += getMaxStrength(restOfParts,otherSideId);

        if (currStrength > maxStrength)
            maxStrength = currStrength;

    }
    return maxStrength;
}


//returns a pair with length, strength
std::pair<int,int> getMaxLength(const BRIDGEPARTS &parts, int prevPart, int currLength){
    auto fittingParts = getIdsOfFittingParts(parts,prevPart);
    if (fittingParts.empty())
        return std::make_pair(currLength,0);

    int maxStrength = 0;
    int maxLength = 0;
    for (auto id:fittingParts){
        auto currPart = parts.at(id);
        int currStrength = currPart.first + currPart.second;
        int otherSideId = (currPart.first == prevPart)?currPart.second:currPart.first;
        BRIDGEPARTS restOfParts = parts;
        restOfParts.erase(restOfParts.begin()+id);
        auto propsOfOtherParts = getMaxLength(restOfParts,otherSideId,currLength+1);

        currStrength += propsOfOtherParts.second;
        if (propsOfOtherParts.first > maxLength) {
            maxLength = propsOfOtherParts.first;
            maxStrength = currStrength;
        }
        else if(propsOfOtherParts.first == maxLength){
            if (currStrength > maxStrength){
                maxStrength = currStrength;
            }
        }

    }
    return std::make_pair(maxLength,maxStrength);
}



std::set<int> getIdsOfFittingParts(const BRIDGEPARTS &parts, int prevPart){
    std::set<int> result;
    for (int i=0;i<parts.size();i++){
        if (parts.at(i).first == prevPart || parts.at(i).second == prevPart)
            result.insert(i);
    }
    return result;
}

BRIDGEPARTS readFile(const std::string &filename) {
    std::fstream fs;
    std::vector<std::pair<int,int>> result;
    std::string line;
    fs.open(filename, std::ios_base::in);

    while (std::getline(fs, line)) {
        std::replace(line.begin(),line.end(),'/',' ');
        std::stringstream strStr(line);
        int side1,side2;
        strStr >> side1 >> side2;
        result.push_back(std::make_pair(side1,side2));
    }
    fs.close();
    return result;
}