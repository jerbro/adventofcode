#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <chrono>
#include <climits>
#include <iomanip>
#include <boost/algorithm/string.hpp>
#include <unordered_set>


std::vector<std::string> readFile(const std::string& filename);
void executeInstruction(std::string& currOrder, const std::string& instruction);
void executeInstructions(std::string& currOrder, const std::vector<std::string> &instructionlist);
void executePartner(std::string & currOrder, const std::string &instruction);
void executeExchange(std::string & currOrder, const std::string &instruction);
void executeSpin(std::string & currOrder, const std::string &instruction);
std::string  findRepetitionAndSolveForXIteractions( const std::vector<std::string> &instructionlist, long nrOfIterations);

int main(int argc, char *argv[]) {

    auto start = std::chrono::steady_clock::now();

    auto instructions = readFile("input.txt");

    std::string programsOrder = "abcdefghijklmnop";
    executeInstructions(programsOrder,instructions);

    auto answerForA = programsOrder;
    auto answerForB = findRepetitionAndSolveForXIteractions(instructions,1e9);

    std::cout << "Advent of code 2017, day 16" << std::endl;
    std::cout << "A: " << answerForA << std::endl;
    std::cout << "B: " << answerForB << std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}

std::string findRepetitionAndSolveForXIteractions( const std::vector<std::string> &instructionlist, long nrOfIterations){
    std::vector<std::string> foundOrdersList;
    std::string currOrder = "abcdefghijklmnop";
    bool duplicateFound = 0;
    while (!duplicateFound){
        executeInstructions(currOrder,instructionlist);
        if (std::find(foundOrdersList.begin(),foundOrdersList.end(),currOrder) ==foundOrdersList.end()){
            foundOrdersList.push_back(currOrder);
        } else{
            duplicateFound = true;
        }
    }

    int finalLocation = (nrOfIterations)%foundOrdersList.size()-1;
    return foundOrdersList.at(finalLocation);
}

void executeSpin(std::string & currOrder, const std::string &instruction){
    std::string lengthStr = instruction;
    lengthStr.erase(0,1);
    int length = atoi(lengthStr.c_str());
    std::rotate(currOrder.begin(),currOrder.end()-length,currOrder.end());
}


void executeExchange(std::string & currOrder, const std::string &instruction){
    std::string instructionStr = instruction;
    instructionStr.erase(0,1);
    std::vector<std::string> positions;
    boost::split(positions,instructionStr,boost::is_any_of("/"));
    auto pos1 = atoi(positions.at(0).c_str());
    auto pos2 = atoi(positions.at(1).c_str());
    char tmpch;
    tmpch = currOrder.at(pos1);
    currOrder.at(pos1) = currOrder.at(pos2);
    currOrder.at(pos2) = tmpch;
}

void executePartner(std::string & currOrder, const std::string &instruction){
    std::string instructionStr = instruction;
    instructionStr.erase(0,1);
    std::vector<std::string> charsToSwap;
    boost::split(charsToSwap,instructionStr,boost::is_any_of("/"));
    char ch1 = charsToSwap.at(0)[0];
    char ch2 = charsToSwap.at(1)[0];
    for (int i=0;i<currOrder.size();i++){
        if (currOrder.at(i) ==ch1){
            currOrder.at(i) = ch2;
        }
        else if (currOrder.at(i) ==ch2){
            currOrder.at(i) = ch1;
        }
    }
}

void executeInstructions(std::string& currOrder, const std::vector<std::string> &instructionlist){
    for (auto currInstr : instructionlist){
        executeInstruction(currOrder,currInstr);
    }
}
void executeInstruction(std::string& currOrder, const std::string& instruction){
    char ch = instruction.at(0);
    switch (ch){
        case 's':
            executeSpin(currOrder,instruction);
            break;
        case 'x' :
            executeExchange(currOrder,instruction);
            break;
        case 'p':
            executePartner(currOrder,instruction);
            break;
        default:
            throw std::runtime_error("Error, unknown instruction");

    }
}

std::vector<std::string> readFile(const std::string& filename){
    std::fstream fs;
    std::vector<std::string> result;
    fs.open(filename, std::ios_base::in);
    std::string filecontent;
    fs >> filecontent;
    boost::split(result,filecontent,boost::is_any_of(","));
    fs.close();
    return result;

}