cmake_minimum_required(VERSION 3.8)
project(2017_07)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp)
add_executable(2017_07 ${SOURCE_FILES})