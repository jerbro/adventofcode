

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <chrono>

#include <list>
#include <boost/functional/hash.hpp>
#include <boost/algorithm/string.hpp>
#include <unordered_set>
#include <regex>




struct rawNodeInfo{
    int weight;
    std::string name;
    std::vector<std::string> childNames;
};

rawNodeInfo getRawnodeByName(std::string name,const std::vector<rawNodeInfo>&rawnodes);

class Node{
public:
    int ownWeight;

    std::vector<Node> childNodes;

    Node(rawNodeInfo nodeinfo, const std::vector<rawNodeInfo>& allRawNodes, Node* parent){
        this->ownWeight = nodeinfo.weight;
        this->name = nodeinfo.name;
        if (!nodeinfo.childNames.empty()){
            for (auto childname:nodeinfo.childNames){
                childNodes.push_back(Node(getRawnodeByName(childname,allRawNodes),allRawNodes,this));
            }
        }
    }
    int getTotalWeight(){
        if (this->childNodes.empty()) {
            return ownWeight;
        }
        else{
            int totWeight = this->ownWeight;
            for (auto& child:childNodes){
                totWeight += child.getTotalWeight();
            }
            return totWeight;
        }
    }
private:
    std::string name;
};

std::vector<rawNodeInfo> readFile(const std::string & filename);
rawNodeInfo parseLine(std::string line);
std::string getNameOfRootNode(const std::vector<rawNodeInfo>&rawnodes);
std::set<std::string> makeListOfAllChildNodes(const std::vector<rawNodeInfo> &rawnodes);
rawNodeInfo getRawnodeByName(std::string name,const std::vector<rawNodeInfo>&rawnodes);
Node buildTreeStartingWithThisNode(std::string startNodeName ,const std::vector<rawNodeInfo>&rawnodes);
int weightForChildnodes(Node& currNode);
int solveB(Node& rootnode);


int main(int argc, char *argv[]) {
    auto start = std::chrono::steady_clock::now();
    auto nodes = readFile("input.txt");
    auto answerForA =  getNameOfRootNode(nodes);

    auto nodeTree = buildTreeStartingWithThisNode(answerForA,nodes);
    auto answerForB = solveB(nodeTree);

    std::cout << "Advent of code 2017, day 07" << std::endl;
    std::cout << "A: "<<answerForA<<std::endl;
    std::cout << "B: "<<answerForB<<std::endl;
//

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took "<<std::chrono::duration <double, std::milli> (end-start).count() << " ms"<<std::endl;

    return 0;
}

rawNodeInfo getRawnodeByName(std::string name,const std::vector<rawNodeInfo>&rawnodes){
    for (auto node:rawnodes){
        if (node.name == name)
        {
            return node;
        }
    }
}

//Get the wrond node (given there is a wrong node in the current path)
// If there are no childs, the current node is the wrong one.
// The offset of the wrong node is known, so the wrong weight of a child is known.
// I a child has the 'wrong' weight, return that child. Else return the current rootnode.
Node getWrongNode(Node& rootnode, int offset, int expectedValue){
    if (rootnode.childNodes.size() < 1)
        return rootnode;
    int childExpectedValue = (expectedValue - rootnode.ownWeight)/rootnode.childNodes.size();
    for (auto child:rootnode.childNodes){
        if (child.getTotalWeight() == childExpectedValue - offset)
            return getWrongNode(child,offset,childExpectedValue);
    }
    return rootnode;
}

// Solve B. First get the 'correct' weight for each of the childnodes of the top-root-node.
// Then find the wrond child, and find out what the offset of the wrong value is. After that,
// get the wrong node, end return the value that the wrong node should have.
int solveB(Node& rootnode){
    int weightForChildsOfRoot = weightForChildnodes(rootnode);
    int valueOffset=0;
    for (auto childNode : rootnode.childNodes){
        if (childNode.getTotalWeight() != weightForChildsOfRoot){
            //The wrong value is in this tree.
            valueOffset = weightForChildsOfRoot-childNode.getTotalWeight(); // Value that should be added to the tree.
        }
    }
    Node wrongNode = getWrongNode(rootnode,valueOffset,rootnode.getTotalWeight()+valueOffset);
    return wrongNode.ownWeight+ valueOffset;
}



//return the correct weight of child-nodes. Assuming more than 2 childnodes are available.
// Also known that only 1 value is wrong, so if more than 2 time the same weight is found,
// that value should be correct.
int weightForChildnodes(Node& currNode){
    std::map<int,int> weightCounts;
    if (currNode.childNodes.size() > 2){
        for (auto child:currNode.childNodes) {
            if (weightCounts.find(child.getTotalWeight())!= weightCounts.end()){
                weightCounts.find(child.getTotalWeight())->second++;
            }
            else{
                weightCounts.insert(std::make_pair(child.getTotalWeight(), 1));
            }
        }
        // Only 1 value is wrong, so if there are more than 2 childs with the same number, these are correct
        for (auto item:weightCounts){
            if (item.second > 1)
                return item.first;
        }
        throw std::runtime_error("Something went wrong, no value with more than 1 occurence.");

    } else
        throw std::runtime_error("Error, expected to have more than 3 childs of root-node");
}

//Build a tree with Nodes starting with a specific node (constructed from rawNodeInfo)
Node buildTreeStartingWithThisNode(std::string startNodeName ,const std::vector<rawNodeInfo>&rawnodes){
    auto startNode = getRawnodeByName(startNodeName, rawnodes);
    return Node(startNode,rawnodes,nullptr);
}

//Find the name of the root-node. This is the node that is no a child of any other node.
std::string getNameOfRootNode(const std::vector<rawNodeInfo>&rawnodes){
    std::set<std::string> namesOfChildnodes = makeListOfAllChildNodes(rawnodes);
    for (auto node:rawnodes){
        if (namesOfChildnodes.find(node.name) == namesOfChildnodes.end())
        {
            return node.name;
        }
    }
    throw std::runtime_error("Error, all nodes appear to be a child-node.");
}

//Put all child-nodes in an set. (no matter how many levels deep)
std::set<std::string> makeListOfAllChildNodes(const std::vector<rawNodeInfo> &rawnodes){
    std::set<std::string> result;
    for (rawNodeInfo currNode:rawnodes){
        for (std::string childStr: currNode.childNames)
            result.insert(childStr);
    }
    return result;
}


//Parse a line to a rawNodeInfo-struct
rawNodeInfo parseLine(std::string line){
    rawNodeInfo result;
    std::smatch match;
    std::regex re("([a-zA-Z]+) \\(([0-9]+)\\)( -> )?(.*)$");
    std::regex_match(line, match, re);

    result.name = match[1];
    std::string weight = match[2];
    result.weight = atoi(weight.c_str());

    if (match.size() > 4) {
        std::string childs =match[4];
        boost::split(result.childNames, childs, boost::is_any_of(", "));
    }
    result.childNames.erase(std::remove_if(result.childNames.begin(),result.childNames.end(),[](std::string txt){return txt.empty();}),result.childNames.end());
    return result;

}


//Read the file.
std::vector<rawNodeInfo> readFile(const std::string & filename){
    std::vector<rawNodeInfo> result;

    std::fstream fs;

    fs.open(filename, std::ios::in);
    std::string line;
    while(std::getline(fs,line)){

        result.push_back(parseLine(line));
    }
    fs.close();

    return result;
}
