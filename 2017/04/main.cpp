

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>

std::vector<std::vector<std::string>> readFile(const std::string & filename);
bool passPhraseIsValid(const std::vector<std::string> passPhrase);
int getNrOfValidPhrases(const std::vector<std::vector<std::string>> phrases);
std::string makeStringAlphabetical(const std::string txt);
std::vector<std::vector<std::string>> getAlphabeticalPassphrases(const std::vector<std::vector<std::string>> phrases);

int main(int argc, char *argv[]) {
    auto passPhrases = readFile("input.txt");
    auto alphabeticalWords =getAlphabeticalPassphrases(passPhrases);

    std::cout << "Advent of code 2017, day 04" << std::endl;
    std::cout << "A: "<<getNrOfValidPhrases(passPhrases)<<std::endl;
    std::cout << "B: "<<getNrOfValidPhrases(alphabeticalWords)<<std::endl;


    return 0;
}


std::vector<std::vector<std::string>> readFile(const std::string & filename){
    std::vector<std::vector<std::string>> result;

    std::fstream fs;
    std::string line;
    fs.open(filename, std::ios::in);
    while(std::getline(fs,line)){
        std::stringstream strStr(line);
        std::vector<std::string> linePasswords;
        std::string currWord;
        while (strStr>>currWord){
            linePasswords.push_back(currWord);
        }
        result.push_back(linePasswords);
    }
    fs.close();

    return result;
}

bool passPhraseIsValid(const std::vector<std::string> passPhrase){
    for (auto currItem:passPhrase){
        if (std::count(passPhrase.begin(),passPhrase.end(),currItem)>1)
            return false;
    }
    return true;
}

int getNrOfValidPhrases(const std::vector<std::vector<std::string>> phrases){
    int result = 0;
    for (auto phrase:phrases){
        if (passPhraseIsValid(phrase)){
            result ++;
        }
    }
    return result;
}
std::string makeStringAlphabetical(const std::string txt){
    std::string result;
    result = txt;
    std::sort(result.begin(), result.end(), [](char a, char b){return a<b;});
    return result;
}

std::vector<std::vector<std::string>> getAlphabeticalPassphrases(const std::vector<std::vector<std::string>> phrases) {
    std::vector<std::vector<std::string>> result;
    for (auto const phrase: phrases){
        std::vector<std::string> newPhrase;
        std::string newWord;
        for (auto const word:phrase){
            newWord = makeStringAlphabetical(word);
            newPhrase.push_back(newWord);
        }
        result.push_back(newPhrase);
    }
    return result;
}