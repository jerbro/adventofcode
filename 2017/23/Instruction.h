//
// Created by jeroen on 8-12-17.
//

#ifndef INC_2017_INSTRUCTION_H
#define INC_2017_INSTRUCTION_H


#include "Command.h"

class Command;

class Instruction {
public:
    Instruction(Command *cmd, long long  *cmdoper1, long long  *cmdoper2) : command(cmd), commandOper1(cmdoper1),
                                                                                  commandOper2(cmdoper2) {};

    void execute();

    int getCommandOper1Value() { return *commandOper1; };

private:
    Command *command;
    long long *commandOper1;
    long long *commandOper2;


};


#endif //INC_2017_INSTRUCTION_H
