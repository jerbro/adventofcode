

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <chrono>

#include <list>
#include <boost/functional/hash.hpp>
#include <boost/algorithm/string.hpp>
#include <unordered_set>
#include <regex>
#include <thread>
#include "Instruction.h"
#include "Processor.h"
#include "Pipe.h"


std::vector<std::string> readFile(const std::string & filename);


int main(int argc, char *argv[]) {
    auto start = std::chrono::steady_clock::now();


    std::vector<Command *> commands;
    std::map<std::string, int> registers;

    auto instructions = readFile("input.txt");
    Processor* proc = new Processor(instructions, "PrA");
    proc->executeProgram();
    auto answerForA =  proc->getMulCnt();

    instructions = readFile("manualSimplifiedInput.txt");
    proc->loadNewProgram(instructions);
    proc->resetCpu();
    proc->setRegister("a",1);
    proc->executeProgram();
    auto answerForB =  proc->getRegister("h");


    std::cout << "Advent of code 2017, day 23" << std::endl;
    std::cout << "A: "<<answerForA<<std::endl;
    std::cout << "B: "<<answerForB<<std::endl;
//

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}




//
////Read the file.
std::vector<std::string> readFile(const std::string & filename){
    std::vector<std::string> lines;

    std::fstream fs;

    fs.open(filename, std::ios::in);
    std::string line;
    while(std::getline(fs,line)){
        if (line.at(line.size()-1) == '\r')
            line.erase(line.size()-1,1);
        lines.push_back(line);
    }
    fs.close();

    return lines;
}
