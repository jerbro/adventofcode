//
// Created by jeroen on 8-12-17.
//

#include "Command.h"

void Command::nextInstr() { (*instrPntr)++; }

Command::Command(std::string commandName, int *instrPntr) : commandName(commandName), instrPntr(instrPntr) {};

std::string Command::getInstructionName() { return commandName; }


cmdSet::cmdSet(int *instrPntr) : Command("set", instrPntr) {};

void cmdSet::execute(long long *oper1, long long *oper2) {
    (*oper1) = (*oper2);
    nextInstr();
}


cmdAdd::cmdAdd(int *instrPntr) : Command("add", instrPntr) {};

void cmdAdd::execute(long long *oper1, long long *oper2) {
    (*oper1) += (*oper2);
    nextInstr();
}



cmdSub::cmdSub(int *instrPntr) : Command("sub", instrPntr) {};

void cmdSub::execute(long long *oper1, long long *oper2) {
    (*oper1) -= (*oper2);
    nextInstr();
}


cmdMul::cmdMul(int *instrPntr) : Command("mul", instrPntr) {};

void cmdMul::execute(long long *oper1, long long *oper2) {
    if (execCnt != nullptr)
        (*execCnt)++;
    (*oper1) *= (*oper2);
    nextInstr();
}

void cmdMul::setExeccntReg(int* executeCount){
    this->execCnt = executeCount;
}


cmdMod::cmdMod(int *instrPntr) : Command("mod", instrPntr) {};

void cmdMod::execute(long long *oper1, long long *oper2) {
    (*oper1) %= (*oper2);
    nextInstr();
}


cmdSnd::cmdSnd(int *instrPntr, int* sndCnt) : Command("snd", instrPntr), runcnt(sndCnt) {};

void cmdSnd::execute(long long *oper1, long long *oper2) {
    this->pipe->put(*oper1);
    (*(this->runcnt))++;
    nextInstr();
}

void cmdSnd::setPipe(Pipe *pipe) {
    this->pipe = pipe;
}


cmdRcv::cmdRcv(int *instrPntr, bool *keepRunning) : Command("rcv", instrPntr), keepCpuRunning(keepRunning),
                                                    mode('A') {};

void cmdRcv::execute(long long *oper1, long long *oper2) {
    if (mode == 'A') { // Instruction as expected by part A;
        if (*oper1 != 0) {
            (*keepCpuRunning) = false;
        }
    } else {
        bool readFailed;
        long long tmp =pipe->get(&readFailed);
        (*oper1) = tmp;
        if (readFailed)
            (*keepCpuRunning) = false;
    }
    nextInstr();
}

void cmdRcv::setPipe(Pipe *pipe) {
    this->pipe = pipe;
}

void cmdRcv::setMode(char mode) {
    this->mode = mode;
}


cmdJgz::cmdJgz(int *instrPntr) : Command("jgz", instrPntr) {};

void cmdJgz::execute(long long *oper1, long long *oper2) {
    if ((*oper1) > 0)
        (*instrPntr) += (*oper2);
    else
        nextInstr();
}


cmdJnz::cmdJnz(int *instrPntr) : Command("jnz", instrPntr) {};

void cmdJnz::execute(long long *oper1, long long *oper2) {
    if ((*oper1) != 0)
        (*instrPntr) += (*oper2);
    else
        nextInstr();
}

cmdNop::cmdNop(int* instrPntr) : Command("nop", instrPntr) {}
void cmdNop::execute(long long  *oper1, long long  *oper2){
    nextInstr();
}

//jump if prime. If operand 1 is prime, jump positions as indicated by oper2
cmdJip::cmdJip(int* instrPntr):Command("jip",instrPntr),maxCheckVal(2e5){
    initializePrimes();
}

void cmdJip::execute(long long *oper1, long long *oper2) {
    if ((*oper1)>maxCheckVal)
        throw std::runtime_error("Checked unexpected large value for prime.");

    if (primes.find(*oper1) != primes.end() ){
        (*instrPntr) += (*oper2);
    } else{
        nextInstr();
    }
}

void cmdJip::initializePrimes(){
    primes.clear();
    //fill the set with all numbers below maxVal
        for (int i=2;i<maxCheckVal;i++){
            primes.insert(i);
        }
    //go throug the list. If a number is found, it is prime. remove all multiples of this nunmber from the list.
        for (int i=2;i<maxCheckVal/2;i++){
            if (primes.find(i)!=primes.end()){
                int prod=0;
                int currCnt=2;
                while (prod<maxCheckVal){
                    prod = i*currCnt;
                    currCnt++;
                    if (primes.find(prod)!=primes.end()){
                        primes.erase(prod);
                    }
                }
            }
        }
}

