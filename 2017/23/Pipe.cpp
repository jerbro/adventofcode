//
// Created by jeroen on 24-12-17.
//

#include <zconf.h>
#include <iostream>
#include "Pipe.h"

Pipe::Pipe(Processor *receivingProc, Processor *sendingProc, std::string name)
        : receivingProc(receivingProc), sendingProc(sendingProc), name(name) {
    receivingProc->setRcvPipe(this);
    sendingProc->setSndPipe(this);
}

void Pipe::put(long long val) {
    lock.lock();
    buffer.push_back(val);
    lock.unlock();

}


long long Pipe::peekLast() {
    if (!buffer.empty())
        return buffer.back();
    else
        return 0;
}

long long Pipe::get(bool *failed) {
    bool newDataExpected = true;
    while (buffer.empty() && newDataExpected) {
        newDataExpected = (sendingProc->getStatus() == EnumProcStatus::Init) ||
                          (sendingProc->getStatus() == EnumProcStatus::Running);
        receivingProc->setStatus(EnumProcStatus::Waiting);
        usleep(100);
    }
    receivingProc->setStatus(EnumProcStatus::Running);
    if (!newDataExpected && buffer.empty())
        *failed = true;
    else
        *failed = false;

    lock.lock();
    long long retval;
    if (buffer.empty())
        retval = 0;
    else {
        retval = buffer.front();
        buffer.pop_front();
    }
    lock.unlock();
    return retval;
}
