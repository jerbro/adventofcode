//
// Created by jeroen on 23-12-17.
//

#include <boost/algorithm/string.hpp>
#include <iostream>
#include "Processor.h"


Processor::Processor(std::vector<std::string> asminstructions, std::string name)
        : name(name){
    registerCommands();
    for (auto instr:asminstructions){
        instructions.push_back(parseAsmInstruction(instr));
    }
    setMulDebugRegister(&mulCnt);
    setStatus ( EnumProcStatus::Init);

}

void Processor::loadNewProgram(std::vector<std::string> asminstructions){
    instructions.clear();
    for (auto instr:asminstructions){
        instructions.push_back(parseAsmInstruction(instr));
    }
}

void Processor::resetCpu(){
    for (auto & reg:registers){
        reg.second = 0;
    }
    instrPntr = 0;
    mulCnt = 0;
    setStatus(EnumProcStatus::Init);
}

void Processor::registerCommands() {
    if (commands.empty()) {
//        commands.push_back(new cmdSnd(&instrPntr,&sndCnt));
        commands.push_back(new cmdSet(&instrPntr));
//        commands.push_back(new cmdAdd(&instrPntr));
        commands.push_back(new cmdSub(&instrPntr));
        commands.push_back(new cmdMul(&instrPntr));
        commands.push_back(new cmdMod(&instrPntr));
        commands.push_back(new cmdNop(&instrPntr));
//        commands.push_back(new cmdRcv(&instrPntr,&keepRunning));
//        commands.push_back(new cmdJgz(&instrPntr));
        commands.push_back(new cmdJnz(&instrPntr));
        commands.push_back(new cmdJip(&instrPntr));
    }
}

long long int * Processor::getRegisterPointer(std::string regname) {

    if (this->registers.find(regname) == this->registers.end()) {
        registers.insert(std::make_pair(regname, 0));
    }

    return &registers.find(regname)->second;

}

long long int * Processor::getConstantPointer(long long int val) {

    if (constants.find(val) == constants.end()) {
        constants.insert(std::make_pair(val, val));
    }

    return &constants.find(val)->second;

}

long long int * Processor::operandToAddress(std::string regname){
    if (regname.empty())
        return nullptr;

    bool regIsNumber = regname.find_first_not_of("-.0123456789") == std::string::npos;
    if (!regIsNumber)
        return getRegisterPointer(regname);
    else
        return getConstantPointer(atoi(regname.c_str()));


}

void Processor::staticExecuteProgram(Processor* proc){
    proc->executeProgram();
}

void printRegisters(const std::map<std::string,long long> &regs){
    for (auto item:regs){
        std::cout << item.first << ": "<<item.second<<"  ";
    }
}

void Processor::executeProgram(){
    setStatus( EnumProcStatus::Running);
    keepRunning = true;
    unsigned long long cntr = 0;
    int programSize = instructions.size();
    while (instrPntr >= 0 && instrPntr < programSize && keepRunning ==true){
        instructions.at(instrPntr)->execute();
        cntr++;

    }
    setStatus(EnumProcStatus::Finished);
}

void Processor::printRegisters(void){
    for (auto reg:registers){
        std::cout << reg.first<<": " <<reg.second<<"; ";
    }
    std::cout<<std::endl;
}

int Processor::getMulCnt(){
    return mulCnt;
}

Instruction *Processor::parseAsmInstruction(std::string line) {
    std::vector<std::string> splittedInstruction;
    boost::split(splittedInstruction, line, boost::is_any_of(" "));
    if (splittedInstruction.size() <1)
        throw std::runtime_error("Error, to few arguments for instruction");

    Instruction *instr;
    long long *oper1;
    long long *oper2;

    if (splittedInstruction.size() >=2)
        oper1 = operandToAddress(splittedInstruction[1]);
    else
        oper1 = nullptr;

    if (splittedInstruction.size() >=3)
         oper2 = operandToAddress(splittedInstruction[2]);
    else
        oper2 = nullptr;

    instr = new Instruction(getCommand(splittedInstruction[0]), oper1, oper2);
    return instr;
};


Command *Processor::getCommand(std::string cmdName) {
    for (auto &comd:commands) {
        if (cmdName == comd->getInstructionName()) {
            return comd;
        }
    }
    throw std::runtime_error("Error, command " + cmdName + " Not found");
}



void Processor::setStatus(EnumProcStatus status){
    this->status = status;
}

EnumProcStatus Processor::getStatus(){
    return this->status;
}

void Processor::setRegister(std::string regname,long long val){
    registers.find(regname)->second = val;
}

long long Processor::getRegister(std::string regname){
    return registers.find(regname)->second;
}

void Processor::setRcvPipe(Pipe* pipe){
    cmdRcv* cmd_rcv = dynamic_cast<cmdRcv*> (this->getCommand("rcv"));

    cmd_rcv->setPipe(pipe);
}

void Processor::setSndPipe(Pipe* pipe){
    cmdSnd* cmd_snd = dynamic_cast<cmdSnd*> (this->getCommand("snd"));
    cmd_snd->setPipe(pipe);

}



void Processor::setRcvMode(char mode){
    cmdRcv* cmd_rcv = dynamic_cast<cmdRcv*> (this->getCommand("rcv"));
    cmd_rcv->setMode(mode);

}

void Processor::setMulDebugRegister(int* cntReg){
    cmdMul* cmd_mul = dynamic_cast<cmdMul*> (this->getCommand("mul"));
    cmd_mul->setExeccntReg(cntReg);
}