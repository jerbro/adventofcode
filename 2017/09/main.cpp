

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <chrono>

#include <list>
#include <boost/functional/hash.hpp>
#include <boost/algorithm/string.hpp>
#include <unordered_set>
#include <regex>
#include "Group.h"

std::stringstream readFile(const std::string &filename);

Group *parseStream(std::stringstream &instream);

int main(int argc, char *argv[]) {
    auto start = std::chrono::steady_clock::now();

    auto stream = readFile("input.txt");
    auto groups = parseStream(stream);

    auto answerForA = groups->getScore();
    auto answerForB = groups->getGarbageSize();

    std::cout << "Advent of code 2017, day 09" << std::endl;
    std::cout << "A: " << answerForA << std::endl;
    std::cout << "B: " << answerForB << std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}

Group *parseStream(std::stringstream &instream) {
    char c;
    instream >> c;
    if (c == '{')
        return new Group(instream, 1);
    else
        throw std::runtime_error("Error, unexpected start of stringstream");
}


////Read the file.
std::stringstream readFile(const std::string &filename) {
    std::stringstream result;
    std::fstream fs;
    fs.open(filename, std::ios::in);
    result << fs.rdbuf();
    fs.close();
    return result;
}
