//
// Created by jeroen on 9-12-2017.
//

#ifndef INC_2017_GROUP_H
#define INC_2017_GROUP_H
#include <sstream>
#include <string>
#include <vector>
#include "Garbage.h"

class Group {
public:
    Group(std::stringstream & instream,int currDepth);
    int getScore();
    unsigned long getGarbageSize();
private:
    int currDepth;
    std::vector<Group*> groups;
    std::vector<Garbage*> garbage;
    std::string otherContent;

};


#endif //INC_2017_GROUP_H
