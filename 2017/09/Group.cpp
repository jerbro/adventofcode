//
// Created by jeroen on 9-12-2017.
//

#include "Group.h"

Group::Group(std::stringstream &instream, int currDepth) : currDepth(currDepth) {
    bool groupIsFinished = false;
    char c;
    char ignore;
    while ((!groupIsFinished) &&  instream>>c) {
        switch (c) {
            case '}':
                groupIsFinished = true;
                break;
            case '{':
                this->groups.push_back(new Group(instream, currDepth + 1));
                break;
            case '!':
                instream>>ignore;
                break;
            case '<':
                this->garbage.push_back( new Garbage(instream,currDepth+1));
                break;
            default:
                otherContent.append(std::string(1,c));

        }
    }
}

int Group::getScore(){
    int result=0;
    for (auto subgroup:groups){
        result += subgroup->getScore();
    }
    result += currDepth;
    return result;
}

unsigned long Group::getGarbageSize(){
    int garbageSize=0;
    for (auto currGarbage:garbage){
        garbageSize += currGarbage->getContentSize();
    }
    for (auto group:groups){
        garbageSize += group->getGarbageSize();
    }
    return garbageSize;
}

