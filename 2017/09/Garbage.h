//
// Created by jeroen on 9-12-2017.
//

#ifndef INC_2017_GARBAGE_H
#define INC_2017_GARBAGE_H

#include <sstream>
#include <string>

class Garbage {
public:
    Garbage(std::stringstream & instream, int currDepth);
    unsigned long getContentSize();
private:
    int currDepth;
    std::string otherContent;

};


#endif //INC_2017_GARBAGE_H
