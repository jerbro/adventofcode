//
// Created by jeroen on 9-12-2017.
//

#include "Garbage.h"
Garbage::Garbage(std::stringstream &instream, int currDepth) : currDepth(currDepth) {
    bool garbageIsFinished = false;
    char c;
    while ((!garbageIsFinished) && instream>>c) {
        switch (c) {
            case '>':
                garbageIsFinished = true;
                break;
            case '!':
                char ignore;
                instream>>ignore ;
                break;
            default:
                otherContent.append(std::string(1,c));

        }
    }
}

unsigned long Garbage::getContentSize(){
    return otherContent.size();
}
