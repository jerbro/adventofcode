

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <chrono>

#include <list>
#include <boost/functional/hash.hpp>
#include <boost/algorithm/string.hpp>
#include <unordered_set>
#include <regex>
#include "Instruction.h"

Comparison *getComparison(std::string cmpName, std::vector<Comparison *> &comps);

Command *getCommand(std::string cmdName, std::vector<Command *> &comds);

void registerComparisons(std::vector<Comparison *> &comps);

void registerCommands(std::vector<Command *> &commands);

Instruction* parseLine(std::string line, std::vector<Comparison *> comparisons,
                      std::vector<Command *> commands,
                      std::map<std::string, int>& registers);
int* getRegisterPointer(std::string regname, std::map<std::string, int>& regs);
std::vector<Instruction*> readFile(const std::string & filename, std::vector<Comparison *> comparisons,
                                   std::vector<Command *> commands,
                                   std::map<std::string, int>& registers);
void executeProgram(std::vector<Instruction*> instructions);
int getLargestDuringExecution(std::vector<Instruction*> instructions);
int getLargestValueInRegisters(std::map<std::string,int> registers);
void resetRegisters(std::map<std::string,int> &registers);

int main(int argc, char *argv[]) {
    auto start = std::chrono::steady_clock::now();

    std::vector<Comparison *> comparisons;
    std::vector<Command *> commands;
    std::map<std::string, int> registers;



    registerCommands(commands);
    registerComparisons(comparisons);


    auto instructions = readFile("input.txt",comparisons,commands,registers);
    executeProgram(instructions);

    auto answerForA =  getLargestValueInRegisters(registers);
    resetRegisters(registers);
    auto answerForB = getLargestDuringExecution(instructions);

    std::cout << "Advent of code 2017, day 08" << std::endl;
    std::cout << "A: "<<answerForA<<std::endl;
    std::cout << "B: "<<answerForB<<std::endl;
//

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}

void resetRegisters(std::map<std::string,int>& registers){
    for (auto &reg:registers){
        reg.second = 0;
    }
}
int getLargestValueInRegisters(std::map<std::string,int> registers){
    int largest = INT_MIN;

    for (auto reg:registers){
        if (reg.second>largest)
            largest = reg.second;
    }
    return largest;
}

int  getLargestDuringExecution(std::vector<Instruction*> instructions){
    int largest = INT_MIN;
    for (auto instruction:instructions){

        (*instruction).execute();
        if ((*instruction).getCommandOper1Value() > largest)
            largest = (*instruction).getCommandOper1Value();
    }
    return largest;
}

void executeProgram(std::vector<Instruction*> instructions){
    for (auto instruction:instructions){
        (*instruction).execute();
    }
}

Comparison *getComparison(std::string cmpName, std::vector<Comparison *> &comps) {
    for (auto &comp:comps) {
        if (cmpName == comp->getInstructionName()) {
            return comp;
        }
    }
    throw std::runtime_error("Error, comparison " + cmpName + " Not found");
}

int* getRegisterPointer(std::string regname, std::map<std::string, int>& regs){
    if (regs.find(regname) == regs.end()){
        regs.insert(std::make_pair(regname,0));
    }

    return &regs.find(regname)->second;
}

Command *getCommand(std::string cmdName, std::vector<Command *> &comds) {
    for (auto &comd:comds) {
        if (cmdName == comd->getInstructionName()) {
            return comd;
        }
    }
    throw std::runtime_error("Error, command " + cmdName + " Not found");
}


void registerComparisons(std::vector<Comparison *> &comps) {
    Comparison *cmp = new cmpEq();
    comps.push_back(cmp);
    comps.push_back(new cmpNe());
    comps.push_back(new cmpGe());
    comps.push_back(new cmpGt());
    comps.push_back(new cmpLe());
    comps.push_back(new cmpLt());
}

void registerCommands(std::vector<Command *> &commands) {
    commands.push_back(new cmdDec());
    commands.push_back(new cmdInc());
}

Instruction* parseLine(std::string line, std::vector<Comparison *> comparisons,
                      std::vector<Command *> commands,
                      std::map<std::string, int> &registers) {
    std::vector<std::string> splittedInstruction;
    boost::split(splittedInstruction, line, boost::is_any_of(" "));
    if (splittedInstruction.size()!= 7)
        throw std::runtime_error("Error, Instruction does not consists of exactly 7 words.");

    Instruction *instr;
    instr = new Instruction(getComparison(splittedInstruction[5],comparisons),
    getCommand(splittedInstruction[1],commands),
                            getRegisterPointer(splittedInstruction[0],registers),
                            atoi(splittedInstruction[2].c_str()),
                            getRegisterPointer(splittedInstruction[4],registers),
                            atoi(splittedInstruction[6].c_str())
    );


    return instr;

}

//
////Read the file.
std::vector<Instruction*> readFile(const std::string & filename, std::vector<Comparison *> comparisons,
                                   std::vector<Command *> commands,
                                   std::map<std::string, int> &registers){
    std::vector<Instruction*> result;

    std::fstream fs;

    fs.open(filename, std::ios::in);
    std::string line;
    while(std::getline(fs,line)){

        result.push_back(parseLine(line,comparisons,commands,registers));
    }
    fs.close();

    return result;
}
