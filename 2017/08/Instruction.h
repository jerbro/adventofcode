//
// Created by jeroen on 8-12-17.
//

#ifndef INC_2017_INSTRUCTION_H
#define INC_2017_INSTRUCTION_H


#include "Comparison.h"
#include "Command.h"


class Instruction {
public:
    Instruction(Comparison *cmp, Command *cmd, int *cmdoper1, int cmdoper2, int *cmpoper1, int cmpoper2) : compare(cmp),
                                                                                                           command(cmd),
                                                                                                           commandOper1(
                                                                                                                   cmdoper1),
                                                                                                           commandOper2(
                                                                                                                   cmdoper2),
                                                                                                           compareOper1(
                                                                                                                   cmpoper1),
                                                                                                           compareOper2(
                                                                                                                   cmpoper2) {};

    void execute();

    int getCommandOper1Value() { return *commandOper1; };

private:
    Comparison *compare;
    Command *command;
    int *commandOper1;
    int commandOper2;
    int *compareOper1;
    int compareOper2;


};


#endif //INC_2017_INSTRUCTION_H
