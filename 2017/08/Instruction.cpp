//
// Created by jeroen on 8-12-17.
//

#include "Instruction.h"

void Instruction::execute() {
    if (compare->compare(compareOper1, &compareOper2)){
        command->execute(commandOper1, &commandOper2);
    }
}