//
// Created by jeroen on 8-12-17.
//

#ifndef INC_2017_COMPARISON_H
#define INC_2017_COMPARISON_H

#include <string>

class Comparison {
private:
    std::string compareInstruction;
public:
    Comparison(std::string commandName):compareInstruction(commandName){};
    virtual bool compare(int * val1, int* val2)=0;
    std::string getInstructionName(){return compareInstruction;}
};

class cmpNe : public Comparison{
public :
    cmpNe() : Comparison("!="){}
    bool compare(int* val1, int* val2){
        return *val1 != *val2;
    }
};

class cmpEq :public  Comparison{
public :
    cmpEq() : Comparison("=="){}
    bool compare(int* val1, int* val2){
        return *val1 == *val2;
    }
};

class cmpGt : public Comparison{
public :
    cmpGt() : Comparison(">"){}
    bool compare(int* val1, int* val2){
        return *val1 > *val2;
    }
};

class cmpGe :public  Comparison{
public :
    cmpGe() : Comparison(">="){}
    bool compare(int* val1, int* val2){
        return *val1 >= *val2;
    }
};

class cmpLt :public  Comparison{
public :
    cmpLt() : Comparison("<"){}
    bool compare(int* val1, int* val2){
        return *val1 < *val2;
    }
};

class cmpLe :public  Comparison{
public :
    cmpLe() : Comparison("<="){}
    bool compare(int* val1, int* val2){
        return *val1 <= *val2;
    }
};

#endif //INC_2017_COMPARISON_H
