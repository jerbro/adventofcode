//
// Created by jeroen on 8-12-17.
//

#ifndef INC_2017_COMMAND_H
#define INC_2017_COMMAND_H

#include <string>
class Command {

    private:
        std::string commandName;
    public:
        Command(std::string commandName): commandName(commandName){};
        virtual void execute(int * val1, int* val2)=0;
        std::string getInstructionName(){return commandName;}

};

class cmdDec:public Command{
public:
    cmdDec():Command("dec"){};
    void execute(int* oper1, int* oper2){
        *oper1 -= *oper2;
    }
};

class cmdInc:public Command{
public:
    cmdInc():Command("inc"){};
    void execute(int* oper1, int* oper2){
        *oper1 += *oper2;
    }
};

#endif //INC_2017_COMMAND_H
