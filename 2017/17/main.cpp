#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <chrono>
#include <climits>
#include <iomanip>
#include <boost/algorithm/string.hpp>
#include <unordered_set>


int readFile(const std::string& filename);
void doLockIteration(std::list<int> &lockBuffer, std::list<int>::iterator &currPos, const int stepsize,int currIteration);
std::list<int> getLockbufferAtIteraction(int iterations, int stepsize);
int getValueAfterValueAtIteration(int iterations, int stepsize);
int getValueAfter0AtIteration(int iterations, int stepsize);

int main(int argc, char *argv[]) {

    auto start = std::chrono::steady_clock::now();

    auto stepsize = readFile("input.txt");
    auto answerForA =  getValueAfterValueAtIteration(2017,  stepsize);
    auto answerForB =  getValueAfter0AtIteration(50e6,  stepsize);

    std::cout << "Advent of code 2017, day 17" << std::endl;
    std::cout << "A: " << answerForA << std::endl;
    std::cout << "B: " << answerForB << std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}

int getValueAfter0AtIteration(int iterations, int stepsize){
    int currSize=1;
    int valAfter0=0;
    int currPos=0;
    int currIter = 1;
    for (int i=0;i<iterations;i++){
        currPos += stepsize;
        currPos%=currSize;
        if (currPos == 0)
            valAfter0=currIter;
        currIter++;
        currSize++;
        currPos++;
    }
    return valAfter0;
}


int getValueAfterValueAtIteration(int iterations, int stepsize){
    auto lockBuffer = getLockbufferAtIteraction(iterations,stepsize);
    auto lastIter = std::find(lockBuffer.begin(),lockBuffer.end(),iterations);
    lastIter++;
    if (lastIter == lockBuffer.end())
        return *(lockBuffer.begin());
    else
        return *lastIter;

}

std::list<int> getLockbufferAtIteraction(int iterations, int stepsize){
    std::list<int> lockBuffer {0};
    auto currPos = lockBuffer.begin();
    for (int i=1;i<=iterations;i++){
        doLockIteration(lockBuffer,currPos,stepsize,i);
    }
    return lockBuffer;
}

void doLockIteration(std::list<int> &lockBuffer, std::list<int>::iterator &currPos, const int stepsize,int currIteration){
   for(int i=0;i<stepsize+1;i++){
       currPos++;
       if (currPos == lockBuffer.end())
           currPos = lockBuffer.begin();
   }
    lockBuffer.insert(currPos,currIteration);
    currPos--;
}

int readFile(const std::string& filename){
    std::fstream fs;
    int result;
    fs.open(filename, std::ios_base::in);
    std::string filecontent;
    fs >> result;
    fs.close();
    return result;

}