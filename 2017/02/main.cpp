

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>

std::vector<std::vector<long>> readFile(const std::string &filename);
long calcChecksumA(std::vector<std::vector<long>> const &inputValues);
long calcChecksumB(std::vector<std::vector<long>> const &inputValues);
long findDivisibleNumber(std::vector<long> const &inputValues);

int main(int argc, char *argv[]) {
    auto values = readFile("input.txt");
    auto checksumA = calcChecksumA(values);
    auto checksumB = calcChecksumB(values);

    std::cout << "Advent of code 2017, day 02" << std::endl;
    std::cout << "A: " << checksumA << std::endl;
    std::cout << "B: " << checksumB << std::endl;

    return 0;
}


std::vector<std::vector<long>> readFile(const std::string &filename) {
    std::vector<std::vector<long>> result;
    std::fstream fs;
    fs.open(filename, std::ios::in);
    std::string line;
    while (std::getline(fs, line)) {
        std::vector<long> lineValues;
        long currVal;
        std::stringstream strStr(line);
        while (strStr >> currVal) {
            lineValues.push_back(currVal);
        }
        result.push_back(lineValues);
    }
    return result;
}


long calcChecksumA(std::vector<std::vector<long>> const &inputValues) {
    long result = 0;
    for (std::vector<long> lineValues:inputValues) {
        long max = *(std::max_element(lineValues.begin(), lineValues.end()));
        long min = *(std::min_element(lineValues.begin(), lineValues.end()));
        result += (max - min);
    }
    return result;
}

long calcChecksumB(std::vector<std::vector<long>> const &inputValues) {
    long result = 0;
    for (auto const & lineValues:inputValues) {
        result += findDivisibleNumber(lineValues);
    }
    return result;
}


long findDivisibleNumber(std::vector<long> const &inputValues) {
    for (unsigned long  i = 0; i < inputValues.size(); i++)
        for (unsigned long j = 0; j < inputValues.size(); j++) {
            if (i != j && (inputValues.at(i) % inputValues.at(j) == 0))
                return inputValues.at(i) / inputValues.at(j);
        }

}

