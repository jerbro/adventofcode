//
// Created by jeroen on 23-12-17.
//

#ifndef INC_2017_GENERATOR_H
#define INC_2017_GENERATOR_H


class Generator {
public:
    Generator(long startVal, long factor,long long matchVal);
    long long getNext();
    long long getNextValid();
    void resetToInitialVals();
private:
    long long startVal;
    long factor;
    long long currVal;
    long long comparePattern;
};


#endif //INC_2017_GENERATOR_H
