#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <chrono>
#include <climits>
#include <iomanip>
#include "Generator.h"


std::map<char, int> readFile(const std::string &filename);
std::pair<char,int> parseLine(const std::string &line);
long countMatches(Generator& gena, Generator& genb, long nrOfValues);
long countValidatedMatches(Generator& gena, Generator& genb, long nrOfValues);


int main(int argc, char *argv[]) {

    auto start = std::chrono::steady_clock::now();

    auto startvals = readFile("input.txt");
    auto genA = Generator(startvals.find('A')->second,16807,4);
    auto genB = Generator(startvals.find('B')->second,48271,8);

    auto answerForA = countMatches(genA,genB,40e6);
    auto answerForB = countValidatedMatches(genA,genB,5e6);


    std::cout << "Advent of code 2017, day 15" << std::endl;
    std::cout << "A: " << answerForA << std::endl;
    std::cout << "B: " << answerForB << std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}

long countValidatedMatches(Generator& gena, Generator& genb, long nrOfValues){
    gena.resetToInitialVals();
    genb.resetToInitialVals();

    long long aval;
    long long bval;
    long long matchCount = 0;
    for (unsigned long i=0;i<nrOfValues;i++){
        aval = gena.getNextValid()&0xffff;
        bval = genb.getNextValid()&0xffff;
        if (aval ==bval)
            matchCount++;
    }
    return matchCount;
}

long countMatches(Generator& gena, Generator& genb, long nrOfValues){
    gena.resetToInitialVals();
    genb.resetToInitialVals();

    long long aval;
    long long bval;
    long long matchCount = 0;
    for (unsigned long i=0;i<nrOfValues;i++){
        aval = gena.getNext()&0xffff;
        bval = genb.getNext()&0xffff;
        if (aval ==bval)
            matchCount++;
    }
    return matchCount;
}


std::pair<char,int> parseLine(const std::string &line){
    std::stringstream strStr(line)    ;
std::string dummy;
    int startval;
    char generator;
    strStr >> dummy;
    strStr >> generator;
    strStr >> dummy;
    strStr >> dummy;
    strStr >> startval;
    return std::make_pair(generator,startval);
};

std::map<char, int> readFile(const std::string& filename){
    std::fstream fs;
    std::map<char,int> result;
    fs.open(filename, std::ios_base::in);
    std::string line;
    while( std::getline(fs,line)){
        result.insert(parseLine(line));
    }
    return result;

}