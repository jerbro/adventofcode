//
// Created by jeroen on 23-12-17.
//

#include <stdexcept>
#include "Generator.h"
Generator::Generator(long startVal, long factor, long long matchVal):startVal(startVal),factor(factor), comparePattern(matchVal-1){
    currVal = startVal;
    if (comparePattern != 3 && comparePattern != 7)
        throw std::runtime_error("Unexpected value for compare Pattern");
}

void Generator::resetToInitialVals() {
    currVal = startVal;
}

long long Generator::getNext() {
    currVal *= factor;
    currVal %= 2147483647;
    return currVal;
}

long long Generator::getNextValid() {
    this->getNext();

    while ((currVal & comparePattern) != 0)
        this->getNext();
    return currVal;

}