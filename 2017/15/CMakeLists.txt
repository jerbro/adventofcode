cmake_minimum_required(VERSION 3.8)
project(2017_15)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp Generator.cpp Generator.h)
add_executable(2017_15 ${SOURCE_FILES})