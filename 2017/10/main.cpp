

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <chrono>

#include <list>
#include <boost/functional/hash.hpp>
#include <boost/algorithm/string.hpp>
#include <unordered_set>
#include <regex>
#include <iomanip>


std::vector<int> readFileForA(const std::string &filename);
std::vector<int> getLengthsForB(const std::string &filename);
std::vector<int> fillCirclevalues(int number);
void swapValues(std::vector<int> &sourceVect, const int length, const int startpos);
void doKnots(std::vector<int> &sourceVect, const std::vector<int> lengths, int &currpos, int &skiplength);
int getSimpleKnotHash(std::vector<int> sourceVect, const std::vector<int> lengths);
std::string getDenseHash(std::vector<int> sourceVect, const std::vector<int> lengths);

int main(int argc, char *argv[]) {
    auto start = std::chrono::steady_clock::now();

    auto lengthsA = readFileForA("input.txt");
    auto lengthsB = getLengthsForB("input.txt");

    auto circlevals = fillCirclevalues(256);


    auto answerForA = getSimpleKnotHash(circlevals, lengthsA);
    auto answerForB = getDenseHash(circlevals,lengthsB);

    std::cout << "Advent of code 2017, day 10" << std::endl;
    std::cout << "A: " << answerForA << std::endl;
    std::cout << "B: " << answerForB << std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}

void swapValues(std::vector<int> &sourceVect, const int length, const int startpos){
    std::vector<int> result;
    int elementcount = sourceVect.size();
    for (int i=0;i<length/2;i++){
        int pos1 = (startpos +i)%elementcount;
        int pos2 = (startpos + length - i-1)%elementcount;
        int tmpval = sourceVect.at(pos1);
        sourceVect.at(pos1) = sourceVect.at(pos2);
        sourceVect.at(pos2) = tmpval;
    }
}


void doKnots(std::vector<int> &sourceVect, const std::vector<int> lengths, int &currpos, int &skiplength) {
    for (auto len:lengths){
        swapValues(sourceVect,len,currpos);
        currpos += len + skiplength;
        skiplength ++;
        currpos = currpos%sourceVect.size();
    }
}

int calcSparseHash(std::vector<int> &currKnot, const std::vector<int> lengths){
    int currpos = 0;
    int skiplength = 0;
    for (int i=0;i<64;i++)
        doKnots(currKnot, lengths, currpos, skiplength);
}

std::string getDenseHash(std::vector<int> sourceVect, const std::vector<int> lengths){
    std::vector<int> currKnot = sourceVect;
    calcSparseHash(currKnot,lengths);

    std::vector<int> denseHashVect;
    int cnt = 0;
    int currHash = 0;
    for (int currVal:currKnot){
        currHash ^= currVal;
        cnt++;
        if (cnt >15){
            cnt = 0;
            denseHashVect.push_back(currHash);
            currHash = 0;
        }
    }

    std::stringstream resultStream;
    for (int nr:denseHashVect){
        resultStream << std::setfill('0') <<std::setw(2) << std::hex << nr;
    }
    std::string result = resultStream.str();
    return result;
}

int getSimpleKnotHash(std::vector<int> sourceVect, const std::vector<int> lengths){
    int currpos = 0;
    int skiplength = 0;
    doKnots(sourceVect, lengths, currpos, skiplength);
   return sourceVect.at(0) * sourceVect.at(1);
}

std::vector<int> fillCirclevalues(int number){
    std::vector<int> result;
    for (int i=0;i<number;i++){
        result.push_back(i);
    }
    return result;
}

////Read the file.
std::vector<int> readFileForA(const std::string &filename) {
    std::vector<int> result;
    std::fstream fs;
    fs.open(filename, std::ios::in);
    std::string content ;
    fs >> content;
    std::replace(content.begin(), content.end(),',',' ');
    std::stringstream contentStrstr(content);

    int curr_nr=0;
    while (contentStrstr >> curr_nr){
        result.push_back(curr_nr);
    }

    fs.close();
    return result;
}


////Read the file.
std::vector<int> getLengthsForB(const std::string &filename) {
    std::vector<int> result;
    std::fstream fs;
    fs.open(filename, std::ios::in);

    char ch;
    while (fs>>std::noskipws>>ch){
        result.push_back((int)ch);
    }

    fs.close();
    result.push_back(17);
    result.push_back(31);
    result.push_back(73);
    result.push_back(47);
    result.push_back(23);
    return result;
}
