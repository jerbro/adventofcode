

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <chrono>

#include <list>
#include <boost/functional/hash.hpp>
#include <unordered_set>

class vectorHash {
public:
    size_t operator()(const std::vector<long> &p) const {
        return boost::hash_value(p);
    }
};

std::vector<long> readFile(const std::string & filename);
int getPositionOfFirstHighest(const std::vector<long> &distribution);
void redistribute(std::vector<long>& distribution);
int getLoopsBeforeFirstDuplicate(std::vector<long> distribution, std::vector<long>& duplicate);
int getLoopsBeforeSpecificDistribution(std::vector<long> startdistribution, const std::vector<long>& distrToFind);

int main(int argc, char *argv[]) {
    auto start = std::chrono::steady_clock::now();
    auto distribution = readFile("input.txt");
//    distribution = {0,2,7,0};

    std::vector<long> duplicate;
    auto answerForA = getLoopsBeforeFirstDuplicate(distribution, duplicate);
    auto answerForB = answerForA - getLoopsBeforeSpecificDistribution(distribution,duplicate);


    std::cout << "Advent of code 2017, day 06" << std::endl;
    std::cout << "A: "<<answerForA<<std::endl;
    std::cout << "B: "<<answerForB<<std::endl;


    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took "<<std::chrono::duration <double, std::milli> (end-start).count() << " ms"<<std::endl;

    return 0;
}

int getLoopsBeforeFirstDuplicate(std::vector<long> distribution, std::vector<long>& duplicate){
    std::vector<long> distr = distribution;

    std::unordered_set<std::vector<long>,vectorHash> listOfPreviousDistrs;
    long counter = 0;
    while (listOfPreviousDistrs.find(distr) == listOfPreviousDistrs.end()){
        listOfPreviousDistrs.insert(distr);
        redistribute(distr);
        counter ++;
    }
    duplicate = distr;
    return counter;
}

int getLoopsBeforeSpecificDistribution(std::vector<long> startdistribution, const std::vector<long>& distrToFind){
    std::vector<long> distr = startdistribution;

    long counter = 0;
    while (distr != distrToFind){
        redistribute(distr);
        counter ++;
    }
    return counter;
}


void redistribute(std::vector<long>& distribution){
    int posOfHighest = getPositionOfFirstHighest(distribution);
    int nrOfPoints =distribution.size();

    int nrsToDistribute = nrOfPoints;
    int restOfValueToRedistribute = distribution.at(posOfHighest);
    distribution.at(posOfHighest) = 0;
    for (int i = 0;i<nrsToDistribute;i++){
        int currPos =(i+ posOfHighest+1 )%nrOfPoints;
        int valueForThisPos = (int)ceil((double)restOfValueToRedistribute / (double)(nrOfPoints-i));
        distribution.at(currPos)+= valueForThisPos;
        restOfValueToRedistribute -= valueForThisPos;
    }

}

int getPositionOfFirstHighest(const std::vector<long> &distribution){
    int posOfHighest=0;
    int highest =0;
    for (int i=0;i<distribution.size();i++){
        if (distribution.at(i)> highest){
            posOfHighest = i;
            highest = distribution.at(i);
        }
    }
    return posOfHighest;
}

std::vector<long> readFile(const std::string & filename){
    std::vector<long> result;

    std::fstream fs;
    long currVal = 0;
    fs.open(filename, std::ios::in);
    while(fs >> currVal){
        result.push_back(currVal);
    }
    fs.close();

    return result;
}
