#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <chrono>
#include <climits>
#include <iomanip>
#include <boost/algorithm/string.hpp>
#include <unordered_set>


enum EnumDir {
    Up, Down, Left, Right
};

struct State {
    int x;
    int y;
    EnumDir dir;
    bool valid;
};

std::vector<std::vector<char>> readFile(const std::string &filename);

State getStartState(const std::vector<std::vector<char>> &maze);

State getNextPos(const std::vector<std::vector<char>> &maze, State currPos);

State getNextPosInCurrDir(const std::vector<std::vector<char>> &maze, State currPos);

std::string solveMaze(const std::vector<std::vector<char>> &maze, int &stepcnt);

char getChar(const std::vector<std::vector<char>> &maze, State currPos);

int main(int argc, char *argv[]) {

    auto start = std::chrono::steady_clock::now();

    auto maze = readFile("input.txt");

    int answerForB;
    auto answerForA = solveMaze(maze, answerForB);


    std::cout << "Advent of code 2017, day 19" << std::endl;
    std::cout << "A: " << answerForA << std::endl;
    std::cout << "B: " << answerForB << std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}

std::string solveMaze(const std::vector<std::vector<char>> &maze, int &stepcnt) {
    int stepcntr = 0;
    std::string result;
    State pos = getStartState(maze);
    while (pos.valid) {
        stepcntr++;
        pos = getNextPos(maze, pos);
        auto currCh = getChar(maze, pos);
        if (currCh != ' ' && currCh != '|' && currCh != '-' && currCh != '+')
            result += currCh;
    }
    stepcnt = stepcntr - 1;
    return result;
}

State getNextPosInCurrDir(const std::vector<std::vector<char>> &maze, State currPos) {
    State result = currPos;
    switch (currPos.dir) {
        case EnumDir::Down:
            result.y += 1;
            break;
        case EnumDir::Up:
            result.y -= 1;
            break;
        case EnumDir::Right:
            result.x += 1;
            break;
        case EnumDir::Left:
            result.x -= 1;
            break;
    }
    return result;
}

char getChar(const std::vector<std::vector<char>> &maze, State currPos) {
    return maze[currPos.y][currPos.x];
}


State getNextPos(const std::vector<std::vector<char>> &maze, State currPos) {
    char currCh = getChar(maze, currPos);
    State invalidPos = currPos;
    invalidPos.valid = false;

    if (currCh == ' ')
        return invalidPos;
    if (currCh == '+') {
        State testNewDir = currPos;
        switch (currPos.dir) {
            case EnumDir::Up:
            case EnumDir::Down: {
                testNewDir.dir = EnumDir::Left;
                char nextchlr = getChar(maze, getNextPosInCurrDir(maze, testNewDir));
                if (nextchlr != ' ') {
                    return getNextPosInCurrDir(maze, testNewDir);
                } else {
                    testNewDir.dir = EnumDir::Right;
                    return getNextPosInCurrDir(maze, testNewDir);
                }
                break;
            }
            case EnumDir::Left:
            case EnumDir::Right: {
                testNewDir.dir = EnumDir::Up;
                char nextchud = getChar(maze, getNextPosInCurrDir(maze, testNewDir));
                if (nextchud != ' ') {
                    return getNextPosInCurrDir(maze, testNewDir);
                } else {
                    testNewDir.dir = EnumDir::Down;
                    return getNextPosInCurrDir(maze, testNewDir);
                }
                break;
            }
        }
        return invalidPos;
    } else
        return getNextPosInCurrDir(maze, currPos);
}

State getStartState(const std::vector<std::vector<char>> &maze) {
    std::vector<char> line = maze.at(0);
    State result;
    result.dir = EnumDir::Down;
    result.y = 0;
    result.valid = true;
    for (int x = 0; x < line.size(); x++) {
        if (line.at(x) == '|') {
            result.x = x;
            return result;
        }
    }
    result.valid = false;
    return result;
}


std::vector<std::vector<char>> readFile(const std::string &filename) {
    std::fstream fs;
    std::vector<std::vector<char>> result;
    std::string line;
    fs.open(filename, std::ios_base::in);

    while (std::getline(fs, line)) {
        std::vector<char> lineVect(line.begin(), line.end());
        result.push_back(lineVect);

    }
    fs.close();
    return result;

}