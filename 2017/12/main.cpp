#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <chrono>


std::map<int, std::set<int>> readFile(const std::string &filename);

std::pair<int, std::set<int>> parseLine(const std::string &txt);

std::map<int, std::set<int>> addOppositeConnections(std::map<int, std::set<int>> conns);

std::set<int> getAllNotAlreadyConnectedTo(const std::set<int> &valuesToCheck, const std::set<int> &alreadyConnected,
                                          const std::map<int, std::set<int>> &connections);

std::set<int> getAllConnectedTo(int node, const std::map<int, std::set<int>> &connections);

int getNumberOfGroups(const std::map<int, std::set<int>> &connections);

int main(int argc, char *argv[]) {
    auto start = std::chrono::steady_clock::now();

    auto fileContent = readFile("input.txt");
    auto connections = addOppositeConnections(fileContent);
    auto allConnectedTo0 = getAllConnectedTo(0, connections);

    auto answerForA = allConnectedTo0.size();
    auto answerForB = getNumberOfGroups(connections);


    std::cout << "Advent of code 2017, day 12" << std::endl;
    std::cout << "A: " << answerForA << std::endl;
    std::cout << "B: " << answerForB << std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}

int getNumberOfGroups(const std::map<int, std::set<int>> &connections) {
    std::set<int> alreadyInAGroup;
    int groupcount = 0;
    for (auto item:connections) {
        if (alreadyInAGroup.find(item.first) == alreadyInAGroup.end()) {
            auto inThisGroup = getAllConnectedTo(item.first, connections);
            groupcount++;
            alreadyInAGroup.insert(inThisGroup.begin(), inThisGroup.end());

        }
    }
    return groupcount;
}

std::set<int> getAllNotAlreadyConnectedTo(const std::set<int> &valuesToCheck, const std::set<int> &alreadyConnected,
                                          const std::map<int, std::set<int>> &connections) {
    std::set<int> result;
    for (auto val:valuesToCheck) {
        for (auto conVal: connections.find(val)->second) {
            if (alreadyConnected.find(conVal) == alreadyConnected.end()) {
                result.insert(conVal);
            }
        }
    }
    return result;
}

std::set<int> getAllConnectedTo(int node, const std::map<int, std::set<int>> &connections) {
    bool keeprunning = true;
    std::set<int> currNewNodes;
    std::set<int> alreadyConnectedNodes;
    currNewNodes.insert(node);
    alreadyConnectedNodes.insert(node);
    while (keeprunning) {
        currNewNodes = getAllNotAlreadyConnectedTo(currNewNodes, alreadyConnectedNodes, connections);
        alreadyConnectedNodes.insert(currNewNodes.begin(), currNewNodes.end());
        if (currNewNodes.empty())
            keeprunning = false;
    }
    return alreadyConnectedNodes;
}


//if 1 is connected to 2, this means also that 2 is
std::map<int, std::set<int>> addOppositeConnections(std::map<int, std::set<int>> conns) {
    std::map<int, std::set<int>> result = conns;
    for (auto item:conns) {
        for (int currcon:item.second) {
            result.find(currcon)->second.insert(item.first);
        }
    }
    return result;
};


std::pair<int, std::set<int>> parseLine(const std::string &txt) {
    std::string removed_commas(txt);
    std::replace(removed_commas.begin(), removed_commas.end(), ',', ' ');
    std::stringstream strStr(removed_commas);
    int first_id;
    std::string separator;
    std::string raw_other_ids;
    std::set<int> connected_ids;
    std::vector<std::string> raw_connected_ids;
    strStr >> first_id;
    strStr >> separator;
    int i;
    while (strStr >> i) {
        connected_ids.insert(i);
    }

    return make_pair(first_id, connected_ids);
};

std::map<int, std::set<int>> readFile(const std::string &filename) {
    std::map<int, std::set<int>> result;
    std::fstream fs;
    std::string line;
    fs.open(filename, std::fstream::openmode::_S_in);
    while (std::getline(fs, line)) {
        result.insert(parseLine(line));
        line = "";
    }
    fs.close();

    return result;
}
