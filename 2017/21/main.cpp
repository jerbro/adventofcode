#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <chrono>
#include <climits>
#include <iomanip>
#include <boost/algorithm/string.hpp>
#include <unordered_set>
#include <regex>



std::map<std::string, std::vector<std::string>> readFile(const std::string &filename) ;
std::pair<std::string, std::vector<std::string>> parseLine(const std::string & line);
std::vector<std::vector<char>> strImgTo2dCharVect(const std::string& src);
std::vector<std::vector<char>> rotateSquare(const std::vector<std::vector<char>> & src);
std::string charVectToStr(const std::vector<std::vector<char>> &src);
std::string getBlock(const std::vector<std::vector<char>> &src, int x, int y, int blockSize);
std::vector<std::vector<char>> doConversion(const std::vector<std::vector<char>> &src, const std::map<std::string, std::vector<std::string>>& conversions);
long countOnPixels(const std::vector<std::vector<char>>& src);
int countPixelsAfterIteration(const std::string &startpos,const std::map<std::string, std::vector<std::string>>& conversions, int iterations);

void printSetup(std::vector<std::vector<char>> src );
void printSetup(std::string src );

int main(int argc, char *argv[]) {

    auto start = std::chrono::steady_clock::now();

    auto conversions = readFile("input.txt");

    std::string startStr(".#./..#/###");

    auto answerForA = countPixelsAfterIteration(startStr,conversions,5);
    auto answerForB = countPixelsAfterIteration(startStr,conversions,18);



    std::cout << "Advent of code 2017, day 21" << std::endl;
    std::cout << "A: " << answerForA << std::endl;
    std::cout << "B: " << answerForB << std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}

int countPixelsAfterIteration(const std::string &startpos,const std::map<std::string, std::vector<std::string>>& conversions, int iterations){
    auto currVect = strImgTo2dCharVect(startpos);

    for (int i=0;i<iterations;i++){
        currVect = doConversion(currVect,conversions);
    }
    return countOnPixels(currVect);
}

void printSetup(std::string src ){
    printSetup(strImgTo2dCharVect(src));
}

void printSetup(std::vector<std::vector<char>> src ){
    for (auto line:src){
        std::cout << std::string(line.begin(),line.end())<<std::endl;
    }
}

long countOnPixels(const std::vector<std::vector<char>>& src){
    std::string setupStr = charVectToStr(src);
    return std::count(setupStr.begin(),setupStr.end(),'#');
}

std::vector<std::vector<char>> doConversion(const std::vector<std::vector<char>> &src, const std::map<std::string, std::vector<std::string>>& conversions){
    std::vector<std::vector<char>> result;
    std::vector<std::string> resultStrs;

    int blockSize = (src.size()%2==0)?2:3;
    int extraLinesByBlock = 0;

    for (int x=0;x<src.size();x+=blockSize){
        extraLinesByBlock=0;
        for (int y=0;y<src.size();y+=blockSize){
            auto block = getBlock(src,x,y,blockSize);
            auto bla = conversions.find(block);
            if (conversions.find(block) == conversions.end())
                throw std::runtime_error("Setup not in conversions");
            auto convertedBlock = conversions.find(block)->second;
            for (int i=0;i<convertedBlock.size();i++){
                if (x==0)
                    resultStrs.push_back("");
                resultStrs.at(y+extraLinesByBlock+i) += convertedBlock.at(i);
            }
            extraLinesByBlock++;

        }
    }

    for (auto resultline:resultStrs){
        result.push_back(std::vector<char>(resultline.begin(), resultline.end()));
    }

    return result;

}

std::string getBlock(const std::vector<std::vector<char>> &src, int x, int y, int blockSize){
    std::vector<std::vector<char>> resultImg;
    for (int i=0;i<blockSize;i++){
        std::vector<char> currLine(src[y+i].begin()+x,src[y+i].begin()+x+blockSize);
        resultImg.push_back(currLine);
    }
    return charVectToStr(resultImg);
}



std::pair<std::string, std::vector<std::string>> parseLine(const std::string & line){
    std::string src, dest, dummy;
    std::vector<std::string> destByLine;
    std::stringstream strStr(line);
    strStr >> src >> dummy >> dest;
    boost::split(destByLine,dest,boost::is_any_of("/"));
    return std::make_pair(src, destByLine);
};

std::vector<std::vector<char>> strImgTo2dCharVect(const std::string& src){
    std::vector<std::string> lines;
    std::vector<std::vector<char>> result;
    boost::split(lines,src,boost::is_any_of("/"));
    for (auto line:lines){
        std::vector<char> linevect(line.begin(),line.end());
        result.push_back(linevect);
    }
    return result;
}

std::vector<std::vector<char>> rotateSquare(const std::vector<std::vector<char>> & src){
    std::vector<std::vector<char>> result = src;
    int width = src.size();
    for (int x=0;x<src.size();x++)
        for (int y=0;y<src.size();y++){
            result[x][y] = src[width-y-1][x];
        }
    return result;
}

std::string charVectToStr(const std::vector<std::vector<char>> &src){
    std::string result;
    std::string currLine;
    for (auto lineVect :src){
        std::string currLine(lineVect.begin(),lineVect.end());
        result += currLine;
        result += "/" ;
    }
    result.erase(result.length()-1,1);
    return result;
}

std::vector<std::string> getAllRotations(const std::string &src){
    auto srcImg = strImgTo2dCharVect(src);
    std::vector<std::string> result;
    auto currImg = srcImg;

    for (int i=0;i<4;i++){
        result.push_back(charVectToStr(currImg));
        currImg = rotateSquare(currImg);
    }
    return result;
}

std::string flipHorizontal(const std::string&src){
    auto srcImg = strImgTo2dCharVect(src);
    int width = srcImg.size();

    auto resultImg = srcImg;
    for (int x=0;x<width;x++){
        for (int y=0;y<width;y++){
            resultImg[x][y] = srcImg[x][width-1-y];
        }
    }

    return charVectToStr(resultImg);
}

std::string flipVertical(const std::string&src){
    auto srcImg = strImgTo2dCharVect(src);
    int width = srcImg.size();

    auto resultImg = srcImg;
    for (int x=0;x<width;x++){
        for (int y=0;y<width;y++){
            resultImg[x][y] = srcImg[width-1-x][y];
        }
    }

    return charVectToStr(resultImg);
}


std::vector<std::string> getAllFlipped(const std::string& src){

    std::vector<std::string> result;

    result.push_back(flipHorizontal(src));
    result.push_back(flipVertical(src));
    result.push_back(flipVertical(flipHorizontal(src)));
    return result;

}

std::map<std::string, std::vector<std::string>> readFile(const std::string &filename) {
    std::fstream fs;
    std::map<std::string, std::vector<std::string>> result;
    std::string line;
    fs.open(filename, std::ios_base::in);

    while (std::getline(fs, line)) {
        auto currConversion = parseLine(line);
        auto allRotations = getAllRotations(currConversion.first);
        for (auto rot:allRotations){
            auto allFlips = getAllFlipped(rot);
            for (auto flp:allFlips){
                result.insert(std::make_pair(flp,currConversion.second));
            }

        }

    }
    fs.close();
    return result;

}