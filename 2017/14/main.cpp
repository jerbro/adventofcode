#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <chrono>
#include <climits>
#include <iomanip>


std::string readFile(const std::string &filename);

std::vector<int> fillCirclevalues(int number);

void swapValues(std::vector<int> &sourceVect, const int length, const int startpos);

void doKnots(std::vector<int> &sourceVect, const std::vector<int> lengths, int &currpos, int &skiplength);

int getSimpleKnotHash(std::vector<int> sourceVect, const std::vector<int> lengths);

std::string getDenseHash(std::vector<int> sourceVect, const std::vector<int> lengths);

std::vector<int> stringToHashSource(const std::string &src);

std::vector<char> getGraphicalHash(const std::string &keystr);

std::vector<std::vector<char>> getDiskStatus(std::string keystring);

int getUsedSquareCount(const std::vector<std::vector<char>> &status);

std::vector<std::pair<int, int>>
getSurroundingUsedCellsAndClearThem(std::vector<std::vector<char>> &status, int x, int y);

void floodfill(std::vector<std::vector<char>> &status, int x, int y);
int countGroups(std::vector<std::vector<char>> status);

int main(int argc, char *argv[]) {

    auto start = std::chrono::steady_clock::now();

    auto keystring = readFile("input.txt");
    auto distStatus = getDiskStatus(keystring);
//
    auto answerForA = getUsedSquareCount(distStatus);
    auto answerForB = countGroups(distStatus);


    std::cout << "Advent of code 2017, day 14" << std::endl;
    std::cout << "A: " << answerForA << std::endl;
    std::cout << "B: " << answerForB << std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}


int getUsedSquareCount(const std::vector<std::vector<char>> &status) {
    int result = 0;
    for (auto line:status) {
        result += std::count(line.begin(), line.end(), '#');
    }
    return result;
}

int countGroups(std::vector<std::vector<char>> status){
    int count = 0;
    auto currStatus = status;
    for (int x=0;x<128;x++)
        for (int y=0;y<128;y++){
            if (currStatus[y][x]=='#'){
                count++;
                floodfill(currStatus,x,y);
            }
        }
    return count;
}

void floodfill(std::vector<std::vector<char>> &status, int x, int y) {
    std::vector<std::pair<int, int>> currCoordinates {std::make_pair(x,y)};
    std::vector<std::pair<int, int>> newCoordinates;
    while (!currCoordinates.empty()) {
        for (auto coor:currCoordinates) {
            auto surrounding = getSurroundingUsedCellsAndClearThem(status, coor.first, coor.second);
            newCoordinates.insert(newCoordinates.end(), surrounding.begin(), surrounding.end());
        }
        currCoordinates = newCoordinates;
        newCoordinates.clear();
    }
}

std::vector<std::pair<int, int>>
getSurroundingUsedCellsAndClearThem(std::vector<std::vector<char>> &status, int x, int y) {
    std::vector<std::pair<int, int>> result;
    if (x > 0)
        if (status[y][x - 1] == '#') {
            status[y][x - 1] = '-';
            result.push_back(std::make_pair(x - 1, y));
        }

    if (x < 127)
        if (status[y][x + 1] == '#') {
            status[y][x + 1] = '-';
            result.push_back(std::make_pair(x + 1, y));
        }

    if (y > 0)
        if (status[y - 1][x] == '#') {
            status[y - 1][x] = '-';
            result.push_back(std::make_pair(x, y - 1));
        }

    if (y < 127)
        if (status[y + 1][x] == '#') {
            status[y + 1][x] = '-';
            result.push_back(std::make_pair(x, y + 1));
        }
    return result;
}

std::vector<std::vector<char>> getDiskStatus(std::string keystring) {
    std::vector<std::vector<char>> result;
    for (int i = 0; i < 128; i++) {
        std::string currLineKey = keystring + "-" + std::to_string(i);
        result.push_back(getGraphicalHash(currLineKey));
    }
    return result;
}

int hexDigitToDec(char ch) {
    char cstr[2];
    cstr[1] = 0;
    cstr[0] = ch;
    return (int) strtol(&ch, NULL, 16);
}

std::vector<char> numberToGraphicalBool(int nr, int len) {
    std::vector<char> result;
    for (int i = 0; i < len; i++) {
        if (nr & 0x01) {
            result.push_back('#');
        } else
            result.push_back('.');
        nr = nr >> 1;
    }
    std::reverse(result.begin(), result.end());
    return result;
}

std::vector<char> getGraphicalHash(const std::string &keystr) {
    auto hashSrc = stringToHashSource(keystr);
    auto hash = getDenseHash(fillCirclevalues(256), hashSrc);
    std::vector<char> result;

    for (char ch: hash) {
        int dec = hexDigitToDec(ch);
        auto currSymbols = numberToGraphicalBool(dec, 4);
        result.insert(result.end(), currSymbols.begin(), currSymbols.end());
    }
    return result;
}

std::vector<int> stringToHashSource(const std::string &src) {
    std::vector<int> result;
    std::stringstream strStr(src);
    char ch;
    while (strStr >> ch) {
        result.push_back(ch);
    }
    result.push_back(17);
    result.push_back(31);
    result.push_back(73);
    result.push_back(47);
    result.push_back(23);
    return result;
}

std::string readFile(const std::string &filename) {
    std::string result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);
    fs >> result;
    fs.close();

    return result;
}


void swapValues(std::vector<int> &sourceVect, const int length, const int startpos) {
    std::vector<int> result;
    int elementcount = sourceVect.size();
    for (int i = 0; i < length / 2; i++) {
        int pos1 = (startpos + i) % elementcount;
        int pos2 = (startpos + length - i - 1) % elementcount;
        int tmpval = sourceVect.at(pos1);
        sourceVect.at(pos1) = sourceVect.at(pos2);
        sourceVect.at(pos2) = tmpval;
    }
}


void doKnots(std::vector<int> &sourceVect, const std::vector<int> lengths, int &currpos, int &skiplength) {
    for (auto len:lengths) {
        swapValues(sourceVect, len, currpos);
        currpos += len + skiplength;
        skiplength++;
        currpos = currpos % sourceVect.size();
    }
}

int calcSparseHash(std::vector<int> &currKnot, const std::vector<int> lengths) {
    int currpos = 0;
    int skiplength = 0;
    for (int i = 0; i < 64; i++)
        doKnots(currKnot, lengths, currpos, skiplength);
}

std::string getDenseHash(std::vector<int> sourceVect, const std::vector<int> lengths) {
    std::vector<int> currKnot = sourceVect;
    calcSparseHash(currKnot, lengths);

    std::vector<int> denseHashVect;
    int cnt = 0;
    int currHash = 0;
    for (int currVal:currKnot) {
        currHash ^= currVal;
        cnt++;
        if (cnt > 15) {
            cnt = 0;
            denseHashVect.push_back(currHash);
            currHash = 0;
        }
    }

    std::stringstream resultStream;
    for (int nr:denseHashVect) {
        resultStream << std::setfill('0') << std::setw(2) << std::hex << nr;
    }
    std::string result = resultStream.str();
    return result;
}

int getSimpleKnotHash(std::vector<int> sourceVect, const std::vector<int> lengths) {
    int currpos = 0;
    int skiplength = 0;
    doKnots(sourceVect, lengths, currpos, skiplength);
    return sourceVect.at(0) * sourceVect.at(1);
}

std::vector<int> fillCirclevalues(int number) {
    std::vector<int> result;
    for (int i = 0; i < number; i++) {
        result.push_back(i);
    }
    return result;
}
