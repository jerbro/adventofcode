#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <chrono>
#include <climits>
#include <iomanip>
#include <boost/algorithm/string.hpp>
#include <unordered_set>
#include <regex>

struct Coord{
    long long x;
    long long y;
    long long z;
};
struct Particle{
    Coord pos;
    Coord vel;
    Coord acc;
};

std::vector<Particle>  readFile(const std::string &filename);

int getSlowestParticle(const std::vector<Particle> &parts);
void moveParticle(Particle& part);
void moveAllParticles(std::vector<Particle>& parts);
bool operator==(const Coord& lhs, const Coord& rhs);
void removeCollisions(std::vector<Particle>&parts);
void moveParticlesSteps(int steps,std::vector<Particle>&parts);

int main(int argc, char *argv[]) {

    auto start = std::chrono::steady_clock::now();

    auto particles = readFile("input.txt");

    auto answerForA = getSlowestParticle(particles);
    moveParticlesSteps(1000,particles);
    auto answerForB = particles.size();


    std::cout << "Advent of code 2017, day 20" << std::endl;
    std::cout << "A: " << answerForA << std::endl;
    std::cout << "B: " << answerForB << std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}

void moveParticlesSteps(int steps,std::vector<Particle>&parts){
    for (int i=0;i<steps;i++){
        removeCollisions(parts);
        moveAllParticles(parts);
    }
}

bool operator==(const Coord& lhs, const Coord& rhs){
    return (lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z);
}
void removeCollisions(std::vector<Particle>&parts){
    std::set<int> collissionIds;
    auto sz = parts.size();
    for (int i=0;i<parts.size();i++){
        for (int j=i+1;j<parts.size();j++){
            if (parts.at(i).pos == parts.at(j).pos){
                collissionIds.insert(i);
                collissionIds.insert(j);
            }
        }
    }
    for (std::set<int>::reverse_iterator iter = collissionIds.rbegin(); iter != collissionIds.rend();iter++){
        parts.erase(parts.begin()+(*iter));
    }
}

void moveAllParticles(std::vector<Particle>& parts){
    for (auto& part:parts){
        moveParticle(part);
    }
}

void moveParticle(Particle& part){
    part.vel.x += part.acc.x;
    part.vel.y += part.acc.y;
    part.vel.z += part.acc.z;

    part.pos.x += part.vel.x;
    part.pos.y += part.vel.y;
    part.pos.z += part.vel.z;

}

int getSlowestParticle(const std::vector<Particle> &parts){
    int slowestAcc = 99999;
    int slowestId=0;
    int nrWithThisAcc = 0;
    for (int i=0;i<parts.size();i++){
        auto currAcc = parts.at(i).acc;
        int acc = abs(currAcc.x)+abs(currAcc.y)+abs(currAcc.z);
        if (acc < slowestAcc){
            slowestAcc = acc;
            slowestId = i;
            nrWithThisAcc=1;
        }
        else if (acc == slowestAcc){
            nrWithThisAcc ++;
        }
    }
    if (nrWithThisAcc==1)
        return slowestId;
    else
        throw std::runtime_error("Multiple elements with same acceleration. This case is not tested for.");
}

std::vector<Particle> readFile(const std::string &filename) {
    std::fstream fs;
    std::vector<Particle> result;
    std::string line;
    fs.open(filename, std::ios_base::in);

    while (std::getline(fs, line)) {
        if (line.at(line.size()-1) == '\r')
            line.erase(line.size()-1,1);
        int px,py,pz,vx,vy,vz,ax,ay,az;
        std::smatch match;
        std::regex re("^p=<([-0-9]+),([-0-9]+),([-0-9]+)>, v=<([-0-9]+),([-0-9]+),([-0-9]+)>, a=<([-0-9]+),([-0-9]+),([-0-9]+)>$");
        std::regex_match(line, match, re);

        px = atoi(std::string(match[1]).c_str());
        py = atoi(std::string(match[2]).c_str());
        pz = atoi(std::string(match[3]).c_str());

        vx = atoi(std::string(match[4]).c_str());
        vy = atoi(std::string(match[5]).c_str());
        vz = atoi(std::string(match[6]).c_str());

        ax = atoi(std::string(match[7]).c_str());
        ay = atoi(std::string(match[8]).c_str());
        az = atoi(std::string(match[9]).c_str());

        Particle part = {{px,py,pz},{vx,vy,vz},{ax,ay,az}};
        result.push_back(part);

    }
    fs.close();
    return result;

}