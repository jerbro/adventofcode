

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <chrono>

#include <list>
#include <boost/functional/hash.hpp>
#include <boost/algorithm/string.hpp>
#include <unordered_set>
#include <regex>
#include <iomanip>

struct cube_coord {
    int x;
    int y;
    int z;
};

std::vector<std::string> readFile(const std::string &filename);

cube_coord
getNewPosition(const cube_coord &startPos, const std::vector<std::string> &directions, long &longestDistance);

long getDistanceAfterTravel(const std::vector<std::string> &directions, long &longestDistance);


int main(int argc, char *argv[]) {
    auto start = std::chrono::steady_clock::now();

    auto directions = readFile("input.txt");

    long answerForB = 0;
    auto answerForA = getDistanceAfterTravel(directions, answerForB);


    std::cout << "Advent of code 2017, day 11" << std::endl;
    std::cout << "A: " << answerForA << std::endl;
    std::cout << "B: " << answerForB << std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}

long getDistanceFromCenter(cube_coord pos) {
    return (abs(pos.x) + abs(pos.y) + abs(pos.z)) / 2;
}

long getDistanceAfterTravel(const std::vector<std::string> &directions, long &longestDistance) {
    cube_coord startPos{0,0,0};
    auto newPos = getNewPosition(startPos, directions, longestDistance);
    return getDistanceFromCenter(newPos);


}

cube_coord
getNewPosition(const cube_coord &startPos, const std::vector<std::string> &directions, long &longestDistance) {
    //cube-coordinates for hexagon grids are described by: https://www.redblobgames.com/grids/hexagons/
    cube_coord pos = startPos;
    for (const auto &dir:directions) {
        if (dir == "n") {
            pos.y++;
            pos.z--;
        } else if (dir == "ne") {
            pos.x++;
            pos.z--;
        } else if (dir == "se") {
            pos.x++;
            pos.y--;
        } else if (dir == "s") {
            pos.y--;
            pos.z++;
        } else if (dir == "sw") {
            pos.x--;
            pos.z++;
        } else if (dir == "nw") {
            pos.x--;
            pos.y++;
        } else {
            throw std::runtime_error("Error, unknown direction");
        }
        if (getDistanceFromCenter(pos) > longestDistance)
            longestDistance = getDistanceFromCenter(pos);


    }
    return pos;

}


////Read the file.
std::vector<std::string> readFile(const std::string &filename) {
    std::vector<std::string> result;
    std::fstream fs;
    fs.open(filename, std::ios::in);
    std::string content;
    fs >> content;
    std::replace(content.begin(), content.end(), ',', ' ');
    std::stringstream contentStrStr(content);
    std::string curr_dir;
    while (contentStrStr >> curr_dir) {
        result.push_back(curr_dir);

    }

    fs.close();
    return result;
}


