

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <math.h>

struct Pos{
    long xPos;
    long yPos;

    bool operator<(const Pos &o)  const {
        return xPos < o.xPos || (xPos == o.xPos && yPos < o.yPos);
    }

    bool operator==(const Pos &o) const {
        return xPos == o.xPos && ( yPos == o.yPos);
    }
};





long readFile(const std::string &filename);
Pos getPositionOfItem(long nr);
long getDistanceFromNrToCenter(long nr);
long getValueOfPosOr0(const std::map<Pos, long>& loc_value_pairs, const Pos pos);
long getValueOfSurroundingFields(const std::map<Pos, long>& loc_value_pairs, const Pos currPos);
long solvePartB(long maxval);

int main(int argc, char *argv[]) {
    auto value = readFile("input.txt");

    std::cout << "Value: "<<value<<std::endl;

    std::cout << "Advent of code 2017, day 03" << std::endl;
    std::cout << "A: "<<getDistanceFromNrToCenter(value)<<std::endl;
    std::cout << "B: "<<solvePartB(value)<<std::endl;


    return 0;
}

long solvePartB(long maxval){
    std::map<Pos, long> loc_value_pair;
    loc_value_pair.insert(std::make_pair(Pos{0,0},1));

    long currVal = 1;
    int nr = 2;

    while (currVal < maxval){
        auto currPos = getPositionOfItem(nr);
        currVal = getValueOfSurroundingFields(loc_value_pair, currPos);
        loc_value_pair.insert(std::make_pair(currPos,currVal));
        nr++;
    }
    return currVal;


}

long getValueOfSurroundingFields(const std::map<Pos, long>& loc_value_pairs, const Pos currPos){
    long result = 0;
    result += getValueOfPosOr0(loc_value_pairs,Pos{currPos.xPos+1,currPos.yPos  });
    result += getValueOfPosOr0(loc_value_pairs,Pos{currPos.xPos+1,currPos.yPos+1});
    result += getValueOfPosOr0(loc_value_pairs,Pos{currPos.xPos  ,currPos.yPos+1});
    result += getValueOfPosOr0(loc_value_pairs,Pos{currPos.xPos-1,currPos.yPos+1});
    result += getValueOfPosOr0(loc_value_pairs,Pos{currPos.xPos-1,currPos.yPos  });
    result += getValueOfPosOr0(loc_value_pairs,Pos{currPos.xPos-1,currPos.yPos-1});
    result += getValueOfPosOr0(loc_value_pairs,Pos{currPos.xPos  ,currPos.yPos-1});
    result += getValueOfPosOr0(loc_value_pairs,Pos{currPos.xPos+1,currPos.yPos-1});
    return result;
}

long getValueOfPosOr0(const std::map<Pos, long>& loc_value_pairs, const Pos pos){
    if (loc_value_pairs.find(pos) != loc_value_pairs.end())
        return loc_value_pairs.find(pos)->second;
    else
        return 0;
}

long getDistanceFromNrToCenter(long nr){
    Pos loc = getPositionOfItem(nr);
    long result;
    result = 0;
    result += abs(loc.xPos);
    result += abs(loc.yPos);
    return result;
}

//Get the position of the numbered item in the spiral.
//17  16  15  14  13
//18   5   4   3  12
//19   6   1   2  11
//20   7   8   9  10
//21  22  23---> ...

//In the above examplepositions are: 1 : 0,0
// 9: 1,1
// 5: -1,-1
// 2: 0,1
// for the calculation 'ringnumbers' are used. ring 0 contains 1.
// ring 1 contains nr 2-9
// ring 2 contains nr 10-25
// number 9 (2^3) is the last number of ring; 25 (2^5) is the last number of ring 2; 49 (2^7) is the last of ring 3 and
// so on.
//
// Number to ringnumber: ceil(floor(sqrt(nr-1))/2)
Pos getPositionOfItem(long nr){
    if (nr==1){
        return Pos{0,0};
    }

    Pos result;
    int ringnr;
    ringnr = ceil(floor(sqrt(nr-1))/2);

    long highestOfRingnr = (ringnr*2+1)*(ringnr*2+1);
    long lowestOfRingnr = (ringnr*2-1)*(ringnr*2-1)+1;

    auto numbersInRing = highestOfRingnr - lowestOfRingnr+1;
    auto currPosInRing = nr - lowestOfRingnr;
    auto numbersOnEverySide = numbersInRing/4;
    auto currPosOnSide = currPosInRing %numbersOnEverySide;
    int sideNr = (int)floor(((double)currPosInRing/ (double)numbersInRing)*4.0);
    switch (sideNr){
        case 0: // Right side. starting above the last pos
            result = {ringnr, -1*ringnr + 1+currPosOnSide};
            break;
        case 1: // Top side
            result = {ringnr-1-currPosOnSide, ringnr};
            break;
        case 2: // left side
            result = {-1*ringnr, ringnr-1-currPosOnSide};
            break;
        case 3: // bottom side
            result = {-1*ringnr+1+currPosOnSide, -1*ringnr};
            break;
        default:
            throw std::runtime_error("Error, unexpected value in case-statement");
    }

    return result;

}


long readFile(const std::string &filename) {
    long result;
    std::fstream fs;
    fs.open(filename, std::ios::in);
    fs >> result;
    fs.close();
    return result;
}

