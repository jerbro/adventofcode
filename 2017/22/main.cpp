#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <chrono>
#include <climits>
#include <iomanip>
#include <boost/algorithm/string.hpp>
#include <unordered_set>
#include <regex>

struct PosAndDir {
    int x, y, dx, dy;
};

typedef std::vector<std::vector<char>> GRID;

GRID readFile(const std::string &filename);

GRID makeGrid(int gridsize);

void putGridInGrid(GRID &biggrid, const GRID &smallgrid, int xoffset, int yoffset);

GRID putGridInLargGridAndGiveCenter(const GRID &smallgrid, int gridsize, PosAndDir &currPos);

bool isInfected(const GRID &grid, const PosAndDir &currpos);

void turnLeft(PosAndDir &curr);

void turnRight(PosAndDir &curr);
void turnAround(PosAndDir &curr);

void doStep(PosAndDir &curr, int gridsize);

void swapInfectstate(GRID &grid, const PosAndDir &currpos);

bool doBurst(GRID &grid, PosAndDir &currpos, int gridsize);
bool doBurstv2(GRID &grid, PosAndDir &currpos, int gridsize);

void printGrid(const GRID &src);

int doNumberOfBursts(GRID &grid, PosAndDir &currpos, int bursts, bool newBurststyle=false);

char getInfectedState(const GRID &grid, const PosAndDir &currpos);
void changeInfectionv2(GRID &grid, const PosAndDir &currpos);

int main(int argc, char *argv[]) {

    auto start = std::chrono::steady_clock::now();

    int gridsize = 10001;

    auto startGrid = readFile("input.txt");
    PosAndDir currpos;
    auto grid = putGridInLargGridAndGiveCenter(startGrid, gridsize, currpos);
    currpos.dy = -1;
    currpos.dx = 0;//start facing up

    auto answerForA = doNumberOfBursts(grid, currpos, 10000);

    grid = putGridInLargGridAndGiveCenter(startGrid, gridsize, currpos);
    auto answerForB = doNumberOfBursts(grid, currpos, 10000000,true);


    std::cout << "Advent of code 2017, day 22" << std::endl;
    std::cout << "A: " << answerForA << std::endl;
    std::cout << "B: " << answerForB << std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}


int doNumberOfBursts(GRID &grid, PosAndDir &currpos, int bursts, bool newBurststyle) {
    int infectcount = 0;
    int gridsize = grid.size();
    for (int i = 0; i < bursts; i++) {
        if (newBurststyle){
            if (doBurstv2(grid, currpos, gridsize))
                infectcount++;
        }else{
        if (doBurst(grid, currpos, gridsize))
            infectcount++;}
    }
    return infectcount;
}

void printGrid(const GRID &src) {
    for (auto line:src) {
        std::cout << std::string(line.begin(), line.end()) << std::endl;
    }
}

GRID putGridInLargGridAndGiveCenter(const GRID &smallgrid, int gridsize, PosAndDir &currPos) {
    auto result = makeGrid(gridsize);
    int smallgridw = smallgrid.at(0).size();
    int smallgridh = smallgrid.size();

    int xoffset = (gridsize / 2) - (smallgridw / 2);
    int yoffset = (gridsize / 2) - (smallgridh / 2);

    putGridInGrid(result, smallgrid, xoffset, yoffset);
    currPos.x = xoffset + smallgridw / 2;
    currPos.y = xoffset + smallgridh / 2;
    return result;
}

bool doBurst(GRID &grid, PosAndDir &currpos, int gridsize) {
    bool infected = isInfected(grid, currpos);
    swapInfectstate(grid, currpos);
    if (infected)
        turnRight(currpos);
    else
        turnLeft(currpos);
    doStep(currpos, gridsize);
    return !infected;//returns true if cell is infected, false if it is cleaned
}

bool doBurstv2(GRID &grid, PosAndDir &currpos, int gridsize) {
    char infectedState = getInfectedState(grid, currpos);
    changeInfectionv2(grid, currpos);
    switch (infectedState){
        case '.':
            turnLeft(currpos);
            break;
        case 'W':
            break;
        case '#':
            turnRight(currpos);
            break;
        case 'F':
            turnAround(currpos);
            break;
    }
    doStep(currpos, gridsize);
    return (infectedState=='W');//returns true if cell is infected, false if it is cleaned
}



void changeInfectionv2(GRID &grid, const PosAndDir &currpos) {
    char currState = getInfectedState(grid, currpos);
    switch (currState){
        case '#':
            grid[currpos.y][currpos.x] = 'F';
            break;
        case '.':
            grid[currpos.y][currpos.x] = 'W';
            break;
        case 'W':
            grid[currpos.y][currpos.x] = '#';
            break;
        case 'F':
            grid[currpos.y][currpos.x] = '.';
            break;
        default:
            throw std::runtime_error("Unknown state");

    }

}


void swapInfectstate(GRID &grid, const PosAndDir &currpos) {
    if (isInfected(grid, currpos))
        grid[currpos.y][currpos.x] = '.';
    else
        grid[currpos.y][currpos.x] = '#';
}

char getInfectedState(const GRID &grid, const PosAndDir &currpos) {
    return grid[currpos.y][currpos.x];
}

bool isInfected(const GRID &grid, const PosAndDir &currpos) {
    return grid[currpos.y][currpos.x] == '#';
}

void turnLeft(PosAndDir &curr) {
    auto dxOld = curr.dx;
    curr.dx = curr.dy;
    curr.dy = dxOld * -1;
}

void turnRight(PosAndDir &curr) {
    auto dxOld = curr.dx;
    curr.dx = curr.dy * -1;
    curr.dy = dxOld;
}

void turnAround(PosAndDir &curr) {
    curr.dx *= -1;
    curr.dy *= -1;
}

void doStep(PosAndDir &curr, int gridsize) {
    curr.x += curr.dx;
    curr.y += curr.dy;

    if (curr.x < 0 || curr.y < 0 || curr.x >= gridsize || curr.y >= gridsize) {
        std::string errText = "Error, out of boundry of grid. pos: ";
        errText += toascii(curr.x) + ", " + toascii(curr.x);
        throw std::runtime_error(errText.c_str());
    }
}

GRID makeGrid(int gridsize) {
    GRID grid;
    for (int i = 0; i < gridsize; i++) {
        grid.push_back(std::vector<char>(gridsize, '.'));
    }
    return grid;
}

void putGridInGrid(GRID &biggrid, const GRID &smallgrid, int xoffset, int yoffset) {
    for (int y = 0; y < smallgrid.size(); y++) {
        for (int x = 0; x < smallgrid.at(0).size(); x++) {
            biggrid.at(y + yoffset).at(x + xoffset) = smallgrid.at(y).at(x);
        }
    }
}

GRID readFile(const std::string &filename) {
    std::fstream fs;
    GRID result;
    std::string line;
    fs.open(filename, std::ios_base::in);

    while (std::getline(fs, line)) {
        if (line.at(line.size() - 1) == '\r') {
            line.erase(line.size() - 1, 1);
        }
        std::vector<char> linechars(line.begin(), line.end());
        result.push_back(linechars);
    }
    fs.close();
    return result;
}