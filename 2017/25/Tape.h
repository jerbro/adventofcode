//
// Created by jeroen on 29-12-17.
//

#ifndef INC_2017_TAPE_H
#define INC_2017_TAPE_H
#include "Common.h"
#include <algorithm>

class Tape {
public:
    Tape() : currpos(0) {
        tape = std::vector<bool>(20000, false);
        currpos = (int) tape.size() / 2;
    };

    long countOnes() {
        return std::count(tape.begin(), tape.end(), true);
    };

    void setValue(bool val) {
        tape.at(currpos) = val;
    }

    void move(int dir) {
        currpos += dir;
    };

    bool getValue() { return tape.at(currpos); }

private:
    std::vector<bool> tape;
    unsigned long currpos;
};
#endif //INC_2017_TAPE_H
