//
// Created by jeroen on 29-12-17.
//

#include "Action.h"

Action::Action(VAL_ID_VECT stateData) {
    auto wrvalIter = getNext(stateData.begin(), stateData.end(), textIds::ACIONWRITE);
    auto moveRIter = getNext(stateData.begin(), stateData.end(), textIds::ACTIONMOVE);
    auto nextstIter = getNext(stateData.begin(), stateData.end(), textIds::ACTIONNEXT);
    if (wrvalIter != stateData.end() && moveRIter != stateData.end() && nextstIter != stateData.end()) {
        writeVal = (*wrvalIter).first == "1";
        moveDir = ((*moveRIter).first == "right") ? 1 : -1;
        nextStateStr = (*nextstIter).first;
    }
    nextstate = nullptr;
}
