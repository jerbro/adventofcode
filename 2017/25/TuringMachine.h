//
// Created by jeroen on 29-12-17.
//

#ifndef INC_2017_TURINGMACHINE_H
#define INC_2017_TURINGMACHINE_H

#include "Common.h"
#include "Tape.h"
#include "State.h"

class TuringMachine {
public:
    explicit TuringMachine(VAL_ID_VECT recept);
    ~TuringMachine();

    int execute();

    long countOnesOnTape();

private:
    std::string getValue(VAL_ID_VECT, textIds::ids id);

    Tape tape;
    std::map<std::string, State *> states;
    int steps;
    State *startstate;
};




#endif //INC_2017_TURINGMACHINE_H
