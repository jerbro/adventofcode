//
// Created by jeroen on 29-12-17.
//

#include "State.h"

State::State(VAL_ID_VECT stateData, Tape *tape, const std::string& name):name(name), tape(tape){
    auto currBeg = getNext(stateData.begin(),stateData.end(),textIds::ACTIONCHECK);
    while (currBeg != stateData.end()){
        auto currEnd = getNext(currBeg,stateData.end(),textIds::ACTIONCHECK);
        auto actionData = VAL_ID_VECT(currBeg,currEnd);
        if (currBeg->first == "1"){
            trueActionPtr = new Action(actionData);
        } else{
            falseActionPtr = new Action(actionData);
        }
        currBeg = currEnd;
    }
}

State::~State(){
    if (trueActionPtr != nullptr)
        delete trueActionPtr;
    if (falseActionPtr != nullptr)
        delete falseActionPtr;
}

State* State::execute2() {
    if (tape->getValue())
        return trueActionPtr->doActions2(tape);
    else
        return falseActionPtr->doActions2(tape);
}

void State::setNextStates(std::map<std::string, State *> &states) {
    auto nextState = (states.find(trueActionPtr->getNextStateStr())->second);
    trueActionPtr->setNextState(nextState);
    nextState = (states.find(falseActionPtr->getNextStateStr())->second);
    falseActionPtr->setNextState(nextState);
}