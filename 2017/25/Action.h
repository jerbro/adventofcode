//
// Created by jeroen on 29-12-17.
//

#ifndef INC_2017_ACTION_H
#define INC_2017_ACTION_H
#include "Common.h"
#include "State.h"

class State;

class Action {
public:
    explicit Action(VAL_ID_VECT stateData);

    State * doActions2(Tape *tape) {
        tape->setValue(writeVal);
        tape->move(moveDir);
        return nextstate;
    }

    void setNextState(State *state) { nextstate = state; }

    std::string getNextStateStr() { return nextStateStr; }

private:
    bool writeVal;
    int moveDir;
    std::string nextStateStr;
    State *nextstate;
};

#endif //INC_2017_ACTION_H
