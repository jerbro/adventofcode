//
// Created by jeroen on 29-12-17.
//

#ifndef INC_2017_COMMON_H
#define INC_2017_COMMON_H

#include <vector>
#include <string>


namespace textIds {
    enum ids {
        STARTSTATE, DIAGCHHECKSUK, EMPTYLINE, STATEHEADER, ACTIONCHECK, ACIONWRITE, ACTIONMOVE, ACTIONNEXT
    };
}

typedef std::vector<std::pair<std::string, textIds::ids>> VAL_ID_VECT;

VAL_ID_VECT::iterator getNext(const VAL_ID_VECT::iterator &begin, const VAL_ID_VECT::iterator &end, textIds::ids id);
#endif //INC_2017_COMMON_H
