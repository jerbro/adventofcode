//
// Created by jeroen on 29-12-17.
//

#ifndef INC_2017_STATE_H
#define INC_2017_STATE_H
#include "Common.h"
#include "Tape.h"
#include "Action.h"
#include <map>

class Action;

class State {
public:

    State(VAL_ID_VECT stateData, Tape *tape, const std::string& name);
    ~State();

    State *execute2() ;

    void setNextStates(std::map<std::string, State *> &states) ;

private:
    Action *trueActionPtr;
    Action *falseActionPtr;

    std::string name;
    Tape *tape;
};



#endif //INC_2017_STATE_H
