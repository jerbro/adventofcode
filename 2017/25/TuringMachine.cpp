//
// Created by jeroen on 29-12-17.
//

#include "TuringMachine.h"




TuringMachine::TuringMachine(std::vector<std::pair<std::string, textIds::ids>> recept) {
    std::string startStateStr = getValue(recept, textIds::STARTSTATE);
    steps = (int)strtol(getValue(recept, textIds::DIAGCHHECKSUK).c_str(),nullptr,10);

    auto currBeg = getNext(recept.begin(), recept.end(), textIds::STATEHEADER);
    while (currBeg != recept.end()) {
        auto currEnd = getNext(currBeg, recept.end(), textIds::STATEHEADER);
        auto id = (*currBeg).first;
        VAL_ID_VECT currStateRecept(currBeg, currEnd);
        auto state = new State(currStateRecept, &tape, id);
        states.insert(std::make_pair(id, state));
        currBeg = currEnd;
    }

    for (auto state:states) {
        state.second->setNextStates(states);
    }
    startstate = states.find(startStateStr)->second;
}

TuringMachine::~TuringMachine(){
    for (auto item:states){
        delete item.second;
    }
    states.clear();
}


std::string TuringMachine::getValue(VAL_ID_VECT vals, textIds::ids id) {
    for (auto item:vals) {
        if (item.second == id) {
            return item.first;
        }
    }
    return nullptr;
}

long TuringMachine::countOnesOnTape() {
    return tape.countOnes();
}

int TuringMachine::execute() {
    State *currState = startstate;
    for (int i = 0; i < steps; i++) {
        currState = currState->execute2();
    }
}