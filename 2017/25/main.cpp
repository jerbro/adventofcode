#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
#include <chrono>
#include <regex>
#include "Tape.h"
#include "Action.h"
#include "TuringMachine.h"

class State;
class Tape;
class Action;

VAL_ID_VECT readFile(const std::string &filename);

int main(int argc, char *argv[]) {

    auto start = std::chrono::steady_clock::now();

    auto lines = readFile("input.txt");

    TuringMachine tm = TuringMachine(lines);
    tm.execute();
    auto answerForA = tm.countOnesOnTape();

    std::cout << "Advent of code 2017, day 25" << std::endl;
    std::cout << "A: " << answerForA << std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}

std::map<textIds::ids, std::regex> generateTextsMap() {
    std::map<textIds::ids, std::regex> result{
            std::make_pair(textIds::STARTSTATE, std::regex(R"(^Begin in state (.)\.$)")),
            std::make_pair(textIds::DIAGCHHECKSUK,
                           std::regex(R"(^Perform a diagnostic checksum after (\d+) steps\.$)")),
            std::make_pair(textIds::EMPTYLINE, std::regex(R"(^\s*$)")),
            std::make_pair(textIds::STATEHEADER, std::regex(R"(^In state (.):$)")),
            std::make_pair(textIds::ACTIONCHECK, std::regex(R"(^  If the current value is (\d):$)")),
            std::make_pair(textIds::ACIONWRITE, std::regex(R"(^    - Write the value (\d)\.$)")),
            std::make_pair(textIds::ACTIONMOVE, std::regex(R"(^    - Move one slot to the (right|left)\.$)")),
            std::make_pair(textIds::ACTIONNEXT, std::regex(R"(^    - Continue with state (.)\.$)"))};
    return result;
};

std::pair<std::string, textIds::ids>
parseLine(const std::string &line, const std::map<textIds::ids, std::regex> &validTexts) {
    std::pair<std::string, textIds::ids> retval;
    bool found = false;
    for (auto item:validTexts) {
        std::smatch result;
        if (std::regex_match(line, result, item.second)) {
            retval = std::make_pair(std::string(result[1]), item.first);
            found = true;
        }
    }
    if (!found) {
        std::cout << "Error, line not found: " << line << std::endl;
    }
    return retval;
};


VAL_ID_VECT::iterator getNext(const VAL_ID_VECT::iterator &begin, const VAL_ID_VECT::iterator &end, textIds::ids id) {
    auto beg = begin;
    if (beg != end)
        beg++;
    while (beg != end && (*beg).second != id)
        beg++;
    return beg;
}

std::vector<std::pair<std::string, textIds::ids>> readFile(const std::string &filename) {
    std::fstream fs;
    std::vector<std::pair<std::string, textIds::ids>> result;
    std::string line;
    auto lineIdMap = generateTextsMap();

    fs.open(filename, std::ios_base::in);
    while (std::getline(fs, line)) {
        result.push_back(parseLine(line, lineIdMap));
    }
    return result;
};
