//
// Created by jeroen on 8-12-17.
//

#ifndef INC_2017_COMMAND_H
#define INC_2017_COMMAND_H

#include <string>
#include "Pipe.h"

class Pipe;

class Command {

    private:
        std::string commandName;
protected:
        int* instrPntr;
        void nextInstr();
    public:
        Command(std::string commandName, int* instrPntr);
        virtual void execute(long long  *val1, long long  *val2)=0;
        std::string getInstructionName();

};

class cmdSet:public Command{
public:
    cmdSet(int* instrPntr);
    void execute(long long  *oper1, long long  *oper2);
};

class cmdAdd:public Command{
public:
    cmdAdd(int* instrPntr);
    void execute(long long  *oper1, long long  *oper2);
};

class cmdMul:public Command{
public:
    cmdMul(int* instrPntr);
    void execute(long long  *oper1, long long  *oper2);
};

class cmdMod:public Command{
public:
    cmdMod(int* instrPntr);
    void execute(long long  *oper1, long long  *oper2);
};

class cmdSnd:public Command{
public:
    cmdSnd(int* instrPntr, int* sndCnt);
    void execute(long long  *oper1, long long  *oper2);
    void setPipe(Pipe* pipe);
private:
    Pipe* pipe;
    int* runcnt;
};

class cmdRcv:public Command{
public:
    cmdRcv(int* instrPntr, bool* keepRunning);
    void execute(long long  *oper1, long long  *oper2);
    void setPipe(Pipe* pipe);
    void setMode(char mode);
private:
    Pipe* pipe;
    int* sndBuffer;
    bool* keepCpuRunning;
    char mode;

};

class cmdJgz:public Command{
public:
    cmdJgz(int* instrPntr);
    void execute(long long  *oper1, long long  *oper2);
private:
};
#endif //INC_2017_COMMAND_H
