//
// Created by jeroen on 23-12-17.
//

#ifndef INC_2017_PROCESSOR_H
#define INC_2017_PROCESSOR_H

#include <vector>
#include <map>
#include "Instruction.h"
#include "Command.h"
#include "Pipe.h"

class Pipe;
class Instruction;
class Command;

enum EnumProcStatus{
    Init,
    Running,
    Waiting,
    Finished
};

class Processor {

public:
    Processor(std::vector<std::string> asminstructions, std::string name);
    ~Processor();
    void executeProgram();
    static void staticExecuteProgram(Processor* proc);

    int sndCnt = 0;
    int instrPntr = 0;

    void registerCommands();
    bool keepRunning;
    void setStatus(EnumProcStatus status);
    EnumProcStatus getStatus();
    void setRegister(std::string regname,long long val);
    void setSndPipe(Pipe* pipe);
    void setRcvPipe(Pipe* pipe);
    void setRcvMode(char mode);

private:
    std::vector<Instruction *> instructions;
    std::vector<Command *> commands;
    std::map<std::string, long long> registers;
    std::map<long long, long long> constants;
    EnumProcStatus status;
    std::string name;
    Instruction *parseAsmInstruction(std::string line);

    long long int * getRegisterPointer(std::string regname);

    long long int * getConstantPointer(long long int val);

    long long int * operandToAddress(std::string regname);

    Command *getCommand(std::string cmdName);

};


#endif //INC_2017_PROCESSOR_H
