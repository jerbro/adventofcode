

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <chrono>

#include <list>
#include <boost/functional/hash.hpp>
#include <boost/algorithm/string.hpp>
#include <unordered_set>
#include <regex>
#include <thread>
#include "Instruction.h"
#include "Processor.h"
#include "Pipe.h"


std::vector<std::string> readFile(const std::string & filename);


int main(int argc, char *argv[]) {
    auto start = std::chrono::steady_clock::now();


    std::vector<Command *> commands;
    std::map<std::string, int> registers;


    auto instructions = readFile("input.txt");
    Processor* proc = new Processor(instructions, "Pr");
    Pipe *pipe = new Pipe(proc, proc, "none");
    proc->executeProgram();

    auto answerForA =  pipe->peekLast();


    Processor* proc0 = new Processor(instructions, "Pr0");
    Processor* proc1 = new Processor(instructions, "Pr1");
    proc1->setRegister("p",1);
    proc0->setRcvMode('B');
    proc1->setRcvMode('B');

    Pipe *pipe0 = new Pipe(proc1, proc0, "P0");
    Pipe *pipe1 = new Pipe(proc0, proc1, "P1");

    std::thread p1(Processor::staticExecuteProgram,proc1);
    std::thread p0(Processor::staticExecuteProgram,proc0);


    p0.join();
    p1.join();

    //    resetRegisters(registers);
    auto answerForB = proc1->sndCnt;




    std::cout << "Advent of code 2017, day 18" << std::endl;
    std::cout << "A: "<<answerForA<<std::endl;
    std::cout << "B: "<<answerForB<<std::endl;
//

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;


    delete proc;
    delete pipe;
    delete proc0;
    delete proc1;
    delete pipe0;
    delete pipe1;

    return 0;
}




//
////Read the file.
std::vector<std::string> readFile(const std::string & filename){
    std::vector<std::string> lines;

    std::fstream fs;

    fs.open(filename, std::ios::in);
    std::string line;
    while(std::getline(fs,line)){
        if (line.at(line.size()-1) == '\r')
            line.erase(line.size()-1,1);
        lines.push_back(line);
    }
    fs.close();

    return lines;
}
