//
// Created by jeroen on 8-12-17.
//

#include "Command.h"

void Command::nextInstr() { (*instrPntr)++; }

Command::Command(std::string commandName, int *instrPntr) : commandName(commandName), instrPntr(instrPntr) {};

std::string Command::getInstructionName() { return commandName; }


cmdSet::cmdSet(int *instrPntr) : Command("set", instrPntr) {};

void cmdSet::execute(long long *oper1, long long *oper2) {
    (*oper1) = (*oper2);
    nextInstr();
}


cmdAdd::cmdAdd(int *instrPntr) : Command("add", instrPntr) {};

void cmdAdd::execute(long long *oper1, long long *oper2) {
    (*oper1) += (*oper2);
    nextInstr();
}


cmdMul::cmdMul(int *instrPntr) : Command("mul", instrPntr) {};

void cmdMul::execute(long long *oper1, long long *oper2) {
    (*oper1) *= (*oper2);
    nextInstr();
}


cmdMod::cmdMod(int *instrPntr) : Command("mod", instrPntr) {};

void cmdMod::execute(long long *oper1, long long *oper2) {
    (*oper1) %= (*oper2);
    nextInstr();
}


cmdSnd::cmdSnd(int *instrPntr, int* sndCnt) : Command("snd", instrPntr), runcnt(sndCnt) {};

void cmdSnd::execute(long long *oper1, long long *oper2) {
    this->pipe->put(*oper1);
    (*(this->runcnt))++;
    nextInstr();
}

void cmdSnd::setPipe(Pipe *pipe) {
    this->pipe = pipe;
}


cmdRcv::cmdRcv(int *instrPntr, bool *keepRunning) : Command("rcv", instrPntr), keepCpuRunning(keepRunning),
                                                    mode('A') {};

void cmdRcv::execute(long long *oper1, long long *oper2) {
    if (mode == 'A') { // Instruction as expected by part A;
        if (*oper1 != 0) {
            (*keepCpuRunning) = false;
        }
    } else {
        bool readFailed;
        long long tmp =pipe->get(&readFailed);
        (*oper1) = tmp;
        if (readFailed)
            (*keepCpuRunning) = false;
    }
    nextInstr();
}

void cmdRcv::setPipe(Pipe *pipe) {
    this->pipe = pipe;
}

void cmdRcv::setMode(char mode) {
    this->mode = mode;
}


cmdJgz::cmdJgz(int *instrPntr) : Command("jgz", instrPntr) {};

void cmdJgz::execute(long long *oper1, long long *oper2) {
    if ((*oper1) > 0)
        (*instrPntr) += (*oper2);
    else
        nextInstr();
}

