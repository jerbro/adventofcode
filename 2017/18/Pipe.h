//
// Created by jeroen on 24-12-17.
//

#ifndef INC_2017_PIPE_H
#define INC_2017_PIPE_H


#include <mutex>
#include <deque>
#include "Processor.h"
class Processor;

class Pipe {
public:
    Pipe(Processor *receivingProc, Processor *sendingProc, std::string name);
    void put(long long val);
    long long get(bool* failed);
    long long peekLast();

    private:
    std::mutex lock;
    Processor* receivingProc;
    Processor* sendingProc;
    std::deque<long long> buffer;
    std::string name;
    const int timeout = 50;
};


#endif //INC_2017_PIPE_H
