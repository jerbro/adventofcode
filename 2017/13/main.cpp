#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <chrono>
#include <climits>


std::map<int, int> readFile(const std::string &filename);

std::pair<int, int> parseLine(const std::string &line);

bool isInTopRowAtTime(unsigned long time, int depth);

unsigned long calcTripSeverity(const std::map<int, int> &firewallLayers);

bool canPassUndetected(const std::map<int, int> &firewallLayers, unsigned long startDelay);

unsigned long getDelayForUndetectedPass(const std::map<int, int> &firewallLayers);

int main(int argc, char *argv[]) {

    auto start = std::chrono::steady_clock::now();

    auto firewallLayers = readFile("input.txt");


    auto answerForA = calcTripSeverity(firewallLayers);
    auto answerForB = getDelayForUndetectedPass(firewallLayers);


    std::cout << "Advent of code 2017, day 13" << std::endl;
    std::cout << "A: " << answerForA << std::endl;
    std::cout << "B: " << answerForB << std::endl;

    auto end = std::chrono::steady_clock::now();
    std::cout << "Execution took " << std::chrono::duration<double, std::milli>(end - start).count() << " ms"
              << std::endl;

    return 0;
}

unsigned long getDelayForUndetectedPass(const std::map<int, int> &firewallLayers) {
    unsigned long delay = 0;
    while (delay < ULONG_MAX) {
        if (canPassUndetected(firewallLayers, delay))
            return delay;
        delay++;
    }
    throw std::runtime_error("Not found within " + std::to_string(delay) + " tries");
}

bool canPassUndetected(const std::map<int, int> &firewallLayers, unsigned long startDelay) {
    for (auto layer:firewallLayers) {
        unsigned long currTime = layer.first + startDelay;
        int layerDepth = layer.second;
        if (isInTopRowAtTime(currTime, layerDepth))
            return false;
    }
    return true;
}

unsigned long calcTripSeverity(const std::map<int, int> &firewallLayers) {
    unsigned long severity = 0;
    for (auto layer:firewallLayers) {
        unsigned long currTime = layer.first;
        int layerDepth = layer.second;
        if (isInTopRowAtTime(currTime, layerDepth))
            severity += (layerDepth * currTime);
    }
    return severity;
}


bool isInTopRowAtTime(unsigned long time, int depth) {
    int up_and_down_time = (depth * 2) - 2;
    return time % up_and_down_time == 0;
}

std::pair<int, int> parseLine(const std::string &line) {
    std::string modifiedLine = line;
    std::replace(modifiedLine.begin(), modifiedLine.end(), ':', ' ');
    std::stringstream strStr(modifiedLine);

    int depth, length;
    strStr >> depth;
    strStr >> length;
    return std::make_pair(depth, length);

};

std::map<int, int> readFile(const std::string &filename) {
    std::map<int, int> result;
    std::fstream fs;
    std::string line;
    fs.open(filename, std::fstream::openmode::_S_in);
    while (std::getline(fs, line)) {
        result.insert(parseLine(line));
        line = "";
    }
    fs.close();

    return result;
}
