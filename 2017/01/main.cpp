#include <iostream>
#include <fstream>
#include <sstream>

std::string readFile(const std::string &filename);
int findCaptchaA(const std::string &txt);
int findCaptchaB(const std::string &txt);
int eraseNonNumericCharsFromString(std::string &txt);

int main(int argc, char *argv[]) {

    auto fileContent = readFile("input.txt");
    eraseNonNumericCharsFromString(fileContent);
    auto captchaA = findCaptchaA(fileContent);
    auto captchaB = findCaptchaB(fileContent);

    std::cout << "Advent of code 2017, day 01" << std::endl;
    std::cout << "A: " << captchaA << std::endl;
    std::cout << "B: " << captchaB << std::endl;


    return 0;
}


std::string readFile(const std::string &filename) {
    std::string result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::stringstream strStr;
    strStr << fs.rdbuf();
    fs.close();
    result = strStr.str();
    return result;
}

int eraseNonNumericCharsFromString(std::string &txt) {
    int changes = 0;
    for (unsigned int i = 0; i < txt.length(); i++) {
        char currCh = txt[i];
        if (!(currCh >= '0' && currCh <= '9')) {
            txt.erase(i, 1);
            changes++;
        }
    }
    return changes;

}

int findCaptchaA(const std::string &txt) {
    int result = 0;
    for (int i = 0; i < txt.length(); i++) {
        char currCh = txt[i];
        char nextCh = txt[(i + 1) % txt.length()];
        if (currCh == nextCh)
            result += currCh - '0';
    }
    return result;
}

int findCaptchaB(const std::string &txt) {
    int result = 0;
    for (int i = 0; i < txt.length(); i++) {
        char currCh = txt[i];
        char nextCh = txt[(i + (txt.length() / 2)) % txt.length()];
        if (currCh == nextCh)
            result += currCh - '0';
    }
    return result;
}