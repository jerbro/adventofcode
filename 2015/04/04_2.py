import md5

m=md5.new()
m.update('abcdef609043')
print m.hexdigest()

teststr=m.hexdigest()
print teststr[:5]=="00000"

string = open('input.txt','r').read()


numberFound = False
testNumber = 0
while numberFound ==False:
	testNumber +=1
	md5result = md5.new(string+str(testNumber)).hexdigest()
	if md5result[:6]=="000000":
		numberFound = True
	if testNumber%1000==0:
		print testNumber



print "For string: '%s', the answer is: %d, and the resulting md5-code: %s" %(string,testNumber,md5result)