import itertools

sizesList = map(lambda x: int(x),open('input.txt').read().splitlines() )

count = 0
for i in range(1,len(sizesList)+1):
	summedSizes = map(lambda x: sum(x),itertools.combinations(sizesList,i))
	count += len(filter(lambda x: x==150,summedSizes))
	if count >0:
		break

print count