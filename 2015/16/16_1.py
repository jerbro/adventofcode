import re


def parseInputfile(filename):
	filecontent = open(filename,'r').read().splitlines()
	result = {}
	for line in filecontent:
		(name,personProperties) = re.match('^(Sue [0-9]*): (.*)$',line).group(1,2)
		result[name] = {}
		for prop in personProperties.split(', '):
			(propName,propVal) = re.match('^([^:]*): ([0-9]*)$',prop).group(1,2)
			result[name][propName] = int(propVal)
	return result

def suePropertiesValid(sueProps,investigatedProps):
	for prop in sueProps:
		if investigatedProps[prop] != sueProps[prop]:
			return False
	return True


MFCSAMresult = {'children': 3, 
				'cats': 7, 
				'samoyeds': 2, 
				'pomeranians': 3, 
				'akitas': 0, 
				'vizslas': 0, 
				'goldfish': 5, 
				'trees': 3, 
				'cars': 2, 
				'perfumes': 1}


sueProperties = parseInputfile('input.txt')
for sue in sueProperties:
	if suePropertiesValid(sueProperties[sue],MFCSAMresult):
		print sue +": " +str(sueProperties[sue])