import collections
import copy

Spells ={	'Magic Missile'	: {'cost': 53, 'type': 'spell' ,'damage':4, 'recharge': 0  , 'armor':0, 'heal': 0},
			'Drain'			: {'cost': 73, 'type': 'spell' ,'damage':2, 'recharge': 0  , 'armor':0, 'heal': 2},
			'Shield' 		: {'cost':113, 'type': 'effect','damage':0, 'recharge': 0  , 'armor':7, 'heal': 0,'effectTime':6},
			'Poison' 		: {'cost':173, 'type': 'effect','damage':3, 'recharge': 0  , 'armor':0, 'heal': 0,'effectTime':6},
			'Recharge'  	: {'cost':229, 'type': 'effect','damage':0, 'recharge': 101, 'armor':0, 'heal': 0,'effectTime':5}}


items = Spells.keys()

#playerInfo = {'mana': 250, 'Hit Points':10, 'EffectTimeLeft': {'Shield':0,'Poison':0,'Recharge':0}}
playerInfo = {'mana': 500, 'Hit Points':50, 'EffectTimeLeft': {'Shield':0,'Poison':0,'Recharge':0}}


def doPlayerAction(bossInfo,playerInfo,spells,spell):
	armor =0
	heal =0
	damage =0
	recharge =0
	for item in playerInfo['EffectTimeLeft']:
		if playerInfo['EffectTimeLeft'][item]>0:
			armor += spells[item]['armor']
			#heal += spells[item].heal
			damage += spells[item]['damage']
			recharge += spells[item]['recharge']
			playerInfo['EffectTimeLeft'][item] -=1			
	if spells[spell]['type'] == 'effect':
		playerInfo['EffectTimeLeft'][spell]=spells[spell]['effectTime']
	else:
		damage += spells[spell]['damage']
		heal += spells[spell]['heal']						
	bossInfo['Hit Points'] -= damage
	playerInfo['Hit Points']+=heal
	playerInfo['mana'] += recharge
	playerInfo['mana'] -= spells[spell]['cost']
	return (playerInfo,bossInfo)

def doBossAction(bossInfo,playerInfo,spells):
	armor =0
	heal =0
	damage =0
	recharge =0
	for item in playerInfo['EffectTimeLeft']:
		if playerInfo['EffectTimeLeft'][item]>0:
			armor += spells[item]['armor']
			#heal += spells[item].heal
			damage += spells[item]['damage']
			recharge += spells[item]['recharge']
			playerInfo['EffectTimeLeft'][item] -=1			

	bossInfo['Hit Points'] -= damage
	playerInfo['Hit Points'] -= max(bossInfo['Damage']-armor,1)
	playerInfo['mana'] += recharge
	return (playerInfo,bossInfo)




def readFileWithBossProperties(filename):
	fileContent = open(filename,'r').read().splitlines()
	bossProperties = {}
	for line in fileContent:
		(key,value) = line.split(':')
		bossProperties[key]=int(value)
	return bossProperties

def getPossibleSpellsToBuy(playerInfo,spells):
	possibleSpells = []
	for spell in spells.keys():
		if (spells[spell]['type']=='effect'):
			if ((playerInfo['EffectTimeLeft'][spell] <=1) and (playerInfo['mana'] >= spells[spell]['cost'])):
				possibleSpells.append(spell)
		elif ((spells[spell]['type']=='spell') and (playerInfo['mana'] >= spells[spell]['cost'])):
			possibleSpells.append(spell)
	return possibleSpells





bossProperties =  readFileWithBossProperties('input.txt')






def getNextSpell(playerInfo,bossInfo,spells,currspell,costTillNow,spellString,minCostTillWin,actionsDone):
	if currspell == "":
		newPlayerInfo = copy.deepcopy(playerInfo)
		newBossProperties = copy.deepcopy(bossInfo)
	else:
		tmpNewPlayerInfo = copy.deepcopy(playerInfo)
		tmpNewBossInfo = copy.deepcopy(bossInfo)
		(tmpNewPlayerInfo,tmpNewBossProperties) = doPlayerAction(tmpNewBossInfo,tmpNewPlayerInfo,spells,currspell)
		(newPlayerInfo,newBossProperties) = doBossAction(tmpNewBossProperties,tmpNewPlayerInfo,spells)

	if newBossProperties['Hit Points']<=0:
		print "minCostToWin:"+str(minCostTillWin)+" spellString:"+spellString+" - "+"  actionsDone" + str(actionsDone)
		return costTillNow
	elif newPlayerInfo['Hit Points'] <=0:
		return 99999999

	if costTillNow > minCostTillWin:
		return 99999999
	if actionsDone > 20:
		return 99999999

	possibleSpells = getPossibleSpellsToBuy(newPlayerInfo,spells)
#	print "spellString"+spellString+"  -  " +str(newPlayerInfo)+str(newBossProperties)+str(actionsDone)+" - minCostTillWin:"+str(minCostTillWin)+" - Actions done:"+str(actionsDone)
#	print "Possible spells to buy: " +str(possibleSpells)

	if not possibleSpells:
		return 99999999

	newMinCostTillWin = minCostTillWin

	for spell in possibleSpells:
		tmpNewPlayerInfo=copy.deepcopy(newPlayerInfo)
		tmpNewBossInfo = copy.deepcopy(newBossProperties)


		costToWinFromThisAction = getNextSpell(tmpNewPlayerInfo,tmpNewBossInfo,spells,spell,costTillNow+spells[spell]['cost'],spellString+" - "+spell,newMinCostTillWin,actionsDone+1)
		if costToWinFromThisAction<newMinCostTillWin:
			newMinCostTillWin=costToWinFromThisAction
	return newMinCostTillWin

#bossProperties = {'Hit Points': 14,'Damage': 8}

getNextSpell(playerInfo,bossProperties,Spells,"",0,"",99999999,0)

