import numpy

filecontent = open('input.txt','r').read().splitlines()

matrix = []
lineStatus = []
for line in filecontent:
	lineStatus = []
	for x in line:
		if x=='#':
			lineStatus.append(1)
		else:
			lineStatus.append(0)
	matrix.append(lineStatus)



newMatrix=[]

def countSurroundingOn(x,y,matrix):
	sum =0
	for y1 in range(max(0,y-1),min(y+2,len(matrix))):
		for x1 in range(max(0,x-1),min(x+2,len(matrix[y1]))):
			sum += matrix[y1][x1]
	sum -= matrix[y][x]
	return sum

def getMatrixWithSurroundingOnCount(matrix):
	newMatrix = []
	for y in range(len(matrix)):
		newRow = []
		for x in range(len(matrix[y])):
			newRow.append(countSurroundingOn(x,y,matrix))
		newMatrix.append(newRow)
	return newMatrix


def calcNextStep(currMatrix,neighboursOnMatrix):
	newMatrix = []
	for y in range(len(currMatrix)):
		newRow = []
		for x in range(len(currMatrix[y])):
			val =currMatrix[y][x]
			if (val==1 and not(neighboursOnMatrix[y][x]==2 or neighboursOnMatrix[y][x]==3)):
				val = 0
			elif val ==0 and ((neighboursOnMatrix[y][x]==3)):
				val =1
			newRow.append(val)
		newMatrix.append(newRow)
	return newMatrix



print numpy.array(matrix)
matrixHeight = len(matrix)
matrixWidth = len(matrix[0])
for i in range(100):
	print "Calculating step: "+str(i+1)
	matrix[0][0]=1
	matrix[0][len(matrix[0])-1]=1
	matrix[len(matrix)-1][0]=1
	matrix[len(matrix)-1][len(matrix[0])-1]=1
	matrix = calcNextStep(matrix,getMatrixWithSurroundingOnCount(matrix))

matrix[0][0]=1
matrix[0][len(matrix[0])-1]=1
matrix[len(matrix)-1][0]=1
matrix[len(matrix)-1][len(matrix[0])-1]=1

print numpy.array(matrix)
print numpy.count_nonzero(numpy.array(matrix))

