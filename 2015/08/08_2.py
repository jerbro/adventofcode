import codecs

inputfile = open('input.txt')
fileContent = inputfile.read().splitlines()

stringLength =0
stringValueLength = 0

for line in fileContent:
	stringLength += ((len(line)))
	print line+"  length: "+str((len(line)))
	stringValueLength += len(line.encode("string_escape"))+line.count('"')+2
	print line.encode("string_escape")+"  length: "+str(len(line.encode("string_escape"))+line.count('"')+2)

print stringValueLength-stringLength

