import codecs

inputfile = open('input.txt')
fileContent = inputfile.read().splitlines()

stringLength =0
stringValueLength = 0

for line in fileContent:
	stringLength += (len(line))
	stringValueLength += len(line.decode("string_escape"))-2
	
print stringLength-stringValueLength

