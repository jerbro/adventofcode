puzzleInput = int(open('input.txt','r').read())
print puzzleInput


maxHouseNr=puzzleInput/10
housePresentList =[0]*maxHouseNr

#make list with all houses, and the number of presents each house has
maxElfNr=maxHouseNr
for elf in range(1,maxElfNr+1):
	for housevisit in range(0,maxHouseNr,elf):
		housePresentList[housevisit]+=(elf*10)

#Find the first house that has more than the puzzleinput number of presents
for i in range(1,len(housePresentList)):
	if housePresentList[i] > puzzleInput:
		print "House: "+str(i)+"  got "+str(housePresentList[i])+" presents."
		break
