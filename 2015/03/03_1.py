
#return a dictionary with keyname: x.._y.. (in which dots is the position), and with value, number of visits by santa (only houses that are visited will be put in the dictionary)
def fillDictWithVisitedHouses(fileContent):
	currX=0
	currY=0
	dictWithVisitedHouses={}
	dictWithVisitedHouses['x0_y0']=1
	for character in fileContent:
		if character == '^':
			currY+=1
		elif character =='<' :
			currX-=1
		elif character =='>' :
			currX+=1
		elif character =='v' :
			currY-=1
		keyname = 'x'+str(currX)+'_y'+str(currY)
		dictWithVisitedHouses[keyname]=dictWithVisitedHouses.get(keyname,0)+1
	return dictWithVisitedHouses



inputfile = open('input.txt','r')
fileContent = inputfile.read()

#The number of keys is the number of visited houses
print len(fillDictWithVisitedHouses(fileContent))