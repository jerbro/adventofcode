
#return a dictionary with keyname: x.._y.. (in which dots is the position), and with value, number of visits by santa (only houses that are visited will be put in the dictionary)
def fillDictWithVisitedHouses(fileContent):
	currX=[0,0] #Xpos for santa, and robosanta (first is for santa, second for robosanta)
	currY=[0,0] #Ypos for santa, and robosanta (first is for santa, second for robosanta)
	dictWithVisitedHouses={}
	dictWithVisitedHouses['x0_y0']=1
	characterNr =0 #Character number, first character is for santa, 2nd for robosanta, 3rd for santa, ....
	for character in fileContent:
		if character == '^':
			currY[characterNr%2]+=1
		elif character =='<' :
			currX[characterNr%2]-=1
		elif character =='>' :
			currX[characterNr%2]+=1
		elif character =='v' :
			currY[characterNr%2]-=1
		keyname = 'x'+str(currX[characterNr%2])+'_y'+str(currY[characterNr%2])
		dictWithVisitedHouses[keyname]=dictWithVisitedHouses.get(keyname,0)+1
		characterNr+=1		
	return dictWithVisitedHouses



inputfile = open('input.txt','r')
fileContent = inputfile.read()

#The number of keys is the number of visited houses
print len(fillDictWithVisitedHouses(fileContent))