import re
totalTravelTime = 2503

def calcFlownDistance(speed,flyTime,restTime,travelTime):
	wholeCyclesFlown = travelTime/(flyTime+restTime)
	timeOutsideCycleFlown = travelTime-(flyTime+restTime)*wholeCyclesFlown
	if timeOutsideCycleFlown>flyTime:
		return (flyTime + wholeCyclesFlown*flyTime)*speed
	else:
		return (timeOutsideCycleFlown+wholeCyclesFlown*flyTime)*speed



filecontent = open('input.txt','r').read().splitlines()

largestTravelDistanceTillNow = 0
for line in filecontent:
	lineReader = re.match('^([a-zA-Z]*) can fly ([0-9]*) km/s for ([0-9]*) seconds, but then must rest for ([0-9]*) seconds.',line)
	(name,speed,flyTime,restTime)=lineReader.group(1,2,3,4)
	speed = int(speed)
	flyTime = int(flyTime)
	restTime = int(restTime)
	if calcFlownDistance(speed,flyTime,restTime,totalTravelTime) > largestTravelDistanceTillNow:		
		print name+" - "+str(speed)+" - "+str(flyTime)+" - "+str(restTime) + " - "+str(calcFlownDistance(speed,flyTime,restTime,totalTravelTime))
		largestTravelDistanceTillNow = calcFlownDistance(speed,flyTime,restTime,totalTravelTime)
#Dancer can fly 3 km/s for 16 seconds, but then must rest for 37 seconds.

print "winner travels: %d" % (largestTravelDistanceTillNow)


