import re
totalTravelTime = 2503

def calcFlownDistance(speed,flyTime,restTime,travelTime):
	wholeCyclesFlown = travelTime/(flyTime+restTime)
	timeOutsideCycleFlown = travelTime-(flyTime+restTime)*wholeCyclesFlown
	if timeOutsideCycleFlown>flyTime:
		return (flyTime + wholeCyclesFlown*flyTime)*speed
	else:
		return (timeOutsideCycleFlown+wholeCyclesFlown*flyTime)*speed





def parseInputFile(filecontent):
	result ={}
	for line in filecontent:
		lineReader = re.match('^([a-zA-Z]*) can fly ([0-9]*) km/s for ([0-9]*) seconds, but then must rest for ([0-9]*) seconds.',line)
		(name,speed,flyTime,restTime)=lineReader.group(1,2,3,4)
		result[name]={}
		result[name]['speed'] = int(speed)
		result[name]['flyTime'] = int(flyTime)
		result[name]['restTime']=int(restTime)
		result[name]['points']=0
		result[name]['distanceTravelled']=0
	return result


#Dancer can fly 3 km/s for 16 seconds, but then must rest for 37 seconds.


largestTravelDistanceTillNow = 0
filecontent = open('input.txt','r').read().splitlines()
reindeerInfo = parseInputFile(filecontent)


for i in range(totalTravelTime):
	for reindeer in reindeerInfo:
		reindeerInfo[reindeer]['distanceTravelled']=calcFlownDistance(reindeerInfo[reindeer]['speed'],reindeerInfo[reindeer]['flyTime'],reindeerInfo[reindeer]['restTime'],i+1)

	nameOfCurrentWinner = max(reindeerInfo.iterkeys(),key= lambda k:reindeerInfo[k]['distanceTravelled'])
	maxTravelledDistanceNow = reindeerInfo[nameOfCurrentWinner]['distanceTravelled']

	for reindeer in reindeerInfo:
		if reindeerInfo[reindeer]['distanceTravelled'] == maxTravelledDistanceNow:
			reindeerInfo[reindeer]['points'] +=1


nameOfWinner = max(reindeerInfo.iterkeys(),key=lambda k:reindeerInfo[k]['points'])
print "the winner, "+str(nameOfWinner)+" has : "+str(reindeerInfo[nameOfWinner]['points']) + " points"
