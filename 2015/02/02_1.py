inputfile = open('input.txt','r')

#Get width,height,length from a line of text
def getwhlFromLine(text):
	textWithoutLineEnd = text.strip('\r\n')
	whl=textWithoutLineEnd.split('x')
	return whl

#calculate the size of the needed paper
def getSquareSizePaperNeeded(whl):
	sortedSizes = sorted(map(int,whl)) #convert the whl-values to int, and sort low-high
	l1 = sortedSizes[0] #length of shortest side
	l2 = sortedSizes[1] #length of middle-sized side
	l3 = sortedSizes[2] #length of longest side
	squareSize = 3*l1*l2+2*l1*l3+2*l2*l3
	return squareSize


totalPaperNeeded = 0
for line in inputfile:
	whl = getwhlFromLine(line)
	totalPaperNeeded += getSquareSizePaperNeeded(whl)

print "Total paper needed: %d" %(totalPaperNeeded)	


