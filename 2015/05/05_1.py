import re

def checkStringIsNice(txt):
	rule1 = re.compile("^.*([aeiou].*){3}.*$") #3 vowels
	rule2 = re.compile("^.*(.)\\1.*$") #at least 1 double character
	rule3 = re.compile("^.*(ab|cd|pq|xy).*$") #contains string ab, cd, pq, or xy (Result must be inverted)
	rule1Nice = bool(rule1.match(txt))
	rule2Nice = bool(rule2.match(txt))
	rule3Nice = not(bool(rule3.match(txt)))

	return rule1Nice and rule2Nice and rule3Nice



inputfile = open('input.txt','r')
filecontent=inputfile.readlines()

niceStrings = 0
for line in filecontent:
	line = line.strip('\r\n')
	if checkStringIsNice(line):
		niceStrings +=1

print "Number of nice strings: %d" % (niceStrings)



