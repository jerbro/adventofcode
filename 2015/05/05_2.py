import re

def checkStringIsNice(txt):
	rule1 = re.compile("^.*(..).*\\1.*$") #It contains a pair of any two letters that appears at least twice in the string without overlapping
	rule2 = re.compile("^.*(.).\\1.*$") #It contains at least one letter which repeats with exactly one letter between them

	rule1Nice = bool(rule1.match(txt))
	rule2Nice = bool(rule2.match(txt))


	return rule1Nice and rule2Nice 



inputfile = open('input.txt','r')
filecontent=inputfile.readlines()

niceStrings = 0
for line in filecontent:
	line = line.strip('\r\n')
	if checkStringIsNice(line):
		niceStrings +=1

print "Number of nice strings(new method): %d" % (niceStrings)



