
def parseInputFile(filename):
	fileContent = open(filename,'r').read().splitlines()
	fileContent = map(lambda x: x.replace(",","").split(" "),fileContent)
	for i in range(0,len(fileContent)):
		for j in range(0,len(fileContent[i])):
			if fileContent[i][j].lstrip("-+").isdigit():
				fileContent[i][j]=int(fileContent[i][j])
	return fileContent


def executeInstruction(instructionList,a,b,currInstructionCounter):
	if instructionList[currInstructionCounter][0] =='inc':		
		return instructionInc(instructionList[currInstructionCounter],a,b,currInstructionCounter)
	elif instructionList[currInstructionCounter][0] =='hlf':		
		return instructionHlf(instructionList[currInstructionCounter],a,b,currInstructionCounter)
	elif instructionList[currInstructionCounter][0] =='tpl':		
		return instructionTpl(instructionList[currInstructionCounter],a,b,currInstructionCounter)
	elif instructionList[currInstructionCounter][0] =='jio':		
		return instructionJio(instructionList[currInstructionCounter],a,b,currInstructionCounter)		
	elif instructionList[currInstructionCounter][0] =='jie':		
		return instructionJie(instructionList[currInstructionCounter],a,b,currInstructionCounter)		
	elif instructionList[currInstructionCounter][0] =='jmp':		
		return instructionJmp(instructionList[currInstructionCounter],a,b,currInstructionCounter)	
	else:
		print "Instruction not found. Error. "+str(instructionList[currInstructionCounter])					


def instructionInc(instructionLine,areg,breg,currInstructionCounter):
	a=areg
	b=breg
	nextInstruction = currInstructionCounter+1
	if instructionLine[1]=='a':
		a=a+1
	else:
		b=b+1
	return (a,b,nextInstruction)

def instructionTpl(instructionLine,areg,breg,currInstructionCounter):
	a=areg
	b=breg
	nextInstruction = currInstructionCounter+1
	if instructionLine[1]=='a':
		a=a*3
	else:
		b=b*3
	return (a,b,nextInstruction)	

def instructionHlf(instructionLine,areg,breg,currInstructionCounter):
	a=areg
	b=breg
	nextInstruction = currInstructionCounter+1
	if instructionLine[1]=='a':
		a=a/2
	else:
		b=b/2
	return (a,b,nextInstruction)	

def instructionJmp(instructionLine,areg,breg,currInstructionCounter):
	a=areg
	b=breg
	nextInstruction = currInstructionCounter
	nextInstruction += instructionLine[1]
	return (a,b,nextInstruction)	

def instructionJie(instructionLine,areg,breg,currInstructionCounter):
	a=areg
	b=breg
	nextInstruction = currInstructionCounter	
	if ((instructionLine[1]=='a') and (a%2==0)) or ((instructionLine[1]=='b') and (b%2==0)) :
		nextInstruction += instructionLine[2]
	else:
		nextInstruction += 1
	return (a,b,nextInstruction)	

def instructionJio(instructionLine,areg,breg,currInstructionCounter):
	a=areg
	b=breg
	nextInstruction = currInstructionCounter
	if ((instructionLine[1]=='a') and (a==1)) or ((instructionLine[1]=='b') and (b==1)) :
		nextInstruction += instructionLine[2]
	else:
		nextInstruction += 1
	return (a,b,nextInstruction)	




instructions= parseInputFile('input.txt')

a=0
b=0
newInstructionCounter=0
while newInstructionCounter < len(instructions):
	(a,b,newInstructionCounter)= executeInstruction(instructions,a,b,newInstructionCounter)
	
print "After finishing, a: %d, b: %d" % (a,b)

