import re

def parseInputFile(filename):
	fileContent = open(filename,'r').read()
	regex = re.match("^.*row ([0-9]*)[^0-9].*column ([0-9]*)[^0-9].*$",fileContent)
	(row,col)= regex.group(1,2)
	return (int(row), int( col))


#sum of all previous rownumbers(not including current row)
def getValOfFirstCol(row):
	return (row-1+1)*((row-1)*0.5)+1

"""Value in of cell:
Diagonal with a value at row,col starts at column:1, row: (row+col-1)
Value = valOfFirstCol(row+col-1)+col-1
"""
def getNumberOfCell(row,col):
	return getValOfFirstCol(row+col-1)+col-1



row,col = parseInputFile('input.txt')
nr= int(getNumberOfCell(row,col))

answer = 20151125 #The first answer

for i in range(1,nr):
	answer = answer*252533
	answer %= 33554393
print answer
