import pprint
import re

#Checks if txt is a number, if yes, return it, if no, return value of variable with that name return -1 if var not exists
def returnValueOfVar(txt,vars_dictionary):
	if unicode(txt).isnumeric():
		return int(txt)
	elif txt in vars_dictionary:
		return vars_dictionary[txt]
	else:
		return -1

#Read the information from a line
def getInformationFromLine(txt,vars_dictionary):
	getInfo = re.compile("^([a-z0-9]*)[ ]?(AND|OR|RSHIFT|LSHIFT|NOT)?[ ]?([0-9a-z]*) -> ([a-z]*)$")
	info = getInfo.match(txt)
	result={}
	result['lVar'] = returnValueOfVar(info.group(1),vars_dictionary)
	result['action'] = (info.group(2))
	result['rVar'] = returnValueOfVar((info.group(3)),vars_dictionary)
	result['resultVar']   = (info.group(4))
	return result

#Check if all nescesarry vars are available
def allVarsAreAvailable(actions):
	if actions['action'] == 'AND'  :
		result = actions['lVar'] != -1  and actions['rVar'] != -1
	elif actions['action'] == 'OR':
		result = actions['lVar'] != -1  and actions['rVar'] != -1
	elif actions['action'] == 'RSHIFT':	
		result = actions['lVar'] != -1  and actions['rVar'] != -1
	elif actions['action'] == 'LSHIFT':
		result = actions['lVar'] != -1  and actions['rVar'] != -1
	elif actions['action'] == 'NOT':
		result = actions['rVar'] != -1
	else :
		result = actions['lVar'] != -1
	return  result

#Execute the actions of a line
def executeLine(actions):
	if actions['action'] == 'AND':
		result = actions['lVar'] & actions['rVar']
	elif actions['action'] == 'OR':
		result = actions['lVar'] | actions['rVar']
	elif actions['action'] == 'RSHIFT':	
		result = actions['lVar'] >> actions['rVar']
	elif actions['action'] == 'LSHIFT':
		result = actions['lVar']<<actions['rVar']
	elif actions['action'] == 'NOT':
		result = ~actions['rVar']
	else :
		result = actions['lVar']
	return  result& 65535 #limit result to 16 bits



#print vars_dictionary
vars_dictionary = {}

inputfile = open('input.txt','r')
linesToProcess =inputfile.readlines()

iteration = 0
while not 'a' in vars_dictionary:
	skippedLines = []
	for line in linesToProcess:
		actionsThisStep = getInformationFromLine(line,vars_dictionary)
		if allVarsAreAvailable(actionsThisStep):
			vars_dictionary[actionsThisStep['resultVar']]= executeLine(actionsThisStep)
		else:
			skippedLines.append(line)

	linesToProcess = skippedLines	

print "a after 1st iteration: "+str(vars_dictionary['a'])

a = vars_dictionary['a']

#Iteration 2: The value of A of the first iteration should be moved to b (and b will not change). 
# Then execute the same actions, as before
vars_dictionary = {}
inputfile = open('input.txt','r')
linesToProcess =inputfile.readlines()

iteration = 0
while not 'a' in vars_dictionary:
	skippedLines = []
	for line in linesToProcess:
		vars_dictionary['b']=a
		actionsThisStep = getInformationFromLine(line,vars_dictionary)
		if allVarsAreAvailable(actionsThisStep):
			vars_dictionary[actionsThisStep['resultVar']]= executeLine(actionsThisStep)
		else:
			skippedLines.append(line)

	linesToProcess = skippedLines	




print "a after 2nd iteration: "+str(vars_dictionary['a'])


#varslist = getInformationFromLine("a AND ab -> b",vars_dictionary)
#print allVarsAreAvailable(varslist)
#getInformationFromLine("2 AND d -> at")

