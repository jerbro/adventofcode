import pprint
import re

#Checks if txt is a number, if yes, return it, if no, return value of variable with that name (or 0 if var not exists)
def returnValueOfVar(txt,vars_dictionary):
	if unicode(txt).isnumeric():
		return int(txt)
	elif txt in vars_dictionary:
		return vars_dictionary[txt]
	else:
		return -1

def getInformationFromLine(txt,vars_dictionary):
	getInfo = re.compile("^([a-z0-9]*)[ ]?(AND|OR|RSHIFT|LSHIFT|NOT)?[ ]?([0-9a-z]*) -> ([a-z]*)$")
	info = getInfo.match(txt)
	result={}
	result['lVar'] = returnValueOfVar(info.group(1),vars_dictionary)
	result['action'] = (info.group(2))
	result['rVar'] = returnValueOfVar((info.group(3)),vars_dictionary)
	result['resultVar']   = (info.group(4))
	return result

def allVarsAreAvailable(actions):
	if actions['action'] == 'AND'  :
		result = actions['lVar'] != -1  and actions['rVar'] != -1
	elif actions['action'] == 'OR':
		result = actions['lVar'] != -1  and actions['rVar'] != -1
	elif actions['action'] == 'RSHIFT':	
		result = actions['lVar'] != -1  and actions['rVar'] != -1
	elif actions['action'] == 'LSHIFT':
		result = actions['lVar'] != -1  and actions['rVar'] != -1
	elif actions['action'] == 'NOT':
		result = actions['rVar'] != -1
	else :
		result = actions['lVar'] != -1
	return  result

def executeLine(actions):
	if actions['action'] == 'AND':
		result = and_action(actions['lVar'],actions['rVar'])
	elif actions['action'] == 'OR':
		result = or_action(actions['lVar'],actions['rVar'])
	elif actions['action'] == 'RSHIFT':	
		result = rshift_action(actions['lVar'],actions['rVar'])
	elif actions['action'] == 'LSHIFT':
		result = lshift_action(actions['lVar'],actions['rVar'])
	elif actions['action'] == 'NOT':
		result = not_action(actions['rVar'])
	else :
		result = actions['lVar']
	result = result & 65535
	return  result


def and_action(lval,rval):
	return lval&rval

def or_action(lval,rval):
	return lval|rval

def not_action(lval):
	return ~lval

def rshift_action(lval,rval):
	return lval >> rval

def lshift_action(lval,rval):
	return lval << rval



#print vars_dictionary

vars_dictionary = {}

inputfile = open('input.txt','r')
linesToProcess =inputfile.readlines()

iteration = 0
while not 'a' in vars_dictionary:
	skippedLines = []
	print "Iteration: " +str(iteration)
	


	for line in linesToProcess:
		actionsThisStep = getInformationFromLine(line,vars_dictionary)
		if allVarsAreAvailable(actionsThisStep):
			print "executed: "+line
			vars_dictionary[actionsThisStep['resultVar']]= executeLine(actionsThisStep)
		else:
			skippedLines.append(line)

	linesToProcess = skippedLines	


pprint.pprint(vars_dictionary)



#varslist = getInformationFromLine("a AND ab -> b",vars_dictionary)
#print allVarsAreAvailable(varslist)
#getInformationFromLine("2 AND d -> at")

