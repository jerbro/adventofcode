import copy

def parseInputfile(filename):
	fileContent = map(int,open(filename,'r').read().splitlines())
	return fileContent

presentWeightList = parseInputfile('input.txt')

print "List with present weights: " +str(presentWeightList)
print "Total weight of all presents: " + str(sum(presentWeightList))

weightPerGroup = sum(presentWeightList)/3
print "Weight per group: " +str(weightPerGroup)

def getAllPossibleGroupsWithShortestLengthWeight(presentsLeft,presentsInGroup,weightPerGroup,maxSize,depth):
	listOfAllPossibleGroups = []
	if sum(presentsInGroup)==weightPerGroup:
#		print presentsInGroup
		return ([presentsInGroup],min(maxSize,len(presentsInGroup)))
	if sum(presentsInGroup)> weightPerGroup:
		return ([],maxSize)
	if (sum(presentsLeft)+sum(presentsInGroup))<weightPerGroup:
		return ([],maxSize)
	if len(presentsInGroup)>= maxSize:
		return ([],maxSize)
	newMaxSize = maxSize
	newPresentsLeft = list(presentsLeft)
	for i in range(0,len(presentsLeft)):
		newPresentsInGroupList = list(presentsInGroup)
		newPresentsInGroupList.append(newPresentsLeft.pop())
		oldMaxSize = newMaxSize
		(newGroupsToAddToList,newMaxSize) = getAllPossibleGroupsWithShortestLengthWeight(newPresentsLeft,newPresentsInGroupList,weightPerGroup,newMaxSize,depth+1)

		if newGroupsToAddToList:
			if oldMaxSize > newMaxSize:
				listOfAllPossibleGroups = list(newGroupsToAddToList)
			else:
				listOfAllPossibleGroups = list(listOfAllPossibleGroups+newGroupsToAddToList)
	return (listOfAllPossibleGroups,newMaxSize)




(ListWithAllPossibleGroups,maxSize) = getAllPossibleGroupsWithShortestLengthWeight(presentWeightList,[],weightPerGroup,len(presentWeightList)/3,1)


print "There are %d valid groups with length %d" %(len(ListWithAllPossibleGroups),maxSize)

def prod(factors):
	result =1
	for x in factors:
		result *=x
	return result

listWithQUantumEntanglement = map(lambda x:prod(x),ListWithAllPossibleGroups)
listWithQUantumEntanglement.sort()
print "The group with minumum quantum entagnelement has quantumentanglement: %d" % (listWithQUantumEntanglement[0])


