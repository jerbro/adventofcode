import re

def getNextPassword(currPwd):
	varToInc = len(currPwd)-1
	incFinished = False
	result = ""
	for char in reversed(currPwd):
		if not incFinished and (ord(char)>= ord('z')):
			char = 'a'
		elif not incFinished:
			char = chr(ord(char)+1)
			incFinished=True
		result = char+result
	return result
	
def removeiolFromPwd(pwd):
	result = ""
	resetFollowingChars = False
	for char in pwd:
		if not resetFollowingChars:
			if char in ['i','l','o']:
				result = result + chr(ord(char)+1)
				resetFollowingChars = True
			else:
				result = result+char
		else:
			result = result +'a'
	return result




def check3IncreasingCharsValid(pwd):
	prevVal = 0
	consecutiveCount =1
	for char in pwd:
		if ord(char) == prevVal+1:
			consecutiveCount+=1
			if consecutiveCount >=3:
				return True
		else:
			consecutiveCount=1
		prevVal=ord(char)

	return False

def checkFor2PairsValid(pwd):
	rule = re.match("^.*(.)\\1.*(.)\\2.*$",pwd)
	if rule:
		if rule.group(1) != rule.group(2):
			return True
		else:
			return False
	else:
		return False



currPwd = "vzbxkghb"
print "  prev Pwd: "+currPwd
while not (check3IncreasingCharsValid(currPwd) and checkFor2PairsValid(currPwd)):
	currPwd = getNextPassword(currPwd)
	currPwd = removeiolFromPwd(currPwd)


print "   new Pwd: " + currPwd
currPwd = getNextPassword(currPwd)
currPwd = removeiolFromPwd(currPwd)

while not (check3IncreasingCharsValid(currPwd) and checkFor2PairsValid(currPwd)):
	currPwd = getNextPassword(currPwd)
	currPwd = removeiolFromPwd(currPwd)
print " newer Pwd: " + currPwd

