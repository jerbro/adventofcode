
inputfile = open('input.txt','r')
fileContent = inputfile.read()

currentFloor=0
positionCounter=0
foundFirstPositionOfReachingBasement=False

for character in fileContent:
	positionCounter +=1
	if character =='(':
		currentFloor +=1
	elif character ==')':
		currentFloor -=1

	if (currentFloor == -1) and foundFirstPositionOfReachingBasement==False:
		foundFirstPositionOfReachingBasement=True
		print "First time reached the basement at position: %d" % positionCounter
