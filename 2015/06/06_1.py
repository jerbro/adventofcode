import numpy
import re


def getInformationFromLine(txt):
	getInfo = re.compile("^([^0-9]*) ([0-9]*),([0-9]*) through ([0-9]*),([0-9]*)$")
	info = getInfo.match(txt)
	result={}
	result['action'] = info.group(1)
	result['xStart'] = int(info.group(2))
	result['yStart'] = int(info.group(3))
	result['xEnd']   = int(info.group(4))
	result['yEnd']   = int(info.group(5))
	return result

def changeMatrix(howToChange,currMatrix):
	changeMatrix = getMatrixWithCellsToChange(howToChange['xStart'],howToChange['xEnd'],howToChange['yStart'],howToChange['yEnd'],999,999)
	if howToChange['action']=='turn on':
		result = turnOn(currMatrix,changeMatrix)
	elif howToChange['action']=='turn off':
		result = turnOff(currMatrix,changeMatrix)
	elif howToChange['action']=='toggle':		
		result = toggle(currMatrix,changeMatrix)
	else:
		result =""
		print "Error, unknown action, %s" %(howToChange['action'])

	return result

def getMatrixWithCellsToChange(xStart,xEnd,yStart,yEnd,matrixHeight,matrixWidth):
	matrix=numpy.ones((xEnd-xStart+1,yEnd-yStart+1))
	matrix = numpy.lib.pad(matrix,((xStart,matrixWidth-xEnd),(yStart,matrixHeight-yEnd)),'constant', constant_values=((0,0),(0,0)))
	return matrix

def turnOn(currMatrix,changeMatrix):
	return numpy.logical_or(currMatrix,changeMatrix)
	
def turnOff(currMatrix,changeMatrix):
	return numpy.logical_and(currMatrix,numpy.logical_not(changeMatrix))

def toggle(currMatrix,changeMatrix):
	return numpy.logical_xor(currMatrix,changeMatrix)
	



inputfile = open('input.txt','r')
filecontent=inputfile.readlines()

ledMatrix = numpy.zeros((1000,1000))

for line in filecontent:
	howToChangeThisStep = getInformationFromLine(line)
	ledMatrix= changeMatrix(howToChangeThisStep,ledMatrix)
	



print "Number of switched-on lights after the instructions: %d" % numpy.count_nonzero(ledMatrix)
