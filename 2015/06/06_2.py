import numpy
import re


def getInformationFromLine(txt):
	getInfo = re.compile("^([^0-9]*) ([0-9]*),([0-9]*) through ([0-9]*),([0-9]*)$")
	info = getInfo.match(txt)
	result={}
	result['action'] = info.group(1)
	result['xStart'] = int(info.group(2))
	result['yStart'] = int(info.group(3))
	result['xEnd']   = int(info.group(4))
	result['yEnd']   = int(info.group(5))
	return result

def changeMatrix(changeInstructions,currMatrix):
	changeMatrix = getMatrixWithCellsToChange(changeInstructions['xStart'],changeInstructions['xEnd'],changeInstructions['yStart'],changeInstructions['yEnd'],999,999)
	if changeInstructions['action']=='turn on':
		result = turnOn(currMatrix,changeMatrix)
	elif changeInstructions['action']=='turn off':
		result = turnOff(currMatrix,changeMatrix)
	elif changeInstructions['action']=='toggle':		
		result = toggle(currMatrix,changeMatrix)
	else:
		result =""
		print "Error, unknown action, %s" %(changeInstructions['action'])
	result = numpy.clip(result,0,999999) 

	return result

#Make a matrix with the cells that need to be changed are 1, all other cells are 0
def getMatrixWithCellsToChange(xStart,xEnd,yStart,yEnd,matrixHeight,matrixWidth):
	matrix=numpy.ones((xEnd-xStart+1,yEnd-yStart+1))
	matrix = numpy.lib.pad(matrix,((xStart,matrixWidth-xEnd),(yStart,matrixHeight-yEnd)),'constant', constant_values=((0,0),(0,0)))
	return matrix

def turnOn(currMatrix,changeMatrix):
	return numpy.add(currMatrix,changeMatrix)
	
def turnOff(currMatrix,changeMatrix):
	return numpy.add(currMatrix,-1*changeMatrix)

def toggle(currMatrix,changeMatrix):
	return numpy.add(currMatrix,2*changeMatrix)
	

inputfile = open('input.txt','r')
filecontent=inputfile.readlines()

ledMatrix = numpy.zeros((1000,1000))

for line in filecontent:
	changeInstructions = getInformationFromLine(line)
	ledMatrix= changeMatrix(changeInstructions,ledMatrix)

print "Number of switched-on lights after the instructions: %d" % numpy.sum(ledMatrix)
