
import itertools
import re


def parseInputfile(filename):
	filecontent = open(filename,'r').read().splitlines()
	result = {}
	for line in filecontent:
		print line
		regex = re.match('^([A-Za-z]*): capacity ([-0-9]*), durability ([-0-9]*), flavor ([-0-9]*), texture ([-0-9]*), calories ([-0-9]*)$',line)	
		(name,capacity,durability,flavor,texture,calories) = regex.group(1,2,3,4,5,6)
		result[name]={}
		result[name]['capacity'] = int(capacity)
		result[name]['durability'] = int(durability)
		result[name]['flavor'] = int(flavor)
		result[name]['texture'] = int(texture)
		result[name]['calories'] = int(calories)
	return result


ingredientProperties = parseInputfile('input.txt')
ingredientNames = ingredientProperties.keys()


ingredientCombinations = itertools.combinations_with_replacement(ingredientProperties.keys(),100)

def calcScoreFromIngredientCombi(combi,ingredientProperties):

	first = True
	capacity = 0
	durability = 0
	flavor = 0
	texture = 0
	calories = 0
	for ingredientName in ingredientProperties.keys():
		ingredientCount = combi.count(ingredientName)
		capacity += ingredientProperties[ingredientName]['capacity']*ingredientCount
		durability += ingredientProperties[ingredientName]['durability']*ingredientCount
		flavor += ingredientProperties[ingredientName]['flavor']*ingredientCount
		texture += ingredientProperties[ingredientName]['texture']*ingredientCount
		calories += ingredientProperties[ingredientName]['calories']*ingredientCount

	if capacity >0 and durability > 0 and flavor >0 and texture >0:
		return (capacity*durability*flavor*texture,calories)
	else:
		return (0,0)


maxScore = 0
for combi in ingredientCombinations:
	(score,calories) = calcScoreFromIngredientCombi(combi, ingredientProperties)
	if score>maxScore and calories == 500:
		maxScore = score
		print "current max score with 500 calories (finding max in progress): "+str(maxScore)+" has: "+str(calories)+" calories"

print "Final max scorewith 500 calories : "+str(maxScore)
