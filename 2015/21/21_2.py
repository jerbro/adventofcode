import collections

weapons ={	'dagger'	:{'cost':  8, 'damage':4,'armor':0},
			'Shortsword':{'cost': 10, 'damage':5,'armor':0},
			'Warhammer' :{'cost': 25, 'damage':6,'armor':0},
			'Longsword' :{'cost': 40, 'damage':7,'armor':0},
			'Greataxe'  :{'cost': 74, 'damage':8,'armor':0}}

armors ={	'Leather'	:{'cost': 13, 'damage':0,'armor':1},
			'Chainmail' :{'cost': 31, 'damage':0,'armor':2},
			'Splintmail':{'cost': 53, 'damage':0,'armor':3},
			'Bandedmail':{'cost': 75, 'damage':0,'armor':4},
			'Platemail' :{'cost':102, 'damage':0,'armor':5},
			'noArmor'   :{'cost':  0, 'damage':0,'armor':0}}

rings = {	'Damage1'	:{'cost': 25, 'damage':1,'armor':0},
			'Damage2'   :{'cost': 50, 'damage':2,'armor':0},
			'Damage3'   :{'cost':100, 'damage':3,'armor':0},
			'Defense1'  :{'cost': 20, 'damage':0,'armor':1},
			'Defense2'  :{'cost': 40, 'damage':0,'armor':2},
			'Defense3'  :{'cost': 80, 'damage':0,'armor':3},
			'NoRing'    :{'cost':  0, 'damage':0,'armor':0}}


def getAllWeaponArmorRingsCombisOrdered():
	combi={}
	for weapon in weapons:
		for armor in armors:
			for ring1 in rings:
				for ring2 in rings:
					if ring1 != ring2:
						combi[weapon+"_"+armor+"_"+ring1+"_"+ring2]={}
						for prop in weapons[weapon]:
							combi[weapon+"_"+armor+"_"+ring1+"_"+ring2][prop]=weapons[weapon][prop]+armors[armor][prop]+rings[ring1][prop]+rings[ring2][prop]

	return collections.OrderedDict(sorted(combi.items(), key = lambda x: x[1]['cost']))


def readFileWithBossProperties(filename):
	fileContent = open(filename,'r').read().splitlines()
	bossProperties = {}
	for line in fileContent:
		(key,value) = line.split(':')
		bossProperties[key]=int(value)
	return bossProperties


def youWinFromBoss(youProperties,bossProperties):
	youHitpoints = youProperties['Hit Points']
	bossHitpoints = bossProperties['Hit Points']
	damageToBossPerAttack = youProperties['Damage']-bossProperties['Armor']
	damageToYouPerAttack = bossProperties['Damage']- youProperties['Armor']
	while True:
		bossHitpoints -=max(damageToBossPerAttack,1)
		youHitpoints -= max(damageToYouPerAttack,1)
		if bossHitpoints <=0:
			return True
		if youHitpoints <= 0:
			return False



bossProperties =  readFileWithBossProperties('input.txt')

combi = getAllWeaponArmorRingsCombisOrdered()
for item in reversed(combi):
	youProperties ={}
	youProperties['Hit Points']=100
	youProperties['Armor'] = combi[item]['armor']
	youProperties['Damage'] = combi[item]['damage']
	if not (youWinFromBoss(youProperties,bossProperties)):
		print item+" : "+str(combi[item])
		print "Highest cost to still lose: "+str(combi[item]['cost'])
		break
