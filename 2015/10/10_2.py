import cStringIO

def calcNextLookAndSay(currText):
	prevChar=""
	charCnt = 0
	result = cStringIO.StringIO()
	for char in currText:
		if prevChar !="":
			if prevChar ==char:
				charCnt +=1
			else:
				result.write(str(charCnt)+prevChar)
				prevChar = char
				charCnt = 1	

		else:
			prevChar = char
			charCnt = 1
		
	return result



txt = "1113222113/"
print str(0) + ": "+ "Length: "+str(len(txt)-1)

for i in range(1,51):
	txt = calcNextLookAndSay(txt).getvalue()+ "/"
	print str(i) + ": "+ "Length: "+str(len(txt)-1)
	
