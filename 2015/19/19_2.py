import time

def parseInputfile(filename):
	filecontent = open(filename,'r').read().splitlines()
	conversionDict = {}
	for line in filecontent:
		if " => " in line:
			(fromElem,toElem)=line.split(' => ')
			if not toElem in conversionDict:
				conversionDict[toElem]=[]
			conversionDict[toElem].append(fromElem)
		else:
			elem = line
	return (conversionDict,elem)






def convertElementStringToList(startElement):
	elems = []
	for i in range(0,len(startElement)-1):
		if startElement[i].isupper() and startElement[i+1].isupper():
			elems.append(startElement[i])
		elif startElement[i].isupper() and startElement[i+1].islower():
			elems.append(startElement[i]+startElement[i+1])

	if startElement[len(startElement)-1].isupper() or startElement[len(startElement)-1]=='e' :
		elems.append(startElement[len(startElement)-1])
	return elems


def createListWithNewElements(elemList,conversionsDict,newListOfAllElems):
	
	result = newListOfAllElems
	for i in range(len(elemList)):
		if elemList[i] in conversionDict:
			origElem=elemList[i]
	 		for conv in conversionDict[elemList[i]]:
	 			elemList[i]=conv
	 			newElem = ''.join(elemList)
	 			result[newElem]=1
	 		elemList[i]=origElem
	return result


#print endElement


def GetAllNewPossibleString(conversionDict, currElement):
	newStrsPossible = []
	for elem in conversionDict:
		for newElem in conversionDict[elem]:
			startnr=0
			while currElement.find(elem,startnr) !=-1:
				startnr = currElement.find(elem,startnr)+1
				newStrsPossible.append(currElement[:startnr-1]+newElem+currElement[startnr+len(elem)-1:])
	return newStrsPossible

def findIfeIsInList(stringlist):
	for txt in stringlist:
		if txt== "e":
			return True
	return False


def leaveOnlyShortestStringInList(list):
	lenList = map(lambda x:len(x),list)
	minLen = min(lenList)
	newList = filter(lambda x:len(x)==minLen,list)
	return newList


(conversionDict,endElement)= parseInputfile('input.txt')

#print conversionDict



step =0
elemlist=[]
elemlist.append(endElement)
while not findIfeIsInList(elemlist):
 	step +=1
 	newList = []
 	for currElement in elemlist:
 		newList += GetAllNewPossibleString(conversionDict,currElement)
 	elemlist = set(newList)
 	elemlist = leaveOnlyShortestStringInList(elemlist)#To prevent out of memory problems, only try to go further with the shortest solutions. (probably not the correct answer for all sets)
 	print "Step: " +str(step)+" Size of new list: " + str(len(elemlist))+" Size of shortest element in list: "+str(len(elemlist[0]))
 	if len(elemlist)>100: #With much results, there are memory errors, so throw away all (but 1) of the possible elements, and try further(probably not the correct answer for all sets)
 		elemlist = [elemlist[0]]

print step
