
import collections

def parseInputfile(filename):
	filecontent = open(filename,'r').read().splitlines()
	conversionDict = {}
	for line in filecontent:
		if " => " in line:
			(fromElem,toElem)=line.split(' => ')
			if not fromElem in conversionDict:
				conversionDict[fromElem]=[]
			conversionDict[fromElem].append(toElem)
		else:
			elem = line
	return (conversionDict,elem)






def convertElementStringToList(startElement):

	elems = []
	for i in range(0,len(startElement)-1):
		if startElement[i].isupper() and startElement[i+1].isupper():
			elems.append(startElement[i])
		elif startElement[i].isupper() and startElement[i+1].islower():
			elems.append(startElement[i]+startElement[i+1])

	if startElement[len(startElement)-1].isupper():
		elems.append(startElement[len(startElement)-1])
	return elems


def createListWithNewElements(elemList,conversionsDict):
	listOfNewElements=collections.Counter()

	for i in range(len(elemList)):
		if elemList[i] in conversionDict:
			origElem=elemList[i]
	 		for conv in conversionDict[elemList[i]]:
	 			elemList[i]=conv
	 			newElem = ''.join(elemList)
	 			listOfNewElements[newElem]+=1
	 		elemList[i]=origElem
	return listOfNewElements


(conversionDict,startElement)= parseInputfile('input.txt')
elems = convertElementStringToList(startElement)
listOfNewElements= createListWithNewElements(elems,conversionDict)



print len(listOfNewElements)
 			#print ''.join(elems)




