import re

fileContent = open('input.txt','r').read().splitlines()

#make a dictionary with the disctances between all places
distancesDict = {}
for line in fileContent :
	rule = re.match('([A-Za-z]*) to ([A-Za-z]*) = ([0-9]*)',line )
	if not rule.group(1) in distancesDict:
		distancesDict[rule.group(1)]={}
	if not rule.group(2) in distancesDict:
		distancesDict[rule.group(2)]={}
	distancesDict[rule.group(1)][rule.group(2)]=int(rule.group(3))
	distancesDict[rule.group(2)][rule.group(1)]=int(rule.group(3))


#make a list with all the places
placesList = []
for key in distancesDict:
	placesList.append(key)




def recursiveLongestDistFinder(placesList,route,lastPlace,distTillNow,distancesDict,longestDist):
	for place in placesList:
		if len(placesList) > 1:
			newRoute = ""
			newDistTillNow = 0
			tempPlaceList = list(placesList)


			if lastPlace != "":
				newDistTillNow = distTillNow+ distancesDict[lastPlace][place]
				newRoute = route + newRoute + str(distancesDict[lastPlace][place]) + " - "+ place+" - "
			else:
				newRoute = route + place+" - "


			tempPlaceList.remove(place)
			longestDist = recursiveLongestDistFinder(tempPlaceList,newRoute,place,newDistTillNow,distancesDict,longestDist)
		else:
			newDistTillNow = distTillNow+ distancesDict[lastPlace][place]
			if longestDist < newDistTillNow:
				longestDist = newDistTillNow
				print route + str(distancesDict[lastPlace][place]) + " - "+place + "("+str(newDistTillNow)+")"
	return longestDist

print "Longest distance: " + str(recursiveLongestDistFinder(placesList,"","",0,distancesDict,0))



