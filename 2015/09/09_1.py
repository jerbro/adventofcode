import re

fileContent = open('input.txt','r').read().splitlines()

distancesDict = {}
for line in fileContent :
	
	rule = re.match('([A-Za-z]*) to ([A-Za-z]*) = ([0-9]*)',line )
	cityFrom , cityTo, distance = rule.group(1,2,3)

	if not cityFrom in distancesDict:
		distancesDict[cityFrom]={}

	if not cityTo in distancesDict:
		distancesDict[cityTo]={}

	distancesDict[cityFrom][cityTo]=int(distance)
	distancesDict[cityTo][cityFrom]=int(distance)

placesList = []
for key in distancesDict:
	placesList.append(key)




def recursiveShortestDistFinder(placesList,route,prevPlace,distTillNow,distancesDict,shortestDist):
	for place in placesList:
		if len(placesList) > 1:
			tempRoute = ""
			tempDistTillNow = 0
			tempPlaceList = list(placesList)

			if prevPlace != "":
				tempDistTillNow = distTillNow+ distancesDict[prevPlace][place]
				tempRoute = route + tempRoute + str(distancesDict[prevPlace][place]) + " - "+ place+" - "
			else:
				tempRoute = route + place+" - "

			tempPlaceList.remove(place)
			shortestDist = recursiveShortestDistFinder(tempPlaceList,tempRoute,place,tempDistTillNow,distancesDict,shortestDist)
		else:
			tempDistTillNow = distTillNow+ distancesDict[prevPlace][place]
			if shortestDist > tempDistTillNow:
				shortestDist = tempDistTillNow
				print route + str(distancesDict[prevPlace][place]) + " - "+place + "("+str(distTillNow)+")"
	return shortestDist

print "Shortest distance: " + str(recursiveShortestDistFinder(placesList,"","",0,distancesDict,999999))



