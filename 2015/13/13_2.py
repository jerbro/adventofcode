import re
import copy
import itertools

filecontent = open('input.txt','r').read().splitlines()


def fillHapinessDict(filecontent):
	happinessDict = {}
	for line in filecontent:

		result = re.match('^([A-Za-z]*) .* (gain|lose) ([0-9]*) .* ([A-Za-z]*).$',line)
		(personName,gainOrLose,hapiness,personNextTo) = result.group(1,2,3,4)

		if not personName in happinessDict:
			happinessDict[personName]={}

		if gainOrLose == 'gain':
			happinessDict[personName][personNextTo]=int(hapiness)
		else:
			happinessDict[personName][personNextTo]=-1*int(hapiness)

	return happinessDict


def makeListWithPeopleFromHappynessdict(happinessDict):
	peopleList = []
	for name in happinessDict:
		peopleList.append(name)
	return peopleList


def calcHapiness(peopleOrder,happynessDict):
	peopleInList = len(peopleOrder)
	totalHapiness=0
	for i in range(peopleInList):
		totalHapiness += happynessDict[peopleOrder[i]][peopleOrder[(i+1)%peopleInList]]
		totalHapiness += happynessDict[peopleOrder[i]][peopleOrder[(i-1)%peopleInList]]
	return totalHapiness

def addYouToHapinessDict(happinessDict):
	youHapiness = {}
	for person in happinessDict:
		happinessDict[person]['you']=0
		youHapiness[person]=0
	happinessDict['you'] = youHapiness
	return happinessDict



happinessDict = fillHapinessDict(filecontent)
happinessDict = addYouToHapinessDict(happinessDict)
peopleList = makeListWithPeopleFromHappynessdict(happinessDict)

#print combinationsList
print "List of happiest configurations found till now:"
happiestConfig = 0
for permutation in itertools.permutations(peopleList):
	if (calcHapiness(permutation,happinessDict)> happiestConfig):
		happiestConfig = calcHapiness(permutation,happinessDict)
		print str(permutation)+" : "+str(happiestConfig)


