import re
import copy
import itertools

filecontent = open('input.txt','r').read().splitlines()

def fillHapinessDict(filecontent):
	happinessDict = {}
	for line in filecontent:

		result = re.match('^([A-Za-z]*) .* (gain|lose) ([0-9]*) .* ([A-Za-z]*).$',line)
		(personName,gainOrLose,hapiness,personNextTo) = result.group(1,2,3,4)

		if not personName in happinessDict:
			happinessDict[personName]={}

		if gainOrLose == 'gain':
			happinessDict[personName][personNextTo]=int(hapiness)
		else:
			happinessDict[personName][personNextTo]=-1*int(hapiness)
		
	return happinessDict


def makeListWithPeopleFromHappynessdict(happinessDict):
	peopleList = []
	for name in happinessDict:
		peopleList.append(name)
	return peopleList


def calcHappiness(peopleOrder,happinessDict):
	peopleInList = len(peopleOrder)
	totalHappiness=0
	for i in range(peopleInList):
		totalHappiness += happinessDict[peopleOrder[i]][peopleOrder[(i+1)%peopleInList]]
		totalHappiness += happinessDict[peopleOrder[i]][peopleOrder[(i-1)%peopleInList]]
	return totalHappiness



happinessDict = fillHapinessDict(filecontent)
peopleList = makeListWithPeopleFromHappynessdict(happinessDict)

print "List of happiest configurations found :"
happiestConfig = 0
for permutations in itertools.permutations(peopleList):
	if (calcHappiness(permutations,happinessDict)> happiestConfig):
		happiestConfig = calcHappiness(permutations,happinessDict)
		print str(permutations)+" : "+str(happiestConfig)


	
