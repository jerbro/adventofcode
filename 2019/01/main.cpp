#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>


std::vector<long> readFile(const std::string &filename);

long calcTotalFuelRequiredForModuleMass(const std::vector<long> &masses);

long calcTotalFuelRequiredForModuleLaunch(const std::vector<long> &masses);

int main(int argc, char *argv[]) {
    auto numbersList = readFile("input.txt");
    auto answerA = calcTotalFuelRequiredForModuleMass(numbersList);
    auto answerB = calcTotalFuelRequiredForModuleLaunch(numbersList);
    std::cout << "Advent of code 2019, day 01" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}

long fuelRequiredForMass(long mass) {
    return (mass / 3) - 2;
}

long calcFuelRequiredForModuleLaunch(long mass) {
    long result = 0;
    long fuelNeededThisIteration = 0;
    long currMassToCalcNext = mass;
    do {
        fuelNeededThisIteration = fuelRequiredForMass(currMassToCalcNext);
        if (fuelNeededThisIteration > 0)
            result += fuelNeededThisIteration;
        currMassToCalcNext = fuelNeededThisIteration;//Next iteration, use mass of fuel to calc extra needed fuel.
    } while (fuelNeededThisIteration > 0);
    return result;
}

long calcTotalFuelRequiredForModuleLaunch(const std::vector<long> &masses) {
    long result = 0;
    for (long moduleMass:masses) {
        result += calcFuelRequiredForModuleLaunch(moduleMass);
    }
    return result;
}

long calcTotalFuelRequiredForModuleMass(const std::vector<long> &masses) {
    long result = 0;
    for (long moduleMass:masses) {
        result += fuelRequiredForMass(moduleMass);
    }
    return result;
}


std::vector<long> readFile(const std::string &filename) {
    std::vector<long> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::stringstream strStr;


    long currNr=0;
    while (fs >> currNr) {
        result.push_back(currNr);
    }
    fs.close();
    return result;
}
