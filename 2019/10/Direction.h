//
// Created by jeroen on 12-12-19.
//

#ifndef INC_2019_DIRECTION_H
#define INC_2019_DIRECTION_H

#include "Coord.h"


class Direction {
public:
    int stepsX;
    int stepsY;

    void normalize();

    Direction(Coord start, Coord end);

    Direction(int stepsX, int stepsY);

    double getAngle() const;

private:
    int getGcd(int a, int b);

};

bool operator==(const Direction &d1, const Direction &d2);

bool operator<(const Direction &d1, const Direction &d2);


#endif //INC_2019_DIRECTION_H
