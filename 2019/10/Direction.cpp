//
// Created by jeroen on 12-12-19.
//

#include <iostream>
#include "Direction.h"
#include <math.h>

bool operator< (const Direction& d1, const Direction& d2) {
    return ((d1.stepsY<d2.stepsY) || (d1.stepsY == d2.stepsY && d1.stepsX < d2.stepsX));
}

bool operator== (const Direction& d1, const Direction& d2) {
    return ((d1.stepsY==d2.stepsY && d1.stepsX == d2.stepsX));
}

void Direction::normalize() {
    auto gcd = getGcd(stepsX,stepsY);
    stepsX=stepsX/gcd;
    stepsY=stepsY/gcd;

}

//get greatest common divider (grootste gemene deler)
int Direction::getGcd(int a, int b) {
    if (b==0)
        return abs(a);
    else
        return getGcd(b, a % b);

}

Direction::Direction(Coord start, Coord end) {
    stepsX = end.x-start.x;
    stepsY = start.y-end.y;
    normalize();
}

Direction::Direction(int stepsX, int stepsY) {
    this->stepsX = stepsX;
    this->stepsY = stepsY;
    normalize();
}

double Direction::getAngle() const {
        auto angle= (atan2(stepsY,stepsX));
        angle -= M_PI/2;//angle now has 0 degree when pointing above.
        angle /= M_PI;//from radian to degree
        angle *= -180;//from radian to degree
        if (angle <0){
            angle +=360;
        }
        return angle;
}


