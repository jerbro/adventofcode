#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include "Coord.h"
#include "Direction.h"
#include "math.h"

namespace day10 {


    std::vector<Coord> readFile(const std::string &filename);

    int getNumberOfAstroidsSeenFrom(const std::vector<Coord> &astroids, Coord startpos);

    std::pair<int, Coord> getLargestNumberOfAstroidsSeenFromAnyAstroid(const std::vector<Coord> &astroids);

    Coord get200thDestroyedAstroidCoordinates(const std::vector<Coord> &astroids, const Coord center);

    int main() {

        double getAngle(const Direction &dir);
        auto astroids = readFile("input.txt");

        auto answerA = getLargestNumberOfAstroidsSeenFromAnyAstroid(astroids);
        auto answerB = get200thDestroyedAstroidCoordinates(astroids, answerA.second);
        std::cout << "Advent of code 2019, day 10" << std::endl;
        std::cout << "A: " << answerA.first << std::endl;
        std::cout << "B: " << answerB.x * 100 + answerB.y << std::endl;

        return 0;
    }


    int getManhattanDistance(const Coord &c1, const Coord &c2) {
        return abs(c1.x - c2.x) + abs(c1.y - c2.y);
    }

    Coord getFirstInCertainDirection(const std::vector<Coord> &astroids, Direction dir, Coord center) {
        auto distanceClosestAstroid = 99999999;
        Coord closestAstroid;
        for (auto astroid:astroids) {
            if (!(astroid == center) && Direction(center, astroid) == dir) {
                auto manDist = getManhattanDistance(center, astroid);
                if (manDist < distanceClosestAstroid) {
                    distanceClosestAstroid = manDist;
                    closestAstroid = astroid;
                }
            }
        }
        return closestAstroid;
    }

    std::set<Direction> getDirectionsSeenFrom(const std::vector<Coord> &astroids, Coord startpos) {
        std::set<Direction> dirs;
        for (auto astroid:astroids) {
            if (!(astroid == startpos)) {
                Direction dir(startpos, astroid);
                dirs.insert(Direction(startpos, astroid));
            }
        }
        return dirs;
    }


    Coord get200thDestroyedAstroidCoordinates(const std::vector<Coord> &astroids, const Coord center) {
        auto dirs = getDirectionsSeenFrom(astroids, center);
        std::vector<Direction> dirsVect(dirs.begin(), dirs.end());
        std::sort(dirsVect.begin(), dirsVect.end(),
                  [](const Direction &d1, const Direction &d2) { return d1.getAngle() < d2.getAngle(); });
        std::vector<Coord> destroyedStars;
        for (auto dir:dirsVect) {
            destroyedStars.push_back(getFirstInCertainDirection(astroids, dir, center));
        }
        return getFirstInCertainDirection(astroids, dirsVect[199], center);
    }


    std::pair<int, Coord> getLargestNumberOfAstroidsSeenFromAnyAstroid(const std::vector<Coord> &astroids) {
        int result = 0;
        Coord resultCoord;
        for (auto astroid:astroids) {
            auto seenFromThis = getNumberOfAstroidsSeenFrom(astroids, astroid);
            if (seenFromThis > result) {
                result = seenFromThis;
                resultCoord = astroid;
            }
        }
        return std::make_pair(result, resultCoord);
    }


    int getNumberOfAstroidsSeenFrom(const std::vector<Coord> &astroids, Coord startpos) {

        return getDirectionsSeenFrom(astroids, startpos).size();
    }

    std::vector<Coord> readFile(const std::string &filename) {

        std::fstream fs;
        fs.open(filename, std::fstream::openmode::_S_in);


        std::string str;

        std::vector<Coord> result;
        auto ypos = 0;
        while (std::getline(fs, str)) {

            for (auto xpos = 0; xpos < str.length(); xpos++) {
                if (str.at(xpos) == '#') {
                    result.push_back(Coord{xpos, ypos});
                }
            }
            ypos++;
        }
        fs.close();
        return result;
    }
}

int main(int argc, char *argv[]) {
    day10::main();
}