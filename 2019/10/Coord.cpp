//
// Created by Jeroen on 03-12-19.
//

#include "Coord.h"

bool operator<(const Coord &c1, const Coord &c2) {
    return ((c1.y < c2.y) || (c1.y == c2.y && c1.x < c2.x));
}

bool operator==(const Coord &c1, const Coord &c2) {
    return ((c1.y == c2.y && c1.x == c2.x));
}
