#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <climits>
#include <map>
#include "InOutBuffer.h"
#include "intCodeComputer.h"
#include <boost/multiprecision/cpp_int.hpp>



namespace day09{
std::map<boost::multiprecision::cpp_int,boost::multiprecision::cpp_int> readFile(const std::string &filename);


int main() {

    auto numbersList = readFile("input.txt");

    InOutBuffer inBuf,outBuf;


    inBuf.push_back(1);
    intCodeComputer comp(numbersList,&inBuf,&outBuf);
    comp.execute();
    auto answerA = outBuf.getMostRecentAddedValue();

    comp.resetProgram();
    inBuf.push_back(2);
    comp.execute();
    auto answerB = outBuf.getMostRecentAddedValue();

    std::cout << "Advent of code 2019, day 09" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}



std::map<boost::multiprecision::cpp_int,boost::multiprecision::cpp_int> readFile(const std::string &filename) {
    std::map<boost::multiprecision::cpp_int,boost::multiprecision::cpp_int> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string nr;
    long position=0;
    while (getline(fs, nr, ',')) {
        result.insert(std::make_pair(position,strtol(nr.c_str(), nullptr, 10)));
        position ++;
    }
    fs.close();
    return result;
}

}

int main(int argc, char *argv[]){
    return day09::main();
}