//
// Created by Jeroen on 02-12-19.
//

#ifndef INC_2019_INTCODECOMPUTER_H
#define INC_2019_INTCODECOMPUTER_H


#include <vector>
#include <map>
#include "Instruction.h"
#include <boost/multiprecision/cpp_int.hpp>

namespace day09 {
    class intCodeComputer {
    public:
        enum exitCode {
            normalEnd = 0, invalidInstruction = 1
        };

        explicit intCodeComputer(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> program, InOutBuffer *inBuffer, InOutBuffer *outBuffer);

        void resetProgram();

        ~intCodeComputer();

        exitCode execute();

        void replacePosWithVal(long position, long value);

        boost::multiprecision::cpp_int getValueAtPosition(boost::multiprecision::cpp_int position);

        void addInputValue(long val){inBuffer->push_back(val);}
        boost::multiprecision::cpp_int getAddedOutputtedValue(){return outBuffer->getMostRecentAddedValue();}
        void printProgram() ;
    private:
        std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> program;
        std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> initialProgram;
        InOutBuffer *inBuffer;
        InOutBuffer *outBuffer;
        boost::multiprecision::cpp_int relBase;

        std::map<int,Instruction*> supportedOpcodes;
    };
}

#endif //INC_2018_INTCODECOMPUTER_H
