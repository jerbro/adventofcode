//
// Created by Jeroen on 02-12-19.
//

#include <iostream>
#include "intCodeComputer.h"


namespace day09 {
    intCodeComputer::exitCode intCodeComputer::execute() {
        boost::multiprecision::cpp_int pc = 0;
        bool keepRunning = true;

        while (keepRunning){

            if (program.find(pc) == program.end()) {
                std::cout << " Error. pc larger than program\n";
                return intCodeComputer::exitCode::invalidInstruction;
            }

            auto instr = program.find(pc)->second;
            int opcode = static_cast<int>(instr % 100);

            auto currOpcode = supportedOpcodes.find(opcode);
            if (currOpcode != supportedOpcodes.end()) {
                keepRunning = (*currOpcode->second).execute(program, pc, relBase);
            }
            else {
                keepRunning = false;
                std::cout << " Error. Invalid opcode: "<<opcode<<" \n";
            }
        }
        return intCodeComputer::exitCode::normalEnd;
    }

    void intCodeComputer::replacePosWithVal(long position, long value) {
        program.at(static_cast<unsigned long>(position)) = value;
    }

    boost::multiprecision::cpp_int intCodeComputer::getValueAtPosition(boost::multiprecision::cpp_int position) {
        return program.at(static_cast<unsigned long>(position));
    }

    void intCodeComputer::resetProgram() {
        program = initialProgram;
        relBase = 0;
    }

    intCodeComputer::intCodeComputer(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> program, InOutBuffer *inBuffer, InOutBuffer *outBuffer)
            : initialProgram(std::move(program)),inBuffer(inBuffer),outBuffer(outBuffer),relBase(0){
        resetProgram();
        std::vector<Instruction*> instructions;

        instructions.push_back(new AddCommand());
        instructions.push_back(new MulCommand());
        instructions.push_back(new ReadInputCommand(inBuffer));
        instructions.push_back(new WriteOutputCommand(outBuffer));
        instructions.push_back(new HaltCommand());
        instructions.push_back(new jumpIfTrueCommand());
        instructions.push_back(new jumpIfFalseCommand());
        instructions.push_back(new lessThanCommand());
        instructions.push_back(new equalsCommand());
        instructions.push_back(new setBaseCommand());

        for (Instruction* instr:instructions){
            supportedOpcodes.insert(std::make_pair(instr->getOpcode(), instr));
        }
    }

    intCodeComputer::~intCodeComputer() {
        for (auto opcode:supportedOpcodes){
            delete opcode.second;
        }

    }

    void intCodeComputer::printProgram() {
        for (auto elem:program){
            std::cout << std::setw(4)<<elem.first<<": "<<std::setw(8)<<elem.second<<std::endl;
        }

    }

}