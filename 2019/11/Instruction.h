//
// Created by jeroen on 05-12-19.
//

#ifndef INC_2019_INSTRUCTION_H
#define INC_2019_INSTRUCTION_H


#include <vector>
#include <deque>
#include <map>
#include "InOutBuffer.h"
#include <boost/multiprecision/cpp_int.hpp>

namespace day11 {
    enum class opcodeMode {
        position = 0,
        immidiate = 1,
        relative = 2
    };


    class Instruction {

    public:
        virtual bool execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) = 0;

        virtual int getOpcode() = 0;
        virtual ~Instruction(){};


    protected:
        static boost::multiprecision::cpp_int getValue(const std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int position, const opcodeMode mode, boost::multiprecision::cpp_int relBase);
        static boost::multiprecision::cpp_int getValueAtPosition(const std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int position);
        static void setValueAtPosition(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int position, boost::multiprecision::cpp_int value, const opcodeMode mode, boost::multiprecision::cpp_int relBase);


        static std::vector<opcodeMode> getOpCodeModes(boost::multiprecision::cpp_int opCode, int nrOfModesRequested);

    };

    class AddCommand : public Instruction {
    public:

        bool execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) override;

        int getOpcode() override { return 1; }
    };

    class MulCommand : public Instruction {
    public:

        bool execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) override;

        int getOpcode() override { return 2; }
    };

    class ReadInputCommand : public Instruction {
    public:
        explicit ReadInputCommand(InOutBuffer *inp) : inputBuffer(inp) {}

        bool execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) override;

        int getOpcode() override { return 3; }

    private:
        InOutBuffer* inputBuffer;
    };

    class WriteOutputCommand : public Instruction {
    public:
        explicit WriteOutputCommand(InOutBuffer *outp) : outputBuffer(outp) {}

        bool execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) override;

        int getOpcode() override { return 4; }

    private:
        InOutBuffer* outputBuffer;
    };

    class HaltCommand : public Instruction {
        bool execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) override;

        int getOpcode() override { return 99; }
    };

    class jumpIfTrueCommand : public Instruction {
        bool execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) override;

        int getOpcode() override { return 5; }
    };

    class jumpIfFalseCommand : public Instruction {
        bool execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) override;

        int getOpcode() override { return 6; }
    };

    class lessThanCommand : public Instruction {
        bool execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) override;

        int getOpcode() override { return 7; }
    };

    class equalsCommand : public Instruction {
        bool execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) override;

        int getOpcode() override { return 8; }
    };

    class setBaseCommand : public Instruction {
        bool execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) override;

        int getOpcode() override { return 9; }
    };

#endif //INC_2019_INSTRUCTION_H
}