cmake_minimum_required(VERSION 3.8)
project(2019_11)


set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp intCodeComputer.cpp Instruction.cpp InOutBuffer.cpp PainterRobot.cpp Coord.cpp)
add_executable(2019_11 ${SOURCE_FILES})
target_link_libraries(2019_11 pthread)
