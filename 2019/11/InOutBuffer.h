//
// Created by jeroen on 07-12-19.
//

#ifndef INC_2019_INOUTBUFFER_H
#define INC_2019_INOUTBUFFER_H


#include <deque>
#include <mutex>
#include <boost/multiprecision/cpp_int.hpp>

namespace day11 {
    class InOutBuffer {
    public:
        InOutBuffer();

        void push_back(boost::multiprecision::cpp_int);

        boost::multiprecision::cpp_int getFrontAndPopFront();
        boost::multiprecision::cpp_int getFrontAndPopFrontNoDataPossible(bool& nodata);

        boost::multiprecision::cpp_int getMostRecentAddedValue();

        void noNewDataWillArive();

    private:
        std::deque<boost::multiprecision::cpp_int> buffer;
        boost::multiprecision::cpp_int mostRecentAddedValue;
        std::mutex mtx;
        volatile bool lastDataIsReceived;
    };

}
#endif //INC_2019_INOUTBUFFER_H
