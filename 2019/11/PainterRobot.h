//
// Created by jeroen on 12-12-19.
//

#ifndef INC_2019_PAINTERROBOT_H
#define INC_2019_PAINTERROBOT_H
#include <map>
#include <boost/multiprecision/cpp_int.hpp>
#include "InOutBuffer.h"

#include "Coord.h"

namespace day11 {

    class PainterRobot {

    public:
        enum class color {
            black = 0, white = 1
        };
        enum class moveDir{
            left=0,right=1
        };

        enum class facingDir{
            north=0, east=1, south=2,west=3
        };

        PainterRobot(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> program);

        void paintAllCells();

        long getNumberOfPaintedCells();

        void reset();

        void paint(color clr);
        void printDrawing();

    private:
        std::map<Coord, color> paintedCells;
        std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> program;
        void activateRobot(InOutBuffer* inbuf, InOutBuffer* outbuf);
        Coord currPos {0,0};

        facingDir currentDirection=facingDir::north;
        color getCellColor();
        void move(moveDir direction);


    };

}
#endif //INC_2019_PAINTERROBOT_H
