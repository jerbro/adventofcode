//
// Created by jeroen on 12-12-19.
//

#include <thread>
#include "PainterRobot.h"
#include "intCodeComputer.h"
#include "InOutBuffer.h"

namespace day11 {

    PainterRobot::PainterRobot(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> program):program(program) {

    }

    long PainterRobot::getNumberOfPaintedCells() {
        return paintedCells.size();
    }

    void PainterRobot::reset(){
        currPos = Coord{0,0};
        paintedCells.clear();
        currentDirection=facingDir::north;
    }

    void PainterRobot::paintAllCells() {
        InOutBuffer compInBuf;
        InOutBuffer compOutBuf;
        intCodeComputer *comp = new intCodeComputer(program, &compInBuf, &compOutBuf);
        std::thread compThr(&intCodeComputer::execute,comp);
        std::thread robotThr(&PainterRobot::activateRobot, (this), &compOutBuf, &compInBuf);

        compThr.join();
        compOutBuf.noNewDataWillArive();
        robotThr.join();
        delete comp;

    }

    void PainterRobot::activateRobot(InOutBuffer* inbuf, InOutBuffer* outbuf) {
        bool keepRunning = true;
        while (keepRunning){

            // output current color
            outbuf->push_back(static_cast<boost::multiprecision::cpp_int>( getCellColor()));

            bool bufferPermanentEmpty;
            auto clr = inbuf->getFrontAndPopFrontNoDataPossible(bufferPermanentEmpty);
            auto mvdir = inbuf->getFrontAndPopFrontNoDataPossible(bufferPermanentEmpty);

            if (bufferPermanentEmpty ){
                keepRunning = false;
                break;
            }

            // paint current cell
            int intclr = static_cast<int>(clr);
            paint(static_cast<PainterRobot::color>( intclr));

            // move
            int intmvdir = static_cast<int>(mvdir);
            move(static_cast<PainterRobot::moveDir>(intmvdir));


        }
    }



    PainterRobot::color PainterRobot::getCellColor() {
        if (paintedCells.find(currPos)!= paintedCells.end()){
            return (paintedCells.find(currPos))->second;
        }
        return color::black;
    }


    void PainterRobot::move(moveDir direction) {
    //Get new direction
        if (direction == moveDir::left){
            currentDirection = static_cast<facingDir >((static_cast<int>(currentDirection)-1+4)%4);
        }
        if (direction == moveDir::right){
            currentDirection = static_cast<facingDir >((static_cast<int>(currentDirection)+1)%4);
        }
    //Move 1 step in that direction
        switch (currentDirection){
            case facingDir::north:
                currPos.y-=1;
                break;
            case facingDir::east:
                currPos.x -=1;
                break;
            case facingDir::south:
                currPos.y+=1;
                break;
            case facingDir::west:
                currPos.x +=1;
                break;
        }

    }

    void PainterRobot::paint(PainterRobot::color clr) {

        if (paintedCells.find(currPos)== paintedCells.end()){
            paintedCells.insert(std::make_pair(currPos,clr));
        }
        else{
            paintedCells.find(currPos)->second = clr;
        }
    }

    void PainterRobot::printDrawing() {
        int minX = paintedCells.begin()->first.x;
        int minY = paintedCells.begin()->first.y;
        int maxX = minX;
        int maxY = minY;
        for (auto cell:paintedCells){
            if (cell.first.x < minX)
                minX = cell.first.x;
            if (cell.first.x > maxX)
                maxX = cell.first.x;
            if (cell.first.y < minY)
                minY = cell.first.y;
            if (cell.first.y > maxY)
                maxY = cell.first.y;
        }

        std::vector<std::vector<char>> drawing;
        for (int y=0;y<maxY-minY+1;y++){
            drawing.push_back(std::vector<char>(maxX-minX+1,' '));
        }

        for (auto cell:paintedCells){
            if (cell.second == color::white) {
                drawing.at(cell.first.y-minY).at((cell.first.x-minX)) = '#';
            }
        }


        for (auto line:drawing){
            std::reverse(line.begin(),line.end());//Flip the image. It seems to be mirrorred.
            std::cout << std::string(line.begin(),line.end())<<std::endl;
        }


    }

}