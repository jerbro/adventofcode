//
// Created by Jeroen on 03-12-19.
//

#ifndef INC_2019_COORD_H
#define INC_2019_COORD_H

class Coord{
public:
    int x;
    int y;

};

bool operator==(const Coord &c1, const Coord &c2);

bool operator<(const Coord &c1, const Coord &c2);

#endif //INC_2019_COORD_H
