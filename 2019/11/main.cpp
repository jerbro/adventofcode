#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <climits>
#include <map>
#include "InOutBuffer.h"
#include "intCodeComputer.h"
#include "PainterRobot.h"
#include <boost/multiprecision/cpp_int.hpp>



namespace day11{
std::map<boost::multiprecision::cpp_int,boost::multiprecision::cpp_int> readFile(const std::string &filename);


int main() {

    auto numbersList = readFile("input.txt");

    PainterRobot robot(numbersList);
    robot.paintAllCells();
    auto answerA = robot.getNumberOfPaintedCells();

    robot.reset();
    robot.paint(PainterRobot::color::white);
    robot.paintAllCells();

    std::cout << "Advent of code 2019, day 11" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << std::endl;
    robot.printDrawing();

    return 0;
}



std::map<boost::multiprecision::cpp_int,boost::multiprecision::cpp_int> readFile(const std::string &filename) {
    std::map<boost::multiprecision::cpp_int,boost::multiprecision::cpp_int> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string nr;
    long position=0;
    while (getline(fs, nr, ',')) {
        result.insert(std::make_pair(position,strtol(nr.c_str(), nullptr, 10)));
        position ++;
    }
    fs.close();
    return result;
}

}

int main(int argc, char *argv[]){
    return day11::main();
}