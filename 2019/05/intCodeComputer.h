//
// Created by Jeroen on 02-12-19.
//

#ifndef INC_2019_INTCODECOMPUTER_H
#define INC_2019_INTCODECOMPUTER_H


#include <vector>
#include <map>
#include "Instruction.h"

namespace day05 {
    class intCodeComputer {
    public:
        enum exitCode {
            normalEnd = 0, invalidInstruction = 1
        };

        explicit intCodeComputer(std::vector<long> &program);

        void resetProgram();

        ~intCodeComputer() = default;

        exitCode execute();

        void replacePosWithVal(long position, long value);

        long getValueAtPosition(long position);

        std::deque<long> getOutputBuffer(){return outputBuffer;}
        void setInputBuffer(const std::deque<long> & buf){inputBuffer = buf;}
        void addInputValue(long val){inputBuffer.push_back(val);}
        long getLastOutputtedValue(){return outputBuffer.back();}
    private:
        std::vector<long> program;
        std::vector<long> initialProgram;
        std::deque<long> inputBuffer;
        std::deque<long> outputBuffer;

        std::map<int,Instruction*> supportedOpcodes;

    };
}

#endif //INC_2018_INTCODECOMPUTER_H
