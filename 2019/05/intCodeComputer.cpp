//
// Created by Jeroen on 02-12-19.
//

#include <iostream>
#include "intCodeComputer.h"

namespace day05 {
    intCodeComputer::exitCode intCodeComputer::execute() {
        long pc = 0;
        bool keepRunning = true;


        while (keepRunning){
            auto instr = program.at(pc);
            auto opcode = instr%100;

            auto currOpcode = supportedOpcodes.find(opcode);
            if (currOpcode != supportedOpcodes.end()) {
                keepRunning = (*currOpcode->second).execute(program, pc);
            }
            else {
                keepRunning = false;
                std::cout << " Error. Invalid opcode: "<<opcode<<" \n";
            }
        }
    }

    void intCodeComputer::replacePosWithVal(long position, long value) {
        program.at(position) = value;
    }

    long intCodeComputer::getValueAtPosition(long position) {
        return program.at(position);
    }

    void intCodeComputer::resetProgram() {
        program = initialProgram;
        outputBuffer.clear();
        inputBuffer.clear();
    }

    intCodeComputer::intCodeComputer(std::vector<long> &program): initialProgram(program) {
        resetProgram();
        std::vector<Instruction*> instructions;

        instructions.push_back(new AddCommand());
        instructions.push_back(new MulCommand());
        instructions.push_back(new ReadInputCommand(inputBuffer));
        instructions.push_back(new WriteOutputCommand(outputBuffer));
        instructions.push_back(new HaltCommand());
        instructions.push_back(new jumpIfTrueCommand());
        instructions.push_back(new jumpIfFalseCommand());
        instructions.push_back(new lessThanCommand());
        instructions.push_back(new equalsCommand());

        for (Instruction* instr:instructions){
            supportedOpcodes.insert(std::make_pair(instr->getOpcode(), instr));
        }
    }
}