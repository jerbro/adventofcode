#include <iostream>
#include <fstream>
#include <vector>
#include "intCodeComputer.h"



namespace day05{
std::vector<long> readFile(const std::string &filename);



int main() {
    auto numbersList = readFile("input.txt");

    intCodeComputer computer(numbersList);
    computer.addInputValue(1);
    computer.execute();
    auto answerA = computer.getLastOutputtedValue();

    computer.resetProgram();
    computer.addInputValue(5);
    computer.execute();
    auto answerB = computer.getLastOutputtedValue();

    std::cout << "Advent of code 2019, day 05" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}




std::vector<long> readFile(const std::string &filename) {
    std::vector<long> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string nr;
    while (getline(fs, nr, ',')) {
        result.push_back(strtol(nr.c_str(), nullptr, 10));
    }
    fs.close();
    return result;
}

}

int main(int argc, char *argv[]){
    return day05::main();
}