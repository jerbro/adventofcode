#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>


std::pair<long,long> readFile(const std::string &filename);
std::vector<unsigned int> numberToDigits(long number);
bool passcodeIsValid(long passcode,bool exactMode);
long getNumberOfValidCodes(long start, long end, bool exactMode=false);
bool hasDoubleDigit( const std::vector<unsigned int>& passcode, bool exactMode);

int main(int argc, char *argv[]) {
    auto startAndStop = readFile("input.txt");
    auto answerA = getNumberOfValidCodes(startAndStop.first,startAndStop.second);
    auto answerB = getNumberOfValidCodes(startAndStop.first,startAndStop.second,true);

    std::cout << "Advent of code 2019, day 04" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}

long getNumberOfValidCodes(long start, long end, bool exactMode){
    long result = 0;
    for (long i=start;i<=end; i++){
        if (passcodeIsValid(i,exactMode))
            result++;
    }
    return result;
}

bool allNumbersAreIncreasing(const std::vector<unsigned int> &passcode){
    int prevChar = passcode.at(0);
    for (auto item:passcode){
        if (item < prevChar){
            return false;
        }
        prevChar = item;
    }
    return true;
}



bool hasDoubleDigit(const std::vector<unsigned int> &passcode, bool exactMode ) {
    int prevChar = 9999;
    int sameCounter=1;
    for (auto item:passcode){
        if (item == prevChar){
            sameCounter++;
        }
        else{
            if (sameCounter == 2 || (sameCounter >2 && !exactMode))
                return true;
            sameCounter = 1;
        }
        prevChar = item;
    }
    return (sameCounter == 2 || (sameCounter > 2 && !exactMode));

}

bool has6digits(const std::vector<unsigned int> &passcode){
    return passcode.size() == 6;
}

bool passcodeIsValid(long passcode, bool exactMode) {
    auto digits = numberToDigits(passcode);
    return (has6digits(digits) &&hasDoubleDigit(digits,exactMode) && allNumbersAreIncreasing(digits));
}


std::vector<unsigned int> numberToDigits(long number){
    std::vector<unsigned int> result;
    long currNumber = number;
    while (currNumber >0){
        auto currDigit = static_cast<unsigned int>(currNumber%10);
        result.push_back(currDigit);
        currNumber -= currDigit;
        currNumber = currNumber / 10;
    }
    std::reverse(result.begin(),result.end());
    return result;
}


std::pair<long,long> readFile(const std::string &filename) {

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string startStr,endStr;
    (getline(fs,startStr,'-'));
    (getline(fs,endStr,'-'));

    fs.close();
    return std::make_pair<long,long>(strtol(startStr.c_str(),nullptr,10),strtol(endStr.c_str(),nullptr,10));
}
