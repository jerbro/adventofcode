//
// Created by jeroen on 07-12-19.
//

#include <thread>
#include "InOutBuffer.h"

    InOutBuffer::InOutBuffer() {
        mostRecentAddedValue = 0;
        someOneWaits=false;
        lastDataIsReceived=false;
    }

    void InOutBuffer::push_back(boost::multiprecision::cpp_int value) {
        mtx.lock();
        mostRecentAddedValue = value;
        buffer.push_back(value);
        someOneWaits=false;
        mtx.unlock();
    }

    boost::multiprecision::cpp_int InOutBuffer::getMostRecentAddedValue() {
        boost::multiprecision::cpp_int result;
        mtx.lock();
        result = mostRecentAddedValue;
        mtx.unlock();
        return result;

    }

    boost::multiprecision::cpp_int InOutBuffer::getFrontAndPopFront() {
        boost::multiprecision::cpp_int result = 0;
        mtx.lock();
        mtx.unlock();
        bool keepTrying = true;
        do {
            mtx.lock();

            if (!buffer.empty()) {
                result = buffer.front();
                buffer.pop_front();
                keepTrying = false;
            }

            if (keepTrying) {
                someOneWaits=true;
            }
            mtx.unlock();
            if (keepTrying) {
                std::this_thread::sleep_for(std::chrono::nanoseconds(0));
            }

        } while (keepTrying);//If between the check, and the mtx for some reason someone else has removed the item from the buffer, try again.
        return result;
    }

    boost::multiprecision::cpp_int InOutBuffer::getFrontAndPopFrontNoDataPossible(bool& nodata) {
        boost::multiprecision::cpp_int result = 0;
        mtx.lock();
        mtx.unlock();
        nodata=false;
        bool keepTrying = true;
        do {
            mtx.lock();

            if (!buffer.empty()) {
                result = buffer.front();
                buffer.pop_front();
                keepTrying = false;
            } else{
                if (lastDataIsReceived){
                    nodata=true;
                    keepTrying=false;
                }
            }

            mtx.unlock();
            if (keepTrying) {
                mtx.lock();
                someOneWaits=true;
                mtx.unlock();
                std::this_thread::sleep_for(std::chrono::nanoseconds(0));
            }

        } while (keepTrying);//If between the check, and the mtx for some reason someone else has removed the item from the buffer, try again.
        someOneWaits=false;
        return result;
    }

    void InOutBuffer::noNewDataWillArive() {
        lastDataIsReceived=true;
    }

bool InOutBuffer::someOneIsWaitingForData() {
    bool retval;
    mtx.lock();
    retval= someOneWaits;
    mtx.unlock();
    return retval;
}

bool InOutBuffer::isEmpty() {
    bool retval;
    mtx.lock();
    retval = buffer.empty();
    mtx.unlock();
    return retval;
}

void InOutBuffer::waitForData() {
    bool keepRunning = true;
    while (keepRunning)  {
            if (!isEmpty())
                keepRunning = false;
            if (lastDataIsReceived)
                keepRunning = false;
            std::this_thread::sleep_for(std::chrono::nanoseconds(0));
        }
    }

bool InOutBuffer::isLastDataReceived(){
    return lastDataIsReceived;
}
