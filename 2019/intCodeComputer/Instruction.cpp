//
// Created by jeroen on 05-12-19.
//

#include "Instruction.h"

    bool AddCommand::execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) {
        boost::multiprecision::cpp_int para1Val, para2Val, resultPos;
        boost::multiprecision::cpp_int opcode = getValueAtPosition(program,pc);
        auto opcodeModes = getOpCodeModes(opcode, 3);
        para1Val = getValue(program, pc + 1, opcodeModes.at(0), relBase);
        para2Val = getValue(program, pc + 2, opcodeModes.at(1), relBase);
        resultPos = getValue(program, pc + 3, opcodeMode::immidiate, relBase);

        setValueAtPosition(program,resultPos, para1Val + para2Val,opcodeModes.at(2),relBase);
        pc += 4;
        return true;
    }

    bool MulCommand::execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) {
        boost::multiprecision::cpp_int para1Val, para2Val, resultPos;
        auto opcode = getValueAtPosition(program,pc);
        auto opcodeModes = getOpCodeModes(opcode, 3);
        para1Val = getValue(program, pc + 1, opcodeModes.at(0), relBase);
        para2Val = getValue(program, pc + 2, opcodeModes.at(1), relBase);
        resultPos = getValue(program, pc + 3, opcodeMode::immidiate, relBase);

        setValueAtPosition(program,resultPos, para1Val * para2Val,opcodeModes.at(2),relBase);
        pc += 4;
        return true;
    }

    bool ReadInputCommand::execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) {
        auto inp = inputBuffer->getFrontAndPopFront();
        auto opcode = getValueAtPosition(program,pc);
        auto opcodeModes = getOpCodeModes(opcode, 1);

        auto resultPos = getValue(program, pc + 1, opcodeMode::immidiate, relBase);


        setValueAtPosition(program,resultPos, inp,opcodeModes.at(0),relBase);

        pc += 2;
        return true;
    }


    bool WriteOutputCommand::execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) {
        boost::multiprecision::cpp_int para1Val;
        auto opcode = getValueAtPosition(program,pc);
        auto opcodeModes = getOpCodeModes(opcode, 1);
        para1Val = getValue(program, pc + 1, opcodeModes.at(0), relBase);
        outputBuffer->push_back(para1Val);

        pc += 2;
        return true;

    }

    bool HaltCommand::execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) {
        return false;
    }

    bool jumpIfTrueCommand::execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) {
        boost::multiprecision::cpp_int para1Val, para2Val;
        auto opcode = getValueAtPosition(program,pc);
        auto opcodeModes = getOpCodeModes(opcode, 2);
        para1Val = getValue(program, pc + 1, opcodeModes.at(0), relBase);
        para2Val = getValue(program, pc + 2, opcodeModes.at(1), relBase);
        if (para1Val != 0) {
            pc = para2Val;
        } else {
            pc += 3;
        }
        return true;
    }

    bool jumpIfFalseCommand::execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) {
        boost::multiprecision::cpp_int para1Val, para2Val;
        auto opcode = getValueAtPosition(program,pc);
        auto opcodeModes = getOpCodeModes(opcode, 2);
        para1Val = getValue(program, pc + 1, opcodeModes.at(0), relBase);
        para2Val = getValue(program, pc + 2, opcodeModes.at(1), relBase);
        if (para1Val == 0) {
            pc = para2Val;
        } else {
            pc += 3;
        }
        return true;
    }

    bool lessThanCommand::execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) {
        boost::multiprecision::cpp_int para1Val, para2Val, resultPos;
        auto opcode = getValueAtPosition(program,pc);
        auto opcodeModes = getOpCodeModes(opcode, 3);
        para1Val = getValue(program, pc + 1, opcodeModes.at(0), relBase);
        para2Val = getValue(program, pc + 2, opcodeModes.at(1), relBase);
        resultPos = getValue(program, pc + 3, opcodeMode::immidiate, relBase);
        boost::multiprecision::cpp_int valToWrite = 0;
        if (para1Val < para2Val) {
            valToWrite = 1;
        }

        setValueAtPosition(program,resultPos, valToWrite,opcodeModes.at(2),relBase);
        pc += 4;
        return true;
    }

    bool equalsCommand::execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) {
        boost::multiprecision::cpp_int para1Val, para2Val, resultPos;
        auto opcode = getValueAtPosition(program,pc);
        auto opcodeModes = getOpCodeModes(opcode, 3);
        para1Val = getValue(program, pc + 1, opcodeModes.at(0), relBase);
        para2Val = getValue(program, pc + 2, opcodeModes.at(1), relBase);
        resultPos = getValue(program, pc + 3, opcodeMode::immidiate, relBase);
        boost::multiprecision::cpp_int valToWrite = 0;
        if (para1Val == para2Val) {
            valToWrite = 1;
        }
        setValueAtPosition(program,resultPos, valToWrite,opcodeModes.at(2),relBase);
        pc += 4;
        return true;
    }

    bool setBaseCommand::execute(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int &pc, boost::multiprecision::cpp_int &relBase) {
        boost::multiprecision::cpp_int para1Val;
        auto opcode = getValueAtPosition(program,pc);
        auto opcodeModes = getOpCodeModes(opcode, 1);
        para1Val = getValue(program, pc + 1, opcodeModes.at(0), relBase);

        relBase += para1Val;
        pc+=2;

        return true;
    }



    std::vector<opcodeMode> Instruction::getOpCodeModes(boost::multiprecision::cpp_int opCode, int nrOfModesRequested) {
        boost::multiprecision::cpp_int currOpCode = opCode / 100;
        std::vector<opcodeMode> result;
        for (int i = 0; i < nrOfModesRequested; i++) {
            result.push_back(static_cast<opcodeMode>( static_cast<long>(currOpCode % 10)));
            currOpCode = currOpCode / 10;
        }
        return result;
    }



    boost::multiprecision::cpp_int
    Instruction::getValue(const std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int position, const opcodeMode mode, boost::multiprecision::cpp_int relBase) {
        boost::multiprecision::cpp_int retVal=0;
        switch (mode){
            case opcodeMode::immidiate:
                retVal= getValueAtPosition(program, position);
                break;
            case opcodeMode::relative:
                retVal= getValueAtPosition(program,  getValueAtPosition(program, position)+relBase);
                break;
            case opcodeMode ::position:
            default:
                retVal= getValueAtPosition(program, getValueAtPosition(program, position));
        }
        return retVal;

    }

    boost::multiprecision::cpp_int Instruction::getValueAtPosition(const std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int position) {
        if (position < 0){
            std::cout <<"Error, position "<<position<<"  is not allowed"<<std::endl;
        }
        if (program.find(position)==program.end()){
            return 0;
        } else
            return program.find(position)->second;

    }

    void Instruction::setValueAtPosition(std::map<boost::multiprecision::cpp_int, boost::multiprecision::cpp_int> &program, boost::multiprecision::cpp_int position, boost::multiprecision::cpp_int value, const opcodeMode mode, boost::multiprecision::cpp_int relBase) {
        auto pos = position;
        if (mode == opcodeMode::relative)
            pos += relBase;
        if (program.find(pos) == program.end()){
            program.insert(std::make_pair(pos,value));
        } else{
            program.find(pos)->second = value;
        }
    }
