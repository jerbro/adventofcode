#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include "Coord.h"

class Direction{
public:
    char dir;
    int steps;
};

std::vector<std::vector<Direction>> readFile(const std::string &filename);
std::map<Coord,int> getCoordsOnRoute(const std::vector<Direction> &directions);
long getManhattanDistance(Coord coord);
std::map<Coord,int> getCoordOnBothRoutes(const std::map<Coord,int> &route1, const std::map<Coord,int> &route2);
long getSmallestManhattanDistance(std::map<Coord,int> coords);
long getSmallestStepsOnCrossing(std::map<Coord,int> coords);

int main(int argc, char *argv[]) {
    std::vector<std::vector<Direction>> bla;

   auto routes = readFile("input.txt");
   auto route1 = getCoordsOnRoute(routes.at(0));
   auto route2 = getCoordsOnRoute(routes.at(1));
   auto pointsOnBothRoutes = getCoordOnBothRoutes(route1,route2);
   pointsOnBothRoutes.erase(Coord{0,0});

   auto answerA = getSmallestManhattanDistance(pointsOnBothRoutes);
   auto answerB = getSmallestStepsOnCrossing(pointsOnBothRoutes);
//    auto answerB = calcTotalFuelRequiredForModuleLaunch(numbersList);
    std::cout << "Advent of code 2019, day 03" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}

std::map<Coord,int> getCoordsOnDirection(const Direction &dir, const Coord &start, Coord& endPos, int &steps){
    std::map<Coord,int> result;
    int xDir=0, yDir=0;
    auto currPos = start;
    switch (dir.dir){
        case 'U':yDir=1;
            break;
        case 'D':yDir=-1;
            break;
        case 'L':xDir=-1;
            break;
        case 'R':xDir=1;
            break;
        default:
            break;
    }
    for (int i=0;i<dir.steps;i++)
    {
        steps ++;
        currPos.y +=yDir;
        currPos.x += xDir;
        result.insert(std::make_pair(currPos,steps));
    }
    endPos = currPos;
    return result;
}

std::map<Coord,int> getCoordsOnRoute(const std::vector<Direction> &directions){
    std::map<Coord,int> result;
    Coord startPos{0, 0};
    Coord endPos{0,0};
    int steps =0;
    for (auto direction:directions){
        auto newPointsOnRoute = getCoordsOnDirection(direction, startPos, endPos,steps);
        startPos = endPos;
        result.insert(newPointsOnRoute.begin(),newPointsOnRoute.end());
    }
    return result;
}



std::map<Coord,int> getCoordOnBothRoutes(const std::map<Coord,int> &route1, const std::map<Coord,int> &route2){
    std::map<Coord,int> result;
    for (auto pointOnRoute1:route1){
        auto coord = pointOnRoute1.first;
        if (route2.find(coord) != route2.end()) {
            result.insert(std::make_pair(coord, pointOnRoute1.second + (route2.find(coord)->second)));
        }
    }
    return result;
}


long getSmallestManhattanDistance(std::map<Coord,int> coords){
    auto result = getManhattanDistance((*coords.begin()).first);
    for (auto coord:coords){
        auto currDist = getManhattanDistance(coord.first);
        if (currDist < result)
            result = currDist;
    }
    return result;
}

long getSmallestStepsOnCrossing(std::map<Coord,int> coords){
    auto result = ((*coords.begin()).second);
    for (auto coord:coords){
        auto currSteps = (coord.second);
        if (currSteps < result)
            result = currSteps;
    }
    return result;

}

long getManhattanDistance(Coord coord){
    return abs(coord.x)+abs(coord.y);
}

std::vector<std::vector<Direction>> readFile(const std::string &filename) {

   std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);


    std::string str;

    std::vector<std::vector<Direction>> result;
    while (std::getline(fs,str)) {
        std::stringstream strStr(str);
        std::string token;
        std::vector<Direction> lineDirs;
        while (std::getline(strStr,token,',')){
           Direction dir{token[0],0};
           dir.dir = token[0];
           dir.steps = (int)strtol(token.erase(0,1).c_str(),nullptr,10);
           lineDirs.push_back(dir);
        }
        result.push_back(lineDirs);

    }
    fs.close();
    return result;
}
