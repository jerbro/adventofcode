//
// Created by jeroen on 15-12-19.
//

#ifndef INC_2019_REACTION_H
#define INC_2019_REACTION_H


#include <string>
#include <map>

class Reaction {
public:
    Reaction(std::string reaction);
    std::map<std::string, int64_t> neededToMakeProduct(int64_t& numberOfNeededProduct);
    std::string getOutProductName(){return outProduct;}
private:
    std::string outProduct;
    int outCount;
    std::map<std::string, int64_t> inProducts;
};


#endif //INC_2019_REACTION_H
