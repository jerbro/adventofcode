//
// Created by jeroen on 15-12-19.
//

#include "Reaction.h"
#include "Utils.h"
#include <regex>
#include <iostream>
#include <cmath>

Reaction::Reaction(std::string reaction) {
    std::stringstream strstr(reaction);
    std::vector<std::string> parts;
    while (!strstr.eof()){

        std::string part;
        strstr>>part;
        part.erase(std::remove(part.begin(), part.end(), ','), part.end());
        parts.push_back(part);
    }

    bool number=true;
    bool inputs=true;
    int count;
    std::string name;
    for (auto part:parts){
        if (part=="=>"){
            inputs = false;
            number = true;
        }else if (number ){
           count = Utils::stringToInt(part);
           number = false;

        }else if (!number && inputs){
            name = part;
            number = true;
            inProducts.insert(std::make_pair(name,count));
        }else if (!number && !inputs){
            name = part;
            outCount = count;
            outProduct = part;
        }

    }

}

std::map<std::string, int64_t> Reaction::neededToMakeProduct(int64_t &numberOfNeededProduct) {
    auto numberOfReactions = ((numberOfNeededProduct+outCount-1) / this->outCount);
    std::map<std::string, int64_t> result;
    for (auto item: this->inProducts){
        result.insert(std::make_pair(item.first,item.second*numberOfReactions));
    }
    numberOfNeededProduct -= numberOfReactions*outCount;
    return result;
}
