#include <iostream>
#include <fstream>
#include <vector>
#include "Reaction.h"


namespace day14{
    std::vector<std::string> readFile(const std::string &filename);
    long getOreNeededForFuel(const std::vector<std::string> &reactions, int64_t fuelNeeded);
    long getMaxFuelProducedWithOre(std::vector<std::string> reactions,int64_t fuelNeeded);

    int main() {
        auto reactions = readFile("input.txt");


        auto answerA =getOreNeededForFuel( reactions,1);

        auto answerB = getMaxFuelProducedWithOre(reactions,1000000000000);
        std::cout << "Advent of code 2019, day 14" << std::endl;
        std::cout << "A: " << answerA << std::endl;
        std::cout << "B: " << answerB << std::endl;



        return 0;
    }

    long getMaxFuelProducedWithOre(std::vector<std::string> reactions,int64_t OrePresent){
        int64_t fuelRequest=1;
        bool keepRunning=true;
        while (keepRunning){
            auto neededForCurrentFuel = getOreNeededForFuel(reactions,fuelRequest);
            if (neededForCurrentFuel<OrePresent){//If there is ore left, try again with more fuel (if only half of the ore is needed. double the ore, and try again).
                //This method does probably not give the correct answer for all possible inputs. In that case, the answer will be
                //in the area of the resulted number. For the given input, searching further was not needed.
                fuelRequest = (float)fuelRequest * ((float)OrePresent/(float)neededForCurrentFuel) ;
            }
            else{
                return fuelRequest-1;
            }
        }
    }

    long getOreNeededForFuel(const std::vector<std::string> &reactions, int64_t fuelNeeded){
        std::map<std::string,Reaction> productReaction;
        for (auto reaction:reactions){
            auto reac = Reaction(reaction);
            productReaction.insert(std::make_pair(reac.getOutProductName(),reac));
        }

        std::map<std::string,int64_t> needed;
        needed.insert(std::make_pair("FUEL",fuelNeeded));

        bool keepRunning = true;
        while(keepRunning){
            bool atLeastOneItemChanged = false;
            for (auto &item:needed){
                if (item.second>0 && item.first != "ORE"){
                    atLeastOneItemChanged=true;
                    auto extraNeeded = productReaction.find(item.first)->second.neededToMakeProduct(item.second);
                    for (auto extraNeededItem:extraNeeded){
                        if (needed.find(extraNeededItem.first) != needed.end()){
                            needed.find(extraNeededItem.first)->second += extraNeededItem.second;
                        } else{
                            needed.insert(extraNeededItem);
                        }
                    }
                }
            }
            if (!atLeastOneItemChanged){
                keepRunning=false;
            }
        }
        return needed.find("ORE")->second;
    }

    std::vector<std::string> readFile(const std::string &filename) {
        std::vector<std::string> result;
        std::fstream fs;
        fs.open(filename, std::fstream::openmode::_S_in);
        std::string line;
        while (getline(fs, line)){
            result.push_back(line);
        }

        fs.close();
        return result;
    }

}

int main(int argc, char *argv[]){
    return day14::main();
}