cmake_minimum_required(VERSION 3.8)
project(2019_14)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp Utils.cpp Reaction.cpp)
add_executable(2019_14 ${SOURCE_FILES})
