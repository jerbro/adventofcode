//
// Created by jeroen on 07-12-19.
//

#ifndef INC_2019_AMPLIFIERSYSTEM_H
#define INC_2019_AMPLIFIERSYSTEM_H


#include <vector>
#include "intCodeComputer.h"
namespace day07 {
    class AmplifierSystem {
    public:
        explicit AmplifierSystem(std::vector<long> &program);
        ~AmplifierSystem()=default;

        long getOutput(std::vector<long> phaseSettings, long startValue);

    private:
        std::vector<long> program;


    };
}

#endif //INC_2019_AMPLIFIERSYSTEM_H
