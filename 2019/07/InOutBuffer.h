//
// Created by jeroen on 07-12-19.
//

#ifndef INC_2019_INOUTBUFFER_H
#define INC_2019_INOUTBUFFER_H


#include <deque>
#include <mutex>

class InOutBuffer {
public:
    InOutBuffer();

    void push_back(long );
    long getFrontAndPopFront();
    long getMostRecentAddedValue();
private:
    std::deque<long> buffer;
    long mostRecentAddedValue;
    std::mutex mtx;
};


#endif //INC_2019_INOUTBUFFER_H
