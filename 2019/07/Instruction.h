//
// Created by jeroen on 05-12-19.
//

#ifndef INC_2019_INSTRUCTION_H
#define INC_2019_INSTRUCTION_H


#include <vector>
#include <deque>
#include "InOutBuffer.h"

namespace day07 {
    enum class opcodeMode {
        position = 0, immidiate = 1
    };


    class Instruction {

    public:
        virtual bool execute(std::vector<long> &program, long &pc) = 0;

        virtual int getOpcode() = 0;
        virtual ~Instruction(){};

    protected:
        static long getValue(const std::vector<long> &program, long position, opcodeMode mode);

        static std::vector<opcodeMode> getOpCodeModes(long opCode, int nrOfModesRequested);

    };

    class AddCommand : public Instruction {
    public:

        bool execute(std::vector<long> &program, long &pc) override;

        int getOpcode() override { return 1; }
    };

    class MulCommand : public Instruction {
    public:

        bool execute(std::vector<long> &program, long &pc) override;

        int getOpcode() override { return 2; }
    };

    class ReadInputCommand : public Instruction {
    public:
        explicit ReadInputCommand(InOutBuffer *inp) : inputBuffer(inp) {}

        bool execute(std::vector<long> &program, long &pc) override;

        int getOpcode() override { return 3; }

    private:
        InOutBuffer* inputBuffer;
    };

    class WriteOutputCommand : public Instruction {
    public:
        explicit WriteOutputCommand(InOutBuffer *outp) : outputBuffer(outp) {}

        bool execute(std::vector<long> &program, long &pc) override;

        int getOpcode() override { return 4; }

    private:
        InOutBuffer* outputBuffer;
    };

    class HaltCommand : public Instruction {
        bool execute(std::vector<long> &program, long &pc) override;

        int getOpcode() override { return 99; }
    };

    class jumpIfTrueCommand : public Instruction {
        bool execute(std::vector<long> &program, long &pc) override;

        int getOpcode() override { return 5; }
    };

    class jumpIfFalseCommand : public Instruction {
        bool execute(std::vector<long> &program, long &pc) override;

        int getOpcode() override { return 6; }
    };

    class lessThanCommand : public Instruction {
        bool execute(std::vector<long> &program, long &pc) override;

        int getOpcode() override { return 7; }
    };

    class equalsCommand : public Instruction {
        bool execute(std::vector<long> &program, long &pc) override;

        int getOpcode() override { return 8; }
    };

#endif //INC_2019_INSTRUCTION_H
}