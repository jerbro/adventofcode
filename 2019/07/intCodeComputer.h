//
// Created by Jeroen on 02-12-19.
//

#ifndef INC_2019_INTCODECOMPUTER_H
#define INC_2019_INTCODECOMPUTER_H


#include <vector>
#include <map>
#include "Instruction.h"

namespace day07 {
    class intCodeComputer {
    public:
        enum exitCode {
            normalEnd = 0, invalidInstruction = 1
        };

        explicit intCodeComputer(std::vector<long> program, InOutBuffer *inBuffer, InOutBuffer *outBuffer);

        void resetProgram();

        ~intCodeComputer();

        exitCode execute();

        void replacePosWithVal(long position, long value);

        long getValueAtPosition(long position);

        void addInputValue(long val){inBuffer->push_back(val);}
        long getAddedOutputtedValue(){return outBuffer->getMostRecentAddedValue();}
    private:
        std::vector<long> program;
        std::vector<long> initialProgram;
        InOutBuffer *inBuffer;
        InOutBuffer *outBuffer;

        std::map<int,Instruction*> supportedOpcodes;
    };
}

#endif //INC_2018_INTCODECOMPUTER_H
