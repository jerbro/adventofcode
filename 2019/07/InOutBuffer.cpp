//
// Created by jeroen on 07-12-19.
//

#include <thread>
#include "InOutBuffer.h"

InOutBuffer::InOutBuffer() {
    mostRecentAddedValue=0;
}

void InOutBuffer::push_back(long value) {
    mtx.lock();
    mostRecentAddedValue = value;
    buffer.push_back(value);
    mtx.unlock();
}

long InOutBuffer::getMostRecentAddedValue() {
    long result;
    mtx.lock();
    result= mostRecentAddedValue;
    mtx.unlock();
    return result;

}

long InOutBuffer::getFrontAndPopFront() {
    long result = 0;
    mtx.lock();
    mtx.unlock();
    bool keepTrying = true;
    do {
        mtx.lock();

        if (!buffer.empty()) {
            result = buffer.front();
            buffer.pop_front();
            keepTrying = false;
        }
        mtx.unlock();
        if (keepTrying){
            std::this_thread::sleep_for(std::chrono::nanoseconds(0));
        }
    } while (keepTrying);//If between the check, and the mtx for some reason someone else has removed the item from the buffer, try again.
    return result;
}

