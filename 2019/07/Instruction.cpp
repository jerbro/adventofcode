//
// Created by jeroen on 05-12-19.
//

#include "Instruction.h"
namespace day07 {
    bool AddCommand::execute(std::vector<long> &program, long &pc) {
        long para1Val, para2Val, resultPos;
        long opcode = program.at(static_cast<unsigned long>(pc));
        auto opcodeModes = getOpCodeModes(opcode, 2);
        para1Val = getValue(program, pc + 1, opcodeModes.at(0));
        para2Val = getValue(program, pc + 2, opcodeModes.at(1));
        resultPos = getValue(program, pc + 3, opcodeMode::immidiate);

        program.at(static_cast<unsigned long>(resultPos)) = para1Val + para2Val;
        pc += 4;
        return true;
    }

    bool MulCommand::execute(std::vector<long> &program, long &pc) {
        long para1Val, para2Val, resultPos;
        long opcode = program.at(static_cast<unsigned long>(pc));
        auto opcodeModes = getOpCodeModes(opcode, 2);
        para1Val = getValue(program, pc + 1, opcodeModes.at(0));
        para2Val = getValue(program, pc + 2, opcodeModes.at(1));
        resultPos = getValue(program, pc + 3, opcodeMode::immidiate);

        program.at(static_cast<unsigned long>(resultPos)) = para1Val * para2Val;
        pc += 4;
        return true;
    }

    bool ReadInputCommand::execute(std::vector<long> &program, long &pc) {
        auto inp = inputBuffer->getFrontAndPopFront();
        auto resultPos = getValue(program, pc + 1, opcodeMode::immidiate);
        program.at(static_cast<unsigned long>(resultPos)) = inp;

        pc += 2;
        return true;
    }


    bool WriteOutputCommand::execute(std::vector<long> &program, long &pc) {
        long para1Val;
        auto opcode = program.at(static_cast<unsigned long>(pc));
        auto opcodeModes = getOpCodeModes(opcode, 1);
        para1Val = getValue(program, pc + 1, opcodeModes.at(0));
        outputBuffer->push_back(para1Val);

        pc += 2;
        return true;

    }

    bool HaltCommand::execute(std::vector<long> &program, long &pc) {
        return false;
    }

    bool jumpIfTrueCommand::execute(std::vector<long> &program, long &pc) {
        long para1Val, para2Val;
        auto opcode = program.at(static_cast<unsigned long>(pc));
        auto opcodeModes = getOpCodeModes(opcode, 2);
        para1Val = getValue(program, pc + 1, opcodeModes.at(0));
        para2Val = getValue(program, pc + 2, opcodeModes.at(1));
        if (para1Val != 0) {
            pc = para2Val;
        } else {
            pc += 3;
        }
        return true;
    }

    bool jumpIfFalseCommand::execute(std::vector<long> &program, long &pc) {
        long para1Val, para2Val;
        auto opcode = program.at(static_cast<unsigned long>(pc));
        auto opcodeModes = getOpCodeModes(opcode, 2);
        para1Val = getValue(program, pc + 1, opcodeModes.at(0));
        para2Val = getValue(program, pc + 2, opcodeModes.at(1));
        if (para1Val == 0) {
            pc = para2Val;
        } else {
            pc += 3;
        }
        return true;
    }

    bool lessThanCommand::execute(std::vector<long> &program, long &pc) {
        long para1Val, para2Val, resultPos;
        auto opcode = program.at(static_cast<unsigned long>(pc));
        auto opcodeModes = getOpCodeModes(opcode, 2);
        para1Val = getValue(program, pc + 1, opcodeModes.at(0));
        para2Val = getValue(program, pc + 2, opcodeModes.at(1));
        resultPos = getValue(program, pc + 3, opcodeMode::immidiate);
        long valToWrite = 0;
        if (para1Val < para2Val) {
            valToWrite = 1;
        }
        program.at(static_cast<unsigned long>(resultPos)) = valToWrite;
        pc += 4;
        return true;
    }

    bool equalsCommand::execute(std::vector<long> &program, long &pc) {
        long para1Val, para2Val, resultPos;
        auto opcode = program.at(static_cast<unsigned long>(pc));
        auto opcodeModes = getOpCodeModes(opcode, 2);
        para1Val = getValue(program, pc + 1, opcodeModes.at(0));
        para2Val = getValue(program, pc + 2, opcodeModes.at(1));
        resultPos = getValue(program, pc + 3, opcodeMode::immidiate);
        long valToWrite = 0;
        if (para1Val == para2Val) {
            valToWrite = 1;
        }
        program.at(static_cast<unsigned long>(resultPos)) = valToWrite;
        pc += 4;
        return true;
    }

    std::vector<opcodeMode> Instruction::getOpCodeModes(long opCode, int nrOfModesRequested) {
        long currOpCode = opCode / 100;
        std::vector<opcodeMode> result;
        for (int i = 0; i < nrOfModesRequested; i++) {
            result.push_back(static_cast<opcodeMode> (currOpCode % 10));
            currOpCode = currOpCode / 10;
        }
        return result;
    }

    long Instruction::getValue(const std::vector<long> &program, long position, const opcodeMode mode) {
        if (mode == opcodeMode::immidiate) {
            return program.at(static_cast<unsigned long>(position));
        } else {
            return program.at(static_cast<unsigned long>(program.at(static_cast<unsigned long>(position))));
        }
    }
}