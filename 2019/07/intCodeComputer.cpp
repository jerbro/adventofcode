//
// Created by Jeroen on 02-12-19.
//

#include <iostream>
#include "intCodeComputer.h"

namespace day07 {
    intCodeComputer::exitCode intCodeComputer::execute() {
        long pc = 0;
        bool keepRunning = true;


        while (keepRunning){
            if (pc>program.size()) {
                std::cout << " Error. pc larger than program\n";
                return intCodeComputer::exitCode::invalidInstruction;
            }
            auto instr = program.at(static_cast<unsigned long>(pc));
            int opcode = static_cast<int>(instr % 100);

            auto currOpcode = supportedOpcodes.find(opcode);
            if (currOpcode != supportedOpcodes.end()) {
                keepRunning = (*currOpcode->second).execute(program, pc);
            }
            else {
                keepRunning = false;
                std::cout << " Error. Invalid opcode: "<<opcode<<" \n";
            }
        }
        return intCodeComputer::exitCode::normalEnd;
    }

    void intCodeComputer::replacePosWithVal(long position, long value) {
        program.at(static_cast<unsigned long>(position)) = value;
    }

    long intCodeComputer::getValueAtPosition(long position) {
        return program.at(static_cast<unsigned long>(position));
    }

    void intCodeComputer::resetProgram() {
        program = initialProgram;
    }

    intCodeComputer::intCodeComputer(std::vector<long> program, InOutBuffer *inBuffer, InOutBuffer *outBuffer)
            : initialProgram(std::move(program)),inBuffer(inBuffer),outBuffer(outBuffer){
        resetProgram();
        std::vector<Instruction*> instructions;

        instructions.push_back(new AddCommand());
        instructions.push_back(new MulCommand());
        instructions.push_back(new ReadInputCommand(inBuffer));
        instructions.push_back(new WriteOutputCommand(outBuffer));
        instructions.push_back(new HaltCommand());
        instructions.push_back(new jumpIfTrueCommand());
        instructions.push_back(new jumpIfFalseCommand());
        instructions.push_back(new lessThanCommand());
        instructions.push_back(new equalsCommand());

        for (Instruction* instr:instructions){
            supportedOpcodes.insert(std::make_pair(instr->getOpcode(), instr));
        }
    }

    intCodeComputer::~intCodeComputer() {
        for (auto opcode:supportedOpcodes){
            delete opcode.second;
        }

    }
}