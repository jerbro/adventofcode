//
// Created by jeroen on 07-12-19.
//

#include <thread>
#include <iostream>
#include "AmplifierSystem.h"


namespace day07 {
    AmplifierSystem::AmplifierSystem(std::vector<long> &program) : program(program) {
    }

    long AmplifierSystem::getOutput(std::vector<long> phaseSettings, long startValue) {
        std::vector<intCodeComputer*> comps(5);
        std::vector<InOutBuffer> buffers(5);
        std::vector<std::thread> threads;


        for (unsigned long i=0;i<5;i++){
            buffers.at(i).push_back(phaseSettings[i]);
        }

        for (unsigned long i=0;i<5;i++){
            if (i==0) {
                buffers[0].push_back(startValue);
            }
            std::string compNr;

            comps[i] = new intCodeComputer(program, &buffers[i], &buffers[(i + 1) % 5]);
            threads.emplace_back(std::thread(&intCodeComputer::execute,comps.at(i)));
        }

        for (int i=0;i<5;i++){
            threads[i].join();
        }

        for(int i=0;i<5;i++){
            delete comps[i];
        }

        return buffers[0].getMostRecentAddedValue();
    }

}