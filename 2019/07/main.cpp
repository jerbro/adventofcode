#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <climits>
#include "AmplifierSystem.h"



namespace day07{
std::vector<long> readFile(const std::string &filename);
long getMaxOutputFromAmplifier(AmplifierSystem& ampSys, std::vector<long> phaseSettings);

int main() {

    auto numbersList = readFile("input.txt");

    AmplifierSystem ampSys(numbersList);
//testQueue();
    auto answerA = getMaxOutputFromAmplifier(ampSys,{0,1,2,3,4});
    auto answerB = getMaxOutputFromAmplifier(ampSys,{5,6,7,8,9});
    std::cout << "Advent of code 2019, day 07" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}

long getMaxOutputFromAmplifier(AmplifierSystem& ampSys, std::vector<long> phaseSettings){

    long currMax = LONG_MIN;

    do{
        long maxThisPermutation = ampSys.getOutput(phaseSettings, 0);
        if (maxThisPermutation > currMax)
            currMax = maxThisPermutation;
    }while (std::next_permutation(phaseSettings.begin(),phaseSettings.end()));
    return currMax;
}


std::vector<long> readFile(const std::string &filename) {
    std::vector<long> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string nr;
    while (getline(fs, nr, ',')) {
        result.push_back(strtol(nr.c_str(), nullptr, 10));
    }
    fs.close();
    return result;
}

}

int main(int argc, char *argv[]){
    return day07::main();
}