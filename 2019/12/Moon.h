//
// Created by jeroen on 13-12-19.
//

#ifndef INC_2019_MOON_H
#define INC_2019_MOON_H

#include <vector>
#include <string>

class Moon{
public:
    Moon(int x, int y, int z);
    explicit Moon(const std::string &positions);

    void updateVelocity(std::vector<Moon*> allMoons);
    void moveMoon();
    long getEnergy();
    int getX();
    int getY();
    int getZ();
    std::vector<long> getConfig(char c);
private:
    static int getVelocityChange(int posThis, int posOther);
    int x,y,z;
    int vX, vY, vZ;
};
#endif //INC_2019_MOON_H
