#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include "Moon.h"


namespace day12{
    std::vector<std::string> readFile(const std::string &filename);
    long getEnergyAfterXSteps(int steps, const std::vector<std::string>& moonConfigs);
    long getRepeatingMoment(const std::vector<std::string> &moonConfigs);

    int main() {
        auto moonPositions = readFile("input.txt");

        auto answerA =getEnergyAfterXSteps(1000,moonPositions);
        auto answerB = getRepeatingMoment(moonPositions);
        std::cout << "Advent of code 2019, day 12" << std::endl;
        std::cout << "A: " << answerA << std::endl;
        std::cout << "B: " << answerB << std::endl;



        return 0;
    }

    long getGcd(long a, long b) {
        if (b==0)
            return labs(a);
        else
            return getGcd(b, a % b);

    }

    long getlcm(long a, long b){
        return ((a*b)/getGcd(a,b));
    }

    long getRepeatingMoment(const std::vector<std::string> &moonConfigs){
        std::vector<Moon*> allMoons;
        for (const auto &conf:moonConfigs){
            allMoons.push_back(new Moon(conf));
        }

        long stepsXRepeat = -1;
        long stepsYRepeat = -1;
        long stepsZRepeat = -1;
        std::set<std::vector<long>> configsX;
        std::set<std::vector<long>> configsY;
        std::set<std::vector<long>> configsZ;
        bool keepRunning = true;
        long steps=0;
        while (keepRunning){
            for (auto moon:allMoons){
                moon->updateVelocity(allMoons);
            }

            for (auto moon:allMoons){
                moon->moveMoon();
            }

            std::vector<long> configX;
            std::vector<long> configY;
            std::vector<long> configZ;
            for (auto moon:allMoons){
                auto extra = moon->getConfig('x');
                configX.insert(configX.end(),extra.begin(),extra.end());

                extra = moon->getConfig('y');
                configY.insert(configY.end(),extra.begin(),extra.end());

                extra = moon->getConfig('z');
                configZ.insert(configZ.end(),extra.begin(),extra.end());
            }

            if (configsX.find(configX) == configsX.end()){
                configsX.insert(configX);
            }
            else{
                if (stepsXRepeat == -1){
                    stepsXRepeat = steps;
                }
            }

            if (configsY.find(configY) == configsY.end()){
                configsY.insert(configY);
            }
            else{
                if (stepsYRepeat == -1){
                    stepsYRepeat = steps;
                }
            }

            if (configsZ.find(configZ) == configsZ.end()){
                configsZ.insert(configZ);
            }
            else{
                if (stepsZRepeat == -1){
                    stepsZRepeat = steps;
                }

            }
            steps +=1;
            if (stepsXRepeat != -1 && stepsYRepeat != -1 && stepsZRepeat != -1){
                keepRunning = false;
            }
        }

        auto lcmAB = getlcm(stepsXRepeat,stepsYRepeat);
        auto lcmABC = getlcm(lcmAB,stepsZRepeat);
        return lcmABC;
    }

    long getEnergyAfterXSteps(int steps, const std::vector<std::string>& moonConfigs){
        std::vector<Moon*> allMoons;
        for (const auto &conf:moonConfigs){
            allMoons.push_back(new Moon(conf));
        }


        for (int step=0;step<steps;step++){
            for (auto moon:allMoons){
                moon->updateVelocity(allMoons);
            }

            for (auto moon:allMoons){
                moon->moveMoon();
            }
        }
        auto result=0;
        for (auto moon:allMoons){
            result +=moon->getEnergy();
        }
        return result;
    }


    std::vector<std::string> readFile(const std::string &filename) {
        std::vector<std::string> result;
        std::fstream fs;
        fs.open(filename, std::fstream::openmode::_S_in);
        std::string line;
        while (getline(fs, line)){
            result.push_back(line);
        }

        fs.close();
        return result;
    }

}

int main(int argc, char *argv[]){
    return day12::main();
}