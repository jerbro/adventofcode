//
// Created by jeroen on 13-12-19.
//

#include <cstdlib>
#include "Moon.h"
#include <regex>
#include "Utils.h"



Moon::Moon(int x, int y, int z) :x(x),y(y),z(z),vX(0),vY(0),vZ(0){

}

void Moon::updateVelocity(std::vector<Moon *> allMoons) {
    for (auto moonPtr:allMoons) {
        vX += getVelocityChange(x, moonPtr->getX());
        vY += getVelocityChange(y, moonPtr->getY());
        vZ += getVelocityChange(z, moonPtr->getZ());
    }
}



void Moon::moveMoon() {
    x += vX;
    y += vY;
    z += vZ;
}

long Moon::getEnergy() {
    return (abs(x)+abs(y)+abs(z))*(abs(vX)+abs(vY)+abs(vZ));
}

int Moon::getX() {
    return x;
}

int Moon::getY() {
    return y;
}

int Moon::getZ() {
    return z;
}

int Moon::getVelocityChange(int posThis, int posOther) {
    auto retval = 0;
    if (posThis > posOther){
        retval =-1;
    }
    if (posThis < posOther){
        retval =1;
    }
    return retval;
}

Moon::Moon(const std::string &positions) {

    std::smatch match;
    std::regex re("^<x=(-?[0-9]+), y=(-?[0-9]+), z=(-?[0-9]+)>$");
    std::regex_match(positions, match, re);

    this->x = Utils::stringToInt(match[1]);
    this->y = Utils::stringToInt(match[2]);
    this->z = Utils::stringToInt(match[3]);
    this->vX = 0;
    this->vY=0;
    this->vZ=0;

}

std::vector<long> Moon::getConfig(char c) {
    std::vector<long> result;
    switch (c){
        case 'x':
            result.push_back(x);
            result.push_back(vX);
            break;
        case 'y':
            result.push_back(y);
            result.push_back(vY);
            break;

        case 'z':
            result.push_back(z);
            result.push_back(vZ);
            break;
        default:
            result.push_back(x);
            result.push_back(vX);
            result.push_back(y);
            result.push_back(vY);
            result.push_back(z);
            result.push_back(vZ);
    }

    return result;

}
