cmake_minimum_required(VERSION 3.8)
project(2019_08)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp Image.cpp Image.h)
add_executable(2019_08 ${SOURCE_FILES})
