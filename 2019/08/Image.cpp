//
// Created by jeroen on 08-12-19.
//

#include <algorithm>
#include <iostream>
#include "Image.h"

Image::Image(int w, int h, const std::string &source) {
    width = w;
    height = h;
    std::string layerSource;
    for (unsigned long i=0;i<source.length();i+=(w*h)){
        long imgSize = w*h;
        layerSource = source.substr(i,static_cast<unsigned long>(imgSize));
        layers.emplace_back(Layer(width,height,layerSource));
    }


}

Layer::Layer(int w, int h, const std::string &source) {
    width=w;
    height=h;
    this->source=source;
}

int Layer::getValAtPos(int x, int y) const{
    long imgPos = x+y*width;
    char c=source.at(static_cast<unsigned long>(imgPos));
    return (c-'0');
}

int Layer::getNumberOfValue(char c) const{
    return static_cast<int>(std::count(source.begin(),source.end(),c));
}

std::vector<int> Image::getNumberOfCharsPerLayer(char c) const{
    std::vector<int> result;
    for (auto& layer : layers){
        result.push_back(layer.getNumberOfValue(c));
    }
    return result;
}

void Image::printMergedImage() {
    std::vector<std::string> result;
    for (int y=0;y<height;y++){
        std::string line;
        for (int x=0;x<width;x++){
            auto color=getColorAtPos(x,y);
            char c;
            switch(color){
                case 0: c=' ';
                break;
                case 1: c='#';
                    break;
                default: c='.';
                    break;
            }
            line.push_back(c);
        }
        result.push_back(line);
    }

    for (auto& line :result){
        std::cout<<line<<"\n";
    }

}



int Image::getColorAtPos(int x, int y) {

    for (auto& layer:layers){
        if (layer.getValAtPos(x,y) != 2)
            return layer.getValAtPos(x,y);
    }
    return 2;
}
