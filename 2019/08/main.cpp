#include <iostream>
#include <fstream>
#include <vector>
#include "Image.h"


namespace day08{
    std::string readFile(const std::string &filename);
    long getAnswerA(const Image& img);


    int main() {
        auto imageSource = readFile("input.txt");
        Image img(25,6,imageSource);
        auto answerA =getAnswerA(img);

        std::cout << "Advent of code 2019, day 08" << std::endl;
        std::cout << "A: " << answerA << std::endl;
        std::cout << "B: " <<std::endl;
        img.printMergedImage();


        return 0;
    }

    long getAnswerA(const Image& img){
        std::vector<int> perLayer0 = img.getNumberOfCharsPerLayer('0');
        std::vector<int> perLayer1 = img.getNumberOfCharsPerLayer('1');
        std::vector<int> perLayer2 = img.getNumberOfCharsPerLayer('2');

        unsigned long lowestLayer=0;
        int lowestValue=perLayer0.at(0);
        for (unsigned long i=0; i < perLayer0.size(); i++){
            int val = perLayer0.at(i);
            if (val<lowestValue){
                lowestValue = val;
                lowestLayer=i;
            }
        }
        return perLayer1[lowestLayer]*perLayer2[lowestLayer];
    }


    std::string readFile(const std::string &filename) {
        std::string result;
        std::fstream fs;
        fs.open(filename, std::fstream::openmode::_S_in);

        getline(fs, result);

        fs.close();
        return result;
    }

}

int main(int argc, char *argv[]){
    return day08::main();
}