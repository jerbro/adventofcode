//
// Created by jeroen on 08-12-19.
//

#ifndef INC_2019_IMAGE_H
#define INC_2019_IMAGE_H


#include <vector>
#include <string>

class Layer;

class Image {
public:
    Image(int w, int h, const std::string &source);
    std::vector<int> getNumberOfCharsPerLayer(char c) const;
    void printMergedImage();

private:
    std::vector<Layer> layers;
    int width;
    int height;
    int getColorAtPos(int x,int y);
};

class Layer {
public:
    Layer(int w, int h, const std::string &source);
    int getValAtPos(int x, int y) const;
    int getNumberOfValue(char c) const;


private:
    std::string source;
    int width;
    int height;

};

#endif //INC_2019_IMAGE_H
