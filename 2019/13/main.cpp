#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include "../intCodeComputer/InOutBuffer.h"
#include "../intCodeComputer/intCodeComputer.h"
#include "../intCodeComputer/Coord.h"
#include "ArcadeComputer.h"


namespace day13{
    std::map<boost::multiprecision::cpp_int,boost::multiprecision::cpp_int> readFile(const std::string &filename) ;

    int main() {
        auto program = readFile("input.txt");

        ArcadeComputer arcComp(program);

        auto answerA = arcComp.getStartAndGetNumberOfBlocks();
        arcComp.reset();
        auto answerB = arcComp.playAndGetScore();

        std::cout << "Advent of code 2019, day 13" << std::endl;
        std::cout << "A: " << answerA << std::endl;
        std::cout << "B: " << answerB << std::endl;



        return 0;
    }


    std::map<boost::multiprecision::cpp_int,boost::multiprecision::cpp_int> readFile(const std::string &filename) {
        std::map<boost::multiprecision::cpp_int,boost::multiprecision::cpp_int> result;
        std::fstream fs;
        fs.open(filename, std::fstream::openmode::_S_in);

        std::string nr;
        long position=0;
        while (getline(fs, nr, ',')) {
            result.insert(std::make_pair(position,strtol(nr.c_str(), nullptr, 10)));
            position ++;
        }
        fs.close();
        return result;
    }
}

int main(int argc, char *argv[]){
    return day13::main();
}