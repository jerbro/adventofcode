//
// Created by jeroen on 13-12-19.
//

#include <thread>
#include "ArcadeComputer.h"
#include "../intCodeComputer/InOutBuffer.h"
#include "../intCodeComputer/intCodeComputer.h"

void ArcadeComputer::start(bool addQuarters) {
    InOutBuffer inBuf;
    InOutBuffer outBuf;

    auto comp = new intCodeComputer(program,&inBuf,&outBuf);
    if (addQuarters){
        comp->replacePosWithVal(0,2);
    }
    comp->execute();
    outBuf.noNewDataWillArive();
    parseScreenData(&outBuf);
}

long ArcadeComputer::playAndGetScore()
{
    InOutBuffer inBuf;
    InOutBuffer outBuf;

    auto comp = new intCodeComputer(program,&inBuf,&outBuf);
    comp->replacePosWithVal(0,2);

    std::thread compThr(&intCodeComputer::execute, comp);
    std::thread parseThr(&ArcadeComputer::parseScreenData,(this),&outBuf);
    std::thread autoPlayThr(&ArcadeComputer::autoPlay,(this),&inBuf,&outBuf);

    compThr.join();
    outBuf.noNewDataWillArive();
    parseThr.join();
    autoPlayThr.join();

    return score;
}
void ArcadeComputer::reset(){
    screen.clear();
    score=0;
}

void ArcadeComputer::parseScreenData(InOutBuffer* buf){
    bool keepParsing = true;
    std::map<Coord,int> tiles;
    while (keepParsing){
        bool readFailed=false;


        buf->waitForData();
        screenDataUpdateLock.lock();
        this->screenUpdateInProgress=true;
        screenDataUpdateLock.unlock();
        auto x=buf->getFrontAndPopFrontNoDataPossible(readFailed);
        auto y=buf->getFrontAndPopFrontNoDataPossible(readFailed);
        auto type=buf->getFrontAndPopFrontNoDataPossible(readFailed);


        long typel = static_cast<long>(type);
        long xl = static_cast<long>(x);
        long yl = static_cast<long>(y);
        screenLock.lock();
        if (x == -1 && y==0){
            score = static_cast<long>(type);//If this are the coordinates, it is not a type, but the score.
        }else {
            Coord pos =Coord{static_cast<int>(x), static_cast<int>(y) };
            if (screen.find(pos) == screen.end()) {
                screen.insert(std::make_pair(pos, static_cast<tileType >(static_cast<int>(type))));
            } else{
                screen.find(pos)->second = static_cast<tileType >(static_cast<int>(type));
            }
        }
        if (type == tileType::paddle){
            this->lastPaddleX = static_cast<long>(x);
        }
        if (type == tileType::ball){
            this->lastBallX = static_cast<long>(x);
        }
        screenLock.unlock();
        if (readFailed)
            keepParsing = false;

        screenDataUpdateLock.lock();
        this->screenUpdateInProgress=false;
        screenDataUpdateLock.unlock();

    }
}



long ArcadeComputer::getStartAndGetNumberOfBlocks() {
    start();
    auto blockCount = 0;
    for (auto tile:screen){
        if (static_cast<int>(tile.second) ==  2){
            blockCount++;
        }
    }
    return blockCount;
}

void ArcadeComputer::drawScreen(){
    screenLock.lock();

        int minX = screen.begin()->first.x;
        int minY = screen.begin()->first.y;
        int maxX = minX;
        int maxY = minY;
        for (auto cell:screen){
            if (cell.first.x < minX)
                minX = cell.first.x;
            if (cell.first.x > maxX)
                maxX = cell.first.x;
            if (cell.first.y < minY)
                minY = cell.first.y;
            if (cell.first.y > maxY)
                maxY = cell.first.y;
        }

        std::vector<std::vector<char>> drawing;
        for (int y=0;y<maxY-minY+1;y++){
            drawing.push_back(std::vector<char>(maxX-minX+1,' '));
        }

        for (auto cell:screen){
            char symbol;
            switch (cell.second) {
                case tileType::ball:
                    symbol = 'o';
                    break;
                case tileType::block:
                    symbol = '#';
                    break;
                case tileType::wall:
                    symbol = '|';
                    break;
                case tileType::empty:
                    symbol = ' ';
                    break;
                case tileType::paddle:
                    symbol = '_';
                    break;
            }
            drawing.at(cell.first.y-minY).at((cell.first.x-minX)) = symbol;
        }



        for (auto line:drawing) {
            std::reverse(line.begin(),line.end());//Flip the image. It seems to be mirrorred.
            std::cout << std::string(line.begin(),line.end())<<std::endl;
        }
   //     std::cout <<"Score: "<<score<<" ballX: "<<lastBallX<< " paddleX: "<<lastPaddleX<<"\n\n";

    screenLock.unlock();
    }

void ArcadeComputer::autoPlay(InOutBuffer *outBuf, InOutBuffer* screenBuf) {
    bool keepRunning = true;
    while (keepRunning){
        screenDataUpdateLock.lock();
        bool screenBufChecks = (screenBuf->isEmpty() && !screenUpdateInProgress);
        screenDataUpdateLock.unlock();

        if (outBuf->someOneIsWaitingForData() && screenBufChecks){
   //         drawScreen();
            if (lastPaddleX> lastBallX){
                outBuf->push_back(-1);
            }else if((lastPaddleX< lastBallX)){
                outBuf -> push_back(1);
            }else{
                outBuf->push_back(0);
            }

        }

        if (screenBuf->isLastDataReceived()){
            keepRunning = false;
        }
        std::this_thread::sleep_for(std::chrono::nanoseconds(0));
    }
    return;
}


