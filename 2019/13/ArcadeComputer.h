//
// Created by jeroen on 13-12-19.
//

#ifndef INC_2019_ARCADECOMPUTER_H
#define INC_2019_ARCADECOMPUTER_H


#include <map>
#include <boost/multiprecision/cpp_int.hpp>
#include "../intCodeComputer/Coord.h"
#include "../intCodeComputer/InOutBuffer.h"

class ArcadeComputer {
public:
    enum class tileType{
        empty=0, wall=1,block=2,paddle=3,ball=4
    };
    ArcadeComputer(const std::map<boost::multiprecision::cpp_int,boost::multiprecision::cpp_int>& program):program(program){};
    void start(bool addQuarters=false);

    long getStartAndGetNumberOfBlocks() ;
    void drawScreen();
    void autoPlay(InOutBuffer *outBuf, InOutBuffer* screenBuf);
    long playAndGetScore();
    void reset();

private:
    std::map<boost::multiprecision::cpp_int,boost::multiprecision::cpp_int> program;
    std::map<Coord, tileType> screen;
    long score;

    void parseScreenData(InOutBuffer* buf);
    std::mutex screenLock;
    long lastBallX;
    long lastPaddleX;
    volatile bool screenUpdateInProgress;
    std::mutex screenDataUpdateLock;

};


#endif //INC_2019_ARCADECOMPUTER_H
