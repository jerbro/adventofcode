#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <sstream>
#include <map>
#include "Planet.h"


std::set<std::pair<std::string, std::string>> readFile(const std::string &filename);

std::map<std::string, Planet> getPlanetsMap(std::set<std::pair<std::string, std::string>> &mapData);

long getNumberOfOrbits(const std::map<std::string, Planet> &planetData);

long getNumberOfStepsFromYouToSanta(const std::map<std::string, Planet> &planetData);

int main(int argc, char *argv[]) {
    auto mapData = readFile("input.txt");
    auto planetsData = getPlanetsMap(mapData);

    auto answerA = getNumberOfOrbits(planetsData);
    auto answerB = getNumberOfStepsFromYouToSanta(planetsData);

    std::cout << "Advent of code 2019, day 06" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}

long getNumberOfOrbits(const std::map<std::string, Planet> &planetData) {
    long result = 0;
    for (auto item:planetData) {
        result += item.second.getOrbitCount();
    }
    return result;
}

long getNumberOfStepsFromYouToSanta(const std::map<std::string, Planet> &planetData) {
    const Planet *start = &planetData.find("YOU")->second;
    const Planet *end = &planetData.find("SAN")->second;

    auto stepsFromYouToCenter = start->getAllCenterPlanets();
    auto stepsFromSantaToCenter = end->getAllCenterPlanets();
    long commonCenters = 0;
    for (int i = 0; i < stepsFromSantaToCenter.size(); i++) {
        if (stepsFromSantaToCenter[i] == stepsFromYouToCenter[i]) {
            commonCenters++;
        } else
            break;
    }
    //The number of steps between SAN and YOU is the number of steps from both locations to the most center-planet
    // except for the steps overlap on both routes. The steps that overlap are on both routes, so the number overlapping
    // steps should be doubled to account for both overlapping routes
    return static_cast<long>(stepsFromSantaToCenter.size()) + static_cast<long>(stepsFromYouToCenter.size()) -
           (commonCenters * 2);
}

std::map<std::string, Planet> getPlanetsMap(std::set<std::pair<std::string, std::string>> &mapData) {
    std::map<std::string, Planet> result;
    for (auto &item:mapData) {
        std::string centerName = item.first;
        std::string orbitingName = item.second;

        //add planets to result if they not yet exist
        if (result.find(centerName) == result.end()) {
            result.insert(std::make_pair(centerName, Planet(centerName)));
        }
        if (result.find(orbitingName) == result.end()) {
            result.insert(std::make_pair(orbitingName, Planet(orbitingName)));
        }

        Planet *centerPlanetPtr = &(result.find(centerName)->second);
        Planet *orbitingPlanetPtr = &(result.find(orbitingName)->second);
        centerPlanetPtr->addOrbitingPlanet(orbitingPlanetPtr);
        orbitingPlanetPtr->setCenterPlanet(centerPlanetPtr);

    }
    return result;
}


std::set<std::pair<std::string, std::string>> readFile(const std::string &filename) {

    std::set<std::pair<std::string, std::string>> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line, centerPlanet, orbitPlanet;

    while (getline(fs, line)) {
        std::stringstream strStr(line);
        getline(strStr, centerPlanet, ')');
        getline(strStr, orbitPlanet);
        result.insert(std::make_pair(centerPlanet, orbitPlanet));
    }


    fs.close();
    return result;
}
