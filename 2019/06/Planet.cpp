//
// Created by jeroen on 06-12-19.
//

#include "Planet.h"

int Planet::getOrbitCount() {
    if (centerPlanet == nullptr)
        return 0;
    else
        return centerPlanet->getOrbitCount() + 1;
}

std::vector<std::string> Planet::getAllCenterPlanets() const {
    std::vector<std::string> result;
    if (centerPlanet != nullptr) {
        result = centerPlanet->getAllCenterPlanets();
        result.push_back(centerPlanet->getName());
    }

    return result;
}
