//
// Created by jeroen on 06-12-19.
//

#ifndef INC_2019_PLANET_H
#define INC_2019_PLANET_H


#include <string>
#include <vector>
#include <map>

class Planet {
public:
    explicit Planet(std::string name) : name(std::move(name)) {};

    void addOrbitingPlanet(Planet *orbitingPlanet) { orbitingPlanets.push_back(orbitingPlanet); };

    void setCenterPlanet(Planet *centerPlanet) { this->centerPlanet = centerPlanet; };

    int getOrbitCount();

    std::vector<std::string> getAllCenterPlanets() const;

    std::string getName() { return name; }

private:
    Planet *centerPlanet = nullptr;
    mutable std::vector<Planet *> orbitingPlanets;
    mutable std::string name;

};


#endif //INC_2019_PLANET_H
