//
// Created by Jeroen on 02-12-19.
//

#ifndef INC_2019_INTCODECOMPUTER_H
#define INC_2019_INTCODECOMPUTER_H


#include <vector>

class intCodeComputer {
public:
    enum exitCode{ normalEnd=0, invalidInstruction=1};
    explicit intCodeComputer(std::vector<unsigned long> & program):initialProgram(program){resetProgram();};
    void resetProgram();
    ~intCodeComputer()=default;
    exitCode execute();
    void replacePosWithVal(unsigned long position, unsigned long value);
    long getValueAtPosition(unsigned long position);
private:
    std::vector<unsigned long> program;
    std::vector<unsigned long> initialProgram;
    void addInstruction(unsigned long pc);
    void mulInstruction(unsigned long pc);
};


#endif //INC_2018_INTCODECOMPUTER_H
