#include <iostream>
#include <fstream>
#include <vector>
#include "intCodeComputer.h"

std::vector<unsigned long> readFile(const std::string &filename);

unsigned long getNounVerbAnswer(std::vector<unsigned long> program, long desiredOutput);

int main(int argc, char *argv[]) {
    auto numbersList = readFile("input.txt");

    intCodeComputer computer(numbersList);
    computer.replacePosWithVal(1,12);
    computer.replacePosWithVal(2,2);
    computer.execute();

    auto answerA = computer.getValueAtPosition(0);
    auto answerB = getNounVerbAnswer(numbersList,19690720);
    std::cout << "Advent of code 2019, day 02" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}

unsigned long getNounVerbAnswer(std::vector<unsigned long> program, long desiredOutput){
    intCodeComputer computer(program);
    for (unsigned long noun = 0;noun<=99;noun++)
        for (unsigned long verb=0;verb<=99;verb++){
            computer.resetProgram();
            computer.replacePosWithVal(1,noun);
            computer.replacePosWithVal(2,verb);
            computer.execute();
            auto answer = computer.getValueAtPosition(0);
            if (answer == desiredOutput){
                return (100*noun + verb);
            }
        }
    return 0;
}


std::vector<unsigned long> readFile(const std::string &filename) {
    std::vector<unsigned long> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string nr;
    while (getline(fs,nr,',')) {
        result.push_back(strtoul(nr.c_str(), nullptr,10));
    }
    fs.close();
    return result;
}
