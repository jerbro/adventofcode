//
// Created by Jeroen on 02-12-19.
//

#include "intCodeComputer.h"


intCodeComputer::exitCode intCodeComputer::execute() {
    unsigned long pc = 0;
    bool keepRunning = true;

    while (keepRunning) {
        switch (program.at(pc)) {
            case 1:
                addInstruction(pc);
                break;
            case 2:
                mulInstruction(pc);
                break;
            case 99:
                keepRunning = false;
                break;
            default:
                keepRunning = false;
                break;
        }
        pc +=4;
    }

}

void intCodeComputer::addInstruction(unsigned long pc) {
    unsigned long para1pos, para2pos, resultPos;
    para1pos = program.at(pc+1);
    para2pos = program.at(pc+2);
    resultPos = program.at(pc+3);

    program.at(resultPos) = program.at(para1pos)+program.at(para2pos);

}

void intCodeComputer::mulInstruction(unsigned long pc) {
    unsigned long para1pos, para2pos, resultPos;
    para1pos = program.at(pc+1);
    para2pos = program.at(pc+2);
    resultPos = program.at(pc + 3);

    program.at(resultPos) = program.at(para1pos) * program.at(para2pos);
}

void intCodeComputer::replacePosWithVal(unsigned long position, unsigned long value) {
    program.at(position) = value;
}

long intCodeComputer::getValueAtPosition(unsigned long position) {
    return program.at(position);
}

void intCodeComputer::resetProgram() {
    program = initialProgram;
}
