#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <regex>
#include <iomanip>
#include "Fighter.h"

std::vector<std::string> readFile(const std::string &filename);

std::vector<Fighter> initializeFightersFromBoard(std::vector<std::string> board);

int startFight(std::vector<std::string> &board, std::vector<Fighter> &fighters);

int tryFightWithoutElfLoss(std::vector<std::string> &board, std::vector<Fighter> &fighters);
int getAnswerB(std::vector<std::string> board, std::vector<Fighter> fighters);

int main(int argc, char *argv[]) {

    auto board = readFile("input.txt");
    std::vector<Fighter> fighters = initializeFightersFromBoard(board);
    auto boardCopy = board;
    auto fightersCopy = fighters;
    int answerA = startFight(board, fighters);
    int answerB = getAnswerB(boardCopy, fightersCopy);

//    auto answerA = getLast10DigitsAfterElvesBakery(input);
//    auto answerB = getNrOfScoresLeftFromScore(input);



    std::cout << "Advent of code 2018, day 15" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

int findNrOfFighterGroups(const std::vector<Fighter> &fighters) {
    std::set<char> groups;
    for (auto fighter:fighters)
        if (!fighter.hasDied())
            groups.insert(fighter.getType());
    return groups.size();
}


std::string getFighterInfoInLine(int linenr, const std::vector<Fighter> &fighters) {
    std::string result;

    for (auto fighter:fighters) {
        if (fighter.getCoord().y == linenr) {
            result += ("  " + std::string{fighter.getType()} + "(" + std::to_string(fighter.getHitpoints()) + ")");
        }
    }
    return result;
}

void printBoard(std::vector<std::string> &board, const std::vector<Fighter> &fighters) {

    for (int i = 0; i < board.size(); i++)
        std::cout << board.at(i) << getFighterInfoInLine(i, fighters) << std::endl;
    std::cout << std::endl;
}

int getNumberOfElfFighters(std::vector<Fighter> &fighters) {
    int result = 0;
    for (auto fighter:fighters)
        if (fighter.getType() == 'E')
            result += 1;
    return result;

}

int getAnswerB(std::vector<std::string> board, std::vector<Fighter> fighters) {
    int result = 0;
    int elfPower = 3;
    bool elvesWon = false;
    while (!elvesWon) {

        std::vector<Fighter> copy_fighters = fighters;
        std::vector<std::string> copy_board = board;

        for (auto &fighter:copy_fighters)
            if (fighter.getType() == 'E')
                fighter.setAttackPower(elfPower);


        result = tryFightWithoutElfLoss(copy_board, copy_fighters);
        if (result > 0)
            elvesWon = true;


        elfPower++;
    }
    return result;
}

int tryFightWithoutElfLoss(std::vector<std::string> &board, std::vector<Fighter> &fighters) {
    int elves = getNumberOfElfFighters(fighters);
    int fullRounds = 0;
    bool fightIsOver = false;
    while (!fightIsOver) {

        std::sort(fighters.begin(), fighters.end());

        for (auto &fighter:fighters) {
            if (findNrOfFighterGroups(fighters) <= 1) {
                fightIsOver = true;
                break;
            }
            fighter.doMove(board);
            fighter.doFight(board, fighters);

        }


        //Remove died fighters from fighterslist
        auto new_end = std::remove_if(fighters.begin(), fighters.end(),
                                      [](const Fighter &fighter) { return fighter.hasDied(); });
        fighters.erase(new_end, fighters.end());

        if (elves > getNumberOfElfFighters(fighters))
            fightIsOver = true;

        if (!fightIsOver)
            fullRounds++;
    }

    int totalHitpoints = 0;
    for (auto fighter:fighters) {
        totalHitpoints += fighter.getHitpoints();
    }

    if (elves > getNumberOfElfFighters(fighters))
        return -1;
    else
        return totalHitpoints * fullRounds;
}


int startFight(std::vector<std::string> &board, std::vector<Fighter> &fighters) {

    int fullRounds = 0;
    bool fightIsOver = false;
    while (!fightIsOver) {

        std::sort(fighters.begin(), fighters.end());

        for (auto &fighter:fighters) {
            if (findNrOfFighterGroups(fighters) <= 1) {
                fightIsOver = true;
                break;
            }
            fighter.doMove(board);
            fighter.doFight(board, fighters);

        }

        //Remove died fighters from fighterslist
        auto new_end = std::remove_if(fighters.begin(), fighters.end(),
                                      [](const Fighter &fighter) { return fighter.hasDied(); });
        fighters.erase(new_end, fighters.end());

        if (!fightIsOver)
            fullRounds++;
    }

    int totalHitpoints = 0;
    for (auto fighter:fighters) {
        totalHitpoints += fighter.getHitpoints();
    }
    return totalHitpoints * fullRounds;
}


std::vector<Fighter> initializeFightersFromBoard(std::vector<std::string> board) {
    std::vector<Fighter> result;
    for (int y = 0; y < board.size(); y++)
        for (int x = 0; x < board.at(y).size(); x++) {
            char currBoardChar = board.at(y).at(x);
            if (currBoardChar == 'G' || currBoardChar == 'E')
                result.push_back(Fighter{x, y, currBoardChar});

        }
    return result;
}


std::vector<std::string> readFile(const std::string &filename) {
    std::vector<std::string> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);
    std::string line;
    while (std::getline(fs, line)) {
        result.push_back(line);
    }

    fs.close();

    return result;
}
