//
// Created by jeroen on 16-12-18.
//


#include <functional>
#include "Fighter.h"

Fighter::Fighter(int x, int y, char type) : x(x), y(y), type(type), hitpoints(200),attackPower(3) {
    if (type == 'G')
        enemyType = 'E';
    if (type == 'E')
        enemyType = 'G';
    id = initializer_id;
    initializer_id++;

}

int Fighter::initializer_id = 0;

bool Fighter::operator==(const Fighter& rhs) const{
    return (this->id == rhs.id);
}
bool Fighter::operator!=(const Fighter& rhs) const{
    return (this->id != rhs.id);
}

std::vector<Coord> Fighter::getNeighbouringCells(std::vector<std::string> board, Coord pos) {
    return getNeighbouringCells(board, pos.x, pos.y);
}

std::vector<Coord> Fighter::getNeighbouringCells(std::vector<std::string> board) {
    return getNeighbouringCells(board, this->x, this->y);
}

std::vector<Coord> Fighter::getNeighbouringCells(std::vector<std::string> board, int x, int y) {
    std::vector<Coord> result;
    //Reading-order: top left right bottom
    if (y > 0) //top
        result.push_back(Coord{x, y - 1});
    if (x > 0) //lift
        result.push_back(Coord{x - 1, y});
    if (x < board.at(y).size() - 2)
        result.push_back(Coord{x + 1, y});
    if (y < board.size() - 2)
        result.push_back(Coord{x, y + 1});

    return result;
}

std::vector<Coord> Fighter::getEnemyNeighbours(std::vector<std::string> board, int x, int y) {
    std::vector<Coord> result;
    for (auto neighbour:getNeighbouringCells(board,x,y)) {
        if (board.at(neighbour.y).at(neighbour.x) == enemyType)
            result.push_back(neighbour);
    }
    return result;
}

std::vector<Coord> Fighter::getEnemyNeighbours(std::vector<std::string> board) {
    return getEnemyNeighbours(board, this->x, this->y);
}

std::vector<Coord> Fighter::getEnemyNeighbours(std::vector<std::string> board, Coord pos) {
    return getEnemyNeighbours(board, pos.x, pos.y);
}
Coord Fighter::getCoord(){
    return Coord{x,y};
}

int getFighterAtLocation(std::vector<Fighter>& fighters, int x, int y){
    int result = -1;
    for (int i=0;i<fighters.size();i++){
        if (fighters.at(i).getCoord() == Coord{x,y})
            return i;
    }
    return result;
}

//Find the victim with the lowest hitpoints. If equal, return the first from the vector.
int Fighter::findVictim(std::vector<std::string> &board, std::vector<Fighter>& fighters){
    auto enemieNeighbours = getEnemyNeighbours(board,x,y);

    if (enemieNeighbours.size() < 1)
        return -1;

    int result = -1;
    int lowestHitpoints = 9999;
    for (int i=0;i<enemieNeighbours.size();i++){
        int currFighter = getFighterAtLocation(fighters,enemieNeighbours.at(i).x,enemieNeighbours.at(i).y);
        if (fighters.at(currFighter).getHitpoints()< lowestHitpoints){
            lowestHitpoints = fighters.at(currFighter).getHitpoints();
            result = currFighter;
        }
    }
    return result;
}

bool Fighter::hasDied() const{
    return (hitpoints <=0);
}
int Fighter::getHitpoints(){
    return hitpoints;
}

bool Fighter::receiveAttack(int attackPower, std::vector<std::string> &board){
    hitpoints -= attackPower;
    if (hitpoints <= 0)
        die(board);

    return hitpoints<=0;
}

void Fighter::die(std::vector<std::string> &board){
    board.at(y).at(x) = '.';
    this->hitpoints = -999;
    x = -1;
    y = -1;
    type = 'D';

}

bool Fighter::doFight(std::vector<std::string> &board, std::vector<Fighter>& fighters){
    if (hasDied())
        return false;

    int victimNr =findVictim(board,fighters);
    if (victimNr >=0){
        fighters.at(victimNr).receiveAttack(attackPower, board);
        return true;
    }
    return false;

}
char Fighter::getType(){
    return type;
}


void Fighter::setAttackPower(int power) {
    attackPower = power;
}

bool Fighter::doMove(std::vector<std::string> &board) {
    if (hasDied())
        return false;

    if (getEnemyNeighbours(board).size() != 0)
        return true; // If there are already enemies next to current loc, do not move


        Coord currLoc = Coord{x,y};
        std::set<Coord> reachedLocs {currLoc};
        std::vector<std::vector<Coord>> currLocPath = {{currLoc}};
    auto findPath = findPathToFirstEnemy(board, reachedLocs, currLocPath);
    if (findPath.size() > 1) {
        Coord nextPos = *(findPath.begin()+1);
        board.at(y).at(x) = '.';
        x = nextPos.x;
        y = nextPos.y;
        board.at(y).at(x) = this->type;
        return true;
    }
    return false;

}

bool Fighter::cellIsTravableAndNotReached(std::vector<std::string> board, std::set<Coord> alreadyReachedLocs, Coord cell) {
    return (board.at(cell.y).at(cell.x) == '.' && alreadyReachedLocs.find(cell) == alreadyReachedLocs.end());
}


bool Fighter::operator< (const Fighter &rhs){
    return ((y < rhs.y )|| (y==rhs.y &&x < rhs.x));
}

std::vector<Coord> Fighter::findPathToFirstEnemy(std::vector<std::string> board, std::set<Coord> alreadyReachedLocs,
                                                 std::vector<std::vector<Coord>> paths) {
    std::vector<std::vector<Coord>> PathsToEnemies;

    for (auto path:paths) {
        auto newEnemies = getEnemyNeighbours(board, *(path.end() - 1));
        if (newEnemies.size() > 0) {
            for (auto enemie:newEnemies) {
                std::vector<Coord> newPath = path;
                newPath.push_back(enemie);
                PathsToEnemies.push_back(newPath);
            }
        }
    }
    if (PathsToEnemies.size() > 0) {
        //There are paths to enemies. Find the best enemy, and best path, and return.
        std::vector<Coord> result;

        for (auto path:PathsToEnemies) {
            if (result.size() == 0 || (*(path.end() - 1)) < (*(result.end() - 1)))
                result = path;
        }
        return result;

    } else {//There are no reachable enemies, expand every path that exists now (if possible), and pass all the new paths to the next iteration. If no path can be extended, there is no move possible, and return null
        bool addedNewLocation = false;
        std::vector<std::vector<Coord>> pathsToFurtherExplore;
        for (auto path:paths) {
            auto newPathPossibilities = getNeighbouringCells(board, *(path.end() - 1));
            for (auto newPath: newPathPossibilities) {
                if (cellIsTravableAndNotReached(board, alreadyReachedLocs, newPath)) {
                    addedNewLocation = true;
                    alreadyReachedLocs.insert(newPath);
                    std::vector<Coord> pathToAdd{path};
                    pathToAdd.push_back(newPath);
                    pathsToFurtherExplore.push_back(pathToAdd);
                }
            }
        }
        if (addedNewLocation) {
            return findPathToFirstEnemy(board, alreadyReachedLocs, pathsToFurtherExplore);
        } else { return std::vector<Coord>{}; }

    }
}
