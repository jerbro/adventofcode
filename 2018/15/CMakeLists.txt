cmake_minimum_required(VERSION 3.8)
project(2018_15)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp Fighter.cpp Fighter.h)
add_executable(2018_15 ${SOURCE_FILES})
