//
// Created by jeroen on 16-12-18.
//

#ifndef INC_2018_FIGHTER_H
#define INC_2018_FIGHTER_H
#include <vector>
#include <string>
#include <set>

struct Coord{
    int x;
    int y;
};

inline bool operator< (const Coord& lhs, const Coord& rhs){
    return ((lhs.y < rhs.y) || (lhs.y == rhs.y && lhs.x<rhs.x));
}

inline bool operator== (const Coord& lhs, const Coord& rhs){
    return ((lhs.y == rhs.y) && lhs.x ==rhs.x);
}

class Fighter {
public:
    Fighter(int x, int y, char type);
    bool doMove(std::vector<std::string> &board);
    bool doFight(std::vector<std::string> &board, std::vector<Fighter>& fighters);
    bool operator< (const Fighter &rhs);
    Coord getCoord();
    int getHitpoints();
    bool receiveAttack(int attackPower,std::vector<std::string> &board);
    bool hasDied() const;
    char getType();
    void setAttackPower(int power);
private:
    int findVictim(std::vector<std::string> &board, std::vector<Fighter>& fighters);
    std::vector<Coord> getNeighbouringCells(std::vector<std::string> board, int x, int y);
    std::vector<Coord> getNeighbouringCells(std::vector<std::string> board, Coord pos);
    std::vector<Coord> getNeighbouringCells(std::vector<std::string> board);
    std::vector<Coord> getEnemyNeighbours(std::vector<std::string> board);
    std::vector<Coord> getEnemyNeighbours(std::vector<std::string> board, int x, int y);
    std::vector<Coord> getEnemyNeighbours(std::vector<std::string> board, Coord pos);
    std::vector<Coord> findPathToFirstEnemy(std::vector<std::string> board, std::set<Coord> alreadyReachedLocs, std::vector<std::vector<Coord>> paths);
    bool cellIsTravableAndNotReached(std::vector<std::string> board,std::set<Coord> alreadyReachedLocs, Coord cell);
    bool operator==(const Fighter& rhs) const;
    bool operator!=(const Fighter& rhs) const;

    void die(std::vector<std::string> &board);
    char type;
    char enemyType;
    int x;
    int y;
    int hitpoints;
    int attackPower;
    int id;
    static int initializer_id;
};


#endif //INC_2018_FIGHTER_H
