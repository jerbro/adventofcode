#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <map>
#include <regex>

struct elfClaim {

    int id;
    int x;
    int y;
    int width;
    int height;
};

std::vector<elfClaim> readFile(const std::string &filename);

elfClaim parseLine(const std::string &line);

int getMinHeightOfFabric(std::vector<elfClaim> elfclaims);

int getMinWidthOfFabric(std::vector<elfClaim> elfclaims);

int stringToInt(const std::string &stringnumber);

std::vector<std::vector<int>> occurrencesOfClaimInFabric(std::vector<elfClaim> elfclaims);

int countCellsLargerThan1(std::vector<std::vector<int>> fabric);

void addRectToFabric(std::vector<std::vector<int>> &fabric, int x, int y, int w, int h);
bool rectHasOverlap(const std::vector<std::vector<int>> &fabric, int x, int y, int w, int h);
int idOfNonOverlap(const std::vector<std::vector<int>> &fabric, const std::vector<elfClaim> &elfclaims);

int main(int argc, char *argv[]) {
    auto elfClaimList = readFile("input.txt");
    auto fabric = occurrencesOfClaimInFabric(elfClaimList);
    auto answerA = countCellsLargerThan1(fabric);
    auto answerB = idOfNonOverlap(fabric, elfClaimList);

    std::cout << "Advent of code 2018, day 03" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}



int countCellsLargerThan1(std::vector<std::vector<int>> fabric) {
    int result = 0;
    for (auto line:fabric)
        for (auto cell:line) {
            if (cell > 1)
                result++;
        }
    return result;

}


void addRectToFabric(std::vector<std::vector<int>> &fabric, int x, int y, int w, int h) {
    for (int xpos = x; xpos < x + w; xpos++)
        for (int ypos = y; ypos < y + h; ypos++) {
            fabric[ypos][xpos] += 1;
        }
}

int idOfNonOverlap(const std::vector<std::vector<int>> &fabric, const std::vector<elfClaim> &elfclaims){
    int result = 0;
    for (auto claim:elfclaims){
        if (!rectHasOverlap(fabric, claim.x, claim.y, claim.width, claim.height))
            result = claim.id;
    }
    return result;
}

bool rectHasOverlap(const std::vector<std::vector<int>> &fabric, int x, int y, int w, int h){
    bool hasOverlap = false;
    for (int xpos = x; xpos < x + w; xpos++)
        for (int ypos = y; ypos < y + h; ypos++) {
            if (fabric[ypos][xpos]>1)
                hasOverlap = true;
        }
    return hasOverlap;
}


std::vector<std::vector<int>> occurrencesOfClaimInFabric(std::vector<elfClaim> elfclaims) {
    //std::vector<std::vector<int>> result;
    int h = getMinHeightOfFabric(elfclaims);
    int w = getMinWidthOfFabric(elfclaims);

    //create vector of height h with vectors of width w with value 0.
    // This is a 2D-matrix with height h, end width w.
    std::vector<std::vector<int>> result(h,std::vector<int>(w,0));


    for (auto claim:elfclaims) {
        addRectToFabric(result, claim.x, claim.y, claim.width, claim.height);
    }

    return result;
}

int getMinHeightOfFabric(std::vector<elfClaim> elfclaims) {
    int result = 0;
    for (auto claim:elfclaims) {
        int currHeight = claim.y + claim.height;
        if (currHeight > result)
            result = currHeight;
    }
    return result;
}

int getMinWidthOfFabric(std::vector<elfClaim> elfclaims) {
    int result = 0;
    for (auto claim:elfclaims) {
        int currWidth = claim.x + claim.width;
        if (currWidth > result)
            result = currWidth;
    }
    return result;
}

int stringToInt(const std::string &stringnumber) {
    std::stringstream strstr{stringnumber};
    int result;
    strstr >> result;
    return result;

}

elfClaim parseLine(const std::string &line) {
    elfClaim result={0,0,0,0,0};
    std::smatch match;
    // example of line: #1 @ 1,3: 4x4
    std::regex re("^#([0-9]+) @ ([0-9]+),([0-9]+): ([0-9]+)x([0-9]+)$");
    std::regex_match(line, match, re);

    result.id = stringToInt(match[1]);
    result.x = stringToInt(match[2]);
    result.y = stringToInt(match[3]);
    result.width = stringToInt(match[4]);
    result.height = stringToInt(match[5]);

    return result;
}

std::vector<elfClaim> readFile(const std::string &filename) {
    std::vector<elfClaim> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    while (std::getline(fs, line)) {

        result.push_back(parseLine(line));
    }

    fs.close();
    return result;
}
