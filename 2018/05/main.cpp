#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <regex>


std::string readFile(const std::string &filename);



bool isPair(char char1, char char2);
std::string reactPolymer(const std::string &polymer);
std::string fullyReactPolymer(const std::string &polymer);
std::string removePairCharsFromPolymer(char pairChar, const std::string& polymer);
unsigned long findShortestLengthWhenElementRemoved(const std::string& polymer);

int main(int argc, char *argv[]) {
    auto polymer = readFile("input.txt");
    std::string reactedPolymer = fullyReactPolymer(polymer);
    auto answerA = reactedPolymer.size();
    auto answerB = findShortestLengthWhenElementRemoved(reactedPolymer);


    std::cout << "Advent of code 2018, day 05" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

bool isPair(char char1, char char2) {
    return ((char1 != char2) && (tolower(char1) == tolower(char2)));

}

unsigned long findShortestLengthWhenElementRemoved(const std::string& polymer){
    unsigned long minLength = polymer.size();
    for (char c = 'a'; c <='z' ;c ++ ) {
        unsigned long length = fullyReactPolymer( removePairCharsFromPolymer(c,polymer)).size();
        if (length < minLength)
            minLength = length;
    }
    return minLength;
}

std::string removePairCharsFromPolymer(char pairChar, const std::string& polymer){
    std::string result;
    auto lowChar = char(tolower(pairChar));
    auto capChar = char(toupper(pairChar));

    for (char c:polymer){
        if (c != lowChar && c != capChar){
            result += c;
        }
    }

    return result;
}

std::string reactPolymer(const std::string &polymer) {
    std::string result;
    unsigned long i=0;
    for (i = 0; i < (polymer.size() - 1); i++) {
        if (isPair(polymer.at(i), polymer.at(i + 1))) {
            i++;
        } else {
            result += polymer[i];
            if (i==polymer.size()-2) //If this is the last item of the polymer, then also add the last.
                result += polymer[i+1];
        }
    }

    return result;

}

std::string fullyReactPolymer(const std::string &polymer){
    unsigned long currSize;
    unsigned long newSize;
    std::string newPolymer;
    std::string oldPolymer = polymer;
    do {
        currSize = oldPolymer.size();
        newPolymer = reactPolymer(oldPolymer);
        newSize = newPolymer.size();
        oldPolymer = newPolymer;
    } while (currSize != newSize);
    return newPolymer;
}

std::string readFile(const std::string &filename) {
    std::string result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::getline(fs, result);

    fs.close();
    return result;
}
