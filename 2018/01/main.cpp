#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>

std::vector<long> readFile(const std::string &filename);
long calcFinalFrequency(std::vector<long> nrs);
long findFirstDuplicateFrequency(std::vector<long> nrs);


int main(int argc, char *argv[]) {

    auto numbersList = readFile("input.txt");
    auto answerA = calcFinalFrequency(numbersList);
    auto answerB = findFirstDuplicateFrequency(numbersList);
    std::cout << "Advent of code 2018, day 01" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

long findFirstDuplicateFrequency(std::vector<long> nrs){
    long result=0;
    unsigned long i=0;
    long currFreq = 0;
    std::set<long> foundNumbers;
    foundNumbers.insert(0);
    bool duplicateFound = false;
    while (!duplicateFound)
    {
        currFreq += nrs.at(i);
        if (foundNumbers.find(currFreq)!= foundNumbers.end()){
            result = currFreq;
            duplicateFound = true;
        } else{
            foundNumbers.insert(currFreq);
        }

        i++;
        if (i>=nrs.size())
            i=0;
    }
    return result;
}

long calcFinalFrequency(std::vector<long> nrs){
    long result = 0;
    for (long nr:nrs)
        result += nr;
    return result;
}

std::vector<long> readFile(const std::string &filename) {
    std::vector<long> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::stringstream strStr;


    long currNr;
    while (fs >>currNr){
        result.push_back(currNr);
    }
    fs.close();
    return result;
}
