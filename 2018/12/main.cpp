#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <regex>
#include <iomanip>

struct inputData {
    std::string initialState;
    std::map<std::string, bool> conversions;
};

inputData readFile(const std::string &filename);
std::string doTransformations(const std::string &text, const  std::map<std::string,bool> &transformations, long long &offsetFirst);
std::string getSituationIngeneration(const std::string &text, const  std::map<std::string,bool> & transformations, long long generations, long long& offset);
long long getScore(const std::string& text, long long& offset );

int main(int argc, char *argv[]) {

    auto input = readFile("input.txt");
    long long offset=0;


    auto  situation = getSituationIngeneration(input.initialState, input.conversions,20, offset);
    long long answerA = getScore(situation, offset );

    offset = 0;
    auto  situationB = getSituationIngeneration(input.initialState, input.conversions,50000000000, offset);
    long long answerB = getScore(situationB, offset );

    std::cout << "Advent of code 2018, day 12" << std::endl;
    std::cout << "A: " << answerA<< std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

long long getScore(const std::string& text, long long& offset ){
    long long result=0;
    for (int i=0;i<text.size();i++){
        if (text.at(i) == '#')
            result += i+offset;
    }
    return result;
}

std::string getSituationIngeneration(const std::string &text,const  std::map<std::string,bool> &transformations, long long generations, long long& offset){

    std::set<std::string> foundSituations;
    foundSituations.insert(text);
    std::vector<std::pair<std::string,long long > > foundSituationsAndOffset;
    foundSituationsAndOffset.push_back(std::make_pair(text,0));
    std::string tempResult = text;
    long long currOffset = 0;
    int cycleSize =0;
    int firstRepeatGen;
    for (long long i=0;i<generations;i++){
        tempResult = doTransformations(tempResult,transformations, currOffset);

        long long score = getScore(tempResult, currOffset);


        if (foundSituations.find(tempResult) == foundSituations.end()) {
            foundSituations.insert(tempResult);
            foundSituationsAndOffset.push_back(std::make_pair (tempResult, currOffset));
        }
        else{
            firstRepeatGen = i+1;
            for (int i=0;i<foundSituationsAndOffset.size();i++){
                if (tempResult == foundSituationsAndOffset.at(i).first)
                    cycleSize = foundSituationsAndOffset.size()-i;
            }
            break;
        }

    }

    // Repeat found.
    //situation in generation x = x%numberOfDifferentSituations
    // offset = x/numberOfDifferentSituations (rounded down) * lastOffset + offset of situation x%numberOfDifferentSituations
if (cycleSize > 0) {
    long long wholeRepeats = (generations - firstRepeatGen) / cycleSize;
    long long partOfLastCycle = (generations - firstRepeatGen) % cycleSize;
    long long increastPerCycle = currOffset-
                           foundSituationsAndOffset.at(firstRepeatGen -1).second;


    offset = wholeRepeats * increastPerCycle + currOffset;
    int idOfResult = foundSituationsAndOffset.size()- cycleSize+ partOfLastCycle;
    return foundSituationsAndOffset.at(idOfResult).first;
} else{
    offset = currOffset;
    return tempResult;
    }

}


std::string doTransformations(const std::string &text, const std::map<std::string,bool> &transformations, long long &offsetFirst){
    std::string textToProcess = "...."+text+"....";
    std::string partialStr;
    std::string result;
    bool firstNonZeroFound = false;
    for (int i=0;i<textToProcess.size()-5;i++){
        partialStr = textToProcess.substr(i,5);
        bool newStateFilled = transformations.find(partialStr)->second;
        if (! firstNonZeroFound && newStateFilled) {
            firstNonZeroFound = true;
            offsetFirst = offsetFirst+i-2;
        }
        if (firstNonZeroFound)
            result.push_back(newStateFilled?'#':'.');
    }

    return result;
}

std::string getInitialState(const std::string &firstLine) {
    std::string textToRemove = "initial state: ";

    std::string result = firstLine;
    auto startToErase = firstLine.find(textToRemove);
    auto lengthToErase = textToRemove.length();
    result.erase(startToErase, lengthToErase);

    return result;
}

inputData readFile(const std::string &filename) {
    inputData result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string firstline;
    std::getline(fs, firstline);
    result.initialState = getInitialState(firstline);

    std::getline(fs, firstline);//Next line is empty

    std::string line;
    while (std::getline(fs, line)) {
        std::pair<std::string, bool> conversion;
        std::stringstream linestrstr(line);
        for (int i = 0; i < 3; i++) {
            std::string str;
            if (std::getline(linestrstr, str, ' ')) {
                switch (i) {
                    case 0:
                        conversion.first = str;
                        break;
                    case 1:
                        break;
                    case 2:
                        conversion.second = (str == "#");
                        break;
                    default:
                        break;
                }
            }
        }
        result.conversions.insert(conversion);

    }

    fs.close();

    return result;
}
