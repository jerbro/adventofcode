//
// Created by jeroen on 13-12-18.
//

#include <vector>
#include "Cart.h"
void Cart::moveStep(const std::vector<std::string> &track) {
    switch (direction){
        case 'N':
            y--;
            break;
        case 'E':
            x++;
            break;
        case 'S':
            y++;
            break;
        case 'W':
            x--;
            break;
    }
    updateDirection(track);
}

//Update the direction after moving to a new spot of the track.
// The new direction is the direction to move the next step
// So if now a crossing is reached, now the direction is set
// to leave the crossing the next tick
void Cart::updateDirection(const std::vector<std::string> &track) {
    char currTrackPos = track.at(y).at(x);

    switch (direction) {
        case 'N': //Currently facing north
            if (currTrackPos == '/')
                direction = 'E';
            if (currTrackPos == '\\')
                direction = 'W';
            break;
        case 'E': //Currently facing East
            if (currTrackPos == '/')
                direction = 'N';
            if (currTrackPos == '\\')
                direction = 'S';
            break;
        case 'S': //Currently facing South
            if (currTrackPos == '/')
                direction = 'W';
            if (currTrackPos == '\\')
                direction = 'E';
            break;
        case 'W': //Currently facing West
            if (currTrackPos == '/')
                direction = 'S';
            if (currTrackPos == '\\')
                direction = 'N';
            break;
        default:
            break;
    }

    if (currTrackPos == '+')
        chooseCrossingDir(track);
}

//Choose the new direction when arriving at a crossing;
void Cart::chooseCrossingDir(const std::vector<std::string> &track){
    int currDirId ;
    if (direction =='N')
        currDirId = 0;
    if (direction =='E')
        currDirId = 1;
    if (direction =='S')
        currDirId = 2;
    if (direction =='W')
        currDirId = 3;

    currDirId +=4;
    int newDirId = (currDirId + crossingDir)%4;
    direction = std::string("NESW").at(newDirId);

    crossingDir++;
    if (crossingDir > 1)
        crossingDir = -1;

}

bool Cart::hasCollission(const std::vector<Cart> &otherCarts){
    for (auto otherCart:otherCarts) {
        if (this->x == otherCart.x && this->y == otherCart.y && this->id != otherCart.id && otherCart.carRemoved == false && carRemoved == false)
            return true;
    }

    return false;
}


bool Cart::operator<( const Cart& otherCart) {

   if (x < otherCart.x)
        return true;

    return (x == otherCart.x && y < otherCart.y);

}

char Cart::getDirection() const {
    return direction;
}

int Cart::getX() const {
    return x;
}

int Cart::getY() const {
    return y;
}

void Cart::setCarRemoved() {
    carRemoved = true;
}

int Cart::getId() const {
    return id;
}

bool Cart::isCarRemoved() const {
    return carRemoved;
}

bool Cart::operator==(const Cart &rhs) const {
    return id == rhs.id;
}

bool Cart::operator!=(const Cart &rhs) const {
    return !(rhs == *this);
}


