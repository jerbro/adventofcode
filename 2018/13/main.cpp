#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <regex>
#include <iomanip>
#include "Cart.h"

std::vector<Cart> getCartAndReplaceTrack(std::vector<std::string>& track);
std::vector<std::string> readFile(const std::string &filename);
bool moveCartsStopAtCrash(std::vector<Cart> &carts, const std::vector<std::string> &track);

bool moveCartsRemoveCarAtCrash(std::vector<Cart> &carts, const std::vector<std::string> &track);

Cart getFirstCrashedCar(const std::vector<Cart> &originalCarts, const std::vector<std::string> &track);
Cart getLastRemainingCart(const std::vector<Cart> &originalCarts, const std::vector<std::string> &track);

int main(int argc, char *argv[]) {

    auto track = readFile("input.txt");
    auto carts = getCartAndReplaceTrack(track);


    Cart firstCrashedCart = getFirstCrashedCar(carts, track);
    std::string answerA = std::to_string(firstCrashedCart.getX())+","+std::to_string(firstCrashedCart.getY());

    Cart lastRemainingCart = getLastRemainingCart(carts, track);
    std::string answerB = std::to_string(lastRemainingCart.getX())+","+std::to_string(lastRemainingCart.getY());

    std::cout << "Advent of code 2018, day 13" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

Cart getLastRemainingCart(const std::vector<Cart> &originalCarts, const std::vector<std::string> &track){
    auto carts = originalCarts;
    while ( carts.size()>1){
        moveCartsRemoveCarAtCrash(carts, track);
    }
    return carts.at(0);
}

Cart getFirstCrashedCar(const std::vector<Cart> &originalCarts, const std::vector<std::string> &track){
    auto carts = originalCarts;
    while (moveCartsStopAtCrash(carts, track));
    for (auto cart:carts){
        if (cart.hasCollission(carts)){
            return cart;
        }
    }
}

bool moveCartsRemoveCarAtCrash(std::vector<Cart> &carts, const std::vector<std::string> &track){
    std::sort(carts.begin(), carts.end());
    bool carRemoved = false;


    for (auto& cart:carts){
        cart.moveStep(track);
        if (cart.hasCollission(carts)){

            for (auto& otherCart:carts) {//find the other collisioned car, and set the status to removed from road
                if (otherCart.hasCollission(carts)&& otherCart != cart) {
                    otherCart.setCarRemoved();
                    cart.setCarRemoved();
                    carRemoved = true;
                }
            }
        }

    }

    if (carRemoved){
        auto new_end = std::remove_if(carts.begin(),carts.end(),[](const Cart& currcart){return currcart.isCarRemoved();});
        carts.erase(new_end,carts.end());
    }

    return carRemoved;
}



bool moveCartsStopAtCrash(std::vector<Cart> &carts, const std::vector<std::string> &track) {
    std::sort(carts.begin(), carts.end());
    for (auto& cart:carts){
        cart.moveStep(track);
        if (cart.hasCollission(carts)){
            return false;
        }

    }
    return true;
}

std::vector<Cart> getCartAndReplaceTrack(std::vector<std::string>& track){
    std::vector<Cart> result;
    int carrId = 0;
    for (int y=0;y<track.size();y++)
        for (int x=0;x<track.at(y).size();x++){
            char currTrackPart = track.at(y).at(x);
            switch (currTrackPart){
                case 'v':
                    result.push_back(Cart(x,y,'S',carrId));
                    carrId++;
                    track.at(y).at(x) = '|';
                    break;
                case '<':
                    result.push_back(Cart(x,y,'W',carrId));
                    carrId++;
                    track.at(y).at(x) = '-';
                    break;
                case '>':
                    result.push_back(Cart(x,y,'E',carrId));
                    carrId++;
                    track.at(y).at(x) = '-';
                    break;
                case '^':
                    result.push_back(Cart(x,y,'N',carrId));
                    carrId++;
                    track.at(y).at(x) = '|';
                    break;
                default:
                    break;
            }
        }
    return result;
}

std::vector<std::string> readFile(const std::string &filename) {
    std::vector<std::string> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;

    while (std::getline(fs, line)) {
        result.push_back(line);
    }

    fs.close();

    return result;
}
