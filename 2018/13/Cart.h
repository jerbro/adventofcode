//
// Created by jeroen on 13-12-18.
//

#ifndef INC_2018_CART_H
#define INC_2018_CART_H
#include <iostream>

class Cart {
public:
    Cart(int x, int y, char direction, int id):direction(direction),x(x),y(y), id(id),crossingDir(-1),carRemoved(false){};
    void moveStep(const std::vector<std::string> &track);
    bool hasCollission(const std::vector<Cart> &otherCarts);

    bool operator<(const Cart& otherCart) ;

    char getDirection() const;

    int getX() const;

    int getId() const;

    int getY() const;
    void setCarRemoved();

    bool operator==(const Cart &rhs) const;

    bool operator!=(const Cart &rhs) const;

    bool isCarRemoved() const;


private:
    void updateDirection(const std::vector<std::string> &track);
    void chooseCrossingDir(const std::vector<std::string> &track);
    char direction;
    int x;
    int y;
    int id;
    int crossingDir; //0=straight, -1=left, 1=right
    bool carRemoved;
};


#endif //INC_2018_CART_H
