#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <regex>


int readFile(const std::string &filename);
int getPowerLevel(int x, int y, int serialId);
std::vector<std::vector<int>> fillField(int startx, int starty, int lastx, int lasty, int serialID);
int getSumOfArea(const std::vector<std::vector<int>> &field, int startx, int starty, int h, int w);
std::pair<int,int> getLargestArea(const std::vector<std::vector<int>> &field, int w, int h);

int main(int argc, char *argv[]) {

    auto serialId = readFile("input.txt");
    int answerB = 0;
    auto field = fillField(1,1,300,300,serialId);
    auto answerA = getLargestArea(field,3,3);

//    auto answerA = playGame(gameInput);

    std::cout << "Advent of code 2018, day 10" << std::endl;
    std::cout << "A: " << answerA.first<< ","<<answerA.second<< std::endl;
//    std::cout << "B: " << answerB << std::endl;


    return 0;
}



std::pair<int,int> getLargestArea(const std::vector<std::vector<int>> &field, int w, int h){
    std::pair<int,int> result;
    int maxSum = -999;
    for (int x=0; x < field.at(0).size()-w;x++)
        for (int y=0; y < field.size()-h;y++){
            int sum = getSumOfArea(field, x, y, 3,3);
            if (sum>maxSum){
                maxSum = sum;
                result = std::make_pair(x+1,y+1);
            };
        }
    return result;
}

int getSumOfArea(const std::vector<std::vector<int>> &field, int startx, int starty, int h, int w){
    int result = 0;
    for (int x=startx; x < startx+w;x++)
        for (int y=starty; y < starty+h;y++){
            result += field.at(y).at(x);
        }
    return result;
}

std::vector<std::vector<int>> fillField(int startx, int starty, int lastx, int lasty, int serialID){
    std::vector<std::vector<int>> result(lasty-starty+1, std::vector<int>(lastx-startx+1,0));
    for (int y=starty; y<= lasty; y++){
        for (int x=startx; x<=lastx;x++){
            result.at(y-starty).at(x-startx) = getPowerLevel(x,y,serialID);
        }
    }
    return result;
}

int getPowerLevel(int x, int y, int serialId){
    int rackID = x+10;
    long long powerlevel = rackID*y;
    powerlevel += serialId;
    powerlevel *= rackID;
    powerlevel = powerlevel -(powerlevel%100);
    powerlevel = (powerlevel%1000 )/100;
    return powerlevel -5;
}

int readFile(const std::string &filename) {
    int result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    fs >> result;

    fs.close();

    return
            result;
}
