//
// Created by jeroen on 14-7-19.
//

#ifndef INC_2018_CONSTELLATION_H
#define INC_2018_CONSTELLATION_H

#include <vector>
#include <string>
#include "Point.h"

class Constellation {
public:
    Constellation(std::string line);
    bool isWithin3Of(const Point& point)const ;
    bool canMergeWith(const Constellation& other)const ;
    void mergeWithOther(Constellation& other);
    bool isEmpty();
    void clearConstellation();
    std::vector<Point> getPoints();
private:
    std::vector<Point> points;


};


#endif //INC_2018_CONSTELLATION_H
