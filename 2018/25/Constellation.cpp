//
// Created by jeroen on 14-7-19.
//

#include "Constellation.h"

void Constellation::mergeWithOther(Constellation &other) {
    for (auto item:other.getPoints()){
        points.push_back(item);
    }
    other.clearConstellation();

}

bool Constellation::isEmpty() {
    return points.size()<=0;
}

void Constellation::clearConstellation() {
    points.clear();
}

std::vector<Point> Constellation::getPoints() {
    return points;
}

bool Constellation::isWithin3Of(const Point &point) const{
    for (auto& item:points){
        if (item.getDist(point)<=3)
            return true;
    }

    return false;
}

bool Constellation::canMergeWith(const Constellation &other)const {
    for (auto & item:points){
        if (other.isWithin3Of(item))
            return true;
    }
    return false;
}

Constellation::Constellation(std::string line) {
    points.push_back(Point(line));

}
