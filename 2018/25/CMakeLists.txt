cmake_minimum_required(VERSION 3.8)
project(2018_25)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp Point.cpp Utils.cpp Constellation.cpp Constellation.h)
add_executable(2018_25 ${SOURCE_FILES})
