#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include "Constellation.h"

std::vector<Constellation> readFile(const std::string &filename);
std::vector<Constellation> mergeConstellations(std::vector<Constellation> constellations);
std::vector<Constellation> mergeAllConstellations(std::vector<Constellation> constellations);

std::vector<Constellation> mergeAllConstellations(std::vector<Constellation> constellations){
    auto constellationsCopy = constellations;
    int newSize = 0;
    int lastSize = constellationsCopy.size();

    while (newSize != lastSize){
        constellationsCopy = mergeConstellations(constellationsCopy);
        lastSize = newSize;
        newSize = constellationsCopy.size();
    }
    return constellationsCopy;
}


std::vector<Constellation> mergeConstellations(std::vector<Constellation> constellations) {
    auto constellationsCopy = constellations;
    std::vector<Constellation> result;
    for (int i=0;i<constellationsCopy.size();i++)
        for (int j=0;j<constellationsCopy.size();j++){
            if (i!=j && !(constellationsCopy.at(i).isEmpty()) && !(constellationsCopy.at(j).isEmpty())){
                if (constellationsCopy.at(i).canMergeWith(constellationsCopy.at(j))){
                    constellationsCopy.at(i).mergeWithOther(constellationsCopy.at(j));
                }
            }
        }

    for (auto& item:constellationsCopy){
        if (!item.isEmpty()){
            result.push_back(item);
        }
    }
    return result;
}

int main(int argc, char *argv[]) {

    auto constellations = readFile("input.txt");
    constellations = mergeAllConstellations(constellations);



    auto answerA = constellations.size();

    std::cout << "Advent of code 2018, day 25" << std::endl;
    std::cout << "A: " << answerA << std::endl;

    //std::cout << "B: " << answerB << std::endl;


    return 0;
}


std::vector<Constellation> readFile(const std::string &filename) {
    std::vector<Constellation> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    while (std::getline(fs,line)){
        result.push_back(Constellation(line));
    }
    fs.close();
    return result;
}
