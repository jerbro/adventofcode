//
// Created by jeroen on 14-7-19.
//

#include "Point.h"
#include "Utils.h"
#include <regex>

Point::Point(std::string str) {

    std::smatch match;
    std::regex re("^\\s*(-?[0-9]+),(-?[0-9]+),(-?[0-9]+),(-?[0-9]+)$");
    std::regex_match(str, match, re);

    this->x = Utils::stringToInt(match[1]);
    this->y = Utils::stringToInt(match[2]);
    this->z = Utils::stringToInt(match[3]);
    this->t = Utils::stringToInt(match[4]);
}

int Point::getDist(const Point &otherpoint) const{
    return abs(x-otherpoint.x)+abs(y-otherpoint.y)+abs(z-otherpoint.z)+abs(t-otherpoint.t);
}
