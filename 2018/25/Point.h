//
// Created by jeroen on 14-7-19.
//

#ifndef INC_2018_POINT_H
#define INC_2018_POINT_H


#include <string>

class Point {
public:
    Point(std::string str) ;
    int getDist(const Point& otherpoint) const;
private:
    int x;
    int y;
    int z;
    int t;

};


#endif //INC_2018_POINT_H
