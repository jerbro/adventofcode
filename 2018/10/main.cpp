#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <regex>


struct pointProps {
    int x;
    int y;
    int vx;
    int vy;
};

struct coord {
    int x;
    int y;
};

std::vector<pointProps> readFile(const std::string &filename);

std::vector<coord> calcCoords(const std::vector<pointProps> &lightPoints, int time);

void getDrawingfieldProps(const std::vector<coord> coords, int *ltx, int *lty, int *brx, int *bry);

void drawLightsAtTime(const std::vector<pointProps> &lightPoints, int time);

bool heightIsBelowMaxHeight(const std::vector<pointProps> &lightPoints, int time, int maxHeight);

int main(int argc, char *argv[]) {

    auto lightPoints = readFile("input.txt");
    int answerB = 0;


//    auto answerA = playGame(gameInput);

    std::cout << "Advent of code 2018, day 10" << std::endl;
    std::cout << "A: See text below" << std::endl;
    for (int i = 0; i < 100000; i++) {
        if (heightIsBelowMaxHeight(lightPoints, i,
                                   15)) {//assume the resulting text is max 1 lines (10 chars) high, so 15 is a safe limit
            drawLightsAtTime(lightPoints, i);
            answerB = i;
        }
    }
    std::cout << "B: " << answerB << std::endl;


    return 0;
}


void drawLightsAtTime(const std::vector<pointProps> &lightPoints, int time) {
    std::vector<coord> coords = calcCoords(lightPoints, time);
    int offsetX, offsetY, height, width;
    getDrawingfieldProps(coords, &offsetX, &offsetY, &height, &width);


    std::vector<std::vector<char>> field(height, std::vector<char>(width, '.'));
    for (auto coord:coords) {
        int xpos = coord.x - offsetX;
        int ypos = coord.y - offsetY;
        field.at(ypos).at(xpos) = '#';
    }

    for (auto line:field) {
        for (auto place:line) {
            std::cout << place;
        }
        std::cout << std::endl;
    }

    std::cout << std::endl << std::endl;

}

std::vector<coord> calcCoords(const std::vector<pointProps> &lightPoints, int time) {
    std::vector<coord> result;
    for (auto point:lightPoints) {
        result.push_back(coord{point.x + point.vx * time, point.y + point.vy * time});
    }
    return result;
}

bool heightIsBelowMaxHeight(const std::vector<pointProps> &lightPoints, int time, int maxHeight) {
    pointProps firstpoint = lightPoints.at(0);
    int miny = firstpoint.y + firstpoint.vy * time;
    int maxy = firstpoint.y + firstpoint.vy * time;

    for (auto point:lightPoints) {
        int currx = point.y + point.vy * time;
        if (currx < miny)
            miny = currx;
        if (currx > maxy)
            maxy = currx;

        if ((maxy - miny) > maxHeight)
            return false;
    }
    return true;
}

void getDrawingfieldProps(const std::vector<coord> coords, int *ltx, int *lty, int *height, int *width) {
    int leftTopX = 999;
    int leftTopY = 999;
    int bottomRightX = -999;
    int bottomRightY = -999;

    for (auto coord:coords) {
        if (coord.x < leftTopX)
            leftTopX = coord.x;
        if (coord.x > bottomRightX)
            bottomRightX = coord.x;
        if (coord.y < leftTopY)
            leftTopY = coord.y;
        if (coord.y > bottomRightY)
            bottomRightY = coord.y;
    }
    *ltx = leftTopX;
    *lty = leftTopY;
    *height = bottomRightY - leftTopY + 1;
    *width = bottomRightX - leftTopX + 1;

}

int stringToInt(const std::string &stringnumber) {
    std::stringstream strstr{stringnumber};
    int result;
    strstr >> result;
    return result;

}

pointProps parseLine(const std::string &line) {
    pointProps result;
    std::smatch match;
    // example of line: position=<-4,  3> velocity=< 2,  0>
    std::regex re("^position=<\\s*(-?[0-9]+),\\s*(-?[0-9]+)> velocity=<\\s*(-?[0-9]+),\\s*(-?[0-9]+)>$");
    std::regex_match(line, match, re);

    result.x = stringToInt(match[1]);
    result.y = stringToInt(match[2]);
    result.vx = stringToInt(match[3]);
    result.vy = stringToInt(match[4]);

    return result;
}

std::vector<pointProps> readFile(const std::string &filename) {
    std::vector<pointProps> result;
    std::fstream fs;
    fs.
            open(filename, std::fstream::openmode::_S_in
    );
    pointProps lightpoint;
    std::string line;
    while (
            std::getline(fs, line
            )) {
        result.
                push_back(parseLine(line)
        );

    }

    fs.

            close();

    return
            result;
}
