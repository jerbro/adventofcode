//
// Created by jeroen on 7-7-19.
//

#include <regex>
#include <boost/algorithm/string.hpp>
#include "ArmyGroup.h"
#include "Utils.h"


void ArmyGroup::parseWeaknessOrImmunitie(const std::string &text) {
/* text can be for example:
 * weak to bludgeoning
 * or
 * immune to slashing, cold, radiation
 * weak are not always both present. Also not always in the same order*/

    std::string delimiters(", ");
    std::vector<std::string> parts;
    std::string trimmedText = boost::trim_copy(text);
    boost::split(parts, trimmedText, boost::is_any_of(delimiters));

    bool typeIsWeakness = false;
    std::set<std::string>* setToStoreProperties  ;
    if (parts.at(0) == "weak"&& parts.at(1) == "to") {
        typeIsWeakness = true;//Else type is not weakness, but immune
        setToStoreProperties = &weaknesses;
    }
    else{
        setToStoreProperties = &immunities;
    }


    for (int i=2;i<parts.size();i++) {
        if (parts.at(i) != "") {
            setToStoreProperties->insert(parts.at(i));
        }
    }
}

void ArmyGroup::parseWeaknessAndImmunities(const std::string &text) {
/* text can be:
 * weak to bludgeoning; immune to slashing, cold, radiation
 * weak are not always both present. Also not always in the same order*/
    std::string delimiters(";");
    std::vector<std::string> parts;
    boost::split(parts, text, boost::is_any_of(delimiters));

    for (const auto& part:parts) {
        parseWeaknessOrImmunitie(part);
    }

}

void ArmyGroup::parseGroupLine(const std::string &line){
    /* Line can be for example:
     * 17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2*/

        std::smatch match;
        std::regex re("^([0-9]+) units each with ([0-9]+) hit points \\(?([^\\)]+)?\\)? ?with an attack that does ([0-9]+) ([a-zA-Z]+) damage at initiative ([0-9]+)$");
        std::regex_match(line, match, re);

        this->numberOfUnits = Utils::stringToInt(match[1]);
        this->hitpointsPerUnit = Utils::stringToInt(match[2]);
        parseWeaknessAndImmunities(match[3]);
        this->attackDamage = Utils::stringToInt(match[4]);
        this->attack_type = (match[5]);
        this->initiative =  Utils::stringToInt(match[6]);
}

ArmyGroup::ArmyGroup(std::string description, int id, std::string parentName):id(id),parentName(parentName),boost(0) {
    /*17 units each with 5390 hit points (weak to radiation, bludgeoning) with
    an attack that does 4507 fire damage at initiative 2*/
    parseGroupLine(description);
}


bool ArmyGroup::groupIsDefeated() const {
    return numberOfUnits <=0;
}

void ArmyGroup::attack(ArmyGroup &enemy) {
    enemy.receiveAttack(attack_type,getEffectivePower());
}

void ArmyGroup::receiveAttack(std::string attackType, int effectivePower) {
    int damage = hypoteticalDamage(attackType,effectivePower);

    int unitsLost = damage/hitpointsPerUnit;
    numberOfUnits -= unitsLost;

    if (numberOfUnits<0)
        numberOfUnits=0;
}

int ArmyGroup::getInitiative() const {
    return initiative;
}



int ArmyGroup::hypoteticalDamage(std::string attackType, int effectivePower) const {
    int damage = effectivePower;
    if (weaknesses.find(attackType) != weaknesses.end()){
        damage *=2;
    }
    if (immunities.find(attackType) != immunities.end()){
        damage = 0;
    }
    return damage;
}

std::string ArmyGroup::getAttackType() const {
    return attack_type;
}

bool ArmyGroup::compareEnemies(ArmyGroup* gr1, ArmyGroup* gr2) const{
    int gr1_damage = gr1->hypoteticalDamage(attack_type,getEffectivePower());
    int gr2_damage = gr2->hypoteticalDamage(attack_type,getEffectivePower());
    if (gr1_damage > gr2_damage)
        return true;

    if (gr1_damage == gr2_damage &&
        gr1->getEffectivePower()> gr2->getEffectivePower())
        return true;

    if (gr1_damage == gr2_damage &&
        gr1->getAttackDamageWithBoost()== gr2->getEffectivePower() &&
        gr1->getInitiative() > gr2->getInitiative())
        return true;

    return false;
}


ArmyGroup *ArmyGroup::selectEnemy(std::vector<ArmyGroup *>& enemies) const{

    if (enemies.empty())
        return nullptr;

 //   auto enemiesVect = enemies;

    std::sort(enemies.begin(),enemies.end(),[this](ArmyGroup* gr1, ArmyGroup* gr2){return this->compareEnemies(gr1,gr2);});

    auto damage = enemies.at(0)->hypoteticalDamage(attack_type,getEffectivePower());
    if (damage<=0)
        return nullptr;

    auto selectedEnemy = enemies.at(0);
    enemies.erase(enemies.begin());
    return selectedEnemy;

}

int ArmyGroup::getEffectivePower() const {
    return numberOfUnits*getAttackDamageWithBoost();
}

int ArmyGroup::getUnitCount() const{
    return numberOfUnits;
}

std::string ArmyGroup::getGroupDescription() {
    std::stringstream strstr;
    strstr << parentName << " group "<<id<<"("<<numberOfUnits<<")" ;
    return strstr.str();
}

int ArmyGroup::getHitpointsPerUnit() {
    return hitpointsPerUnit;
}

void ArmyGroup::setBoost(int boost) {
    this->boost = boost;

}

int ArmyGroup::getAttackDamageWithBoost() const{
    return attackDamage+boost;
}

