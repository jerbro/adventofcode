//
// Created by jeroen on 7-7-19.
//

#include <boost/algorithm/string.hpp>
#include "Army.h"

Army::Army(const std::list<std::string> &armyLines) {
    bool firstLine=true;
    int id=1;
    for (const auto& line:armyLines){
        if (firstLine){
            name = line;
            boost::erase_all(name,":");
        }
        else{
            groups.push_back(ArmyGroup(line,id,name));
            id++;
        }
        firstLine = false;
    }

}

int Army::getNumberOfUnits() {
    int result=0;
    for (auto& item:groups)
    { result += item.getUnitCount();}
    return result;
}

std::vector<ArmyGroup*> Army::getGroups()  {
    std::vector<ArmyGroup*> result;
    for (auto& item:groups){
        result.push_back(&item);
    }
    return result;
}

void Army::removeDefeatedGroups() {
    auto newEnd = std::remove_if(groups.begin(),groups.end(),[]( const ArmyGroup&  grp){return (grp.groupIsDefeated());});
    groups.erase(newEnd,groups.end());
}


bool Army::targetSelectionCompareOrder(const ArmyGroup& gr1, const ArmyGroup& gr2){
    if (gr1.getEffectivePower()> gr2.getEffectivePower())
        return true;

    if (gr1.getEffectivePower() == gr2.getEffectivePower() && gr1.getInitiative()> gr2.getInitiative())
        return true;

    return false;

}

std::vector<std::pair<ArmyGroup* , ArmyGroup *>> Army::findTargetForEveryGroup(Army &enemy)  {
    std::vector<ArmyGroup*> enemies = enemy.getGroups();
    std::vector<std::pair<ArmyGroup* , ArmyGroup *>> result;
    std::vector<ArmyGroup*> groupPtrs;
    groupPtrs = getGroups();


    std::sort(groupPtrs.begin(),groupPtrs.end(),[](ArmyGroup* grpA, ArmyGroup* grpB){return targetSelectionCompareOrder(*grpA, *grpB);});

    for (ArmyGroup* item:groupPtrs){
        ArmyGroup* selectedEnemy = item->selectEnemy(enemies);


        if (selectedEnemy != nullptr){
            result.push_back(std::make_pair(item, selectedEnemy));
        }
    }



    return result;
}

std::string Army::getName() {
    return name;
}

void Army::addBoost(int boost) {
    for (auto& group:groups){
        group.setBoost(boost);
    }

}

void Army::createGroupsBackup() {
    backupGroups.clear();
    for (auto item:groups){
        backupGroups.push_back(item);
    }
}

void Army::restoreGroupsBackup() {
    groups.clear();
    for (auto item:backupGroups){
        groups.push_back(item);
    }
}

