//
// Created by jeroen on 7-7-19.
//

#ifndef INC_2018_ARMY_H
#define INC_2018_ARMY_H

#include <vector>
#include <list>
#include "ArmyGroup.h"

class ArmyGroup;

class Army {
public:
    Army(const std::list<std::string> &armyLines);
    int getNumberOfUnits();
    std::vector<ArmyGroup*> getGroups() ;
    void removeDefeatedGroups();
    std::vector<std::pair<ArmyGroup*,ArmyGroup*>> findTargetForEveryGroup(Army& enemy) ;
    static bool targetSelectionCompareOrder(const ArmyGroup& gr1, const ArmyGroup& gr2);
    std::string getName();
    void addBoost(int boost);
    void createGroupsBackup();
    void restoreGroupsBackup();
private:
    std::string name;
    std::vector<ArmyGroup> groups;
    std::vector<ArmyGroup> backupGroups;
};


#endif //INC_2018_ARMY_H
