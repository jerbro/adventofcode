#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <iomanip>
#include <regex>
#include <boost/algorithm/string.hpp>

#include "ArmyGroup.h"
#include "Army.h"
#include "ArmyFightManager.h"


std::vector<Army> readFile(const std::string &filename);


int main(int argc, char *argv[]) {

    auto armies = readFile("input.txt");
    ArmyFightManager fightManager(armies);



    auto answerA = fightManager.getNrOfUnitsAfterFight();
    auto answerB = fightManager.getNrOfUnitsAfterFightWithLowestBoostFirstArmyWin();


    std::cout << "Advent of code 2018, day 24" << std::endl;
    std::cout << "A: " << answerA<< std::endl;

    std::cout << "B: " << answerB << std::endl;


    return 0;
}



std::vector<Army> readFile(const std::string &filename) {
    std::vector<Army> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    std::list<std::string> armyLines;
    while (std::getline(fs,line)){
        if (line.find(':') != std::string::npos){
             if (armyLines.size()>0){
                 result.push_back(Army(armyLines));
                 armyLines.clear();
             }

        }
        if (line != "") {
            armyLines.push_back(line);
        }

    }
    result.push_back(Army(armyLines));
    fs.close();

    return result;
}
