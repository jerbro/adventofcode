//
// Created by jeroen on 7-7-19.
//

#include <sstream>
#include "Utils.h"

namespace Utils {
    int stringToInt(const std::string &stringnumber) {
        std::stringstream strstr{stringnumber};
        int result;
        strstr >> result;
        return result;

    }
}