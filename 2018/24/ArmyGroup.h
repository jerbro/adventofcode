//
// Created by jeroen on 7-7-19.
//

#ifndef INC_2018_ARMYGROUP_H
#define INC_2018_ARMYGROUP_H


#include <string>
#include <set>

class ArmyGroup {
public:
    ArmyGroup(std::string description, int id, std::string parentName);
    int getEffectivePower() const;
    int getUnitCount() const;
    bool groupIsDefeated() const;
    void attack(ArmyGroup &enemy);
    void receiveAttack(std::string attackType, int effectivePower);
    int getInitiative() const;
    int hypoteticalDamage(std::string attackType, int effectivePower) const;
    std::string getAttackType() const;
    ArmyGroup* selectEnemy(std::vector<ArmyGroup*>& enemies) const;
    std::string getGroupDescription();
    int getHitpointsPerUnit();
    void setBoost(int boost);
private:
    int hitpointsPerUnit;
    int attackDamage;
    int getAttackDamageWithBoost() const;
    int initiative;
    std::string attack_type;
    std::set<std::string> weaknesses;
    std::set<std::string> immunities;
    int numberOfUnits;
    void parseGroupLine(const std::string &line);
    void parseWeaknessAndImmunities(const std::string &text);
    void parseWeaknessOrImmunitie(const std::string &text);
    bool compareEnemies(ArmyGroup* gr1, ArmyGroup* gr2) const;
    std::string parentName;
    int id;
    int boost;
};


#endif //INC_2018_ARMYGROUP_H
