//
// Created by jeroen on 8-7-19.
//

#ifndef INC_2018_ARMYFIGHTMANAGER_H
#define INC_2018_ARMYFIGHTMANAGER_H
#include <vector>
#include "Army.h"

class ArmyFightManager {
public:
    ArmyFightManager(std::vector<Army> armies);
    int getNrOfUnitsAfterFight();
    int getNrOfUnitsAfterFightWithLowestBoostFirstArmyWin();

private:
    std::vector<Army> armies;
    std::vector<std::pair<ArmyGroup*,ArmyGroup*>> battles;
    std::pair<int,int> getNrOfUnitsAndWinnerAfterFightWithBoost(int boost);
    void targetSelection();
    void fight();
    static bool fightOrderCompare(const std::pair<ArmyGroup*,ArmyGroup*> gr1, const std::pair<ArmyGroup*,ArmyGroup*> gr2);
};


#endif //INC_2018_ARMYFIGHTMANAGER_H
