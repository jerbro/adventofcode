//
// Created by jeroen on 7-7-19.
//

#ifndef INC_2018_UTILS_H
#define INC_2018_UTILS_H


#include <string>

namespace Utils {
    int stringToInt(const std::string &stringnumber);


}


#endif //INC_2018_UTILS_H
