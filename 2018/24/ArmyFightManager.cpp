//
// Created by jeroen on 8-7-19.
//

#include "ArmyFightManager.h"
#include <algorithm>
#include <iostream>

ArmyFightManager::ArmyFightManager(std::vector<Army> armies):armies(armies) {

    for (auto& armie:this->armies){
        armie.createGroupsBackup();
        std::cout << "";
    }
}

std::pair<int,int> ArmyFightManager::getNrOfUnitsAndWinnerAfterFightWithBoost(int boost) {
    bool draw=false;
    for (auto& armie:armies){
        armie.restoreGroupsBackup();
    }
    armies.at(0).addBoost(boost);
    bool fightEnded = false;
    while (! fightEnded){
        targetSelection();
        fight();
        if (battles.size() ==0){
            draw=true;
            fightEnded=true;
        }
        armies.at(0).removeDefeatedGroups();
        armies.at(1).removeDefeatedGroups();

        if (armies.at(0).getNumberOfUnits() <=0 || armies.at(1).getNumberOfUnits() <=0)
            fightEnded = true;

    }

    if (draw){
        return (std::make_pair(2,2));
    }
    int winnerNr = 0;
    for (auto &army :armies){

        if (army.getNumberOfUnits()>0) {
            return std::make_pair(army.getNumberOfUnits(),winnerNr);
        }
        winnerNr++;
    }
}



int ArmyFightManager::getNrOfUnitsAfterFight() {
    return  getNrOfUnitsAndWinnerAfterFightWithBoost(0).first;
}



bool targetSelectionCompareDamage(std::string attackType, int effectivePower, const ArmyGroup& gr1, const ArmyGroup& gr2){
    if (gr1.hypoteticalDamage(attackType,effectivePower)> gr2.hypoteticalDamage(attackType,effectivePower))
        return true;

    if (gr1.hypoteticalDamage(attackType,effectivePower)== gr2.hypoteticalDamage(attackType,effectivePower) &&
        gr1.getEffectivePower() > gr2.getEffectivePower())
        return true;

    if (gr1.hypoteticalDamage(attackType,effectivePower)== gr2.hypoteticalDamage(attackType,effectivePower) &&
        gr1.getEffectivePower() == gr2.getEffectivePower() &&
        gr1.getInitiative() > gr2.getInitiative())
        return true;

    return false;

}

bool ArmyFightManager::fightOrderCompare(const std::pair<ArmyGroup*,ArmyGroup*> gr1, const std::pair<ArmyGroup*,ArmyGroup*> gr2)
{
    return ((gr1.first->getInitiative()) >  (gr2.first->getInitiative()));

}

void ArmyFightManager::targetSelection() {
    auto fights0_1 = armies.at(0).findTargetForEveryGroup(armies.at(1));
    auto fighst1_0 = armies.at(1).findTargetForEveryGroup(armies.at(0));
    auto allFights = fights0_1;
    allFights.insert(allFights.end(),fighst1_0.begin(),fighst1_0.end());
    std::sort(allFights.begin(),allFights.end(),fightOrderCompare);
    battles = allFights;

}

void ArmyFightManager::fight() {
    for (auto& battle: battles){
        battle.first->attack( *(battle.second) );
    }
}


int ArmyFightManager::getNrOfUnitsAfterFightWithLowestBoostFirstArmyWin() {
    int boost = 0;

    while (true){
        auto fightResult = getNrOfUnitsAndWinnerAfterFightWithBoost(boost);
        if (fightResult.second == 0) {
            return fightResult.first;
        }
        boost++;

    }

}
