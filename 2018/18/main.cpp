#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <regex>
#include <iomanip>

struct Stats{
    int tree;
    int lumber;
    int empty;
};

Stats getSurroundingStats(const std::vector<std::string>& area, int x, int y);
Stats getTotalStats(const std::vector<std::string>& area);
std::vector<std::string> getSituationAfterMinute(const std::vector<std::string>& originalArea);
long getResourceValueAfterTime(const std::vector<std::string>& originalArea, int time );

std::vector<std::string> readFile(const std::string &filename);

int main(int argc, char *argv[]) {

    auto area = readFile("input.txt");
    auto answerA = getResourceValueAfterTime(area, 10);
    auto answerB = getResourceValueAfterTime(area, 1000000000);



//    auto answerA = getLast10DigitsAfterElvesBakery(input);
//    auto answerB = getNrOfScoresLeftFromScore(input);



    std::cout << "Advent of code 2018, day 18" << std::endl;
    std::cout << "A: " << answerA<< std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

void printArea(const std::vector<std::string>& area){
    for (auto line:area){
        std::cout << line << std::endl;
    }
    std::cout << std::endl<<std::endl;

}

int getPositionOfValInVector(const std::vector<std::vector<std::string>>& source, const std::vector<std::string>& searchValue){
    for (int i=0;i<source.size();i++){
        if (source.at(i) == searchValue)
            return i;
    }
    return -1;
}

long getResourceValueAfterTime(const std::vector<std::string>& originalArea, int time ){
    std::vector<std::vector<std::string>> foundAreas;
    bool repetitionFound = false;
    int repetitionStart;
    int repetitionNext;
    auto area = originalArea;
    foundAreas.push_back(area);
    for (int i=0;i<time;i++){
        area = getSituationAfterMinute(area);
        int posInVect = getPositionOfValInVector(foundAreas,area);
        if ( posInVect>=0) {
            repetitionFound = true;
            repetitionNext = foundAreas.size();
            repetitionStart = posInVect;
            break;
        }
        else
            foundAreas.push_back(area);
    }

    if (repetitionFound){
        int findId = time - repetitionStart;
        findId %= (repetitionStart-repetitionNext);
        area = foundAreas.at(repetitionStart+findId);


    }



    auto endStats = getTotalStats(area);
    return endStats.tree * endStats.lumber;
}

bool isInArea(const std::vector<std::string>& area, int x, int y){
    return (x >= 0 &&
    y >= 0 &&
    y < area.size() &&
    x < area.at(y).size());
}

void addTypeToStats(Stats& stats,char type ){

        switch (type){
            case '|':
                stats.tree++;
                break;
            case '#':
                stats.lumber++;
                break;
            case '.':
                stats.empty++;
        }
}

void addCellValueToStats(Stats& stats, const std::vector<std::string>& area, int x, int y ){
    if (isInArea(area,x,y)){
        char type = area.at(y).at(x);
        switch (type){
            case '|':
                stats.tree++;
                break;
            case '#':
                stats.lumber++;
                break;
            case '.':
                stats.empty++;
        }
    }
}
Stats getSurroundingStats(const std::vector<std::string>& area, int x, int y){
    Stats result{0,0,0};
    //Get value of the 8 surrounding cells
    addCellValueToStats(result,area,x-1,y-1);
    addCellValueToStats(result,area,x-1,y);
    addCellValueToStats(result,area,x-1,y+1);
    addCellValueToStats(result,area,x,y-1);
    addCellValueToStats(result,area,x,y+1);
    addCellValueToStats(result,area,x+1,y-1);
    addCellValueToStats(result,area,x+1,y);
    addCellValueToStats(result,area,x+1,y+1);
    return result;
}

Stats getTotalStats(const std::vector<std::string>& area){
    Stats result {0,0,0};
    for (auto line:area)
        for (auto ch:line){
            addTypeToStats(result,ch);
        }
    return result;
}

char getNextState(const char currState, const Stats& surroundingstats){
    char result;

/*    An open acre will become filled with trees if three or more adjacent acres contained trees. Otherwise, nothing happens.
            An acre filled with trees will become a lumberyard if three or more adjacent acres were lumberyards. Otherwise, nothing happens.
            An acre containing a lumberyard will remain a lumberyard if it was adjacent to at least one other lumberyard and at least one acre containing trees. Otherwise, it becomes open.
*/
    switch( currState){
        case '|':
            if (surroundingstats.lumber >=3)
                result = '#';
            else
                result = currState;
            break;

        case '#':
            if (surroundingstats.lumber >=1 && surroundingstats.tree >=1)
                result = '#';
            else
                result = '.';
            break;
        case '.':
            if (surroundingstats.tree >=3)
                result = '|';
            else
                result = currState;
            break;

    }
    return result;
}

std::vector<std::string> getSituationAfterMinute(const std::vector<std::string>& area){
    std::vector<std::string> result;

    for (int y=0;y<area.size();y++) {
        std::string newLineState;
        for (int x = 0; x < area.at(y).size(); x++) {
            auto surroundingStats = getSurroundingStats(area,x,y);
            newLineState.push_back(getNextState(area.at(y).at(x),surroundingStats));
        }
        result.push_back(newLineState);
    }
    return result;
}

std::vector<std::string> readFile(const std::string &filename) {
    std::vector<std::string> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);
    std::string line;
    while (getline(fs,line))
        result.push_back(line);
    fs.close();

    return result;
}
