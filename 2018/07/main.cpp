#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <regex>
#include <iomanip>


std::map<char, std::set<char>> readFile(const std::string &filename);

std::set<char> getTaskIds(const std::map<char, std::set<char>> &taskDependencies);

std::string getSolutionOrder(const std::map<char, std::set<char>> &taskDependencies);

std::set<char> getSolvableTasks(const std::map<char, std::set<char>> &dependencies, std::set<char> solvedTasks,
                                std::set<char> unhandledTasks);

bool
isSolvable(const char stepID, const std::set<char> &solvedSteps, const std::map<char, std::set<char>> &dependencies);

int getTimeToSolveAllTasks(const std::map<char, std::set<char>> &dependencies, int workerCnt, int defaultTime);

//A class for a Worker on a task.
class TaskWorker {
public :
    TaskWorker(int tasktime) : defaultTaskTime(tasktime), taskId('.') ,timeTicksLeft(-1){};

    bool taskIsFinished() { return timeTicksLeft <= 0; };

    void doTimeTick() {
        timeTicksLeft--;
    }

    void startTask(char newTaskId) {
        timeTicksLeft = newTaskId - 'A' + defaultTaskTime + 1;
        taskId = newTaskId;
    }

    char getLastTaskId() {
        return taskId;
    }

private:
    int timeTicksLeft;
    char taskId;
    int defaultTaskTime;

};

int main(int argc, char *argv[]) {
    auto taskDeps = readFile("input.txt");

    auto answerA = getSolutionOrder(taskDeps);
    auto answerB = getTimeToSolveAllTasks(taskDeps, 5, 60);

    std::cout << "Advent of code 2018, day 07" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}

bool allWorkersFinished(std::vector<TaskWorker *> workers){
    for (auto & worker:workers){
        if (!worker->taskIsFinished())
            return false;
    }
    return true;

}

int getTimeToSolveAllTasks(const std::map<char, std::set<char>> &dependencies, int workerCnt, int defaultTime) {
    //1) getSolvableTasks() // Get the steps than can be solved now
    //2) if there are free elves, give every elf (if available) a task (if available)
    //3)  update elf-tasks with 1 timetick
    //4) check tasks update solved tasks
    //5)  increase timecounter
    //6) where there are unsolved steps left, or workers have not finished start over again

    std::vector<TaskWorker *> workers;
    int tickcount = 0;

    for (int i = 0; i < workerCnt; i++) {
        TaskWorker *worker = new TaskWorker(defaultTime);
        workers.push_back(worker);
    }

    std::set<char> unhandledTasks = getTaskIds(dependencies);
    std::set<char> solvedTasks;
    //6) where there are unsolved steps left, or workers have not finished start over again
    while (!unhandledTasks.empty() || !allWorkersFinished(workers)) {
        //1) getSolvableTasks() // Get the steps than can be solved now
        std::set<char> solvableTasks = getSolvableTasks(dependencies, solvedTasks, unhandledTasks);
        //2) if there are free elves, give every elf (if available) a task (if available)
        for (auto &worker:workers) {
            if (worker->taskIsFinished() && !solvableTasks.empty()) {
                char taskToStart = *(solvableTasks.begin());
                worker->startTask(taskToStart);
                unhandledTasks.erase(taskToStart);
                solvableTasks.erase(taskToStart);
            }
        }

        //3)  update elf-tasks with 1 timetick
        //4) check tasks update solved tasks
        for (auto &worker:workers) {
            worker->doTimeTick();
            if (worker->taskIsFinished() && worker->getLastTaskId() != 0) {
                if (solvedTasks.find(worker->getLastTaskId()) == solvedTasks.end()) {
                    solvedTasks.insert(worker->getLastTaskId());
                }

            }
        }
        //5)  increase timecounter
        tickcount++;

    }

    return tickcount;

}

std::set<char> getSolvableTasks(const std::map<char, std::set<char>> &dependencies, std::set<char> solvedTasks,
                                std::set<char> unhandledTasks) {
    std::set<char> result;

    if (unhandledTasks.empty()) // If there are no unhandled tasks, return empty set
        return result;

    for (char currId : unhandledTasks) {
        if (isSolvable(currId, solvedTasks, dependencies))
            result.insert(currId);
    }
    return result;

}


bool
isSolvable(const char stepID, const std::set<char> &solvedSteps, const std::map<char, std::set<char>> &dependencies) {
    auto currStep = dependencies.find(stepID);
    if ((currStep->second.size() == 0) || (currStep == dependencies.end()))
        return true;
    for (auto dep: currStep->second) {
        if (solvedSteps.find(dep) == solvedSteps.end())
            return false; //If current dependency is not solved, current step is not solvable
    }
    return true;
}

std::string getSolutionOrder(const std::map<char, std::set<char>> &taskDependencies) {
    std::string result;
    std::set<char> unresolvedTaskIDs = getTaskIds(taskDependencies);
    std::set<char> solvedTasksIDs;

    while (unresolvedTaskIDs.size() > 0) {
        for (auto stepID: unresolvedTaskIDs) {
            if (isSolvable(stepID, solvedTasksIDs, taskDependencies)) {
                solvedTasksIDs.insert(stepID);
                unresolvedTaskIDs.erase(stepID);
                result += stepID;
                break;
            }
        }
    }

    return result;
}


std::set<char> getTaskIds(const std::map<char, std::set<char>> &taskDependencies) {
    std::set<char> result;
    for (auto task:taskDependencies) {
        result.insert(task.first);
        for (auto dep: task.second) {
            result.insert(dep);
        }
    }
    return result;
}

std::pair<char, char> parseLine(const std::string &line) {
    std::smatch match;
    std::regex re("^Step (.) must be finished before step (.) can begin\\.$");
    std::regex_match(line, match, re);

    std::string ch1 = match[2];
    std::string ch2 = match[1];

    return std::make_pair(ch1[0], ch2[0]);
}

//read the file, and write to a vector of coordinates.
std::map<char, std::set<char>> readFile(const std::string &filename) {
    std::map<char, std::set<char>> result;

    std::string line;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    while (std::getline(fs, line)) {

        std::pair<char, char> dependencPair = parseLine(line);
        if (result.find(dependencPair.first) == result.end())
            result.insert(std::make_pair(dependencPair.first, std::set<char>{dependencPair.second}));
        else {
            result.find(dependencPair.first)->second.insert(dependencPair.second);
        }
    }

    fs.close();
    return result;
}
