//
// Created by jeroen on 18-12-18.
//

#include <sstream>
#include "Processor.h"
#include "Instruction.h"

Processor::Processor() {
    while (registers.size() < 6){
        registers.push_back(0);
    }
    registerCommands();
}

Processor::Processor(std::vector<std::string> code){
    while (registers.size() < 6){
    registers.push_back(0);
    }
    registerCommands();
    instructionPointerRegister=-1;
    loadProgram(code);

}

void Processor::run(){
    while (registers.at(instructionPointerRegister)< programCode.size()){
        programCode.at(registers.at(instructionPointerRegister))->execute();
        registers.at(instructionPointerRegister)++;
    }
}

void Processor::registerCommands(){
    if (commands.empty()) {
        commands.push_back(new cmdAddr(&registers));
        commands.push_back(new cmdAddi(&registers));

        commands.push_back(new cmdMulr(&registers));
        commands.push_back(new cmdMuli(&registers));

        commands.push_back(new cmdBanr(&registers));
        commands.push_back(new cmdBani(&registers));

        commands.push_back(new cmdBorr(&registers));
        commands.push_back(new cmdBori(&registers));

        commands.push_back(new cmdSetr(&registers));
        commands.push_back(new cmdSeti(&registers));

        commands.push_back(new cmdGtri(&registers));
        commands.push_back(new cmdGtir(&registers));
        commands.push_back(new cmdGtrr(&registers));

        commands.push_back(new cmdEqri(&registers));
        commands.push_back(new cmdEqir(&registers));
        commands.push_back(new cmdEqrr(&registers));

//Extra own-invented commands
        commands.push_back(new cmdModr(&registers));
        commands.push_back(new cmdNop(&registers));
        commands.push_back(new cmdNeir(&registers));//Not equal immediate register
    }
}

void Processor::setRegisterState(std::vector<long long> registerVals){
    registers.clear();
    for (auto val:registerVals)
        registers.push_back(val);
}

std::vector<long long> Processor::getRegisterState(){
    return registers;
}

void Processor::runSingleInstruction(std::string asmInstruction){
    Instruction* instr = parseInstruction(asmInstruction);
    instr->execute();
}

void Processor::runSingleInstruction(int instructionId, long long oper1, long long oper2, long long oper3){
    Instruction instr = Instruction(commands.at(instructionId), oper1, oper2, oper3);
    instr.execute();
}

std::vector<std::string> Processor::getSupportedInstructions(){
    std::vector<std::string> result;
    for (auto cmd:commands){
        result.push_back(cmd->getInstructionName());
    }
    return result;
}

Instruction* Processor::parseInstruction(std::string instruction) {
    std::stringstream instruction_strstr{instruction};
    std::string instrname;
    long long oper1;
    long long oper2;
    long long oper3;
    instruction_strstr >> instrname>>oper1>>oper2>>oper3;

    Command* foundCmd;
    for (auto cmd:commands)
    {
        if (cmd->getInstructionName() == instrname)
            foundCmd = cmd;
    }
    return new Instruction(foundCmd,oper1,oper2,oper3);
}

void Processor::handleDerective(std::string line){
    std::stringstream instruction_strstr{line};
    std::string derictiveName;
    long long oper1;
    instruction_strstr >> derictiveName>>oper1;
    if (derictiveName == "#ip")
        this->instructionPointerRegister =  oper1;
}

void Processor::loadProgram(std::vector<std::string> program){
    programCode.erase(programCode.begin(),programCode.end());
    for (auto line:program){
        if (line.at(0) == '#') //No instruction. Parse as derective
            handleDerective(line);
        else {
            Instruction* instr = parseInstruction(line);
            programCode.push_back(instr);
        }
    }

}