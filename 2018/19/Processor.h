//
// Created by jeroen on 18-12-18.
//

#ifndef INC_2018_PROCESSOR_H
#define INC_2018_PROCESSOR_H
#include <vector>
#include "Command.h"
#include "Instruction.h"


class Processor {

public:
    Processor();
    Processor(std::vector<std::string> code);
    void setRegisterState(std::vector<long long> registerVals);
    std::vector<long long> getRegisterState();
    void runSingleInstruction(std::string asmInstruction);
    void runSingleInstruction(int instructionId, long long oper1, long long oper2, long long oper3);
    std::vector<std::string> getSupportedInstructions();
    void run();
    void handleDerective(std::string line);
private:
    std::vector<long long> registers;
    std::vector<Command*> commands;
    void registerCommands();
    std::vector<Instruction*> programCode;
    int instructionPointerRegister;
    void loadProgram(std::vector<std::string> code);
    Instruction* parseInstruction(std::string instruction);

};


#endif //INC_2018_PROCESSOR_H
