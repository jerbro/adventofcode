//
// Created by jeroen on 18-12-18.
//

#include "Command.h"

Command::Command(std::string commandName, std::vector<long long>* registers) : commandName(commandName), registers(registers) {};

std::string Command::getInstructionName() { return commandName; }

cmdAddr::cmdAddr( std::vector<long long>* registers) : Command("addr",registers) {};

void cmdAddr::execute(long long oper1, long long oper2, long long oper3) {
    registers->at(oper3) = registers->at(oper1)+registers->at(oper2);
}


cmdAddi::cmdAddi( std::vector<long long>* registers) : Command("addi",registers) {};

void cmdAddi::execute(long long oper1, long long oper2, long long oper3) {
    registers->at(oper3) = registers->at(oper1)+oper2;
}


cmdMulr::cmdMulr( std::vector<long long>* registers) : Command("mulr",registers) {};

void cmdMulr::execute(long long oper1, long long oper2, long long oper3) {
    registers->at(oper3) = registers->at(oper1)*registers->at(oper2);
}


cmdMuli::cmdMuli( std::vector<long long>* registers) : Command("muli",registers) {};

void cmdMuli::execute(long long oper1, long long oper2, long long oper3) {
    registers->at(oper3) = registers->at(oper1) * oper2;
}


cmdBanr::cmdBanr( std::vector<long long>* registers) : Command("banr",registers) {};

void cmdBanr::execute(long long oper1, long long oper2, long long oper3) {
    registers->at(oper3) = registers->at(oper1) & registers->at(oper2);
}


cmdBani::cmdBani( std::vector<long long>* registers) : Command("bani",registers) {};

void cmdBani::execute(long long oper1, long long oper2, long long oper3) {
    registers->at(oper3) = registers->at(oper1) & oper2;
}


cmdBorr::cmdBorr( std::vector<long long>* registers) : Command("borr",registers) {};

void cmdBorr::execute(long long oper1, long long oper2, long long oper3) {
    registers->at(oper3) = registers->at(oper1) | registers->at(oper2);
}


cmdBori::cmdBori( std::vector<long long>* registers) : Command("bori",registers) {};

void cmdBori::execute(long long oper1, long long oper2, long long oper3) {
    registers->at(oper3) = registers->at(oper1) | oper2;
}



cmdSetr::cmdSetr( std::vector<long long>* registers) : Command("setr",registers) {};

void cmdSetr::execute(long long oper1, long long oper2, long long oper3) {
    registers->at(oper3) = registers->at(oper1);
}


cmdSeti::cmdSeti( std::vector<long long>* registers) : Command("seti",registers) {};

void cmdSeti::execute(long long oper1, long long oper2, long long oper3) {
    registers->at(oper3) = oper1;
}


cmdGtir::cmdGtir( std::vector<long long>* registers) : Command("gtir",registers) {};

void cmdGtir::execute(long long oper1, long long oper2, long long oper3) {
    if (oper1 > registers->at(oper2))
        registers->at(oper3) = 1;
    else
        registers->at(oper3) = 0;
}

cmdGtri::cmdGtri( std::vector<long long>* registers) : Command("gtri",registers) {};

void cmdGtri::execute(long long oper1, long long oper2, long long oper3) {
    if (registers->at(oper1) > oper2)
        registers->at(oper3) = 1;
    else
        registers->at(oper3) = 0;
}


cmdGtrr::cmdGtrr( std::vector<long long>* registers) : Command("gtrr",registers) {};

void cmdGtrr::execute(long long oper1, long long oper2, long long oper3) {
    if (registers->at(oper1) >  registers->at(oper2))
        registers->at(oper3) = 1;
    else
        registers->at(oper3) = 0;
}

cmdEqir::cmdEqir( std::vector<long long>* registers) : Command("eqir",registers) {};

void cmdEqir::execute(long long oper1, long long oper2, long long oper3) {
    if (oper1 == registers->at(oper2))
        registers->at(oper3) = 1;
    else
        registers->at(oper3) = 0;
}

cmdEqri::cmdEqri( std::vector<long long>* registers) : Command("eqri",registers) {};

void cmdEqri::execute(long long oper1, long long oper2, long long oper3) {
    if (registers->at(oper1) == oper2)
        registers->at(oper3) = 1;
    else
        registers->at(oper3) = 0;
}


cmdEqrr::cmdEqrr( std::vector<long long>* registers) : Command("eqrr",registers) {};

void cmdEqrr::execute(long long oper1, long long oper2, long long oper3) {
    if (registers->at(oper1) ==  registers->at(oper2))
        registers->at(oper3) = 1;
    else
        registers->at(oper3) = 0;
}

cmdModr::cmdModr( std::vector<long long>* registers) : Command("modr",registers) {};

void cmdModr::execute(long long oper1, long long oper2, long long oper3) {
    registers->at(oper3) = registers->at(oper1)%registers->at(oper2);
}


cmdNop::cmdNop( std::vector<long long>* registers) : Command("nop",registers) {};

void cmdNop::execute(long long oper1, long long oper2, long long oper3) {

}

cmdNeir::cmdNeir( std::vector<long long>* registers) : Command("neir",registers) {};

void cmdNeir::execute(long long oper1, long long oper2, long long oper3) {
    if (oper1 != registers->at(oper2))
        registers->at(oper3) = 1;
    else
        registers->at(oper3) = 0;
}

