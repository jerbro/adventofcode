#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <iomanip>
#include "Instruction.h"
#include "Command.h"
#include "Processor.h"
#include <boost/algorithm/string.hpp>


std::vector<std::string> readFile(const std::string &filename);
long long getRegister0AfterRunningProgram(std::vector<std::string> program);
long long getRegister0AfterRunningProgramStartWith1Reg0(std::vector<std::string> program);

int main(int argc, char *argv[]) {

    auto program = readFile("input.txt");
    auto optimizedProgram = readFile("input_manual_optimized.txt");

    auto answerA = getRegister0AfterRunningProgram(program);
    auto answerB = getRegister0AfterRunningProgramStartWith1Reg0(optimizedProgram);


    std::cout << "Advent of code 2018, day 19" << std::endl;
    std::cout << "A: " << answerA<< std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

long long getRegister0AfterRunningProgram(std::vector<std::string> program){

    auto proc = Processor(program);
    proc.run();
    auto registers = proc.getRegisterState();
    return registers.at(0);
}


long long getRegister0AfterRunningProgramStartWith1Reg0(std::vector<std::string> program){

    auto proc = Processor(program);
    auto registers = proc.getRegisterState();
    registers.at(0) = 1;
    proc.setRegisterState(registers);
    proc.run();
    registers = proc.getRegisterState();
    return registers.at(0);
}

int stringToInt(const std::string &stringnumber) {
    std::stringstream strstr{stringnumber};
    int result;
    strstr >> result;
    return result;

}

std::vector<std::string> readFile(const std::string &filename) {
    std::vector<std::string> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    while (std::getline(fs,line))
        result.push_back(line);
    fs.close();

    return result;
}
