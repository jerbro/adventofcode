//
// Created by jeroen on 18-12-18.
//

#ifndef INC_2018_COMMAND_H
#define INC_2018_COMMAND_H


#include <string>
#include <vector>

class Command {

private:
    std::string commandName;
    int commandId;

protected:
    std::vector<long long>* registers;
public:
    Command(std::string commandName, std::vector<long long>* registers);
    virtual void execute(long long  oper1, long long  oper2, long long  oper3)=0;
    std::string getInstructionName();

};

//addition
class cmdAddr:public Command{
public:
    cmdAddr( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};

class cmdAddi:public Command{
public:
    cmdAddi( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};


//multiplication
class cmdMulr:public Command{
public:
    cmdMulr( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};

class cmdMuli:public Command{
public:
    cmdMuli( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};

//Bitwise and
class cmdBanr:public Command{
public:
    cmdBanr( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};

class cmdBani:public Command{
public:
    cmdBani( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};

//Bitwise or
class cmdBorr:public Command{
public:
    cmdBorr( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};

class cmdBori:public Command{
public:
    cmdBori( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};

//Assignment
class cmdSetr:public Command{
public:
    cmdSetr( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};

class cmdSeti:public Command{
public:
    cmdSeti( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};


//Greater-than testing
class cmdGtir:public Command{
public:
    cmdGtir( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};

class cmdGtri:public Command{
public:
    cmdGtri( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};

class cmdGtrr:public Command{
public:
    cmdGtrr( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};


//Equality testing
class cmdEqir:public Command{
public:
    cmdEqir( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};

class cmdEqri:public Command{
public:
    cmdEqri( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};

class cmdEqrr:public Command{
public:
    cmdEqrr( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};

class cmdModr:public Command{
public:
    cmdModr( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};

class cmdNop:public Command{
public:
    cmdNop( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};

class cmdNeir:public Command{
public:
    cmdNeir( std::vector<long long>* registers);
    void execute(long long  oper1, long long  oper2, long long  oper3);
};
#endif //INC_2018_COMMAND_H
