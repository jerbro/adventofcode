#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <iomanip>
#include "Instruction.h"
#include "Command.h"
#include "Processor.h"
#include <boost/algorithm/string.hpp>


struct SampleEffect{
    std::vector<long long> before;
    std::vector<long long> code;
    std::vector<long long > after;
};

std::vector<std::string> readFile(const std::string &filename) ;
std::vector<SampleEffect> getExamples(std::vector<std::string> data);
std::vector<std::vector<long long>>  getProgram(std::vector<std::string> data);
int getSamplesWith3PlusValidOpcodes(std::vector<SampleEffect> samples);
std::map<int,std::vector<int>> getPossibleInstructionidPerOpcode(std::vector<SampleEffect> samples);
std::map<int,int> solveHorizontal(std::map<int,std::vector<int>>& possibilities);
long long getRegister0AfterRunningProgram(std::vector<std::vector<long long>> program, std::map<int,int> instructionConversion);

int main(int argc, char *argv[]) {

    auto input = readFile("input.txt");
    auto examples = getExamples(input);
    auto program = getProgram(input);
    auto answerA = getSamplesWith3PlusValidOpcodes(examples);
    std::cout << "NUmber of examples: "<<examples.size()<<std::endl;
    auto possibleInstrPerOpcode = getPossibleInstructionidPerOpcode(examples);
    auto instrIdByOpcode = solveHorizontal(possibleInstrPerOpcode);
    auto answerB = getRegister0AfterRunningProgram( program,  instrIdByOpcode);


    std::cout << "Advent of code 2018, day 16" << std::endl;
    std::cout << "A: " << answerA<< std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

long long getRegister0AfterRunningProgram(std::vector<std::vector<long long>> program, std::map<int,int> instructionConversion){

    auto proc = Processor();
    for (auto instruction:program){
        auto convertedInstructionId = instructionConversion.find(instruction.at(0))->second;
        proc.runSingleInstruction(convertedInstructionId,instruction.at(1),instruction.at(2),instruction.at(3));
    }
    auto registers = proc.getRegisterState();
    return registers.at(0);
}

int getSamplesWith3PlusValidOpcodes(std::vector<SampleEffect> samples){
    int result = 0;

    auto proc = Processor();

    for (auto sample:samples){

        int validOpcodes = 0;
        for (int i=0;i<15;i++){
            proc.setRegisterState(sample.before);
            proc.runSingleInstruction(i,sample.code.at(1),sample.code.at(2),sample.code.at(3));
            if (proc.getRegisterState() == sample.after)
                validOpcodes ++;
        }

        if (validOpcodes>=3)
            result ++;

    }
    return result;
}


std::map<int,int> solveHorizontal(std::map<int,std::vector<int>>& possibilities){
    std::map<int,int> result;
    std::set<int> foundOpcodes;
    for (auto it=possibilities.begin(); it != possibilities.end();){
        if ((*it).second.size() == 1){
            result.insert(std::make_pair((*it).first, (*it).second.at(0)));
            foundOpcodes.insert((*it).second.at(0));
            possibilities.erase(it++);
        }
        else{
            it++;
        }

    }

    for (auto &item:possibilities){
        auto newEnd = std::remove_if(item.second.begin(),item.second.end(),[foundOpcodes](const int& nr){return foundOpcodes.find(nr) != foundOpcodes.end();});
        item.second.erase(newEnd, item.second.end());
    }

    if (result.size() > 0){
        auto extraResult =solveHorizontal( possibilities);
        result.insert( extraResult.begin(),extraResult.end());
    }
    return result;
}


std::map<int,std::vector<int>> getPossibleInstructionidPerOpcode(std::vector<SampleEffect> samples){
    std::map<int,int> result; // First= instructionId in processor; second = instructionId in example;
    std::map<int,std::vector<int>> possibilities; // Set with the different CPU-instruction possibilities that are compatible with the given sample-insturction

    auto proc = Processor();
    std::vector<std::string> instructions =proc.getSupportedInstructions();

    for (int i=0;i<16;i++) {
        std::vector<int> pos;
        for (int j = 0; j < 16; j++) {
            pos.push_back(j);
        }
        possibilities.insert(std::make_pair(i,pos));
    }

    for (auto sample:samples){

        int validOpcodes = 0;
        std::set<int> possibleCpuCodes;
        for (int i=0;i<16;i++){
            proc.setRegisterState(sample.before);
            proc.runSingleInstruction(i,sample.code.at(1),sample.code.at(2),sample.code.at(3));
            if (proc.getRegisterState() == sample.after) {
                validOpcodes++;
                possibleCpuCodes.insert(i);
            }
        }

        auto currItem = possibilities.find(sample.code.at(0));
        auto new_end = std::remove_if(currItem->second.begin(), currItem->second.end(),
                                      [possibleCpuCodes](const int& nr) {return (possibleCpuCodes.find(nr) == possibleCpuCodes.end()) ; });
        currItem->second.erase(new_end, currItem->second.end());

    }

    return possibilities;
}


int stringToInt(const std::string &stringnumber) {
    std::stringstream strstr{stringnumber};
    int result;
    strstr >> result;
    return result;

}

std::vector<long long> getBeforeData(std::string line){
    //example of before-line:
    //Before: [3, 3, 1, 2]
    std::string delimiters(", []");
    std::vector<std::string> parts;
    boost::split(parts, line, boost::is_any_of(delimiters));

    auto newEnd = std::remove_if(parts.begin(),parts.end(),[](const std::string& str){return str.size() == 0;});
    parts.erase(newEnd, parts.end()); // remove empty strings from result

    //example of before-line:
    //Before: [3, 3, 1, 2]
    std::vector<long long> result;
    if (parts.size() ==5 && parts.at(0) == "Before:"){
        result.push_back(stringToInt(parts.at(1)));
        result.push_back(stringToInt(parts.at(2)));
        result.push_back(stringToInt(parts.at(3)));
        result.push_back(stringToInt(parts.at(4)));
    }
    return result;
}

std::vector<long long> getCodeData(std::string line){
    //example of code-line:
    //3 2 3 2
    std::string delimiters(" ");
    std::vector<std::string> parts;
    boost::split(parts, line, boost::is_any_of(delimiters));

    std::vector<long long> result;
    if (parts.size() ==4 ){
        result.push_back(stringToInt(parts.at(0)));
        result.push_back(stringToInt(parts.at(1)));
        result.push_back(stringToInt(parts.at(2)));
        result.push_back(stringToInt(parts.at(3)));
    }
    return result;

}


std::vector<long long> getAfterData(std::string line){
    //example of after-line:
    //After: [3, 3, 1, 2]
    std::string delimiters(", []");
    std::vector<std::string> parts;
    boost::split(parts, line, boost::is_any_of(delimiters));

    auto newEnd = std::remove_if(parts.begin(),parts.end(),[](const std::string& str){return str.size() == 0;});
    parts.erase(newEnd, parts.end()); // remove empty strings from result

    std::vector<long long> result;
    if (parts.size() ==5 && parts.at(0) == "After:"){
        result.push_back(stringToInt(parts.at(1)));
        result.push_back(stringToInt(parts.at(2)));
        result.push_back(stringToInt(parts.at(3)));
        result.push_back(stringToInt(parts.at(4)));
    }
    return result;
}


std::vector<SampleEffect> getExamples(std::vector<std::string> data){
    std::vector<long long> before;
    std::vector<long long> code;
    std::vector<long long> after;
    std::vector<SampleEffect> result;

    int state=1; //1=line = before ; 2=line = code; 3= line = after; 0 = not valid for any of those (or not in correct order)

    for (auto line:data){
        switch(state){
            case 0:
            case 1:
                before = getBeforeData(line);
                if (before.size()>0){
                    state = 2;
                }
                break;
            case 2:
                code = getCodeData(line);
                if (code.size()>0){
                    state = 3;
                }
                break;
            case 3:
                after = getAfterData(line);
                if (after.size()>0){
                    state = 0;
                    result.push_back(SampleEffect{before,code,after});
                }
                break;
        }

    }
    return result;
}

std::vector<std::vector<long long>> getProgram(std::vector<std::string> data){
    std::vector<std::vector<long long>> result;
    int state=1; //1=line = before ; 2=line = code; 3= line = after; 4=programdata (after the examples) 0 = not valid for any of those (or not in correct order)
    std::vector<long long> code;
    for (auto line:data){
        switch(state){
            case 0:
            case 1:
                if (getBeforeData(line).size()>0){
                    state = 2;
                }
                code = getCodeData(line);
                if (code.size()>0){
                    state = 4;
                    result.push_back(code);
                }
                break;
            case 2:
                if (getCodeData(line).size()>0){
                    state = 3;
                }
                break;
            case 3:

                if (getAfterData(line).size()>0){
                    state = 0;
                }
                break;
            case 4:
                code = getCodeData(line);
                if (code.size()>0){
                    state = 4;
                    result.push_back(code);
                }
        }

    }
    return result;
}

std::vector<std::string> readFile(const std::string &filename) {
    std::vector<std::string> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    while (std::getline(fs,line))
        result.push_back(line);
    fs.close();

    return result;
}
