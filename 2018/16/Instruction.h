//
// Created by jeroen on 18-12-18.
//

#ifndef INC_2018_INSTRUCTION_H
#define INC_2018_INSTRUCTION_H


#include "Command.h"

class Command;

class Instruction {
public:
    Instruction(Command *cmd, long long  cmdoper1, long long  cmdoper2, long long  cmdoper3) : command(cmd), commandOper1(cmdoper1),
                                                                            commandOper2(cmdoper2),commandOper3(cmdoper3)  {};

    void execute();


private:
    Command *command;
    long long commandOper1;
    long long commandOper2;
    long long commandOper3;


};


#endif //INC_2018_INSTRUCTION_H
