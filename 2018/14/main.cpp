#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <regex>
#include <iomanip>



unsigned long readFile(const std::string &filename);
void doElvesBakery(std::vector<int> &scores, unsigned long &elf1pos, unsigned long &elf2pos);
std::string getLast10DigitsAfterElvesBakery(unsigned long nrOfRecipes);
std::vector<int> getScoresToFind(unsigned long nr);
std::string getNrOfScoresLeftFromScore(unsigned long score);
int main(int argc, char *argv[]) {

    auto input = readFile("input.txt");
    auto answerA = getLast10DigitsAfterElvesBakery(input);
    auto answerB = getNrOfScoresLeftFromScore(input);



    std::cout << "Advent of code 2018, day 14" << std::endl;
    std::cout << "A: " << answerA<< std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

bool requestedScoreInVectorStartingFromPos(const std::vector<int> &scores, const std::vector<int> &scoreToMatch,
                                           unsigned long start){
    for (unsigned long i=0;i<scoreToMatch.size();i++){
        if (scores.at(i+start) != scoreToMatch.at(i))
            return false;
    }
    return true;
}

unsigned long requestedScoreInLastSteps(const std::vector<int> &scores, const std::vector<int> &scoreToMatch){
    auto nrOfScoresMoreThanMatch = (int)(scores.size() - scoreToMatch.size());
    if (nrOfScoresMoreThanMatch < 2)
        return 0;

    if (requestedScoreInVectorStartingFromPos(scores,scoreToMatch,scores.size()-scoreToMatch.size()-1))
        return scores.size()-scoreToMatch.size()-1;
    if (requestedScoreInVectorStartingFromPos(scores,scoreToMatch,scores.size()-scoreToMatch.size()))
        return scores.size()-scoreToMatch.size();

    return 0;
}

std::vector<int> getScoresToFind(unsigned long nr){
    unsigned long tempNr = nr;
    std::deque<int> result;
    while (tempNr >0){
        result.push_front((int)tempNr%10);
        tempNr -= tempNr%10;
        tempNr /=10;
    }

    return std::vector<int>{result.begin(),result.end()};
}

std::string getNrOfScoresLeftFromScore(unsigned long score){
    std::vector<int> scores {3,7};
    std::vector<int> scoresToMatch = getScoresToFind(score);;
    unsigned long elf1pos=0;
    unsigned long elf2pos=1;
    unsigned long posFound = 0;
    while (!posFound){

        doElvesBakery(scores,elf1pos,elf2pos);
        posFound = requestedScoreInLastSteps(scores, scoresToMatch);
    }


    std::stringstream result;
    result << posFound;

    return result.str();
}


std::string getLast10DigitsAfterElvesBakery(unsigned long nrOfRecipes){
    std::vector<int> scores {3,7};
    unsigned long elf1pos=0;
    unsigned long elf2pos=1;
    while (scores.size()<nrOfRecipes+10){
        doElvesBakery(scores,elf1pos,elf2pos);
    }

    std::stringstream result;
    for (unsigned long i=nrOfRecipes;i<nrOfRecipes+10;i++){
        result << scores.at(i);
    }
    return result.str();
}

void doElvesBakery(std::vector<int> &scores, unsigned long &elf1pos, unsigned long &elf2pos){
    int currScore = scores.at(elf1pos)+scores.at(elf2pos);
    int scoreDigits = currScore%10;
    int scoreTens = currScore/10;

    if (scoreTens)
        scores.push_back(scoreTens);
    scores.push_back(scoreDigits);

    int elf1Steps = scores.at(elf1pos)+1;
    int elf2Steps = scores.at(elf2pos)+1;

    elf1pos = (elf1pos+elf1Steps) % scores.size();
    elf2pos = (elf2pos+elf2Steps) % scores.size();

}

unsigned long readFile(const std::string &filename) {
    unsigned long result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    fs >> result;
    fs.close();

    return result;
}
