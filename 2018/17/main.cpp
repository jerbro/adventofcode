#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <regex>
#include <iomanip>
// #include <boost/algorithm/string/split.hpp>
// #include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string.hpp>


struct Coord {
    int x;
    int y;
};

bool operator<(const Coord &c1, const Coord &c2) {
    return (c1.x < c2.x || (c1.x == c2.x && c1.y < c2.y));
}

std::set<Coord> readFile(const std::string &filename);

std::vector<std::string> makeBoard(std::set<Coord> clayCoordinates, int &xOffset);

void printBoard(const std::vector<std::string> &board);

void flowToBottom(std::vector<std::string> &board, Coord startpos);

void letWaterFlow(std::vector<std::string> &board, Coord startpos, int xoffset);

long countWaterTiles(const std::vector<std::string> &board);
long countUndrainableTiles(const std::vector<std::string> &board);

int main(int argc, char *argv[]) {

    auto clayCoordinates = readFile("input.txt");
    int xoffset;
    auto board = makeBoard(clayCoordinates, xoffset);
    letWaterFlow(board, Coord{500, 0}, xoffset);
    printBoard(board);

    auto answerA = countWaterTiles(board);
    auto answerB = countUndrainableTiles(board);



    std::cout << "Advent of code 2018, day 17" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

void letWaterFlow(std::vector<std::string> &board, Coord startpos, int xoffset) {

    flowToBottom(board, Coord{startpos.x - xoffset, startpos.y + 1});
}

void printBoard(const std::vector<std::string> &board) {
    for (auto line:board) {
        std::cout << line << std::endl;
    }
    std::cout << std::endl;
}

void findMinAndMax(std::set<Coord> clayCoordinates, int &minx, int &maxx, int &miny, int &maxy) {
    minx = (clayCoordinates.begin())->x;
    maxx = (clayCoordinates.begin())->x;
    miny = (clayCoordinates.begin())->y;
    maxy = (clayCoordinates.begin())->y;
    for (auto item:clayCoordinates) {
        if (item.x < minx)
            minx = item.x;

        if (item.x > maxx)
            maxx = item.x;

        if (item.y < miny)
            miny = item.y;

        if (item.y > maxy)
            maxy = item.y;
    }

}

std::vector<std::string> makeBoard(std::set<Coord> clayCoordinates, int &xOffset) {

    int minx;
    int maxx;
    int miny;
    int maxy;
    findMinAndMax(clayCoordinates, minx, maxx, miny, maxy);

    xOffset = minx - 1;// 1 left of the minimum clay particle (because water can flow to the left)
    int width = maxx - xOffset +
                2; // width is one more on both sides, because water can flow to the right if the rightmost clayparticle
    std::vector<std::string> result(maxy + 1, std::string(width, '.'));

    for (auto item:clayCoordinates) {
        result.at(item.y).at(item.x - xOffset) = '#';
    }

    result.at(0).at(500 - xOffset) = '+';
    return result;
}


bool canFlowToThisCell(const std::vector<std::string> &board, Coord pos) {
    //Check if the current position exists, en is not equal to '#'


    return ((long long) board.size() > (long long) pos.y &&
            board.at(pos.y).size() >= pos.x &&
            board.at(pos.y).at(pos.x) != '#' &&
            board.at(pos.y).at(pos.x) != '~');

}

bool canFlowDown(const std::vector<std::string> &board, Coord pos) {
    return (canFlowToThisCell(board, Coord{pos.x, pos.y + 1}) || pos.y >= board.size());
}

bool canFlowRight(const std::vector<std::string> &board, Coord pos) {
    return canFlowToThisCell(board, Coord{pos.x + 1, pos.y});
}

bool canFlowLeft(const std::vector<std::string> &board, Coord pos) {
    return canFlowToThisCell(board, Coord{pos.x - 1, pos.y});
}

bool cellIsClay(std::vector<std::string> &board, Coord pos) {
    return ((long long) board.size() > (long long) pos.y &&
            pos.x >= 0 &&
            board.at(pos.y).at(pos.x) == '#');
}


bool flowToLeft(std::vector<std::string> &board, Coord startpos) {
    bool leftEndReached;
    int xpos = startpos.x;
    int ypos = startpos.y;
    while (canFlowToThisCell(board, Coord{xpos, ypos})) {
        board.at(ypos).at(xpos) = '|';
        if (canFlowDown(board, Coord{xpos, ypos})) {
            flowToBottom(board, Coord{xpos, ypos}); // Fill the pit, then check again if still unlimited flow down
            if (canFlowDown(board, Coord{xpos, ypos}))
                break;
        }
        xpos--;
    }


    leftEndReached = (cellIsClay(board, Coord{xpos, ypos}));
    return leftEndReached;


}


bool flowToRight(std::vector<std::string> &board, Coord startpos) {
    bool rightEndReached;
    int xpos = startpos.x;
    int ypos = startpos.y;
    while (canFlowToThisCell(board, Coord{xpos, ypos})) {
        board.at(ypos).at(xpos) = '|';
        if (canFlowDown(board, Coord{xpos, ypos})) {
            flowToBottom(board, Coord{xpos, ypos});
            if (canFlowDown(board, Coord{xpos, ypos}))
                break;
        }
        xpos++;
    }


    rightEndReached = (cellIsClay(board, Coord{xpos, ypos}));
    return rightEndReached;


}

void setTrappedWaterLine(std::vector<std::string> &board, Coord startpos) {
    //Fill to the left
    int xpos = startpos.x;
    int ypos = startpos.y;
    while (canFlowToThisCell(board, Coord{xpos, ypos})) {
        board.at(ypos).at(xpos) = '~';
        xpos--;
    }

    xpos = startpos.x + 1;
    while (canFlowToThisCell(board, Coord{xpos, ypos})) {
        board.at(ypos).at(xpos) = '~';
        xpos++;
    }
}

bool flowLine(std::vector<std::string> &board, Coord startpos) {
    bool leftToEnd = flowToLeft(board, startpos);
    bool rightToEnd = flowToRight(board, startpos);
    if (leftToEnd && rightToEnd) { // If both directions filling leads to a clay-layer, then fill the line
        setTrappedWaterLine(board, startpos);
        return true;
    }

    return false;

}

void flowToBottom(std::vector<std::string> &board, Coord startpos) {
    static std::set<Coord> previousStartPos;
    if (previousStartPos.find(startpos) != previousStartPos.end())
        return;
    previousStartPos.insert(startpos);

    auto oldboard = board;
    int ypos;

    bool keepRunning = true;
    while (keepRunning) {
        ypos = startpos.y;
        while (canFlowToThisCell(board, Coord{startpos.x, ypos})) {
            board.at(ypos).at(startpos.x) = '|';
            ypos++;
        }
        if (board.size() > ypos && ypos > startpos.y + 1)
            keepRunning = flowLine(board, Coord{startpos.x, ypos - 1});
        else
            keepRunning = false;

        if (board != oldboard) {
            oldboard = board;
        } else
            keepRunning = false;

    }


}

long countUndrainableTiles(const std::vector<std::string> &board) {
    long result = 0;
    bool countingEnabled = false;
    for (auto line:board) {
        if (!countingEnabled && line.find('#') != std::string::npos)
            countingEnabled = true;

        if (countingEnabled) {
            for (char cell:line) {
                if ( cell == '~')
                    result++;
            }
        }
    }
    return result;
}

long countWaterTiles(const std::vector<std::string> &board) {
    long result = 0;
    bool countingEnabled = false;
    for (auto line:board) {
        if (!countingEnabled && line.find('#') != std::string::npos)
            countingEnabled = true;

        if (countingEnabled) {
            for (char cell:line) {
                if (cell == '|' || cell == '~')
                    result++;
            }
        }
    }
    return result;
}

int stringToInt(const std::string &stringnumber) {
    std::stringstream strstr{stringnumber};
    int result;
    strstr >> result;
    return result;

}


std::set<Coord> parseLine(std::string line) {
    std::set<Coord> result;
    // A line is formatted like: x=495, y=2..7
//    or: y=7, x=495..501
    std::string delimiters(", =.");
    std::vector<std::string> parts;
    boost::split(parts, line, boost::is_any_of(delimiters));

    auto newEnd = std::remove_if(parts.begin(), parts.end(), [](const std::string &txt) { return txt.size() == 0; });
    parts.erase(newEnd, parts.end());


    if (parts.size() == 5) {

        int singleNr = stringToInt(parts.at(1));
        int rangeStart = stringToInt(parts.at(3));
        int rangeEnd = stringToInt(parts.at(4));
        for (int i = rangeStart; i <= rangeEnd; i++) {
            Coord loc;
            if (parts.at(0) == "x") {
                loc.x = singleNr;
                loc.y = i;
            } else {
                loc.x = i;
                loc.y = singleNr;
            }
            result.insert(loc);
        }
    }
    return result;
}


std::set<Coord> readFile(const std::string &filename) {
    std::set<Coord> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    while (std::getline(fs, line)) {
        auto lineData = parseLine(line);
        result.insert(lineData.begin(), lineData.end());
    }

    fs.close();

    return result;
}
