//
// Created by jeroen on 30-6-19.
//
#include <vector>
#include <stack>
#include <iomanip>
#include <iostream>
#include "RoomsMap.h"

RoomsMap::RoomsMap(const std::string &input) {
    std::stack<Coord> startPos;
    int x=0;
    int y=0;
    int steps=0;
    roomsMap.insert(std::make_pair(Coord{0,0},0));

    for (char ch:input){
        if (ch=='N'){
            y-=1;
        }
        else if (ch=='E'){
            x+=1;
        }
        else if (ch=='S'){
            y+=1;
        }
        else if (ch=='W'){
            x-=1;
        }

        if (ch=='N' | ch=='E'|| ch== 'S' || ch=='W'){
            steps+=1;
            Coord currPos{x,y};
            if (roomsMap.find(currPos) == roomsMap.end()){
                roomsMap.insert(std::make_pair(currPos,steps));
            }
            else {
                auto foundRoom = roomsMap.find(currPos);
                if (foundRoom->second > steps) {
                    foundRoom->second = steps;
                }
            }
        }
        if (ch == '('){
            startPos.push(Coord{x,y});
        }
        if (ch==')'){
            startPos.pop();
        }
        if (ch=='|'){
            x = startPos.top().x;
            y= startPos.top().y;
            steps = roomsMap.find(Coord{x,y})->second;
        }


    }


}
std::map<Coord,int> RoomsMap::getMap() {
    return roomsMap;
}

void RoomsMap::printMap() {
    int minx=0,miny=0,maxx=0,maxy=0;
    for (auto item:roomsMap)
    {
        if (item.first.x<minx){
            minx=item.first.x;}
        if (item.first.y<miny){
            miny=item.first.y;}
        if (item.first.x>maxx){
            maxx=item.first.x;}
        if (item.first.y>maxy){
            maxy=item.first.y;}
    }
    std::vector<std::vector<int>> map(maxy-miny+1,std::vector<int>(maxx-minx+1));
    for (auto item:roomsMap){
        auto xinmap = item.first.x-minx;
        auto yinmap = item.first.y-miny;
        map.at(yinmap).at(xinmap) = item.second;
    }

    for (int i=0;i<=maxy-miny;i++){
        for (int j=0;j<=maxx-minx;j++){

            std::cout << std::setw(4)<< map.at(i).at(j)<<"|";
        }
        std::cout << '\n';
    }
}

int RoomsMap::getLargestDistance(){
    int maxdist=0;
    for (auto item:roomsMap){
        if (item.second>maxdist)
            maxdist = item.second;
    }
    return maxdist;
}

int RoomsMap::getNumberOfRoomsOfMinDistance(int mindist){
    int nrAboveMin=0;
    for (auto item:roomsMap){
        if (item.second>=mindist)
            nrAboveMin++;
    }
    return nrAboveMin;
}