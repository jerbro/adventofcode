//
// Created by jeroen on 30-6-19.
//

#ifndef INC_2018_ROOMSMAP_H
#define INC_2018_ROOMSMAP_H

#include <utility>
#include <map>
#include "Coord.h"


class RoomsMap{
public:
    RoomsMap(const std::string &input);
    std::map<Coord,int> getMap();
    void printMap();
    int getLargestDistance();
    int getNumberOfRoomsOfMinDistance(int mindist);
private:
    std::map<Coord,int> roomsMap;

};

#endif //INC_2018_ROOMSMAP_H
