#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <regex>
#include <iomanip>
#include "RoomsMap.h"


std::string readFile(const std::string &filename);

int main(int argc, char *argv[]) {

    auto routeDescription = readFile("input.txt");

    std::string desc;
    routeDescription = readFile("input.txt");

    RoomsMap roommap(routeDescription);

    auto answerA = roommap.getLargestDistance();
    auto answerB = roommap.getNumberOfRoomsOfMinDistance(1000);

    std::cout << "Advent of code 2018, day 20" << std::endl;
    std::cout << "A: " << answerA<< std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}


std::string readFile(const std::string &filename) {
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);
    std::string line;
    getline(fs,line);


    fs.close();

    return line;
}
