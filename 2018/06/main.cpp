#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <regex>


struct coord {
    int x;
    int y;
};

std::vector<coord> readFile(const std::string &filename);

std::vector<std::vector<int>> createField(const std::vector<coord> &coords);

int manhattanDistance(const coord &coord1, const coord &coord2);

void fillFieldWithClosestDistanceId(const std::vector<coord> &coords, std::vector<std::vector<int>> &field);

std::map<int, int> getFrequencyMap(const std::vector<std::vector<int>> &field);

std::set<int> getExcludedIds(const std::vector<std::vector<int>> &field);

int getLargestAreaNotOnSide(const std::vector<coord> &coords);

int countFieldsWithinRange(int range, const std::vector<coord> &coords);


int main(int argc, char *argv[]) {
    auto coords = readFile("input.txt");

    auto answerA = getLargestAreaNotOnSide(coords);
    auto answerB = countFieldsWithinRange(10000, coords);

    std::cout << "Advent of code 2018, day 06" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}

//get the total distance from all coordinates to the current coordinate.
int getTotalDistance(coord currLoc, const std::vector<coord> &coords) {
    int result = 0;
    for (auto coord:coords) {
        result += manhattanDistance(currLoc, coord);
    }
    return result;
}

//count all the fields within range
int countFieldsWithinRange(int range, const std::vector<coord> &coords) {
    auto field = createField(coords);
    int result = 0;
    for (int y = 0; y < (int)field.size(); y++)
        for (int x = 0; x < (int)field[y].size(); x++) {
            if (getTotalDistance(coord{x, y}, coords) < range) {
                result++;
            }

        }
    return result;
}

//get the area with the largest distance from other coordinates
int getLargestAreaNotOnSide(const std::vector<coord> &coords) {
    auto field = createField(coords);//create a field with the correct size

    fillFieldWithClosestDistanceId(coords, field);//fill every cell with the id of the closest coordinate

    auto freqMap = getFrequencyMap(field);//create map with the id, and the number of occurrences
    auto sideIDs = getExcludedIds(field); // get the ID's of the coordinates on the side of the field

    int result = 0; // find the largest area not on the side.
    for (auto pair:freqMap) {
        if (sideIDs.find(pair.first) == sideIDs.end()
            && pair.second > result) {
            result = pair.second;
        }
    }
    return result;
}

//Get a map with the coordinate-id, and the frequency it occurrences in the field.
std::map<int, int> getFrequencyMap(const std::vector<std::vector<int>> &field) {
    std::map<int, int> result;
    for (int y = 0; y < (int)field.size(); y++)
        for (int x = 0; x < (int)field[y].size(); x++) {
            int currId = field[y][x];
            if (result.find(currId) == result.end())
                result.insert(std::make_pair(currId, 0));
            result.find(currId)->second++;
        }
    return result;
}

//get ID's of coordinates which are on the side of the map. Also include -1, since this is an invalid id
std::set<int> getExcludedIds(const std::vector<std::vector<int>> &field) {
    std::set<int> result;
    result.insert(-1);
    for (int x = 0; x < (int)field[0].size(); x++) {
        result.insert(field[0][x]);
        result.insert(field[field.size() - 1][x]);
    }

    for (unsigned long y = 0; y < field.size(); y++) {
        result.insert(field[y][0]);
        result.insert(field[y][field[0].size() - 1]);
    }
    return result;
}

//Calculate distance between two coordinates
int manhattanDistance(const coord &coord1, const coord &coord2) {
    return (abs(coord1.x - coord2.x) + abs(coord1.y - coord2.y));
}

//get the id of the closest coordinate to the current coordinate. If multiple coordinates have same distance, return -1
int getClosestCoordinateId(coord currCoord, const std::vector<coord> &coords) {
    int smallestDist = 9999;
    int result = -1;
    for (int i = 0; i < (int)coords.size(); i++) {
        int dist = manhattanDistance(currCoord, coords.at((unsigned long)i));
        if (dist == smallestDist) {
            result = -1;
        }
        if (dist < smallestDist) {
            smallestDist = dist;
            result = i;
        }
    }
    return result;
}


//Put the ID of the field with closest distance in every cell of field. If multiple have equal distance, -1 is put in the cell
void fillFieldWithClosestDistanceId(const std::vector<coord> &coords, std::vector<std::vector<int>> &field) {
    for (int y = 0; y < (int)field.size(); y++)
        for (int x = 0; x < (int)field[y].size(); x++) {
            field[y][x] = getClosestCoordinateId(coord{x, y}, coords);
        }

}

//Create a field with the size large enough to contain all coordinates
std::vector<std::vector<int>> createField(const std::vector<coord> &coords) {
    int maxX = 0;
    int maxY = 0;
    for (auto loc : coords) {
        if (loc.x > maxX)
            maxX = loc.x;
        if (loc.y > maxY)
            maxY = loc.y;
    }

    std::vector<std::vector<int>> result;
    for (int y = 0; y < maxY + 1; y++)
        result.emplace_back(std::vector<int>(maxX + 1, 0));
    return result;
}

//read the file, and write to a vector of coordinates.
std::vector<coord> readFile(const std::string &filename) {
    std::vector<coord> result;
    coord coord1{0,0};
    std::string line;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    while (std::getline(fs, line)) {

        std::stringstream strStr(line);
        char c;
        strStr >> coord1.x;
        strStr >> c;
        strStr >> coord1.y;
        result.push_back(coord1);
    }


    fs.close();
    return result;
}
