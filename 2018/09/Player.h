//
// Created by jeroen on 9-12-18.
//

#ifndef INC_2018_PLAYER_H
#define INC_2018_PLAYER_H


#include <list>

class Player {
public: Player();
        void doTurn(std::list<long long>& currBoard, std::list<long long>::iterator& currMarble, long long currTurn);
    long long getPoints();
        void moveClockWise(std::list<long long>& currBoard, std::list<long long>::iterator& currMarble, int steps);
        void moveCounterClockwise(std::list<long long>& currBoard, std::list<long long>::iterator& currMarble, int steps);
    private:
    long long points;

};


#endif //INC_2018_PLAYER_H
