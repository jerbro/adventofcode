//
// Created by jeroen on 9-12-18.
//

#include <iostream>
#include "Player.h"

Player::Player() : points(0) {}

void Player::doTurn(std::list<long long> &currBoard, std::list<long long>::iterator &currMarble, long long currTurn) {
    if (currTurn % 23 != 0) {
        moveClockWise(currBoard, currMarble, 1);
        currBoard.insert(currMarble, currTurn);
    }
    else
    {
        points += currTurn;
        moveCounterClockwise(currBoard,currMarble,8);
        points += (*currMarble);
        auto marbleToDelete = currMarble;
        moveClockWise(currBoard,currMarble,2);
        currBoard.erase(marbleToDelete);
    }
}


long long Player::getPoints() {
    return points;
}


void Player::moveClockWise(std::list<long long> &currBoard, std::list<long long>::iterator &currMarble, int steps) {
    for (int i = 0; i < steps; i++) {
        currMarble++;
        if (currMarble == currBoard.end()) {
            currMarble = currBoard.begin();
        }
    }
}

void Player::moveCounterClockwise(std::list<long long> &currBoard, std::list<long long>::iterator &currMarble, int steps) {
    for (int i = 0; i < steps; i++) {
        if (currMarble == currBoard.begin()) {
            currMarble = currBoard.end();
        }
        currMarble--;
    }
}