#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <regex>
#include "Player.h"

std::pair<int,long long> readFile(const std::string &filename);
long long playGame(std::pair<int,long long>input);

int main(int argc, char *argv[]) {

    auto gameInput = readFile("input.txt");


    auto answerA = playGame(gameInput);
    auto answerB = playGame(std::make_pair(gameInput.first, gameInput.second*100));
    std::cout << "Advent of code 2018, day 09" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

long long playGame(std::pair<int,long long>input){
    int playerCnt = input.first;
    int steps = input.second;
    std::vector<Player> players(playerCnt);
    std::list<long long> board{0};
    std::list<long long>::iterator currMarble = board.begin();
    for (long long i=1;i<=steps; i++){
        players.at(i%playerCnt).doTurn(board,currMarble, i);
    }

    long long result=0;
    for (auto player:players){
        if (player.getPoints()>result){
            result = player.getPoints();
        }
    }
    return result;

}

int stringToInt(const std::string &stringnumber) {
    std::stringstream strstr{stringnumber};
    int result;
    strstr >> result;
    return result;

}

std::pair<int,long long> parseLine(const std::string &line) {
    std::pair<int,long long> result;
    std::smatch match;
    // example of line: 10 players; last marble is worth 1618 points
    std::regex re("^([0-9]+) players; last marble is worth ([0-9]+) points$");
    std::regex_match(line, match, re);

    result.first = stringToInt(match[1]);
    result.second = stringToInt(match[2]);

    return result;
}

std::pair<int,long long> readFile(const std::string &filename) {
    std::pair<int,long long> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    std::getline(fs, line);

    result = parseLine(line);

    fs.close();
    return result;
}
