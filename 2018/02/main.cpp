#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <map>

std::vector<std::string> readFile(const std::string &filename);

std::map<char, int> charFrequencyInLine(const std::string &line);

bool mapHasKeyWithValue(const std::map<char, int> &map, const int &value);

long calcChecksum(const std::vector<std::string> &textList);

int numberOfDifferencesInString(const std::string &text1, const std::string &text2);


std::string findAlmostEqualStrings(const std::vector<std::string> & stringList);

std::string removeDifferentCharsFromString(std::string text1, std::string text2);

int main(int argc, char *argv[]) {
    auto textsList = readFile("input.txt");
    auto answerA = calcChecksum(textsList);
    auto answerB = findAlmostEqualStrings(textsList);

    std::cout << "Advent of code 2018, day 02" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

std::string removeDifferentCharsFromString(std::string text1, std::string text2) {
    std::string result;
    for (int i = 0; i < text1.length(); i++) {
        if (text1[i] == text2[i])
            result.push_back(text1[i]);
    }
    return result;
}

std::string findAlmostEqualStrings(const std::vector<std::string> &stringList) {
    for (int i = 0; i < stringList.size(); i++)
        for (int j = 0; j < stringList.size(); j++) {
            if (i != j) {
                if (numberOfDifferencesInString(stringList[i], stringList[j]) == 1) {
                    return removeDifferentCharsFromString(stringList[i], stringList[j]);
                }
            }
        }
    return "";
}


int numberOfDifferencesInString(const std::string &text1, const std::string &text2) {
    int differences = 0;
    for (int i = 0; i < text1.length(); i++) {
        if (text1[i] != text2[i]) {
            differences++;
        }
    }
    return differences;
}


long calcChecksum(const std::vector<std::string> &textList) {
    int linesWithDouble = 0;
    int linesWithTriple = 0;
    for (const std::string &line : textList) {
        auto freqMap = charFrequencyInLine(line);
        if (mapHasKeyWithValue(freqMap, 2))
            linesWithDouble++;
        if (mapHasKeyWithValue(freqMap, 3))
            linesWithTriple++;
    }
    return linesWithDouble * linesWithTriple;

}

bool mapHasKeyWithValue(const std::map<char, int> &map, const int &value) {
    for (auto pair:map) {
        if (pair.second == value)
            return true;
    }
    return false;
}

std::map<char, int> charFrequencyInLine(const std::string &line) {
    std::map<char, int> result;
    for (char c :line) {
        if (result.find(c) != result.end()) {
            (result.find(c)->second) += 1;
        } else {
            result.insert(std::make_pair(c, 1));
        }
    }
    return result;
};

std::vector<std::string> readFile(const std::string &filename) {
    std::vector<std::string> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    while (fs >> line) {
        result.push_back(line);
    }
    fs.close();
    return result;
}
