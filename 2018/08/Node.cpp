//
// Created by jeroen on 8-12-18.
//

#include "Node.h"

Node::Node(std::deque<int> &rawData) : qtyChild(0), qtyMeta(0) {
    qtyChild = rawData.front();
    rawData.pop_front();
    qtyMeta = rawData.front();
    rawData.pop_front();
    for (int i = 0; i < qtyChild; i++) {
        Node *child;
        child = new Node(rawData);
        childNodes.push_back(child);
    }
    for (int i = 0; i < qtyMeta; i++) {
        metaIds.push_back(rawData.front());
        rawData.pop_front();
    }
}

int Node::getSumOfMetadata() {
    int result = 0;
    for (auto child:childNodes) {
        result += child->getSumOfMetadata();
    }
    for (auto metaId: metaIds) {
        result += metaId;
    }
    return result;
}

int Node::getNodeValue() {
    int result = 0;
    if (childNodes.empty()) {
        for (auto metaId:metaIds)
            result += metaId;
    } else {
        for (auto metaId: metaIds) {
            if (childNodes.size() >= metaId) {
                result += childNodes.at((unsigned long)metaId - 1)->getNodeValue();
            }
        }
    }
    return result;
}