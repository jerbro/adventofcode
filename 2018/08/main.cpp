#include <iostream>
#include <fstream>

#include "Node.h"

std::deque<int> readFile(const std::string &filename);

int main(int argc, char *argv[]) {
    auto licFileNrs = readFile("input.txt");

    Node tree(licFileNrs);

    auto answerA = tree.getSumOfMetadata();
    auto answerB = tree.getNodeValue();

    std::cout << "Advent of code 2018, day 08" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}


//read the file, create vector with the numbers in the file
std::deque<int> readFile(const std::string &filename) {
    std::deque<int> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);
    int nr;
    while (fs >> nr) {
        result.push_back(nr);
    }

    fs.close();
    return result;
}
