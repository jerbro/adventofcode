//
// Created by jeroen on 8-12-18.
//

#ifndef INC_2018_NODE_H
#define INC_2018_NODE_H

#include <vector>
#include <deque>


class Node{
public:
    Node(std::deque<int> & rawData);

    int getSumOfMetadata();

    int getNodeValue();
private:
    int qtyChild;
    int qtyMeta;
    std::vector<int> metaIds;
    std::vector<Node*> childNodes;
};


#endif //INC_2018_NODE_H
