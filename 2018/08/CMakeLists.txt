cmake_minimum_required(VERSION 3.8)
project(2018_08)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp Node.cpp Node.h)
add_executable(2018_08 ${SOURCE_FILES})
