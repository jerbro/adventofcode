//
// Created by jeroen on 2-7-19.
//

#ifndef INC_2018_ENUMTYPES_H
#define INC_2018_ENUMTYPES_H
enum class equipment{
    neither=0,
    torch=1,
    climbing=2
};

enum class area{
    rocky=0,
    wet=1,
    small=2
};

#endif //INC_2018_ENUMTYPES_H
