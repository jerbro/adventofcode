//
// Created by jeroen on 1-7-19.
//

#ifndef INC_2018_CAVESYSTEM_H
#define INC_2018_CAVESYSTEM_H

#include "Coord.h"
#include "enumTypes.h"
#include <map>
#include <vector>

class CaveSystem {
public:
    CaveSystem(int depth):depth(depth),moduloFactorGeoToErosion(20183){};

    void makeMap(int targetx, int targety);
    void makeMap(int targetx, int targety,int maxx, int maxy);
    void printMap();
    long long getTotalRisk(int targetx, int targety);

    std::vector<std::vector<area>> getAreaMap();




private:
    std::vector<std::vector<int>> erosionMap;
    int depth;
    int geoLevelToErosion(int);
    int moduloFactorGeoToErosion;
    static std::vector<int> erosionToRiskFactor(const std::vector<int>& erosion);

};


#endif //INC_2018_CAVESYSTEM_H
