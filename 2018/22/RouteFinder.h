//
// Created by jeroen on 1-7-19.
//

#ifndef INC_2018_ROUTEFINDER_H
#define INC_2018_ROUTEFINDER_H
#include <vector>
#include <map>
#include "State.h"
#include "enumTypes.h"



class RouteFinder {
public:
    RouteFinder(){};
    void setLocationMap(const std::vector<std::vector<area>>& locationMap);
    int findMinTimeTillLocation(int destx, int desty);
    std::map<State,int>  getReachableStatesNextIteration(std::map<State,int> startPositions);
private:
    bool equipmentAllowed(area terrain, equipment tool);
    bool stateIsAllowed(State state);
    std::map<State, int> getReachableStates(const State &currState, int currSteps);
    std::vector<std::vector<area>> locationMap;
    std::map<State,int> reachedStates;

};


#endif //INC_2018_ROUTEFINDER_H
