//
// Created by jeroen on 1-7-19.
//

#include <iostream>
#include <algorithm>
#include "CaveSystem.h"
#include "enumTypes.h"

int CaveSystem::geoLevelToErosion(int geoLevel) {
    return (geoLevel + depth)% moduloFactorGeoToErosion;
}

void CaveSystem::makeMap(int targetx, int targety) {
    makeMap(targetx,targety,targetx,targety);
}

void CaveSystem::makeMap(int targetx, int targety,int maxx, int maxy) {
    erosionMap.clear();
    erosionMap.resize(maxy+1, std::vector<int>(maxx+1,-1));

    long long geoLevel = 0;
    for (long long y = 0; y <= maxy; y++) {
        for (long long x = 0; x <= maxx; x++) {
            if (x == 0 && y == 0 || x == targetx && y == targety) {
                geoLevel = 0;
            } else if (y == 0) {
                geoLevel = x * 16807;
            } else if (x == 0) {
                geoLevel = y * 48271;
            } else {
                long long erosionlevel1 = erosionMap.at(y).at(x - 1);
                long long erosionlevel2 = erosionMap.at(y - 1).at(x);
                geoLevel = erosionlevel1 * erosionlevel2;
            }
            erosionMap.at(y).at(x) = geoLevelToErosion(geoLevel);
        }
    }
}

void CaveSystem::printMap() {
    for (const auto &line:erosionMap) {
        for (const auto &pos:line) {
            auto regionType = pos % 3;
            char ch;
            switch (regionType) {
                case 0:
                    ch = '.';
                    break;
                case 1:
                    ch = '=';
                    break;
                case 2:
                    ch = '|';
                    break;
            }
            std::cout << ch;
        }
        std::cout << '\n';
    }
}




long long CaveSystem::getTotalRisk(int targetx, int targety){
    std::vector<std::vector<int>> riskMap;
    for (auto& line: erosionMap){
        std::vector<int>  riskLine(line.size());
        std::transform(line.begin(),line.end(),riskLine.begin(),[](int erosion){return erosion%3;});
        riskMap.push_back(riskLine);
    }


    long long totalRisk = 0;
    for (int y=0;y<=targety;y++){
        const auto& line = riskMap.at(y);
        for (int x=0;x<=targetx;x++){
            const auto& risk= line.at(x);

            totalRisk +=risk;
        }
    }
    return  totalRisk;

}

std::vector<std::vector<area>> CaveSystem::getAreaMap() {

    std::vector<std::vector<area>> result;
    for (const auto &line:erosionMap) {
        std::vector<area> resultline;
        for (const auto &pos:line) {
            auto regionType = pos % 3;
            area areatype;
            switch (regionType) {
                case 0:
                    areatype = area::rocky;
                    break;
                case 1:
                    areatype = area::wet;
                    break;
                case 2:
                    areatype = area::small;
                    break;
            }
            resultline.push_back(areatype);
        }
        result.push_back(resultline);
    }
    return result;
}

