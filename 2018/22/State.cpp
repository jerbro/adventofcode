//
// Created by jeroen on 2-7-19.
//

#include "State.h"
bool operator< (const State& c1, const State& c2) {
    return ((c1.ypos<c2.ypos) || (c1.ypos == c2.ypos && c1.xpos < c2.xpos || (c1.ypos == c2.ypos && c1.xpos == c2.xpos && c1.usedEquipment < c2.usedEquipment)));
}

bool operator== (const State& c1, const State& c2) {
    return ((c1.ypos==c2.ypos && c1.xpos == c2.xpos));
}