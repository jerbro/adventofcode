//
// Created by jeroen on 1-7-19.
//

#include "RouteFinder.h"
#include <set>
#include <iostream>

bool RouteFinder::equipmentAllowed(area terrain, equipment tool) {
    if ((terrain == area::rocky && tool != equipment::neither) ||
        (terrain == area::wet && tool != equipment::torch) ||
        (terrain == area::small && tool != equipment::climbing)) {
        return true;
    }
    return false;
}


bool RouteFinder::stateIsAllowed(State state) {
    //State is allowed if equipment of state is valid in terrain of location
    // and location is valid (x and y not negative)
    if (state.xpos < 0)
        return false;
    if (state.ypos < 0)
        return false;
    if (state.ypos >= locationMap.size()) {
        std::cout << "Out of area\n";
        return false;
    }
    if (state.xpos >= locationMap.at(state.ypos).size()) {
        std::cout << "Out of area\n";
        return false;
    }

    area currTerrain = locationMap.at(state.ypos).at(state.xpos);
    if (!equipmentAllowed(currTerrain, state.usedEquipment))
        return false;
    return true;


}

//Find the reachable states. A state is reachable if the equipment is allowed in the next state
std::map<State, int> RouteFinder::getReachableStates(const State &currState, int currSteps) {

    std::map<State, int> unverifiedStates;
    unverifiedStates.insert(
            std::make_pair(State{currState.xpos - 1, currState.ypos, currState.usedEquipment}, currSteps + 1));
    unverifiedStates.insert(
            std::make_pair(State{currState.xpos, currState.ypos - 1, currState.usedEquipment}, currSteps + 1));
    unverifiedStates.insert(
            std::make_pair(State{currState.xpos + 1, currState.ypos, currState.usedEquipment}, currSteps + 1));
    unverifiedStates.insert(
            std::make_pair(State{currState.xpos, currState.ypos + 1, currState.usedEquipment}, currSteps + 1));


    if (currState.usedEquipment == equipment::torch) {
        unverifiedStates.insert(
                std::make_pair(State{currState.xpos, currState.ypos, equipment::climbing}, currSteps + 7));
        unverifiedStates.insert(
                std::make_pair(State{currState.xpos, currState.ypos, equipment::neither}, currSteps + 7));
    } else if (currState.usedEquipment == equipment::climbing) {
        unverifiedStates.insert(std::make_pair(State{currState.xpos, currState.ypos, equipment::torch}, currSteps + 7));
        unverifiedStates.insert(
                std::make_pair(State{currState.xpos, currState.ypos, equipment::neither}, currSteps + 7));
    } else if (currState.usedEquipment == equipment::neither) {
        unverifiedStates.insert(
                std::make_pair(State{currState.xpos, currState.ypos, equipment::climbing}, currSteps + 7));
        unverifiedStates.insert(std::make_pair(State{currState.xpos, currState.ypos, equipment::torch}, currSteps + 7));
    }

    std::map<State, int> verifiedStates;
    for (const auto &unverifiedState:unverifiedStates) {
        if (stateIsAllowed(unverifiedState.first)) {
            verifiedStates.insert(unverifiedState);
        }
    }
    return verifiedStates;
}

void RouteFinder::setLocationMap(const std::vector<std::vector<area>> &locationMap) {
    this->locationMap = locationMap;
}


std::map<State, int> RouteFinder::getReachableStatesNextIteration(std::map<State, int> startPositions) {
    std::map<State, int> result;
    for (const auto &state:startPositions) {
        for (const auto &newstate: getReachableStates(state.first, state.second)) {
            //If the new state is not in the current map, insert it.
            //If the new state is in the current map, but with more time passed, update it
            // else ignore the new state
            if (reachedStates.find(newstate.first) == reachedStates.end()) {
                reachedStates.insert(newstate);
                result.insert(newstate);
            } else if (reachedStates.find(newstate.first)->second > newstate.second) {
                reachedStates.find(newstate.first)->second = newstate.second;
                if (result.find(newstate.first) == result.end())
                    result.insert(newstate);
                else if (result.find(newstate.first)->second > newstate.second) {
                    result.find(newstate.first)->second = newstate.second;
                }
            }
        }
    }
    return result;
}

int RouteFinder::findMinTimeTillLocation(int destx, int desty) {
    std::map<State, int> currReachedStates;
    const State finalDestination{destx, desty, equipment::torch};
    currReachedStates.insert(std::make_pair(State{0, 0, equipment::torch}, 0));
    reachedStates.insert(std::make_pair(State{0, 0, equipment::torch}, 0));


    bool keepRunning = true;
    long long iterationcounter = 0;
    bool destinationReached = false;
    long long lastIterationCounter = -1;
    while (keepRunning) {
        iterationcounter++;

        currReachedStates = getReachableStatesNextIteration(currReachedStates);

        if (this->reachedStates.find(finalDestination) != reachedStates.end()) {
            destinationReached = true;
            lastIterationCounter = this->reachedStates.find(
                    finalDestination)->second;//When there are more iterations than the current shortest route, a shorter route will not be found.
        }

        if (destinationReached && iterationcounter > lastIterationCounter) {
            keepRunning = false;
        }
    }

    return this->reachedStates.find(finalDestination)->second;
}





