//
// Created by jeroen on 2-7-19.
//

#ifndef INC_2018_STATE_H
#define INC_2018_STATE_H
#include "enumTypes.h"



class State {

public:
        int xpos;
        int ypos;
        equipment usedEquipment;

};

bool operator< (const State& c1, const State& c2);
bool operator== (const State& c1, const State& c2);

#endif //INC_2018_STATE_H
