#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <regex>

enum guardAction {
    startShift, wakeup, fallAsleep, uninitialized
};

struct guardStats {
    int minutesOfSleep;
    std::vector<int> timesSleptPerMinute;
};

struct logRecord {
    int year;
    int month;
    int day;
    int hour;
    int minute;
    int guardID;
    guardAction action;
};

std::list<logRecord> readFile(const std::string &filename);

std::map<int, guardStats> getStatsFromLogrecords(const std::list<logRecord> &records);

int findGuardWithMostMinutesSleep(const std::map<int, guardStats> &stats);

int getMinuteWithMostSleep(const guardStats &stat);

int getPartAChecksum(const std::map<int, guardStats> &stats);

int getPartBChecksum(const std::map<int, guardStats> &stats);

bool logRecordSmallerThan(const logRecord &first, const logRecord &second);

int main(int argc, char *argv[]) {
    auto logRecords = readFile("input.txt");
    logRecords.sort(logRecordSmallerThan);
    auto stats = getStatsFromLogrecords(logRecords);
    auto answerA = getPartAChecksum(stats);
    auto answerB = getPartBChecksum(stats);


    std::cout << "Advent of code 2018, day 04" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

int getPartBChecksum(const std::map<int, guardStats> &stats) {
    int guardID = -1;
    int maxMinutesSleep = -1;
    int minuteWithMaxSleep = -1;
    for (auto stat: stats)
        for (int i = 0; i < stat.second.timesSleptPerMinute.size(); i++) {
            if (stat.second.timesSleptPerMinute[i] > maxMinutesSleep) {
                guardID = stat.first;
                maxMinutesSleep = stat.second.timesSleptPerMinute[i];
                minuteWithMaxSleep = i;
            }
        }
    return guardID * minuteWithMaxSleep;
}

int getPartAChecksum(const std::map<int, guardStats> &stats) {
    int guardWithMostSleep = findGuardWithMostMinutesSleep(stats);
    guardStats statsOfMostSleepGuard = stats.find(guardWithMostSleep)->second;
    int minuteWithMostSleep = getMinuteWithMostSleep(statsOfMostSleepGuard);
    return minuteWithMostSleep * guardWithMostSleep;
}

bool logRecordSmallerThan(const logRecord &first, const logRecord &second) {
    if (first.year > second.year)
        return false;
    if (first.year < second.year)
        return true;

    if (first.month > second.month)
        return false;
    if (first.month < second.month)
        return true;

    if (first.day > second.day)
        return false;
    if (first.day < second.day)
        return true;

    if (first.hour > second.hour)
        return false;
    if (first.hour < second.hour)
        return true;

    if (first.minute > second.minute)
        return false;
    if (first.minute < second.minute)
        return true;

}


int getMinuteWithMostSleep(const guardStats &stat) {
    int currMax = -1;
    int minuteWithMax = -1;
    for (int i = 0; i < stat.timesSleptPerMinute.size(); i++) {
        if (stat.timesSleptPerMinute[i] > currMax) {
            currMax = stat.timesSleptPerMinute[i];
            minuteWithMax = i;
        }
    }
    return minuteWithMax;
}

int findGuardWithMostMinutesSleep(const std::map<int, guardStats> &stats) {
    int idMaxSleep = -1;
    int maxMinutesOfSleep = -1;
    for (std::pair<int, guardStats> idStats: stats) {
        if (idStats.second.minutesOfSleep > maxMinutesOfSleep) {
            maxMinutesOfSleep = idStats.second.minutesOfSleep;
            idMaxSleep = idStats.first;
        }
    }
    return idMaxSleep;
}


bool
addSleeptimeToStats(const int &startSleep, const int &stopSleep, const int &guardID, std::map<int, guardStats> &stats) {
    if (stats.find(guardID) == stats.end()) {
        stats.insert(std::make_pair(guardID, guardStats{0, std::vector<int>(60, 0)}));
    }

    guardStats &stat = stats.find(guardID)->second;

    for (int i = startSleep; i < stopSleep; i++) {
        if (i >= 0 && i < 60) {
            stat.timesSleptPerMinute[i]++;
            stat.minutesOfSleep++;
        }
    }
    return true;
}

std::map<int, guardStats> getStatsFromLogrecords(const std::list<logRecord> &records) {
    std::map<int, guardStats> result;
    int currGuardId = -1;
    int startSleepTime = -1;
    for (auto record: records) {
        switch (record.action) {
            case startShift:
                currGuardId = record.guardID;
                break;
            case fallAsleep:
                startSleepTime = record.minute;
                break;
            case wakeup:
                addSleeptimeToStats(startSleepTime, record.minute, currGuardId, result);
                break;
            default:
                break;

        }
    }
    return result;
}


int stringToInt(const std::string &stringnumber) {
    std::stringstream strstr{stringnumber};
    int result;
    strstr >> result;
    return result;

}

bool addActionTologRecord(const std::string &actionStr, logRecord &record) {
    bool actionFound = false;
    std::smatch matchShiftBegin;
    // example of line: [1518-11-05 00:55] wakes up
    std::regex re_wakeup("^wakes up$");
    std::regex re_asleep("^falls asleep$");
    std::regex re_shiftbegin("^Guard #([0-9]+) begins shift$");

    if (std::regex_match(actionStr, re_wakeup)) {
        record.guardID = -1;
        record.action = wakeup;
        actionFound = true;

    }

    if (std::regex_match(actionStr, re_asleep)) {
        record.action = fallAsleep;
        record.guardID = -1;
        actionFound = true;
    }

    if (std::regex_match(actionStr, matchShiftBegin, re_shiftbegin)) {
        record.guardID = stringToInt(matchShiftBegin[1]);
        record.action = startShift;
        actionFound = true;
    }
    return actionFound;
}


logRecord parseLine(const std::string& line) {
    logRecord result = {0, 0, 0, 0, 0, 0, uninitialized};
    std::smatch match;
    // example of line: [1518-11-05 00:55] wakes up
    std::regex re("^\\[([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+)\\] (.*)$");
    std::regex_match(line, match, re);

    result.year = stringToInt(match[1]);
    result.month = stringToInt(match[2]);
    result.day = stringToInt(match[3]);
    result.hour = stringToInt(match[4]);
    result.minute = stringToInt(match[5]);


    addActionTologRecord(match[6], result);
    return result;
}

std::list<logRecord> readFile(const std::string &filename) {
    std::list<logRecord> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    while (std::getline(fs, line)) {

        result.push_back(parseLine(line));
    }

    fs.close();
    return result;
}
