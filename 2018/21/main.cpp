#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <iomanip>
#include "Instruction.h"
#include "Command.h"
#include "Processor.h"
#include <boost/algorithm/string.hpp>


std::vector<std::string> readFile(const std::string &filename);

long long getR3ValueOnBreakpoint(std::vector<std::string> program);
long long getLastR3ValueBeforeRepeating(std::vector<std::string> program);

int main(int argc, char *argv[]) {

    auto program = readFile("input.txt");
 //   auto optimizedProgram = readFile("input_manual_optimized.txt");

    auto answerA = getR3ValueOnBreakpoint(program);
    auto answerB = getLastR3ValueBeforeRepeating(program);


    std::cout << "Advent of code 2018, day 21" << std::endl;
    std::cout << "A: " << answerA<< std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

long long getR3ValueOnBreakpoint(std::vector<std::string> program){
    //By manually analysing the program, found that Address R0 is only used on line 28.
    // The value of R3 is then compared with R0. If these are equal, the program will be stopped.
    //
    auto proc = Processor(program);
    proc.runWithBreakpoint(28);
    auto regs=proc.getRegisterState();

    return regs.at(3);
}

long long getLastR3ValueBeforeRepeating(std::vector<std::string> program){
    //By manually analysing the program, found that Address R0 is only used on line 28.
    // The value of R3 is then compared with R0. If these are equal, the program will be stopped.
    // Find the last value of R3 before R3 is repeating itselves.
    std::set<long long> alreadyCaptured;
    long long prevReg3Val = 0;
    auto proc = Processor(program);
    bool keepRunning = true;

    while (keepRunning){
        auto regs = proc.getRegisterState();
        prevReg3Val = regs.at(3);

        proc.runWithBreakpoint(28);
        regs = proc.getRegisterState();

        if (alreadyCaptured.find(regs.at(3)) != alreadyCaptured.end()){
            keepRunning = false;
        }
        alreadyCaptured.insert(regs.at(3));
    }
    return prevReg3Val;
}



int stringToInt(const std::string &stringnumber) {
    std::stringstream strstr{stringnumber};
    int result;
    strstr >> result;
    return result;

}

std::vector<std::string> readFile(const std::string &filename) {
    std::vector<std::string> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    while (std::getline(fs,line))
        result.push_back(line);
    fs.close();

    return result;
}
