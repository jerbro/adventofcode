//
// Created by jeroen on 18-12-18.
//

#include "Instruction.h"

void Instruction::execute() {
    command->execute(commandOper1, commandOper2, commandOper3);
}