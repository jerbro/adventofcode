//
// Created by jeroen on 3-7-19.
//

#include "botInfo.h"

bool operator== (const botInfo& b1, const botInfo& b2){
    return (b1.pos == b2.pos && b1.radius == b2.radius);
}
bool operator!= (const botInfo& b1, const botInfo& b2){
    return (!(b1==b2));
}
