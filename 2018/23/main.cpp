#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <iomanip>
#include <regex>
#include <boost/algorithm/string.hpp>
#include "Coord.h"
#include "botInfo.h"
#include "botInformationCollector.h"



std::vector<botInfo> readFile(const std::string &filename);


int main(int argc, char *argv[]) {

    auto botinfo = readFile("input.txt");

    botInformationCollector botinfosystem(botinfo);

    auto answerA = botinfosystem.getNrOfBotsInRangeOfBotWithLargestRadius();
    auto answerB = botinfosystem.getMinAndMaxDistance();

    std::cout << "Advent of code 2018, day 23" << std::endl;
    std::cout << "A: " << answerA<< std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

int stringToInt(const std::string &stringnumber) {
    std::stringstream strstr{stringnumber};
    int result;
    strstr >> result;
    return result;

}

botInfo parseBotInfoLine(const std::string &line) {
    botInfo result;
    std::smatch match;
    // pos=<1,3,1>, r=1
    std::regex re("^pos=<(-?[0-9]+),(-?[0-9]+),(-?[0-9]+)>, r=(-?[0-9]+)$");
    std::regex_match(line, match, re);

    result.pos.x = stringToInt(match[1]);
    result.pos.y = stringToInt(match[2]);
    result.pos.z = stringToInt(match[3]);
    result.radius = stringToInt(match[4]);

    return result;
}



std::vector<botInfo> readFile(const std::string &filename) {
    std::vector<botInfo> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    while (std::getline(fs,line)){
        auto info = parseBotInfoLine(line);
        result.push_back(info);
    }

    fs.close();

    return result;
}
