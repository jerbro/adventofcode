//
// Created by jeroen on 3-7-19.
//

#ifndef INC_2018_BOTINFORMATIONCOLLECTOR_H
#define INC_2018_BOTINFORMATIONCOLLECTOR_H
#include "botInfo.h"
#include <vector>

class botInformationCollector {

public:
    botInformationCollector(std::vector<botInfo> bots);
    void setBots(std::vector<botInfo> bots);

    int getNrOfBotsInRangeOfBotWithLargestRadius();
    long long getMinAndMaxDistance();
private:
    bool botIsInRange(const botInfo & bot1, const botInfo & bot2);

    botInfo getBotWithLargestRadius();
    int nrOfBotsInRange(botInfo centerBot);


    std::vector<botInfo> bots;
};


#endif //INC_2018_BOTINFORMATIONCOLLECTOR_H
