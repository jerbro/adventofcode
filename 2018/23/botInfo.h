//
// Created by jeroen on 3-7-19.
//

#ifndef INC_2018_BOTINFO_H
#define INC_2018_BOTINFO_H
#include "Coord.h"

class botInfo {
    public:
        Coord pos;
        long long radius;

};


bool operator== (const botInfo& b1, const botInfo& b2);
bool operator!= (const botInfo& b1, const botInfo& b2);

#endif //INC_2018_BOTINFO_H
