//
// Created by jeroen on 3-7-19.
//

#include "botInformationCollector.h"
#include <set>
#include <list>
#include <iostream>
#include <iomanip>

bool botInformationCollector::botIsInRange(const botInfo & bot1, const botInfo & bot2){
    long long currRadius = bot1.radius;
    long long distance = manhattanDistance(bot1.pos,bot2.pos);
    return (distance<=currRadius);
}

botInfo botInformationCollector::getBotWithLargestRadius(){
    long long largestRadius=0;
    botInfo result;
    for (const botInfo& bot: bots){
        if (bot.radius > largestRadius){
            largestRadius=bot.radius;
            result = bot;
        }
    }
    return result;
}

int botInformationCollector::nrOfBotsInRange(botInfo centerBot){
    int result = 0;
    for (const auto& bot:bots){
        if (botIsInRange(centerBot,bot)){
            result ++;}
    }
    return result;
}

int botInformationCollector::getNrOfBotsInRangeOfBotWithLargestRadius(){
    auto centerbot = getBotWithLargestRadius();
    return nrOfBotsInRange(centerbot);
}

botInformationCollector::botInformationCollector(std::vector<botInfo> bots):bots(bots){

}
void botInformationCollector::setBots(std::vector<botInfo> bots){
    this->bots = bots;
}

long long botInformationCollector::getMinAndMaxDistance(){

    std::list<std::pair<long long, bool>> minmaxDistAndType;

    for (auto bot:bots){
        auto dist = manhattanDistance(Coord{0, 0, 0}, bot.pos);

        auto rad = bot.radius;
        auto mindist = dist-bot.radius;
        auto maxdist = dist+bot.radius;
        minmaxDistAndType.push_back(std::make_pair(dist-bot.radius,false));
        minmaxDistAndType.push_back(std::make_pair(dist+bot.radius,true));

    }

    minmaxDistAndType.sort();

    int mostOccurring = 0;
    int currOccurring=0;
    long long mostOccurringDist=0;
    bool getnext;

    for (auto item: minmaxDistAndType){
        if (item.second){
            if (currOccurring == mostOccurring){
                mostOccurringDist = item.first;//For this set, it seems that the last distance with the highest number of occurrences is the answer.
            }
            currOccurring--;
        }
        else{
            currOccurring++;
        }

        if (mostOccurring<currOccurring){
            mostOccurring = currOccurring;
        }

    }
    return mostOccurringDist;

}