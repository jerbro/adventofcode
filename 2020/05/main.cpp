#include <iostream>
#include <fstream>
#include <vector>
#include <regex>

struct Seat{int row;
int column;
int seatId;};
bool operator < (const Seat& a, const Seat&b){return a.seatId<b.seatId;}
std::vector<Seat> readFile(const std::string &filename);

int findUnusedSeatId(std::vector<Seat> seats);


int main() {
    auto seatsData = readFile("input.txt");
    auto answerA = std::max_element(seatsData.begin(), seatsData.end());

    auto answerB = findUnusedSeatId(seatsData);

    std::cout << "Advent of code 2020, day 04" << std::endl;
    std::cout << "A: " << answerA->seatId << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}

int findUnusedSeatId(std::vector<Seat> seats){
  std::sort(seats.begin(),seats.end());
  int previd = 0;
  for (auto& seat:seats){
      if (previd +2 == seat.seatId)
      {
          return previd+1; // Missing seat is found.return id of missing seat
      } else {
          previd = seat.seatId;
      }
  }

    return 0;
}


// Parse a line. A line can contain multiple space-separated keyvaluepairs.
Seat parseLine(std::string line) {
    Seat result;
    result.column=0;
    result.row=0;
    for (auto ch:line){
        switch (ch){
            case 'B':
                result.column *=2;
                result.column +=1;
                break;
            case 'F':
                result.column *=2;
                break;
            case 'R':
                result.row *=2;
                result.row +=1;
                break;
            case 'L':
                result.row *=2;
                break;
            default:
                break;
        }
    }
    result.seatId = result.column*8+result.row;
    return result;
}

std::vector<Seat> readFile(const std::string &filename) {
    std::vector<Seat> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    std::map<std::string, std::string> singlePassportData;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        result.push_back(parseLine(line));
    }
    fs.close();
    return result;
}
