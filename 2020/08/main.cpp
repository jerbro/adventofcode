#include <iostream>
#include <fstream>
#include <vector>
#include "Processor.h"


using namespace day8;
std::vector<std::string> readFile(const std::string &filename);
long long getAccWhenProgramEndsAtPastLastInstruction(std::vector<std::string> programcode);

int main() {
    auto programCode = readFile("input.txt");
    Processor proc(programCode);
    auto answerA = proc.executeStopAtFirstDupclicateInstruction();

    auto answerB = getAccWhenProgramEndsAtPastLastInstruction(programCode);
    std::cout << "Advent of code 2020, day 08" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

long long getAccWhenProgramEndsAtPastLastInstruction(std::vector<std::string> programcode){
    Processor proc(programcode);
    for (auto i=0;i<proc.getInstructionsCount();i++){
        proc.resetAccPc();
        proc.swapJmpNop(i);
        proc.executeStopAtFirstDupclicateInstruction();
        if (proc.getPc() == proc.getInstructionsCount()){
            return proc.getAcc();
        }
        proc.swapJmpNop(i);

    }
    return proc.getAcc();
}


std::vector<std::string> readFile(const std::string &filename) {
    std::vector<std::string> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (!line.empty())
            result.push_back(line);
    }

    fs.close();
    return result;
}


