//
// Created by jeroen on 18-12-18.
//

#include "Command.h"
namespace day8 {
    Command::Command(std::string commandName) : commandName(commandName) {};

    std::string Command::getInstructionName() { return commandName; }

    cmdAcc::cmdAcc() : Command("acc") {};

    void cmdAcc::execute(long long &pc, long long &acc, long long &oper) {
        acc += oper;
        pc += 1;
    }

    cmdJmp::cmdJmp() : Command("jmp") {};

    void cmdJmp::execute(long long &pc, long long &acc, long long &oper) {
        pc += oper;
    }

    cmdNop::cmdNop() : Command("nop") {};

    void cmdNop::execute(long long &pc, long long &acc, long long &oper) {
        pc += 1;
    }
}