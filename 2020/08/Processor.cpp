//
// Created by jeroen on 18-12-18.
//

#include <sstream>
#include <iostream>
#include <set>
#include "Processor.h"
#include "Instruction.h"
namespace day8 {
    Processor::Processor(std::vector<std::string> programCode) {
        registerCommands();
        for (auto &inst:programCode) {
            this->instructions.push_back(parseInstruction(inst));
        }
    }


    void Processor::registerCommands() {
        std::vector<Command *> currCommands;
        if (commands.empty()) {
            currCommands.push_back(new cmdAcc());
            currCommands.push_back(new cmdNop());
            currCommands.push_back(new cmdJmp());


            for (auto &cmnd:currCommands) {
                commands.insert(std::make_pair<std::string, Command *>(cmnd->getInstructionName(), &(*cmnd)));
            }
        }
    }

    void Processor::executeStopAtFirstDupclicateInstructionOrPcOutOfBounds() {
        std::set<long long> executedInstructionPcs;
        resetAccPc();
        bool keeprunning = true;
        while (keeprunning) {
            if (executedInstructionPcs.find(pc) == executedInstructionPcs.end())
                executedInstructionPcs.insert(pc);
            else {
                keeprunning = false;
                break;
            }

            if (pc >= 0 && pc < instructions.size())
                instructions.at(pc).execute(pc, acc);
            else
                keeprunning = false;

        }
    }

    long long Processor::executeStopAtFirstDupclicateInstruction() {
        executeStopAtFirstDupclicateInstructionOrPcOutOfBounds();
        return acc;
    }

//swap a jmp for an nop-command (or vice versa). Return true if swappen. Return false if not swapped.
    bool Processor::swapJmpNop(long long pc) {
        if (instructions.at(pc).getCommandName() == "jmp") {
            instructions.at(pc).setCommand(commands.find("nop")->second);
            return true;
        }
        if (instructions.at(pc).getCommandName() == "nop") {
            instructions.at(pc).setCommand(commands.find("jmp")->second);
            return true;
        }
        return false;

    }


    Instruction Processor::parseInstruction(std::string instruction) {
        std::stringstream instruction_strstr{instruction};
        std::string instrname;
        long long oper;
        instruction_strstr >> instrname >> oper;

        if (commands.find(instrname) == commands.end()) {
            std::cout << "Error finding command: " << instrname << std::endl;
        }
        Command *foundCmd = commands.find(instrname)->second;


        return Instruction(foundCmd, oper);
    }
}