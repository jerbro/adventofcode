//
// Created by jeroen on 18-12-18.
//

#ifndef INC_2020_COMMAND_H
#define INC_2020_COMMAND_H


#include <string>
#include <vector>

namespace day8{

class Command {

private:
    std::string commandName;
    int commandId;

protected:
    std::vector<long long>* registers;
public:
    Command(std::string commandName);
    virtual void execute(long long &pc, long long& acc,long long& oper)=0;
    std::string getInstructionName();

};


class cmdAcc:public Command{
public:
    cmdAcc( );
    void execute(long long &pc, long long& acc,long long& oper);
};

class cmdNop:public Command{
public:
    cmdNop( );
    void execute(long long &pc, long long& acc,long long& oper);
};

class cmdJmp:public Command{
public:
    cmdJmp( );
    void execute(long long &pc, long long& acc,long long& oper);
};


}
#endif //INC_2018_COMMAND_H
