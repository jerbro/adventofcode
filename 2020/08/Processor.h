//
// Created by jeroen on 18-12-18.
//

#ifndef INC_2018_PROCESSOR_H
#define INC_2018_PROCESSOR_H
#include <vector>
#include <map>
#include "Command.h"
#include "Instruction.h"

namespace day8 {
    class Processor {

    public:
        Processor(std::vector<std::string> programCode);

        long long executeStopAtFirstDupclicateInstruction();

        long long getAcc() { return acc; };

        long long getPc() { return pc; };

        long long getInstructionsCount() { return instructions.size(); }

        bool swapJmpNop(long long pc);

        void resetAccPc() {
            acc = 0;
            pc = 0;
        };
    private:
        std::vector<Instruction> instructions;

        void executeStopAtFirstDupclicateInstructionOrPcOutOfBounds();

        Instruction parseInstruction(std::string instruction);

        void registerCommands();

        std::map<std::string, Command *> commands;
        long long acc;
        long long pc;


    };

}
#endif //INC_2018_PROCESSOR_H
