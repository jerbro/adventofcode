//
// Created by jeroen on 18-12-18.
//

#ifndef INC_2020_INSTRUCTION_H
#define INC_2020_INSTRUCTION_H


#include "Command.h"
namespace day8 {
    class Command;

    class Instruction {
    public:
        Instruction(Command *cmd, long long cmdoper1) : command(cmd), commandOper(cmdoper1) {};

        std::string getCommandName() { return command->getInstructionName(); };

        void setCommand(Command *cmd) { command = cmd; }

        void execute(long long &pc, long long &acc);


    private:
        Command *command;
        long long commandOper;


    };

}
#endif //INC_2020_INSTRUCTION_H
