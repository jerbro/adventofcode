//
// Created by jeroen on 18-12-18.
//

#include "Instruction.h"
namespace day8 {
    void Instruction::execute(long long &pc, long long &acc) {
        command->execute(pc, acc, commandOper);
    }
}