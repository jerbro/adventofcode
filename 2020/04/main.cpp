#include <iostream>
#include <fstream>
#include <vector>
#include <regex>


std::vector<std::map<std::string, std::string>> readFile(const std::string &filename);

bool passportIsValidBasedOnFieldsPresent(std::map<std::string, std::string> passportData);

bool passportIsValidBasedOnFieldsAndValues(std::map<std::string, std::string> passportData);

int main() {
    auto passportsData = readFile("input.txt");
    auto answerA = std::count_if(passportsData.begin(), passportsData.end(), passportIsValidBasedOnFieldsPresent);

    auto answerB = std::count_if(passportsData.begin(), passportsData.end(), passportIsValidBasedOnFieldsAndValues);

    std::cout << "Advent of code 2020, day 04" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}

bool yrvalid(std::string value, int min, int max) {
    int value_nr = std::stoi(value);
    return value_nr >= min && value_nr <= max;

}

bool hghtvalid(std::string value, int minin, int maxin, int mincm, int maxcm) {
    std::string unit = value.substr(value.length() - 2, 2);
    int value_nr = std::stoi(value.substr(0, value.length() - 2));
    if (unit == "cm" && value_nr >= mincm && value_nr <= maxcm)
        return true;
    if (unit == "in" && value_nr >= minin && value_nr <= maxin)
        return true;
    return false;
}

bool regexVerify(std::string value, std::regex regexpr) {
    std::smatch matchResult;
    bool retVal = std::regex_search(value, matchResult, regexpr);
    return retVal;
}

bool passportIsValidBasedOnFieldsAndValues(std::map<std::string, std::string> passportData) {
    if (!passportIsValidBasedOnFieldsPresent(passportData)) {
        return false;
    }

    return ((yrvalid(passportData.find("byr")->second, 1920, 2002)) &&
            (yrvalid(passportData.find("iyr")->second, 2010, 2020)) &&
            (yrvalid(passportData.find("eyr")->second, 2020, 2030)) &&
            (hghtvalid(passportData.find("hgt")->second, 59, 76, 150, 193)) &&
            regexVerify(passportData.find("hcl")->second, std::regex("#[0-9a-f]{6}")) &&
            regexVerify(passportData.find("ecl")->second, std::regex("amb|blu|brn|gry|grn|hzl|oth")) &&
            regexVerify(passportData.find("pid")->second, std::regex("^[0-9]{9}$")));

}

bool passportIsValidBasedOnFieldsPresent(std::map<std::string, std::string> passportData) {
    std::vector requiredFields = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"};
    for (auto &field:requiredFields) {
        if (passportData.find(field) == passportData.end())
            return false;
    }
    return true;

}

//Get key and value from a keyvaluepair. Key and value are separated by ':'
std::pair<std::string, std::string> getKeyValue(std::string keyvaluepair) {
    std::string key, value;
    int separator = keyvaluepair.find_first_of(':', 0);
    key = keyvaluepair.substr(0, separator);
    value = keyvaluepair.substr(separator + 1, keyvaluepair.length() - 1 - separator);
    return std::make_pair(key, value);

}

// Parse a line. A line can contain multiple space-separated keyvaluepairs.
std::map<std::string, std::string> parseLine(std::string line) {
    std::map<std::string, std::string> result;
    int startpos = 0;
    int currEndPos;
    do {
        currEndPos = line.find_first_of(' ', startpos);
        if (currEndPos == std::string::npos)
            currEndPos = line.length();
        result.insert(getKeyValue(line.substr(startpos, currEndPos - startpos)));
        startpos = currEndPos + 1;
    } while (startpos < line.length());
    return result;
}

std::vector<std::map<std::string, std::string>> readFile(const std::string &filename) {

    std::vector<std::map<std::string, std::string>> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    std::map<std::string, std::string> singlePassportData;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (line.empty()) {
            result.push_back(singlePassportData);
            singlePassportData.clear();
        } else {
            auto newData = parseLine(line);
            singlePassportData.insert(newData.begin(), newData.end());
        }
    }
    fs.close();
    return result;
}
