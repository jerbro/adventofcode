#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>


std::pair<int,std::vector<int>> readFile(const std::string &filename);
int getAnswerA(std::pair<int,std::vector<int>>  linesAndTime );
long long getAnswerB(std::vector<int> buslines);

int main() {
    std::pair<int,std::vector<int>>  linesAndTime = readFile("input.txt");
    auto answerA = getAnswerA(linesAndTime);
    auto answerB = getAnswerB(linesAndTime.second);
    std::cout << "Advent of code 2020, day 13" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;



    return 0;
}

struct lineAndOffset{
    long long line;
    long long offset;
};

long long getAnswerB(std::vector<int> buslines){
    std::vector<lineAndOffset > linesAndOffset;
    for (int i=0;i<buslines.size();i++){
        if (buslines.at(i) >0){
            lineAndOffset currline;
            currline.line=buslines.at(i);
            currline.offset=i;
            linesAndOffset.push_back(currline);
        }
    }

    long long stepsize = 1;
    long long currTime =0;
    lineAndOffset lineToDelete;

    while (linesAndOffset.size()>0){
        currTime+=stepsize;
        for (int i=0;i<linesAndOffset.size();i++){
            auto line = linesAndOffset.at(i);
            if ((currTime+line.offset)%line.line==0 ){
                stepsize*=line.line;
                linesAndOffset.erase(linesAndOffset.begin()+i);
                break;
            }
        }
    }
    return currTime;

}


int getAnswerA(std::pair<int,std::vector<int>>  linesAndTime ){
    int arriveTime = linesAndTime.first;
    int shortestWaitTime=linesAndTime.first;
    int buslineShortestWaittime=linesAndTime.first;

    for (auto& line:linesAndTime.second){
        int waitingtime = line-(arriveTime%line);
        if (waitingtime < shortestWaitTime && line>0){
            shortestWaitTime =waitingtime;
            buslineShortestWaittime = line;
        }
    }
    return buslineShortestWaittime*shortestWaitTime;

}

std::vector<int> parseBusLines(std::string line){
    std::vector<int> result;
    std::string part;
    for(auto ch:line){
        if (ch != ',' && ch != '\r'&& ch != '\n'){
            part.push_back(ch);
        } else if (part.length()>0){
            if (part == "x"){
                result.push_back(-1);
            } else {
                result.push_back(stoi(part));
            }
            part="";
        }
    }
    if (part.size()>0){
        result.push_back(stoi(part));
    }
    return result;
}
std::pair<int,std::vector<int>> readFile(const std::string &filename) {
    std::pair<int,std::vector<int>> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    if (fs && !fs.eof()) {
        std::getline(fs, line);
        result.first = stoi(line);

        std::getline(fs, line);
        result.second = parseBusLines(line);
    }
    fs.close();
    return result;
}

