#include <iostream>
#include <fstream>
#include <vector>
#include <regex>


std::vector<std::string> readFile(const std::string &filename);

struct Path {
    int right;
    int down;
};

int countTreesOnPath(const Path &path, const std::vector<std::string> &map);

long prodOfAllPaths(const std::vector<Path> &paths, const std::vector<std::string> &map);


int main() {
    auto treesMap = readFile("input.txt");
    auto answerA = countTreesOnPath(Path{3, 1}, treesMap);
    std::vector<Path> allPaths = {Path{1, 1}, Path{3, 1}, Path{5, 1}, Path{7, 1}, Path{1, 2}};
    auto answerB = prodOfAllPaths(allPaths, treesMap);

    std::cout << "Advent of code 2020, day 03" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}

char getCharAtPos(const int x, const int y, const std::vector<std::string> &map) {
    if (y < map.size()) {
        auto mapWidth = map.at(y).length();
        return map.at(y).at(x % mapWidth);
    }
    return 0;
}


long prodOfAllPaths(const std::vector<Path> &paths, const std::vector<std::string> &map) {
    long result = 1;
    for (auto path:paths) {
        result *= countTreesOnPath(path, map);
    }
    return result;

}


int countTreesOnPath(const Path &path, const std::vector<std::string> &map) {
    int x = 0;
    int y = 0;
    char ch ;
    int treeCount = 0;
    do {
        ch = getCharAtPos(x, y, map);
        if (ch == '#')
            treeCount++;
        x += path.right;
        y += path.down;
    } while (ch != 0);
    return treeCount;
}


std::vector<std::string> readFile(const std::string &filename) {
    std::vector<std::string> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (!line.empty()) {
            result.push_back(line);
        }
    }
    fs.close();
    return result;
}
