#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>

struct FoodDescription{
    std::set<std::string> ingredients;
    std::set<std::string> allergens;
};

FoodDescription getValuesFromLine(const std::string &line);

std::vector<FoodDescription> readFile(const std::string &filename);

long long getAnswerA(const std::vector<FoodDescription> &foodDescriptions);
std::string getAnswerB(const std::vector<FoodDescription> &foodDescriptions);

int main() {
    auto  filecontent = readFile("input.txt");

   auto answerA = getAnswerA(filecontent);
   auto answerB = getAnswerB(filecontent);

    std::cout << "Advent of code 2020, day 21" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;
    return 0;
}

std::map<std::string,std::set<std::string>> getAllergenAndPossibleCauses(const std::vector<FoodDescription> &foodDescriptions){
    std::map<std::string,std::set<std::string>> retval;
    for (const auto& item:foodDescriptions){
        for (const auto& allergen:item.allergens){
            if (retval.find(allergen) != retval.end()){
                // allergen was already found. Check if all ingredients that previously could cause the allergen are
                // also available in this food. If not, dismiss the ingredient
                std::set<std::string>dismissedIngredients;
                for (auto possibleCause :retval.find(allergen)->second){
                    if (item.ingredients.find(possibleCause) == item.ingredients.end()) {
                        dismissedIngredients.insert( possibleCause);
                    }
                }
                for (auto ing:dismissedIngredients){
                    retval.find(allergen)->second.erase(ing);
                }
            } else {
                //allergen not yet found. All ingredients are possible causes
                retval.insert(std::make_pair(allergen, item.ingredients));
            }
        }
    }
    return retval;

}

std::set<std::string> getIngredientsPossilbyHavingAllergen(const std::vector<FoodDescription> &foodDescriptions){
    auto allergensAndPossibleCauses = getAllergenAndPossibleCauses(foodDescriptions);
    std::set<std::string> retval;
    for (auto item:allergensAndPossibleCauses){
        for (auto ing:item.second){
            retval.insert(ing);
        }
    }
    return retval;
}

std::map<std::string,std::string> getAllergenAndCause(const std::vector<FoodDescription> &foodDescriptions){
    auto allergenPossibleCauses = getAllergenAndPossibleCauses(foodDescriptions);
    std::map<std::string,std::string> retval;
    std::set<std::string> usedIngredients;
    while (!allergenPossibleCauses.empty()){
        std::string currItem;
        bool removeCurrItem = false;
        for (auto &item:allergenPossibleCauses){
            if (item.second.size() == 1){
                usedIngredients.insert(*item.second.begin());
                currItem = item.first;
                removeCurrItem = true;
                retval.insert(std::make_pair(item.first, *item.second.begin()));
                break;
            }
        }

        if (removeCurrItem){
            allergenPossibleCauses.erase(currItem);
        }

        //remove all unused ingredients for every allergen
        for (auto &item:allergenPossibleCauses){
            for (auto ing:usedIngredients){
                if (item.second.find(ing) != item.second.end()){
                    item.second.erase(ing);
                }
            }
        }
    }
    return retval;

}

long long getAnswerA(const std::vector<FoodDescription> &foodDescriptions){
    auto possibleAllergenIngredients = getIngredientsPossilbyHavingAllergen(foodDescriptions);
    int counter=0;
    for (auto food:foodDescriptions){
        for (auto ingredient:food.ingredients){
            if (possibleAllergenIngredients.find(ingredient) == possibleAllergenIngredients.end()){
                counter++;
            }
        }
    }
    return counter;
}

std::string getAnswerB(const std::vector<FoodDescription> &foodDescriptions){
    auto allergenCause = getAllergenAndCause(foodDescriptions);
    std::string result;
    bool first = true;
    for (auto item:allergenCause) {
        if (!first){
            result.push_back(',');
        }
        first = false;
        result.append(item.second);
    }
    return result;
}

FoodDescription getValuesFromLine(const std::string &line){
    std::string ingredientsString = line;
    std::set<std::string> ingredients;
    std::string allergensString = "";
    std::set<std::string> allergens;
    if (line.find('(')){
        ingredientsString = line.substr(0, line.find('('));
        allergensString = line.substr(line.find('('));
    }

    //Parse the ingredients
    std::stringstream strstr {ingredientsString};
    while(strstr){
        std::string ingr;
        strstr>>ingr;
        if (ingr != "")
            ingredients.insert(ingr);
    }

    //Parse the alergens
    if (allergensString.substr(0,10) == "(contains "){
      allergensString = allergensString.substr(10);
        std::stringstream allergensStrStr {allergensString};
        while(allergensStrStr){
            std::string allerg;
            allergensStrStr>>allerg;
            if (allerg != ""){
                if (allerg.substr(allerg.size()-1) == ","|| allerg.substr(allerg.size()-1) == ")"){
                    allerg = allerg.substr(0, allerg.size() - 1);
                }
                allergens.insert(allerg);
            }
        }
    }
    return FoodDescription{ingredients,allergens};
}

std::vector<FoodDescription> readFile(const std::string &filename){
    std::vector<FoodDescription> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (line != "")
            result.push_back(getValuesFromLine(line));
    }
    fs.close();
    return result;
}

