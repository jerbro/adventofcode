#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>




std::vector<long> readFile(const std::string &filename);
long findFirstNumberNotMultipleOfPreviusXnrs(std::vector<long> &numberslist, int preamble, int searchLength);
long findSmallestPlusLargestOfRangeWichSumsUpToNr(std::vector<long> & numberslist, long nrToReach);

int main() {
    auto numberslist = readFile("input.txt");
    auto answerA = findFirstNumberNotMultipleOfPreviusXnrs(numberslist,25,25);
    auto answerB = findSmallestPlusLargestOfRangeWichSumsUpToNr(numberslist,answerA);
    std::cout << "Advent of code 2020, day 09" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

long findSmallestPlusLargestOfRangeWichSumsUpToNr(std::vector<long> & numberslist, long nrToReach){
    long start=0;
    long end=0;
    long currSum = 0;
    bool keepTrying=true;
    while (end<(numberslist.size()-1) && keepTrying){
        if (currSum < nrToReach){
            end+=1;
            currSum+=numberslist.at(end);
        } else if (currSum > nrToReach){
            start+=1;
            currSum-=numberslist.at(start);
        } else if (currSum == nrToReach){
            keepTrying=false;
        }

    }
    int smallest = numberslist.at(start+1);
    int largest = numberslist.at(start+1);
    for (int i=start+1;i<=end;i++){
        if (numberslist.at(i)<smallest)
            smallest=numberslist.at(i);
        if (numberslist.at(i)>largest)
            largest=numberslist.at(i);

    }
    return smallest+largest;
}


bool numberIsSumOfNumbersInList(long nr,const std::vector<long>& numbersList, long start, long end ){
    for (int i=start;i<end;i++){
        for (int j=i+1;j<=end;j++){
            long a=numbersList.at(i);
            long b=numbersList.at(j);
            if (a!=b && a+b==nr)
                return true;
        }
    }
    return false;
}

long findFirstNumberNotMultipleOfPreviusXnrs(std::vector<long> &numberslist, int preamble, int searchLength){

    for (int i=preamble;i<numberslist.size();i++){
        if (!numberIsSumOfNumbersInList(numberslist.at(i),numberslist,i-searchLength,i-1))
            return numberslist.at(i);
    }


}



std::vector<long> readFile(const std::string &filename) {
    std::vector<long> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    std::map<std::string, std::string> singlePassportData;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (!line.empty())
            result.push_back(std::stol(line));
    }
    fs.close();
    return result;
}

