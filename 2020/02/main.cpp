#include <iostream>
#include <fstream>
#include <vector>
#include <regex>


struct pwdDesc {
    int low=0;
    int high=0;
    char letter=' ';
    std::string passwd;
};

std::vector<pwdDesc> readFile(const std::string &filename);


bool isValidPassword(pwdDesc pwd);

bool isValidTobogganPassword(pwdDesc pwd);

int main() {
    auto passwordsList = readFile("input.txt");
    auto answerA = std::count_if(passwordsList.begin(), passwordsList.end(), isValidPassword);
    auto answerB = std::count_if(passwordsList.begin(), passwordsList.end(), isValidTobogganPassword);

    std::cout << "Advent of code 2020, day 02" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}


bool isValidPassword(pwdDesc pwd) {
    long nrLetter = std::count(pwd.passwd.begin(), pwd.passwd.end(), pwd.letter);
    return (nrLetter >= pwd.low && nrLetter <= pwd.high);
}

bool isValidTobogganPassword(pwdDesc pwd) {
    if (pwd.passwd.length() < pwd.high || pwd.low < 1)
        return false;

    char firstCh = pwd.passwd.at(static_cast<unsigned long>(pwd.low - 1));
    char secondCh = pwd.passwd.at(static_cast<unsigned long>(pwd.high - 1));
    if (firstCh != secondCh && (firstCh == pwd.letter || secondCh == pwd.letter))
        return true;
    return false;
}

//Lines from file are like: 2-9 c: ccccccccc
// This means: low: 2; high: 9; character: c; passwd: ccccccccc
pwdDesc parseLine(const std::string &line) {
    pwdDesc result;
    std::regex pwdLine("([0-9]+)-([0-9]+) (.): ([0-9a-zA-Z]*)");
    std::smatch match;
    std::regex_match(line, match, pwdLine);
    if (match.size() == 5) {
        result.low = std::stoi(match[1]);
        result.high = std::stoi(match[2]);
        result.letter = std::string(match[3]).at(0);
        result.passwd = match[4];
    }
    return result;
}


std::vector<pwdDesc> readFile(const std::string &filename) {
    std::vector<pwdDesc> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (!line.empty()) {
            result.push_back(parseLine(line));
        }
    }


    fs.close();
    return result;
}
