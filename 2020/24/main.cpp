#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>
#include <list>
#include <array>

enum class direction{
    se,ne,e,nw,sw, w
};

struct coordinate {
    int q;
    int r;
};

bool operator==(const coordinate& lhs, const coordinate& rhs) {
    return lhs.q == rhs.q&& lhs.r == rhs.r;
}

bool operator<= (const coordinate& lhs, const coordinate& rhs){
    return (lhs.q <= rhs.q || (lhs.q == rhs.q && lhs.r <= rhs.r));
}

bool operator< (const coordinate& lhs, const coordinate& rhs){
    return (lhs.q < rhs.q || (lhs.q == rhs.q && lhs.r < rhs.r));
}

coordinate operator+(coordinate lhs, const coordinate& rhs){
    coordinate retval;
    retval.q = lhs.q + rhs.q;
    retval.r = lhs.r + rhs.r;
    return retval;
}


coordinate& operator +=(coordinate& lhs, const coordinate& rhs){
    lhs.q += rhs.q;
    lhs.r += rhs.r;
    return lhs;
}

// Change in coordinates by a step into a direction.
// Axial coordinates are used as described on: https://www.redblobgames.com/grids/hexagons/
std::map<direction,coordinate> stepEffects{
{direction::ne, {1,-1}},
{direction::e, {1,0}},
{direction::se, {0,1}},
{direction::sw, {-1,1}},
{direction::w, {-1,0}},
{direction::nw, {0,-1}}
};


std::vector<std::vector<direction>> readFile(const std::string &filename);

long long getAnswerA(const std::vector<std::vector<direction>>  &directions);
long long getAnswerB(const std::vector<std::vector<direction>>  &directions);

int main() {
    auto  fileContent = readFile("input.txt");

   auto answerA = getAnswerA(fileContent);
   auto answerB = getAnswerB(fileContent);

    std::cout << "Advent of code 2020, day 24" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;
    return 0;
}

coordinate getPositionAfterSteps(std::vector<direction> steps){
    coordinate result{0,0};
    for (auto step:steps){
        result += stepEffects.at(step);
    }
    return result;
}


std::set<coordinate> getSurroundingCoordinates(coordinate currPos){
    std::set<coordinate> result;
    for (auto step:stepEffects){
        result.insert(currPos + step.second);
    }
    return result;
}

struct tileInfo{
    int surroundingBlackTiles=0;
    bool isBlack = false;
};

void flipTiles(std::set<coordinate>& currBlackTiles, const std::set<coordinate>& tilesToFlip){
    for (auto fliptile:tilesToFlip){
        if (currBlackTiles.find(fliptile) != currBlackTiles.end()) {
            currBlackTiles.erase(fliptile);
        } else {
            currBlackTiles.insert(fliptile);
        }
    }
}

void doDailyFlipping(std::set<coordinate>& blackTiles){
    std::map<coordinate,tileInfo> tilesToConsider;
    for (auto tileCoord:blackTiles){
        tilesToConsider[tileCoord].isBlack = true;
        for (auto surroundingTile:getSurroundingCoordinates(tileCoord)){
            tilesToConsider[surroundingTile].surroundingBlackTiles++;
        }
    }

    std::set<coordinate> tilesToFlip;
    for (auto tile:tilesToConsider){
        // flip if tile is black, and 0 or more than 2 surrounding tiles are black, or tile is white and exactly 2 surrounding tiles are black
        if ((tile.second.isBlack && (tile.second.surroundingBlackTiles ==0 || tile.second.surroundingBlackTiles>2))
         || (! tile.second.isBlack && tile.second.surroundingBlackTiles ==2)){
            tilesToFlip.insert(tile.first);
        }
    }
    flipTiles(blackTiles, tilesToFlip);

}

std::set<coordinate> getFlippedTilesFromCoordinates(const std::vector<std::vector<direction>>  &directions){
    std::set<coordinate> flippedTiles;
    for (auto currTileSteps:directions){
        auto currTile = getPositionAfterSteps(currTileSteps);
        if (flippedTiles.find(currTile) != flippedTiles.end()){
            flippedTiles.erase(currTile);
        } else {
            flippedTiles.insert(currTile);
        }
    }
    return flippedTiles;
}

long long getAnswerA(const std::vector<std::vector<direction>>  &directions){
    auto flippedTiles = getFlippedTilesFromCoordinates(directions);

    return flippedTiles.size();
}

long long getAnswerB(const std::vector<std::vector<direction>>  &directions){
    auto flippedTiles = getFlippedTilesFromCoordinates(directions);

    for (int i=0;i<100;i++){
        doDailyFlipping(flippedTiles);
    }

    return flippedTiles.size();

}

std::vector<direction> parseLine(const std::string &line){
    std::vector<direction> result;
    bool useWithNextCh = false;
    char prevChar = '0';
    for (auto ch:line){
        if (useWithNextCh){
            if (ch =='e' && prevChar == 'n'){
                result.push_back(direction::ne);
            } else if (ch =='e' && prevChar == 's'){
                result.push_back(direction::se);
            } else if (ch =='w' && prevChar == 'n'){
                result.push_back(direction::nw);
            } else if (ch =='w' && prevChar == 's'){
                result.push_back(direction::sw);
            }
            useWithNextCh = false;
        } else {
            if (ch =='n' || ch=='s'){
                useWithNextCh = true;
                prevChar = ch;
            } else if (ch =='e'){
                result.push_back(direction::e);
            } else if (ch =='w'){
                result.push_back(direction::w);
            }
        }
    }
    return result;
}

std::vector<std::vector<direction>> readFile(const std::string &filename){
    std::vector<std::vector<direction>> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);
    std::string line;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (!line.empty()) {
            result.push_back(parseLine(line));
        }
    }

    fs.close();
    return result;
}

