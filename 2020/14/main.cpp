#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>

struct lineData{
    bool isMemData;
    long memAddress;
    long memWishValue;
    long maskAndValues;
    long maskOrValues;
    std::string maskString;
    std::string rawData;
};

std::vector<lineData> readFile(const std::string &filename);
long getAnswerA(std::vector<lineData> data);
long long getAnswerB(std::vector<lineData> data);

int main() {
    std::vector<lineData> linesData = readFile("input.txt");
    auto answerA = getAnswerA(linesData);
    auto answerB = getAnswerB(linesData);
    std::cout << "Advent of code 2020, day 14" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;
    return 0;
}

void handleLine(const lineData& data, std::map<long,long>& memAddresses, long &currOrMask, long& currAndMask){
    if (data.isMemData){
        memAddresses[data.memAddress] = ((data.memWishValue | currOrMask) & currAndMask);
    } else {
        currOrMask=data.maskOrValues;
        currAndMask = data.maskAndValues;
    }
}

long getAnswerA(std::vector<lineData> data){
    long currOrMask = 0;
    long currAndMask = 2^37-1;
    std::map<long,long> memAddresses;
    for (const auto& currLine:data){
        handleLine(currLine, memAddresses, currOrMask, currAndMask);
    }
    long result=0;
    for (auto memdata:memAddresses){
        result += memdata.second;
    }
    return result;

}

void changeAllMemAddressValues(const lineData& data, std::map<long,long>& memAddresses, std::string& currMask){
    if (!data.isMemData){
        currMask = data.maskString;
        return;
    }
    //Data is memory-data.

    std::string address = std::bitset<36>(data.memAddress).to_string();
    std::vector<long> floatingBits;
    for (int i=0;i<currMask.length();i++){
        switch(currMask.at(i)){
            case '0':

                break;
            case '1':
                address.at(i) = '1';
                break;
            case 'X':
                address.at(i) = 'X';
                floatingBits.push_back(currMask.length()-i-1);
                break;
        }
    }

    long addressBaseValue = 0;
    for (auto ch:address){
        addressBaseValue *= 2;
        if (ch=='1'){
            addressBaseValue+=1;
        }
    }

    long nrOfCombinations = 1<<floatingBits.size();
    for (int i=0;i<nrOfCombinations;i++){
       long addressExtra = 0;
       for (int j=0;j<floatingBits.size();j++){
           if ((i>>j &0x1) != 0){
               addressExtra |= 1l<<floatingBits.at(j);
           }
       }

       memAddresses[addressBaseValue+addressExtra]=data.memWishValue;
    }
    long long sumOfAllValues=0;
    for (auto memdata:memAddresses){
        sumOfAllValues += memdata.second;
    }
}

long long getAnswerB(std::vector<lineData> data){
    std::map<long, long> memAddressValues;
    std::string currAddressMask;
    for (const auto& currLine:data){
        changeAllMemAddressValues(currLine, memAddressValues, currAddressMask);
    }

    long long result=0;
    for (auto memdata :memAddressValues){
        result += memdata.second;

    }

    return result;

}


lineData parseMaskData(std::string maskdata){
    lineData result;
    long andMask = 0;
    long orMask = 0;
    for (auto ch:maskdata){
        andMask <<=1;
        orMask <<=1;
        if (ch=='X'){
            andMask |=1;
        } else if (ch =='1'){
            orMask |=1;
            andMask |=1;
        } else if (ch == '0'){
            //Nothing to do
        } else {
            std::cout << "Unknown character in mask.\r\n";
        }

    }
    result.maskString = maskdata;
    result.maskAndValues = andMask;
    result.maskOrValues = orMask;
    result.isMemData=false;
    return result;
}
lineData parseMemData(std::string memData){
    lineData result;
    result.isMemData=true;
    std::string currSubstr;
    for (auto ch:memData){
        if (ch>='0' && ch<='9'){
            currSubstr.push_back(ch);
        } else if (ch==']'){
            result.memAddress=stol(currSubstr);
            currSubstr.clear();
        }
    }
    result.memWishValue = stol(currSubstr);
    return result;
}

lineData parseLine(const std::string& line){
    lineData result;
    if (line.find("mask = ") != std::string::npos){
        std::string maskData = line.substr(7, line.length() - 7);
        result = parseMaskData(maskData);
    } else if (line.find("mem") != std::string::npos){
        std::string memData = line.substr(3, line.length() - 3);
        result = parseMemData(memData);
    }
    result.rawData=line;
    return result;
}

std::vector<lineData> readFile(const std::string &filename) {
    std::vector<lineData> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (!line.empty())
            result.push_back(parseLine(line));
    }
    fs.close();
    return result;
}

