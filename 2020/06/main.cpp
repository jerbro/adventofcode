#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>


std::vector<std::vector<std::string>> readFile(const std::string &filename);
int countGroupValue(std::vector<std::string> groupValues);
int countAllGroupsValue(std::vector<std::vector<std::string>> groupValues);
int partB(std::vector<std::vector<std::string>> groupValues);

int main() {
    auto groupsData = readFile("input.txt");
    auto answerA = countAllGroupsValue(groupsData);
    

    auto answerB = partB(groupsData);

    std::cout << "Advent of code 2020, day 04" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}

int countAllGroupsValue(std::vector<std::vector<std::string>> groupValues){
    int result = 0;
     for (const auto & group:groupValues){
         result += countGroupValue(group);
     }
     return result;
}


int countGroupValue(std::vector<std::string> groupValues){
    std::set<char> questions;
    for (auto person:groupValues){
        for (char ch:person){
            questions.insert(ch);
        }
    }
    return questions.size();
}

int partB_valuePerGroup(std::vector<std::string> groupValues){
    std::map<char,int> question_answers;
    for (const auto&person:groupValues){
        for (char ch:person){
            question_answers[ch]++;
        }
    }

    return std::count_if(question_answers.begin(),question_answers.end(),[groupValues](auto question){return  question.second == groupValues.size();});
}

int partB(std::vector<std::vector<std::string>> groupValues){
    int result = 0;
    for (const auto & group:groupValues){
        result += partB_valuePerGroup(group);
    }
    return result;

}

std::vector<std::vector<std::string>> readFile(const std::string &filename) {
    std::vector<std::vector<std::string>> result;
    std::vector<std::string> group;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    std::map<std::string, std::string> singlePassportData;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (line.empty()){
            result.push_back(group);
            group.clear();
        } else {
            group.push_back(line);
        }
    }
    if (!line.empty()){
        result.push_back(group);
    }
    fs.close();
    return result;
}

