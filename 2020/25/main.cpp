#include <iostream>
#include <fstream>
#include <vector>
#include <array>

std::vector<long long> readFile(const std::string &filename);

long long getAnswerA(const std::vector<long long> &pubKeys);
long long getAnswerB(const std::vector<long long> &pubKeys);

int main() {
    auto  fileContent = readFile("input.txt");

   auto answerA = getAnswerA(fileContent);
   auto answerB = getAnswerB(fileContent);

    std::cout << "Advent of code 2020, day 25" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;
    return 0;
}

long long findLoopSize(long long subject, long long pubkey){
    long long currVal =1;
    long long loops=0;
    while (currVal != pubkey){
        currVal *= subject;
        currVal %= 20201227;
        loops++;
    }
    return loops;
}

long long executeLoopXtimes(long long subject, long long loopCounter){
    long long retval = 1;
    for (int i=0;i<loopCounter;i++){
        retval *= subject;
        retval %= 20201227;
    }
    return retval;
}

long long getAnswerA(const std::vector<long long> &pubKeys){
    int loopSize = findLoopSize(7, pubKeys.at(0));
    return executeLoopXtimes(pubKeys.at(1),  loopSize);
}

long long getAnswerB(const std::vector<long long> &pubKeys){

    return 0;

}

std::vector<long long> readFile(const std::string &filename){
    std::vector<long long> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);
    std::string line;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (!line.empty()) {
            result.push_back(std::stoi(line));
        }
    }

    fs.close();
    return result;
}

