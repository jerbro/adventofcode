#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>

struct Token{
    char type;
    long value;
};

std::vector<std::deque<Token>> readFile(const std::string &filename);
long solveSumA(std::deque<Token> sum);
long expressionA(std::deque<Token>& sum);
long termA(std::deque<Token>& sum);
long primaryA(std::deque<Token>& sum);

long solveSumB(std::deque<Token> sum);
long expressionB(std::deque<Token>& sum);
long termB(std::deque<Token>& sum);
long primaryB(std::deque<Token>& sum);


long getAnswerA (std::vector<std::deque<Token>> sums);
long getAnswerB (std::vector<std::deque<Token>> sums);
//long getAnswerB(std::set<coordinate> currActive);

int main() {
    auto  sums = readFile("input.txt");
    auto answerA = getAnswerA(sums);
    auto answerB = getAnswerB(sums);

    std::cout << "Advent of code 2020, day 18" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;
    return 0;
}




long getAnswerA(std::vector<std::deque<Token>> sums){
    long long result =0;
    for (auto&sum:sums){
        result += solveSumA(sum);
    }
    return result;
}

long getAnswerB(std::vector<std::deque<Token>> sums){
    long long result =0;
    for (auto&sum:sums){
        result += solveSumB(sum);
    }
    return result;
}


long solveSumA(std::deque<Token> sum){
    return expressionA(sum);

}

//Algorithm based on: 'principles and practrice using c++' , bjarne stroustrup, chapter 6
long expressionA(std::deque<Token>& sum){
    long left = termA(sum);
    while (true){
        if (sum.empty())
            return left;
        switch (sum.front().type) {
            case '*':
                sum.pop_front();
                left*=termA(sum);
                break;
            case '+':
                sum.pop_front();
                left+=termA(sum);
                break;
            default:
                return left;
        }
    }
}
long termA(std::deque<Token>& sum){
    return primaryA(sum);

}
long primaryA(std::deque<Token>& sum){
    long val;
    switch (sum.front().type) {
        case '(':
            {
                sum.pop_front();
                val  = expressionA(sum);
                if (sum.front().type!=')'){
                    std::cout<<"Error, expected )\r\n";
                }else{ sum.pop_front(); }
                return val;
            }
        case '8':
            val=sum.front().value;
            sum.pop_front();
            return val;
        default:
            std::cout << "Error, unexpected value\r\n";
            return -1;
    }
}

long solveSumB(std::deque<Token> sum){
    return expressionB(sum);

}

long expressionB(std::deque<Token>& sum){
    long left = termB(sum);
    while (true){
        if (sum.empty())
            return left;
        switch (sum.front().type) {
            case '*':
                sum.pop_front();
                left*=termB(sum);
                break;
            default:
                return left;
        }
    }
}
long termB(std::deque<Token>& sum){
    long left = primaryB(sum);
    while (true){
        if (sum.empty())
            return left;
        switch (sum.front().type) {
            case '+':
                sum.pop_front();
                left+=termB(sum);
                break;
            default:
                return left;
        }
    }

}
long primaryB(std::deque<Token>& sum){
    long val;
    switch (sum.front().type) {
        case '(':
        {
            sum.pop_front();
            val  = expressionB(sum);
            if (sum.front().type!=')'){
                std::cout<<"Error, expected )\r\n";
            }else{ sum.pop_front(); }
            return val;
        }
        case '8':
            val=sum.front().value;
            sum.pop_front();
            return val;
        default:
            std::cout << "Error, unexpected value\r\n";
            return -1;
    }
}

std::deque<Token> parseLine(std::string line){
    std::deque<Token> result;
    std::string currNr;
    for (auto&ch:line){
        if (ch>='0' && ch <='9'){
            currNr.push_back(ch);
        } else {
            if (currNr.size()>0){
                result.push_back(Token{'8', std::stol(currNr)});
                currNr.clear();
            }
            if (ch=='(' ||ch==')' ||ch=='+'||ch=='*'){
                result.push_back({ch, -1});
            }
        }
    }
    if (currNr.size()>0){
        result.push_back(Token{'8', std::stol(currNr)});
        currNr.clear();
    }
    return result;
}

std::vector<std::deque<Token>> readFile(const std::string &filename) {
    std::vector<std::deque<Token>> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;

    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (!line.empty()){
            result.push_back(parseLine(line));
        }


    }
    fs.close();
    return result;
}

