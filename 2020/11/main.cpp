#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>




std::vector<std::string> readFile(const std::string &filename);
int getAnswer(std::vector<std::string> seats, bool partA );

int main() {
    std::vector<std::string> seats = readFile("input.txt");
    auto answerA = getAnswer(seats,true);
    auto answerB = getAnswer(seats,false);
    std::cout << "Advent of code 2020, day 11" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

int getNrOfOccuppiedSeats(const std::vector<std::string> currSeatsOccuppied);
std::vector<std::string> getNextStep(const std::vector<std::string>& currSeatsOccuppied, bool &somethingChanged,bool partA);

void printSeats(const std::vector<std::string>& currSeatsOccuppied){
    for (auto&line:currSeatsOccuppied){
        std::cout<<line<<std::endl;
    }

}
int getAnswer(std::vector<std::string> seats, bool partA){
    bool keepRunning=true;
    auto nextStep = seats;
    bool changeDetected=false;
    int round=0;
    while (keepRunning)   {
        changeDetected=false;
        nextStep=getNextStep(nextStep,changeDetected,partA);
        if (!changeDetected)
            keepRunning=false;

    }
    return getNrOfOccuppiedSeats(nextStep);

}

int getNrOfOccuppiedSeats(const std::vector<std::string> currSeatsOccuppied){
    int result = 0;
    for (const auto& line:currSeatsOccuppied)
        for (const char& ch:line)
            if (ch=='#')
                result++;

    return result;

}

bool isValidLocation(int x, int y, int maxX, int maxY){
    return (x >= 0 && x < maxX && y >= 0 && y < maxY);
}

bool seatIsUsedInDirection(const std::vector<std::string>& currSeatsOccuppied, int seatx,int seaty, int dirx, int diry){
    bool keepRunning = true;
    bool result = false;
    int currx = seatx+dirx;
    int curry = seaty + diry;
    int maxy=currSeatsOccuppied.size();
    int maxx = currSeatsOccuppied[0].size();

    while (keepRunning){
        if (!isValidLocation(currx,curry,maxx,maxy) ){
            keepRunning = false;
        } else {
            char currLoc = currSeatsOccuppied[curry][currx];
            if (currLoc == '.'){
                currx+=dirx;
                curry+=diry;
            } else if (currLoc == 'L'){
                return false;
            } else {
                return true;
            }
        }

    }
    return false;
}

int getNrOfOccuppiedAdjecentSeatsFurtherLook(const std::vector<std::string>& currSeatsOccuppied, int seatx,int seaty){
    int result=0;
    std::vector<std::pair<int,int>> adjecentPositionOffsets = {{-1,-1},{0,-1},{1,-1},
                                                               {-1,0},{1,0},
                                                               {-1,1},{0,1},{1,1}};
    for (const auto&offset: adjecentPositionOffsets){
        if (seatIsUsedInDirection(currSeatsOccuppied, seatx, seaty, offset.first, offset.second)) {
            result++;
        }
    }
    return result;

}

int getNrOfOccuppiedAdjecentSeats(const std::vector<std::string>& currSeatsOccuppied, int seatx,int seaty);

std::vector<std::string> getNextStep(const std::vector<std::string>& currSeatsOccuppied, bool &somethingChanged,bool partA)
{
    int adjecentLimit = partA?4:5;


    somethingChanged=false;
    std::vector<std::string> result;
    std::string line;
    for (int y=0;y<currSeatsOccuppied.size();y++){
        line = currSeatsOccuppied[y];
        for (int x=0;x<currSeatsOccuppied[0].size();x++){
            int usedAdjecentSeats=0;
            if (partA)
                usedAdjecentSeats = getNrOfOccuppiedAdjecentSeats(currSeatsOccuppied,x,y);
            else
                usedAdjecentSeats = getNrOfOccuppiedAdjecentSeatsFurtherLook(currSeatsOccuppied,x,y);

            if (currSeatsOccuppied[y][x]=='L' &&  usedAdjecentSeats==0){
                line[x]='#';
                somethingChanged=true;
            }
            else if (currSeatsOccuppied[y][x]=='#' && usedAdjecentSeats>=adjecentLimit){
                line[x]='L';
                somethingChanged=true;
            }

        }
        result.push_back(line);
    }
    return result;


}



int getNrOfOccuppiedAdjecentSeats(const std::vector<std::string>& currSeatsOccuppied, int seatx,int seaty){
    int result=0;
    int height=currSeatsOccuppied.size();
    int width = currSeatsOccuppied[0].size();
    std::vector<std::pair<int,int>> adjecentPositionOffsets = {{-1,-1},{0,-1},{1,-1},
                                                               {-1,0},{1,0},
                                                               {-1,1},{0,1},{1,1}};
    for (const auto&offset: adjecentPositionOffsets){
        if (isValidLocation(seatx+offset.first,seaty+offset.second,width,height) && currSeatsOccuppied[seaty+offset.second][seatx+offset.first] == '#')
            result++;
    }
    return result;

}




std::vector<std::string> readFile(const std::string &filename) {
    std::vector<std::string> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    std::map<std::string, std::string> singlePassportData;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (!line.empty())
            result.push_back(line);
    }
    fs.close();
    return result;
}

