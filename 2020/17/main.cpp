#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>

struct coordinate{
    long x;
    long y;
    long z;
};

struct coordinate4d{
    long x;
    long y;
    long z;
    long w;
};

bool operator==(const coordinate& a, const coordinate& b){
    return a.x==b.x && a.y==b.y && a.z==b.z;
}

bool operator<(const coordinate& a, const coordinate& b){
    return (a.x<b.x || (a.x==b.x&&a.y<b.y) ||( a.x==b.x&&a.y==b.y && a.z<b.z));
}

bool operator==(const coordinate4d& a, const coordinate4d& b){
    return a.x==b.x && a.y==b.y && a.z==b.z && a.w == b.w;
}
bool operator<(const coordinate4d& a, const coordinate4d& b){
    return (a.x<b.x || (a.x==b.x&&a.y<b.y) ||( a.x==b.x&&a.y==b.y && a.z<b.z)||( a.x==b.x&&a.y==b.y && a.z==b.z && a.w < b.w));
}

std::set<coordinate> readFile(const std::string &filename);
long getAnswerA(std::set<coordinate> currActive);
long getAnswerB(std::set<coordinate> currActive);

int main() {
    std::set<coordinate>  coordinates = readFile("input.txt");
    auto answerA = getAnswerA(coordinates);
    auto answerB = getAnswerB(coordinates);
    std::cout << "Advent of code 2020, day 17" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;
    return 0;
}

template<typename T> std::set<T> doStep(std::set<T> currActive);

long getAnswerA(std::set<coordinate> currActive){
    auto newActive = currActive;

    for (int i=0;i<6;i++){
        newActive = doStep(newActive);
    }
    return newActive.size();
}

long getAnswerB(std::set<coordinate> currActive){
    std::set<coordinate4d> newActive;
    for (auto&curr:currActive){
        newActive.insert( {curr.x, curr.y, curr.z, 0});
    }

    for (int i=0;i<6;i++){
        newActive = doStep(newActive);
    }
    return newActive.size();
}


std::set<coordinate4d> getSurroundingCoordinates(coordinate4d center){
    std::set<coordinate4d> result;
    for (int x=-1;x<=1;x++)
        for (int y=-1;y<=1;y++)
            for (int z=-1;z<=1;z++)
                for (int w=-1;w<=1;w++)
                if (! (x==0&&y==0&&z==0&& w==0) ){
                    result.insert({center.x + x, center.y + y, center.z + z,center.w+w});
                }
    return result;
}

std::set<coordinate> getSurroundingCoordinates(coordinate center){
    std::set<coordinate> result;
    for (int x=-1;x<=1;x++)
        for (int y=-1;y<=1;y++)
            for (int z=-1;z<=1;z++)
                if (! (x==0&&y==0&&z==0) ){
                    result.insert({center.x + x, center.y + y, center.z + z});
                }
    return result;
}

template <typename T>
int countSurroundingActiveCells(const std::set<T> &active, const T& currCell){
    int nrSurroundingActive=0;
    std::set<T> surroundingCells = getSurroundingCoordinates(currCell);
    for (auto& cell:surroundingCells){
        if (active.find(cell)!= active.end()){
            nrSurroundingActive++;
        }
    }
    return nrSurroundingActive;
}

template <typename T>
std::set<T> doStep(std::set<T> currActive){
    std::set<T> result;
    std::set<T> cellsToEvaluate;
    for (auto& curr:currActive){
        cellsToEvaluate.insert(curr);
        auto surrounding = getSurroundingCoordinates(curr);
        cellsToEvaluate.insert(surrounding.begin(), surrounding.end());
    }

    for (auto&cell:cellsToEvaluate){
        bool currentCellActive = currActive.find(cell) != currActive.end();
        int surroundingActive = countSurroundingActiveCells(currActive,cell);
        if (currentCellActive && (surroundingActive==2 ||surroundingActive==3)){
            result.insert(cell);
        } else if (!currentCellActive && surroundingActive==3){
            result.insert(cell);
        }
    }
    return result;
}

std::set<coordinate> readFile(const std::string &filename) {
    std::set<coordinate> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    int z=0;
    int y=0;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        int x=0;
        for (auto ch:line){
            if (ch=='#'){
                result.insert({x, y, z});
            }
            x++;
        }
        y++;
    }
    fs.close();
    return result;
}

