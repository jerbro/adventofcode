#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>

struct SubRule{
    std::vector<int> ruleIds;
};

struct RuleContent{
    bool isEndRule=false;
    char endChar;
    std::vector<SubRule> subrules;
    std::string rule;
};

struct FileContent {
    std::map<int, RuleContent> rules;
    std::vector<std::string> messages;
};

FileContent readFile(const std::string &filename);
std::string createRegex(const std::map<int,RuleContent> &rules, int ruleToDescribe, int currRecursion=0);
long getAnswerA(const FileContent &content);
long getAnswerB( FileContent content);

int main() {
    auto  filecontent = readFile("input.txt");
    auto answerA = getAnswerA(filecontent);
    auto answerB = getAnswerB(filecontent);

    std::cout << "Advent of code 2020, day 19" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;
    return 0;
}

long nrOfMessagesMatchingRegex(std::string regexStr, std::vector<std::string> messages);
long getAnswerA(const FileContent &content){
    std::string regexStr = "^"+createRegex(content.rules, 0)+"$";
    return nrOfMessagesMatchingRegex(regexStr, content.messages);

}

RuleContent parseRule(std::string rule);
long getAnswerB( FileContent content){

    content.rules[8] = parseRule("42 | 8 42");
    content.rules[11] = parseRule("42 31 | 42 11 31");
    std::string regexStr = "^"+createRegex(content.rules, 0)+"$";
    return nrOfMessagesMatchingRegex(regexStr, content.messages);

}


long nrOfMessagesMatchingRegex(std::string regexStr, std::vector<std::string> messages){
    std::regex reg(regexStr);
    long result=0;
    for (auto message:messages){
        if (std::regex_match(message, reg)) {
            result++;
        }
    }
    return result;
}

std::string createRegex(const std::map<int,RuleContent>& rules, int ruleToDescribe, int currRecursion){
    std::string result;
    int maxRecursion = 10; // Just a guess that 10 recursions would be enough for the input.
    if (currRecursion>=maxRecursion)
        return result;
    const auto currRule = rules.at(ruleToDescribe);
    if (rules.at(ruleToDescribe).isEndRule){
        result = rules.at(ruleToDescribe).endChar;
    } else {
        result ="(";
        for (auto subrule:currRule.subrules){
            for (auto subruleid:subrule.ruleIds) {
                if (ruleToDescribe==subruleid){
                    currRecursion++;
                }
                result += createRegex(rules, subruleid,currRecursion);
            }
            result += "|";
        }
        result.pop_back();//remove last '|'
        result +=")";
    }
    return result;
}

RuleContent parseRule(std::string rule){
    RuleContent result;
    if (rule.front()=='\"'){
        result.isEndRule = true;
        result.endChar = rule.at(1);
    } else{
        std::string currNr;
        SubRule subRule;
        for (auto ch:rule){
            if (ch >= '0' && ch <= '9') {
                currNr.push_back(ch);
            } else{
                if (!currNr.empty()) {
                    subRule.ruleIds.push_back(std::stoi(currNr));
                }
                currNr = "";
                if (ch=='|'){
                    result.subrules.push_back(subRule);
                    subRule.ruleIds.clear();
                }
            }
        }
        if (!currNr.empty()){subRule.ruleIds.push_back(std::stoi(currNr));}
        result.subrules.push_back(subRule);
    }
    return result;
}

std::pair<int,RuleContent> parseRuleLine(std::string line){
    int idEnd = line.find(':');
    int id;
    id = std::stol(line.substr(0,idEnd));
    auto ruleText = line.substr(idEnd+2);
    auto rules = parseRule(ruleText);
    return std::make_pair(id, rules);

}


FileContent readFile(const std::string &filename) {
    FileContent result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;

    bool parsingRules=true;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (line.empty()){
            parsingRules=false;
        } else {
            if (parsingRules) {
                result.rules.insert(parseRuleLine(line));
            } else {
                result.messages.push_back(line);
            }
        }

    }
    fs.close();
    return result;
}

