#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>

struct valueRange{
    long min;
    long max;
};

struct ticketField{
    std::string name;
    std::vector<valueRange> allowedValues;
};

struct fileData{
    std::vector<ticketField> fields;
    std::vector<int> yourTicket;
    std::vector<std::vector<int>> nearbyTickets;
};

fileData readFile(const std::string &filename);
long getAnswerA(const fileData& ticketsData);
long getAnswerB(fileData ticketsData);

int main() {
    fileData ticketsData = readFile("input.txt");
    auto answerA = getAnswerA(ticketsData);
    auto answerB = getAnswerB(ticketsData);
    std::cout << "Advent of code 2020, day 16" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;
    return 0;
}

bool valueIsAllowed(int value, const std::vector<valueRange>& specs){
    bool currNrValidForField=false;
    for (auto&currSpec:specs){
        if (value>=currSpec.min && value <= currSpec.max){
            currNrValidForField=true;
        }
    }
    return currNrValidForField;
}

long getCurrTicketScanningErrorRate(const fileData& ticketsdata, std::vector<int>& currTicket){
    long result=0;

    for (auto nr:currTicket){
        bool currNrValid = false;
        for (auto& field: ticketsdata.fields){
            currNrValid = valueIsAllowed(nr,field.allowedValues);
        }
        if (!currNrValid){
            result += nr;
        }
    }
    return result;

}

long getAnswerA(const fileData& ticketsData){
    long result = 0;
    for (auto ticket:ticketsData.nearbyTickets){
        result+=getCurrTicketScanningErrorRate(ticketsData,ticket);
    }
    return result;

}

//std::vector<int> getAllValuesForFieldNr(fileData ticketsData, )



bool fieldIsAllowed(const fileData &ticketsData, int ticketColumnId, int fieldId){
    bool result = true;
    for (auto ticket:ticketsData.nearbyTickets){
        auto valToCheck = ticket[ticketColumnId];
        auto allowedRanges = ticketsData.fields[fieldId].allowedValues;
        if (!valueIsAllowed(valToCheck,allowedRanges)){
            result=false;
            break;
        }
    }
    return result;
}

long getAnswerB(fileData ticketsData){
    //Remove all invalid tickets
    ticketsData.nearbyTickets.erase(std::remove_if(ticketsData.nearbyTickets.begin(), ticketsData.nearbyTickets.end(),
                                                   [ticketsData](std::vector<int> ticket) {
                                                       return getCurrTicketScanningErrorRate(ticketsData, ticket) > 0;
                                                   }),ticketsData.nearbyTickets.end());

    std::map<int,std::set<std::string>> valueId_vs_allowedFieldnames;
    for (int i=0;i<ticketsData.nearbyTickets[0].size();i++){
        for (int j=0;j<ticketsData.fields.size();j++){
            if (fieldIsAllowed(ticketsData,i,j)){
                valueId_vs_allowedFieldnames[i].insert(ticketsData.fields[j].name);
            }
        }
    }

    bool keeprunning=true;


    while(keeprunning){
        keeprunning=false;

        for (int i=0;i<valueId_vs_allowedFieldnames.size();i++){//remove fieldname from all other fieldId's if fieldId for fieldname is found.
            if (valueId_vs_allowedFieldnames[i].size()==1){
                std::string currentFieldname = *valueId_vs_allowedFieldnames[i].begin();
                for (int j=0;j<valueId_vs_allowedFieldnames.size();j++){
                    if (i!=j && valueId_vs_allowedFieldnames[j].find(currentFieldname)!= valueId_vs_allowedFieldnames[j].end()){
                        valueId_vs_allowedFieldnames[j].erase(valueId_vs_allowedFieldnames[j].find(currentFieldname));
                        keeprunning=true;
                    }
                }
            }
        }

        //Find fieldnames which are only valid for 1 fieldId. In that case, all other fields for that fieldID can be removed.
        for (int i=0;i<ticketsData.fields.size();i++){
            auto fieldName = ticketsData.fields[i].name;
            int fieldnameOccurrences=0;
            int lastFoundFieldId = -1;
            for (int j=0;j<valueId_vs_allowedFieldnames.size();j++){
                if (valueId_vs_allowedFieldnames[j].find(fieldName) != valueId_vs_allowedFieldnames[j].end()) {
                    fieldnameOccurrences++;
                    lastFoundFieldId=j;
                }
            }
            if (fieldnameOccurrences==1 &&valueId_vs_allowedFieldnames[lastFoundFieldId].size()>1 ){
                keeprunning=true;
                valueId_vs_allowedFieldnames[lastFoundFieldId].clear();
                valueId_vs_allowedFieldnames[lastFoundFieldId].insert(fieldName);
            }
        }
    }


    for (auto valueId:valueId_vs_allowedFieldnames){
        if (valueId.second.size()>1){
            std::cout << " Error, no complete find. Result can be incorrect.\r\n";
        }
    }

    long result=1;
    for (auto valueId:valueId_vs_allowedFieldnames){
        std::string fieldname = *valueId.second.begin();
        if (fieldname.find("departure")!= std::string::npos){
            result *= ticketsData.yourTicket[valueId.first];
        }
    }
    return result;

}



enum class parseStep{
    fields, yourticket,nearbytickets
};
ticketField parseField(std::string fieldLine){
    ticketField result;
    auto nameEnd = fieldLine.find(':');
    result.name = fieldLine.substr(0, nameEnd);

    std::string ranges = fieldLine.substr(nameEnd + 2, fieldLine.size() - nameEnd);
    std::vector<int> numbers;
    std::string currNr;
    for (auto ch:ranges){

        if (ch>='0' && ch <='9'){
            currNr.push_back(ch);
        } else if (!currNr.empty()){
            numbers.push_back(std::stoi(currNr));
            currNr.clear();
        }
    }
    if (!currNr.empty()){
        numbers.push_back(std::stoi(currNr));
        currNr.clear();
    }

    for (int i=0;i<numbers.size();i+=2){
        result.allowedValues.push_back({numbers[i], numbers[i + 1]});
    }


    return result;
}



std::vector<int> parseTicketnrs(std::string fieldLine){
    std::vector<int> result;
    std::string currNr;
    for (auto ch:fieldLine){
        if (ch>='0' && ch<='9'){
            currNr.push_back(ch);
        } else {
            if (currNr.size()>0){
                result.push_back(std::stoi(currNr));
                currNr.clear();
            }
        }
    }
    if (currNr.size()>0){
        result.push_back(std::stoi(currNr));
        currNr.clear();
    }
    return result;
}

void parseLine(const std::string& line, fileData& data,parseStep& step ){
    if (step== parseStep::fields && line == "your ticket:"){
        step=parseStep::yourticket;
    } else if (step== parseStep::yourticket && line == "nearby tickets:"){
        step=parseStep::nearbytickets;
    } else if (step== parseStep::fields){
        data.fields.push_back(parseField(line));
    } else if (step == parseStep::yourticket){
        data.yourTicket = parseTicketnrs(line);
    } else if (step == parseStep::nearbytickets){
        data.nearbyTickets.push_back(parseTicketnrs(line));
    }
}


fileData readFile(const std::string &filename) {
    fileData result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    parseStep step=parseStep::fields;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (!line.empty()){
            parseLine(line, result, step);
        }
    }
    fs.close();
    return result;
}

