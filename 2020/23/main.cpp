#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>
#include <list>
#include <array>

std::list<int> readFile(const std::string &filename);

std::string  getAnswerA(const std::list<int>  &cups);
long long getAnswerB(const std::list<int>  &cups);

int main() {
    auto  fileContent = readFile("input.txt");

   auto answerA = getAnswerA(fileContent);
   auto answerB = getAnswerB(fileContent);

    std::cout << "Advent of code 2020, day 23" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;
    return 0;
}

void makeCurrentcupFirst(std::list<int>&cups, std::list<int>::iterator& currentCup){
    if (currentCup != cups.begin()) {
        cups.splice(cups.begin(), cups, currentCup, cups.end());
    }
}

std::list<int>::iterator getDestinationCup(std::list<int>&cups, int prevVal,int maxValToSearch,std::vector<std::list<int>::iterator>& valIterList){
    std::list<int>::iterator destinationCup;
    int valToSearch = prevVal-1;
    while (true){
        if (valToSearch<=0){
            valToSearch = maxValToSearch;
        }

        if (valIterList.at(valToSearch) != cups.end()) {
            destinationCup = valIterList.at(valToSearch);
            break;
        }
        valToSearch--;
    }
    return destinationCup;
}

//Assume the first cup in the list is the current cup.
void doTurn(std::list<int>&cups,int maxValToSearch,std::vector<std::list<int>::iterator>& valIterList){

    int currVal = *cups.begin();
    auto currCupIter = cups.begin();
    std::list<int>::iterator firstToRemove = cups.begin();
    std::list<int>::iterator lastToRemove = cups.begin();
    std::advance(firstToRemove, 1);
    std::advance(lastToRemove, 4);
    std::list<int> itemsToRemove {firstToRemove,lastToRemove};
    cups.erase(firstToRemove, lastToRemove);

    for (auto item:itemsToRemove){
        valIterList.at(item) = cups.end();
    }

    auto destCup = getDestinationCup(cups, currVal,maxValToSearch,valIterList);
    destCup++;
    cups.insert(destCup, itemsToRemove.begin(), itemsToRemove.end());

    auto newPlacedItem = destCup;
    newPlacedItem--;
    for (int i=0;i<3;i++){//Add the 3 newly placed items.
        valIterList.at(*newPlacedItem) = newPlacedItem;
        newPlacedItem--;
    }

    currCupIter++;
    makeCurrentcupFirst(cups, currCupIter);

}
std::vector<std::list<int>::iterator> createValIterVector(std::list<int>  &cups){
    int highestValue = *std::max_element(cups.begin(),cups.end());
    std::vector<std::list<int>::iterator> valIterList(highestValue+1);
    std::list<int>::iterator iter = cups.begin();
    while (iter != cups.end()) {
        valIterList.at(*iter) = iter;
        iter++;
    }
    return valIterList;
}


std::string getAnswerA(const std::list<int>  &cups){
    auto cupsCopy = cups;
    auto valIterList = createValIterVector(cupsCopy);

    for (int i=0;i<100;i++){
        doTurn(cupsCopy,9,valIterList);
    }

    std::list<int>::iterator iteratorCupId1 = std::find(cupsCopy.begin(), cupsCopy.end(), 1);
    makeCurrentcupFirst(cupsCopy, iteratorCupId1);
    std::string result;
    cupsCopy.erase(cupsCopy.begin());
    for (auto ch:cupsCopy){
        result.append(std::to_string(ch));
    }
    return result;
}

long long getAnswerB(const std::list<int>  &cups){
    auto cupsCopy = cups;
    //Add extra cups
    for (int i = 10; i <= 1000000; i++) {
        cupsCopy.push_back(i);
    }

    auto valIterList = createValIterVector(cupsCopy);

    for (int i=0;i<10000000;i++){
        doTurn(cupsCopy,1000000,valIterList);
    }

    std::list<int>::iterator iteratorCupId1 = std::find(cupsCopy.begin(), cupsCopy.end(), 1);
    makeCurrentcupFirst(cupsCopy, iteratorCupId1);
    auto cupIter = cupsCopy.begin();

    cupIter++;//multiply second and third element to get result;
    long long result = *cupIter;
    cupIter++;
    result *= *cupIter;

    return result;

}

std::list<int> readFile(const std::string &filename){
    std::list<int> cups;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);
    std::string line;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (!line.empty()) {
            for (auto ch:line){
                char nr = ch - '0';
                cups.push_back(nr);
            }
        }
    }

    fs.close();
    return cups;
}

