cmake_minimum_required(VERSION 3.8)
project(2020_20)

set(CMAKE_CXX_STANDARD 20)

set(SOURCE_FILES main.cpp)
add_executable(2020_20 ${SOURCE_FILES})
