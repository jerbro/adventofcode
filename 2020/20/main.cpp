#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>

enum SidePos{
    Top=0,
    Right=1,
    Bottom=2,
    Left=3,
    TopFlipped=10,
    RightFlipped=11,
    BottomFlipped=12,
    LeftFlipped=13
};

struct Tile{
    std::vector<std::string> tileLines;
    std::map<SidePos, int> sideValues;
    long tileId;
};

bool operator <(const Tile& a, const Tile& b){return ((a.tileId < b.tileId));}
bool operator ==(const Tile& a, const Tile& b){return ((a.tileId == b.tileId));}

std::pair<SidePos, int> getValuesFromLine(std::string line, SidePos position, bool inverted);

std::vector<Tile> readFile(const std::string &filename);

long long getAnswerA(const std::vector<Tile> &tiles);
long long getAnswerB(const std::vector<Tile> &tiles);

int main() {
    auto  filecontent = readFile("input.txt");

    auto answerA = getAnswerA(filecontent);
    auto answerB = getAnswerB(filecontent);

    std::cout << "Advent of code 2020, day 20" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;
    return 0;
}

std::pair<SidePos, int> getValuesFromLine(std::string line, SidePos position, bool inverted) {
    int forwardDir=0;
    int backwardDir=0;
    for (auto ch:line){
        forwardDir<<=1;
        backwardDir>>=1;
        if (ch=='#'){
            forwardDir |=0x01;
            backwardDir |= (1<<(line.length()-1));
        }
    }
    if (inverted){
        return std::make_pair(position,backwardDir);
    } else {
        return std::make_pair(position, forwardDir);
    }
}



std::map<int,int> getEdgevalueVsOccurrences(const std::vector<Tile> &tiles){
    std::map<int,int> valOccurrences;
    for (auto tile:tiles){
        for (auto val:tile.sideValues){
            valOccurrences[val.second] +=1;
        }
    }
    return valOccurrences;
}

long long getAnswerA(const std::vector<Tile> &tiles){
    std::map<int,int> edgeValAndOccurrences = getEdgevalueVsOccurrences(tiles);

    long long result=1;
    for (auto tile:tiles){
        int nrOfSidesWIthMatchingOtherBox=0;
        for (auto edgeValues:tile.sideValues){
            if (edgeValAndOccurrences[edgeValues.second] > 1){
                nrOfSidesWIthMatchingOtherBox++;
            }
        }
        nrOfSidesWIthMatchingOtherBox /=2; // Every side is matched double. Because the normal and inverted direction are both matched
        //Only the corner-pieces have only 2 sides matching another piece. All other pieces have 3 (edge), or 4 (in center of image) overlapping sides
        if (nrOfSidesWIthMatchingOtherBox == 2){
            result *= tile.tileId;
        }
    }
    return result;
}

void calcSideValues(Tile& tile){
    std::string leftSideString;
    std::string rightSideString;
    for (auto line:tile.tileLines){
        leftSideString.push_back(line.at(0));
        rightSideString.push_back(line.at(line.length()-1));
    }

    // all values are clockwise. So at top-row value is left to right. on right side, value is top to bottom, on bottom side, value is right to left, and on left side value is from bottom to top.
    auto lineValsTopLine = getValuesFromLine(tile.tileLines.at(0), Top, false);
    auto lineValsTopLineInverted = getValuesFromLine(tile.tileLines.at(0), TopFlipped, true);
    auto lineValsBottomLine = getValuesFromLine(tile.tileLines.at(tile.tileLines.size() - 1), Bottom, true);
    auto lineValsBottomLineInverted = getValuesFromLine(tile.tileLines.at(tile.tileLines.size() - 1), BottomFlipped, false);
    auto lineValsLeftLine = getValuesFromLine(leftSideString, Left, true);
    auto lineValsLeftLineInverted = getValuesFromLine(leftSideString, LeftFlipped, false);
    auto lineValsRightLine = getValuesFromLine(rightSideString, Right, false);
    auto lineValsRightLineInverted = getValuesFromLine(rightSideString, RightFlipped, true);

    tile.sideValues.insert(lineValsTopLine);
    tile.sideValues.insert(lineValsTopLineInverted);
    tile.sideValues.insert(lineValsBottomLine);
    tile.sideValues.insert(lineValsBottomLineInverted);
    tile.sideValues.insert(lineValsLeftLine);
    tile.sideValues.insert(lineValsLeftLineInverted);
    tile.sideValues.insert(lineValsRightLine);
    tile.sideValues.insert(lineValsRightLineInverted);
}

bool getAndRemovePieceWithOneSideWithValue(std::set<Tile> &tiles, int sideValue, Tile& foundTile){
    bool tileFound=false;
    std::vector<Tile> tileVect(tiles.begin(),tiles.end());
    int tileCnt = 0, sidevalcnt = 0;
    auto edgeValVsOccurrences = getEdgevalueVsOccurrences(tileVect);
    for (auto& tile:tiles){
        tileCnt++;
        for (auto& currSideval:tile.sideValues){
            sidevalcnt++;
            if (currSideval.second == sideValue){
                foundTile = tile;
                tileFound = true;
            }
        }
    }
    if (tileFound){
        tiles.erase(foundTile);
    }
    return tileFound;
}

std::pair<Tile,std::set<int>> getAndRemoveOneCornerpieceFromTiles(std::set<Tile> &tiles){
    std::pair<Tile,std::set<int>>  retval;
    std::vector<Tile> tileVect(tiles.begin(),tiles.end());
    auto edgeValVsOccurrences = getEdgevalueVsOccurrences(tileVect);
    for (auto& tile:tiles){
        auto nrOfEdgePieces=0;
        std::set<int> edgeSideValues;
        for (auto& side:tile.sideValues){
            if (edgeValVsOccurrences[side.second] == 1){
                edgeSideValues.insert(side.second);
                nrOfEdgePieces++;
            }
        }
        nrOfEdgePieces/=2;//divide by 2. Each edge is found double because both the edge and inversed are found.
        if (nrOfEdgePieces == 2){
            retval = std::make_pair(tile, edgeSideValues);
            break;
        }
    }
    tiles.erase(retval.first);
return retval;
}


std::vector<std::string> rotateImageClockwiseOneQuarter(const std::vector<std::string>& srcImg){
    int origX, origY, origWidth,origHeight;
    int destX, destY;
    origWidth = static_cast<int>(srcImg.at(0).size());
    origHeight = static_cast<int>(srcImg.size());

    std::vector<std::string> result(origWidth,std::string(origHeight,' '));

    for ( origX=0;origX<origWidth;origX++){
        for ( origY=0; origY < origHeight; origY++) {
            destX = origHeight - origY - 1;
            destY = origX ;
            result.at(destY).at(destX) = srcImg.at(origY).at(origX);
        }
    }
    return result;
}

std::vector<std::string> rotateImageClockwise(const std::vector<std::string>& srcImg, int nrOfClockwiseQuarterRotations){
    std::vector<std::string> retval = srcImg;
    for (int i=0;i<nrOfClockwiseQuarterRotations;i++){
        retval = rotateImageClockwiseOneQuarter(retval);
    }
    return retval;
}

std::vector<std::string> flipImageAroundHorizontalAxis(const std::vector<std::string>& srcImg) {
    int nrOfLines = static_cast<int>(srcImg.size());
    std::vector<std::string> retval;
    for (int i=0;i<nrOfLines;i++){
        retval.push_back(srcImg.at(nrOfLines-1-i));
    }
    return retval;
}

std::vector<std::string> flipImageAroundVerticalAxis(const std::vector<std::string>& srcImg) {
    std::vector<std::string> retval;
    for (auto line:srcImg){
        std::string invertedLine;
        for (int i=line.size()-1;i>=0;i--){
            invertedLine.push_back(line.at(i));
        }
        retval.push_back(invertedLine);
    }
    return retval;
}

Tile rotateTile(Tile tile, int nrOfClockwiseRotations){
   Tile retval;
   retval.tileId = tile.tileId;
   retval.tileLines = rotateImageClockwise(tile.tileLines, nrOfClockwiseRotations);
    calcSideValues(retval);
   return retval;
}

Tile flipTileAroundVerticalAxis(Tile tile){
    Tile retval;
    retval.tileLines = flipImageAroundVerticalAxis(tile.tileLines);
    retval.tileId = tile.tileId;
    calcSideValues(retval);
    return retval;
}

Tile flipTileAroundHorizontalAxis(Tile tile){
    Tile retval;
    retval.tileLines = flipImageAroundHorizontalAxis(tile.tileLines);
    retval.tileId = tile.tileId;
    calcSideValues(retval);
    return retval;
}

Tile rotateCornerpieceToBeLeftAbove(Tile piece, std::set<int> edgeSideValues){
    Tile tile = piece;
    for (int i=0;i<4;i++){
        tile = rotateTile(tile,1);
        // if the top and left side of the image are in the set of edgevalues, the topleft-piece is found
        if (edgeSideValues.find(tile.sideValues[Top]) != edgeSideValues.end() &&
             edgeSideValues.find(tile.sideValues[Left]) != edgeSideValues.end()){
            break;
        }
    }
    return tile;
}

Tile rotatePieceToHaveSideValue(Tile tile,int valueToSearch, SidePos sideToSearch){
    Tile tileCopy = tile;
    SidePos invertedSide = static_cast<SidePos>(static_cast<int>(sideToSearch)+10);

    for (int i=0;i<4;i++){
        if (tileCopy.sideValues[sideToSearch] == valueToSearch || tileCopy.sideValues[invertedSide] == valueToSearch ){
            break;
        } else {
            tileCopy = rotateTile(tileCopy,1);
        }
    }

    if (tileCopy.sideValues[invertedSide] == valueToSearch ){
        if (invertedSide == TopFlipped || invertedSide == BottomFlipped){
            tileCopy = flipTileAroundVerticalAxis(tileCopy);
        } else {
            tileCopy = flipTileAroundHorizontalAxis(tileCopy);
        }
    }
    calcSideValues(tileCopy);
    return tileCopy;
}

Tile rotatePieceToHaveTopValue(Tile tile,int valueToSearch){
    return rotatePieceToHaveSideValue(tile,valueToSearch,Top);
}

Tile rotatePieceToHaveLeftValue(Tile tile,int valueToSearch){
    return rotatePieceToHaveSideValue(tile,valueToSearch,Left);
}

std::vector<std::string> constructImageLine(const std::vector<Tile>& tileLine){
    std::vector<std::string> retval;
    if (tileLine.size()>2){
        int tileHeight = tileLine.at(0).tileLines.size();
        std::vector<std::string> printBuffer(tileHeight-2);
        for (const auto&tile:tileLine){
            for (int i=1;i<tile.tileLines.size()-1;i++){
                std::string lineString = tile.tileLines.at(i);
                printBuffer[i-1].append(lineString.begin()+1,lineString.end()-1);
            }
        }
        retval = printBuffer;
    }
    return retval;
}

std::vector<std::string> constructImageFromTiles(const std::vector<std::vector<Tile>> tiles){
    std::vector<std::string> result;
    for (auto tileLine:tiles){
        auto newLines = constructImageLine(tileLine);
        result.insert(result.end(),newLines.begin(),newLines.end());
    }
    return result;
}

std::vector<std::string> solveWholeImage(const std::vector<Tile> &tiles){
    std::vector<std::string> retval;
    //Solve the whole image.
    //Start by finding an corner-piece, and rotate it, so it is the left-above corner.
    // Then start finding pieces to add on the right side until another corner-piece is found.
    // Then the width of the image is known. Start Keep adding pieces
    // When the fourth corner-piece is found. the image is solved

    std::set<Tile> availableTiles(tiles.begin(),tiles.end());
    auto cornerPiece = getAndRemoveOneCornerpieceFromTiles(availableTiles);

    auto lastImagePiece = rotateCornerpieceToBeLeftAbove(cornerPiece.first,cornerPiece.second);
    std::vector<std::vector<Tile>> solvedImageTiles = {{lastImagePiece}};
    int currLine = 0;
    int currCol = 0;
    bool startNewLine = false;
    while (availableTiles.size() > 0) {
        if (startNewLine) {
            currCol = 0;
            currLine ++;
            solvedImageTiles.push_back(std::vector<Tile> {});
            startNewLine = false;

            auto lastBottomValue = solvedImageTiles[currLine - 1][0].sideValues[BottomFlipped];
            auto img = solvedImageTiles[currLine - 1][0];

            if (getAndRemovePieceWithOneSideWithValue(availableTiles, lastBottomValue,lastImagePiece)){
                Tile rotatedTile = rotatePieceToHaveTopValue(lastImagePiece,lastBottomValue);
                solvedImageTiles[currLine].push_back(rotatedTile);
                lastImagePiece = rotatedTile;
            } else {
                break;
            }
        } else {
            currCol++;
            auto lastRightValue = lastImagePiece.sideValues[RightFlipped];
            if (getAndRemovePieceWithOneSideWithValue(availableTiles, lastRightValue,lastImagePiece)){
                auto rotatedTile =  rotatePieceToHaveLeftValue(lastImagePiece,lastRightValue);
                lastImagePiece = rotatedTile;
                solvedImageTiles[currLine].push_back(rotatedTile);
            } else {
                startNewLine = true;
            }
        }
    }
    retval = constructImageFromTiles(solvedImageTiles);
    return retval;
}



bool seamonsterIsAtPosition(const std::vector<std::string>&image, const std::vector<std::string>&seamonster, int xOffset, int yOffset){
    int seamonsterWidth = seamonster.at(0).size();
    int seamonsterHeight = seamonster.size();
    for (int y=0;y<seamonsterHeight;y++){
        for (int x=0;x<seamonsterWidth;x++){
            if (seamonster.at(y).at(x) == '#'){
                if (image.at(y+yOffset).at(x+xOffset) != '#'){
                    return false;
                }
            }
        }
    }
    return true;
}

std::vector<std::vector<std::string>> getAllPossibleRotationsAndFlippedFormsOfImage(std::vector<std::string> img){
    std::vector<std::vector<std::string>> retval;
    auto flippedImg = flipImageAroundHorizontalAxis(img);
    auto normalImage = img;
    for (int i=0;i<4;i++){
        retval.push_back(normalImage);
        retval.push_back(flippedImg);
        normalImage = rotateImageClockwiseOneQuarter(normalImage);
        flippedImg = rotateImageClockwiseOneQuarter(flippedImg);
    }
    return retval;

}

int countSeamonstersInImage(const std::vector<std::string>&image, const std::vector<std::string>&seamonster ){

    auto seamonsterRotations = getAllPossibleRotationsAndFlippedFormsOfImage(seamonster);

    int imageWidth = image.at(0).size();
    int imageHeight = image.size();
    int counter = 0;
    for (auto monster : seamonsterRotations) {
        int seamonsterWidth = static_cast<int>(monster.at(0).size());
        int seamonsterHeight = static_cast<int>(monster.size());

        for (int x = 0; x < imageWidth - seamonsterWidth; x++) {
            for (int y = 0; y < imageHeight - seamonsterHeight; y++) {

                if (seamonsterIsAtPosition(image, monster, x, y)) {
                    counter++;

                }
            }

        }
    }
    return counter;

}
long countCharacterInImage(const std::vector<std::string>& image, char ch){
    long counter = 0;
    for (const auto &line:image){
        for (auto currCh:line){
            if (currCh == ch){
                counter++;
            }
        }
    }
    return counter;
}

long long getAnswerB(const std::vector<Tile> & tiles){
    auto image = solveWholeImage(tiles);
    std::vector<std::string> seaMonster ={
            "                  # ",
            "#    ##    ##    ###",
            " #  #  #  #  #  #   "};

    long nrOfSeamonsters = countSeamonstersInImage(image, seaMonster);

    long nrOfHashCharsInImage = countCharacterInImage(image, '#');
    long nrOfHashCharsInSeamonster = countCharacterInImage(seaMonster, '#');
    return nrOfHashCharsInImage - (nrOfHashCharsInSeamonster * nrOfSeamonsters);
}

std::vector<Tile> readFile(const std::string &filename){
    std::vector<Tile> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;

    Tile currTile = {};
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (line.empty()){
            if (!currTile.tileLines.empty()){
                calcSideValues(currTile);
                result.push_back(currTile);
                currTile.tileLines.clear();
                currTile.sideValues.clear();
                currTile.tileId=0;
            }
        } else {
            if (line.substr(0,5) == "Tile "){
                currTile.tileId = std::stoi(line.substr(4));
            }
            else{
                currTile.tileLines.push_back((line));
            }
        }
    }
    if (!currTile.tileLines.empty()){
        calcSideValues(currTile);
        result.push_back(currTile);
    }
    fs.close();
    return result;
}

