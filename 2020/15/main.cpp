#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>



std::vector<long long> readFile(const std::string &filename);
long getAnswerA(std::vector<long long> startSeq, long turn);
//long long getAnswerB(std::vector<lineData> data);

int main() {
    std::vector<long long> linesData = readFile("input.txt");
    auto answerA = getAnswerA(linesData,2020);
    auto answerB = getAnswerA(linesData,30000000);
    std::cout << "Advent of code 2020, day 15" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;
    return 0;
}

struct prevSpoken{
    long prevTurn=-1;
    long prevprevTurn = -1;
};

void updateSpeakHistory(std::map<long, prevSpoken> & history, long turn, long nr){
    if (history.find(nr) !=history.end()){
        history.at(nr).prevprevTurn =history.at(nr).prevTurn;
        history.at(nr).prevTurn = turn;
    } else{
        history[nr].prevTurn=turn;
    }
}

long getAnswerA(std::vector<long long> startSeq, long turns){
    long prevSpokenNr = 0;
    long currSpeak = 0;
    std::map<long, prevSpoken> nr_lastTurn;
    for (int i=1;i<=turns;i++){
        if (i<=startSeq.size()){
            currSpeak = startSeq.at(i-1);
        } else {
            if (nr_lastTurn.find(prevSpokenNr) != nr_lastTurn.end() && nr_lastTurn[prevSpokenNr].prevprevTurn != -1 ){
                currSpeak = nr_lastTurn[prevSpokenNr].prevTurn-nr_lastTurn[prevSpokenNr].prevprevTurn;
            } else {
                currSpeak = 0;
            }
        }
        prevSpokenNr = currSpeak;
        updateSpeakHistory(nr_lastTurn,i,currSpeak);
    }
    return currSpeak;

}
std::vector<long long> readFile(const std::string &filename) {
    std::vector<long long> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (!line.empty()){
            std::string currNr;
            for (auto ch:line){
                if (ch>='0' && ch <='9'){
                    currNr.push_back(ch);
                } else if (!currNr.empty()){
                    result.push_back(std::stoll(currNr));
                    currNr.clear();
                }
            }
            if (!currNr.empty()){
                result.push_back(std::stoll(currNr));
                currNr.clear();
            }
        }

    }
    fs.close();
    return result;
}

