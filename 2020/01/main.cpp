#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>


std::vector<long> readFile(const std::string &filename);

long findProductOfSum2ValsFromVector(std::vector<long> numbersList, long desiredSum);
long findProductOfSum3ValsFromVector(std::vector<long> numbersList, long desiredSum);
int main() {
    auto numbersList = readFile("input.txt");
    auto answerA = findProductOfSum2ValsFromVector(numbersList, 2020);
    auto answerB = findProductOfSum3ValsFromVector(numbersList, 2020);

    std::cout << "Advent of code 2020, day 01" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;

    return 0;
}



long findProductOfSum2ValsFromVector(std::vector<long> numbersList, long desiredSum){
    for (int i=0;i<numbersList.size();i++){
        for (int j=0;j<numbersList.size();j++){
            if (i!=j && numbersList[i]+numbersList[j] == desiredSum){
                return numbersList[i]*numbersList[j];
            }
        }
    }
    std::cout << " error, value not found";
}

long findProductOfSum3ValsFromVector(std::vector<long> numbersList, long desiredSum){
    for (int i=0;i<numbersList.size();i++){
        for (int j=0;j<numbersList.size();j++){
            for (int k=0;k<numbersList.size();k++) {
                if (i != j && i != k && j != k && numbersList[i] + numbersList[j] + numbersList[k] == desiredSum) {
                    return numbersList[i] * numbersList[j] * numbersList[k];
                }
            }
        }
    }
    std::cout << " error, value not found";
}

std::vector<long> readFile(const std::string &filename) {
    std::vector<long> result;
    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::stringstream strStr;


    long currNr=0;
    while (fs >> currNr) {
        result.push_back(currNr);
    }
    fs.close();
    return result;
}
