#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>
#include <chrono>

struct BagContents{
    std::string color;
    long long nrContainingBags=-1;
    std::map<std::string, int> containingBags;
};


std::map<std::string, BagContents> readFile(const std::string &filename);
int nrOfColorBagsThatCanContainBag(std::map<std::string, BagContents> bagdata, std::string bagToContain);
long long getNrContainingBags(std::map<std::string, BagContents> bagdata, std::string currColor);

int main() {
    auto bagsData = readFile("input.txt");
    auto answerA = nrOfColorBagsThatCanContainBag(bagsData,"shiny gold");
    auto answerB = getNrContainingBags(bagsData,"shiny gold");
    std::cout << "Advent of code 2020, day 07" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}


long long getNrContainingBags(std::map<std::string, BagContents> bagdata, std::string currColor){
    const auto& currBag = bagdata.find(currColor);
    if (currBag != bagdata.end() ){
        if ( currBag->second.nrContainingBags>=0) {
            return currBag->second.nrContainingBags;
        }

        if (currBag->second.containingBags.size()==0){
            bagdata.find(currColor)->second.nrContainingBags=0;
            return 0;
        }

        long long result =0;
        for(auto&bag:currBag->second.containingBags){
            result += bag.second;
            result+=getNrContainingBags(bagdata,bag.first) * bag.second;
        }
        bagdata.find(currColor)->second.nrContainingBags=result;
        return result;
    }
    std::cout <<"Error, bag "<<currColor<<" not found"<<std::endl;
}

int nrOfColorBagsThatCanContainBag(std::map<std::string, BagContents> bagdata, std::string bagToContain){
    std::set<std::string> currentBagSet;
    std::set<std::string> extraBags;
    int currBagCount = 0;
    currentBagSet.insert(bagToContain);
    while (currBagCount < currentBagSet.size()){
        currBagCount = currentBagSet.size();
        for (auto& innerBag:currentBagSet){
            for (auto& outerBag:bagdata){
                if (outerBag.second.containingBags.find(innerBag) != outerBag.second.containingBags.end()){
                    extraBags.insert(outerBag.first);
                }
            }
        }
        currentBagSet.insert(extraBags.begin(), extraBags.end());
    }
    return currBagCount-1;

}

std::pair<std::string,int> getContainingBag(const std::string& bagCountAndDesc){
    //bagCountAndDesc contains for example: // line for example: 3 bright white bags,
    std::regex pwdLine("^([0-9]+) ([A-Za-z]+ [A-Za-z]+) bag[s,. ]*$");
    std::smatch match;
    std::regex_match(bagCountAndDesc, match, pwdLine);
    if (match.size()==3){
        return std::make_pair(match[2],std::stoi(match[1]));
    }
    else {std::cout << "Error parsing bag: "<<bagCountAndDesc<<std::endl;}
    return std::make_pair("",0);
}

BagContents getContainingBags(const std::string& description){
    // description for example: 3 bright white bags, 4 muted yellow bags.
    BagContents result;
    if (description == "no other bags.")
        return result;

    std::regex pwdLine("^([0-9]+ [A-Za-z]+ [A-Za-z]+ bag[s, ]*)(.*[.])$");
    std::smatch match;
    std::regex_match(description, match, pwdLine);
    if (match.size()==3){
        result.containingBags.insert(getContainingBag(match[1]));
    }

    if (!std::string(match[2]).empty()){
        auto extraBags = getContainingBags(match[2]);
        result.containingBags.insert(extraBags.containingBags.begin(),extraBags.containingBags.end());
    }

    return result;
}

std::pair<std::string,BagContents> parseLine(const std::string &line){
    // line for example: dark orange bags contain 3 bright white bags, 4 muted yellow bags.
    std::pair<std::string,BagContents> result;
    std::regex pwdLine("^([A-Za-z]+ [A-Za-z]+) bags contain (.*)$");
    std::smatch match;
    std::regex_match(line, match, pwdLine);
    if (match.size() == 3){
        std::string keyName =std::string(match[1]) ;
        BagContents value = getContainingBags(match[2]);

        result = std::make_pair(keyName,value);

    } else {
        std::cout <<"Cannot parse line: "<<line<<std::endl;
    }
    return result;
}

std::map<std::string, BagContents> readFile(const std::string &filename) {
    std::map<std::string, BagContents> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    std::map<std::string, std::string> singlePassportData;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        result.insert(parseLine(line));
    }

    fs.close();
    return result;
}

