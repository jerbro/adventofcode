#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>




std::vector<long> readFile(const std::string &filename);
long getAnswerA(std::vector<long> numbersList);

long long getAnswerB(std::vector<long> numbersList);

int main() {
    auto numberslist = readFile("input.txt");
    auto answerA = getAnswerA(numberslist);
    auto answerB = getAnswerB(numberslist);
    std::cout << "Advent of code 2020, day 10" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;


    return 0;
}

long long getAnswerB(std::vector<long> numbersList){
    std::vector<long> nrsList = numbersList;
    nrsList.push_back(0);
    std::sort(nrsList.begin(),nrsList.end());
    nrsList.push_back(nrsList.at(nrsList.size()-1)+3);

    //Found by hand. 1 consecutive 1 (between three's) has 1 possible path. 2 consecutive 1's have 2 paths...4 consecutive ones have 7 paths.
    std::map<long, long long> possibilities = {{1,1},{2,2},{3,4},{4,7}};

    bool first=true;
    long prev;
    int currConcecutiveOnes = 0;
    long long result=1;
    for (auto nr:nrsList){
        if (first){
            prev=nr;
            first=false;
            continue;
        }
        if (nr-prev == 1){
            currConcecutiveOnes++;
        } else {
            if (currConcecutiveOnes>0) {
                result *= possibilities[currConcecutiveOnes];
            }
            currConcecutiveOnes=0;
        }

        prev=nr;
    }
    return result;


}


long getAnswerA(std::vector<long> numbersList){
    std::vector<long> nrsList = numbersList;
    nrsList.push_back(0);
    std::sort(nrsList.begin(),nrsList.end());
    nrsList.push_back(nrsList.at(nrsList.size()-1)+3);

    std::map<int,int> differencesCount;
    bool first=true;
    long prev;
    for (auto nr:nrsList){
        if (first){
            prev=nr;
            first=false;
            continue;
        }
        differencesCount[nr-prev]++;
        prev=nr;
    }
    return differencesCount[3]*differencesCount[1];
}

std::vector<long> readFile(const std::string &filename) {
    std::vector<long> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    std::map<std::string, std::string> singlePassportData;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (!line.empty())
            result.push_back(std::stol(line));
    }
    fs.close();
    return result;
}

