#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>


struct routeStep{
    char dir=' ';
    int distance=0;
};

struct direction{
    int x=0;
    int y=0;
};
struct position {
    int x;
    int y;
    long manhattanDistance(){return abs(x)+abs(y);}
};
const std::map<char,direction> windDirs {{'N',{0,1}},
                                         {'E',{1,0}},
                                         {'S',{0,-1}},
                                         {'W',{-1,0}}};


position followRoute(std::vector<routeStep> routeSteps, position startpos, char startdir);
position followRouteWithWaypoint(std::vector<routeStep> routeSteps, position startposShip, position startposWaypoint);
std::vector<routeStep> readFile(const std::string &filename);

int main() {
    std::vector<routeStep> routeSteps = readFile("input.txt");
    auto answerA = followRoute(routeSteps, {0,0},'E');
    auto answerB = followRouteWithWaypoint(routeSteps, {0,0},{10,1});
    std::cout << "Advent of code 2020, day 12" << std::endl;
    std::cout << "A: " << answerA.manhattanDistance() << std::endl;
    std::cout << "B: " << answerB.manhattanDistance() << std::endl;



    return 0;
}

void doStepWithWaypoint(const routeStep& step, position& currPosShip,position& currPosWaypoint) {
    if (windDirs.find(step.dir)!= windDirs.end()){
        currPosWaypoint.x += windDirs.find(step.dir)->second.x*step.distance;
        currPosWaypoint.y += windDirs.find(step.dir)->second.y*step.distance;
    } else if (step.dir=='F'){
        currPosShip.x += (currPosWaypoint.x)*step.distance;
        currPosShip.y += (currPosWaypoint.y)*step.distance;
    } else {
        int dirChange = (step.distance/90)%4;
        if (step.dir=='L'){
            dirChange%=4;
            dirChange =4-dirChange;//rotating 90deg left = 270deg right
        }
        for (int i=0;i<dirChange;i++){
            position oldPosWaypoint = currPosWaypoint;

            currPosWaypoint.x =(oldPosWaypoint.y);
            currPosWaypoint.y =(oldPosWaypoint.x)*-1;
        }
    }
}

void doStep(const routeStep& step, position& currPos, char& currDir){
    if (windDirs.find(step.dir)!= windDirs.end()){
        currPos.x += windDirs.find(step.dir)->second.x*step.distance;
        currPos.y += windDirs.find(step.dir)->second.y*step.distance;
    } else if (step.dir=='F'){
        currPos.x += windDirs.find(currDir)->second.x*step.distance;
        currPos.y += windDirs.find(currDir)->second.y*step.distance;
    } else {
        std::string dirs = "NESW";
        char prevdir = currDir;
        int currDirNr=dirs.find(currDir);
         int dirChange = (step.distance/90)%4;

        if (step.dir=='L'){
            dirChange*=-1;
        }
        if (currDirNr+dirChange<0){
            dirChange+=4;
        }
        currDir = dirs.at((currDirNr+dirChange)%4);
    }
}

position followRouteWithWaypoint(std::vector<routeStep> routeSteps, position startposShip, position startposWaypoint){
    position currposShip=startposShip;
    position currposWaypoint=startposWaypoint;

    for (const auto& step:routeSteps){
        doStepWithWaypoint(step,currposShip,currposWaypoint);
    }
    return currposShip;
}


position followRoute(std::vector<routeStep> routeSteps, position startpos, char startdir){
    char currdir=startdir;
    position currpos=startpos;

    for (const auto& step:routeSteps){
        doStep(step,currpos,currdir) ;
    }
    return currpos;
}

routeStep parseLine(const std::string& line){
    routeStep result;
    result.dir=line.at(0);
    std::string nrPart = line.substr(1, line.length() - 1);
    result.distance=std::stoi(nrPart);
    return result;
}


std::vector<routeStep> readFile(const std::string &filename) {
    std::vector<routeStep> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);

    std::string line;
    std::map<std::string, std::string> singlePassportData;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (!line.empty())
            result.push_back(parseLine(line));
    }
    fs.close();
    return result;
}

