#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>

typedef  std::deque<int> playerDeck;

std::vector<playerDeck> readFile(const std::string &filename);

long long getAnswerA(const std::vector<playerDeck> &decks);
long long getAnswerB(const std::vector<playerDeck> &decks);

int main() {
    auto  fileContent = readFile("input.txt");

   auto answerA = getAnswerA(fileContent);
   auto answerB = getAnswerB(fileContent);

    std::cout << "Advent of code 2020, day 22" << std::endl;
    std::cout << "A: " << answerA << std::endl;
    std::cout << "B: " << answerB << std::endl;
    return 0;
}

void playGame(std::deque<int>& player1Deck, std::deque<int>& player2Deck){
    while (!player1Deck.empty() && !player2Deck.empty()){
        int p1card = player1Deck.front();
        player1Deck.pop_front();
        int p2card = player2Deck.front();
        player2Deck.pop_front();

        if (p1card > p2card){
            player1Deck.push_back(p1card);
            player1Deck.push_back(p2card);
        } else {
            player2Deck.push_back(p2card);
            player2Deck.push_back(p1card);
        }
    }
}

long long calcScore(const std::deque<int> &deck){
    auto deckCopy = deck;
    long long result = 0;
    long long counter=1;
    while (!deckCopy.empty()){
        int card = deckCopy.back();
        deckCopy.pop_back();
        result += counter*card;
        counter++;
    }
    return result;

}


std::string createGameState(std::deque<int>& player1Deck, std::deque<int>& player2Deck){
    std::string result;

    result.append("P1:");
    for (auto card:player1Deck) {
        result.append(std::to_string(card));
        result.append(",");
    }

    result.append("P2:");
    for (auto card:player2Deck) {
        result.append(std::to_string(card));
        result.append(",");
    }

    return result;
}

bool playRecursiveGameP1Wins(std::deque<int>& player1Deck, std::deque<int>& player2Deck){
    bool p1IsWinner;
    std::set<std::string> previousGameStates;
    while (!player1Deck.empty() && !player2Deck.empty()){

        std::string newDeckState = createGameState(player1Deck, player2Deck);
        if (previousGameStates.find(newDeckState) != previousGameStates.end()) {
            p1IsWinner = true;
            break;
        }
        previousGameStates.insert(newDeckState);


        int p1card = player1Deck.front();
        player1Deck.pop_front();
        int p2card = player2Deck.front();
        player2Deck.pop_front();

        if (p1card <= player1Deck.size() && p2card <= player2Deck.size()) {
            std::deque<int> p1recursiveDeck {player1Deck.begin(),player1Deck.begin()+p1card};
            std::deque<int> p2recursiveDeck {player2Deck.begin(),player2Deck.begin()+p2card};
            p1IsWinner = playRecursiveGameP1Wins(p1recursiveDeck, p2recursiveDeck);
        } else {
            p1IsWinner = (p1card>p2card);
        }

        if (p1IsWinner){
            player1Deck.push_back(p1card);
            player1Deck.push_back(p2card);
        } else {
            player2Deck.push_back(p2card);
            player2Deck.push_back(p1card);
        }
    }
    return p1IsWinner;
}

long long getAnswerA(const std::vector<playerDeck> &decks){
    playerDeck player1deck = decks.at(0);
    playerDeck player2deck = decks.at(1);
    long long score;
    playGame(player1deck, player2deck );
    if (!player1deck.empty()) {
        score = calcScore(player1deck);
    } else {
        score = calcScore(player2deck);
    }
    return score;
}

long long getAnswerB(const std::vector<playerDeck> &decks){
    playerDeck player1deck = decks.at(0);
    playerDeck player2deck = decks.at(1);
    long long score;
    bool p1IsWinner = playRecursiveGameP1Wins(player1deck, player2deck );
    if (p1IsWinner) {
        score = calcScore(player1deck);
    } else {
        score = calcScore(player2deck);
    }
    return score;
}

std::vector<playerDeck> readFile(const std::string &filename){
    std::vector<playerDeck> result;

    std::fstream fs;
    fs.open(filename, std::fstream::openmode::_S_in);
    playerDeck currDeck;
    std::string line;
    while (fs && !fs.eof()) {
        std::getline(fs, line);
        if (!line.empty()){
            if (line.substr(0,7) == "Player "){
                if (!currDeck.empty()){
                    result.push_back(currDeck);
                }
                currDeck.clear();
            } else {
                int currCard = std::stoi(line);
                currDeck.push_back(currCard);
            }
        }
    }
    if (!currDeck.empty()){
        result.push_back(currDeck);
    }
    fs.close();
    return result;
}

