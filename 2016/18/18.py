def main():
    txt = "^.^^^.^..^....^^....^^^^.^^.^...^^.^.^^.^^.^^..^.^...^.^..^.^^.^..^.....^^^.^.^^^..^^...^^^...^...^."
    print("18 A: Number of safe tiles in 40 lines: " + str(get_number_of_safe_tiles(txt, 40)))
    print("18 B: Number of safe tiles in 400000 lines: " + str(get_number_of_safe_tiles(txt, 400000)))


def get_number_of_safe_tiles(start_line, nr_of_lines):
    nr_of_bits = len(start_line)
    curr_line = string_to_bits(start_line)
    nr_of_traps = 0
    for i in range(0, nr_of_lines):
        # print (format(curr_line, '0'+str(nr_of_bits)+'b'))
        nr_of_traps += format(curr_line, '0' + str(nr_of_bits) + 'b').count('0')
        curr_line = get_next_line(curr_line, nr_of_bits)
    return nr_of_traps


def get_next_line(bin_prev_line, nr_of_bits):
    left = bin_prev_line << 1
    left &= (1 << nr_of_bits) - 1
    right = bin_prev_line >> 1
    result = left ^ right
    return result


def string_to_bits(txt):
    index = 0
    result = 0
    for char in txt:
        result *= 2
        if char == '^':
            result += 1
        index += 1
    return result


main()
