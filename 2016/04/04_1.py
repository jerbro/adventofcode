import re
import operator

def getKey(item):
	return item[0]

def roomIsReal(code):
	charCnt = {}
	parts = re.search('([a-z\-]*)([0-9]*)\[([a-z]{5})\]',code)

	for ch in parts.group(1):
		if ch != '-':
			if ch in charCnt:
				charCnt[ch] += 1
			else:
				charCnt[ch] = 1

	charCnt2 = charCnt.items() #dict to tuples
	charCnt2.sort(key=lambda x:(-x[1],x[0])) #sort first by count (descending), then alphabetical by key

	#Caluclate checksum
	chksum = ""
	for x in range(0,5):
		chksum +=charCnt2[x][0] 

	result = {}
	result['checksum'] = chksum
	result['sectorId'] = parts.group(2)
	result['valid'] = (chksum == parts.group(3))
	
	return result



lines = open('input.txt','r').read().splitlines()
sectorSum = 0
for line in lines:
	roomStatus = roomIsReal(line)
	if roomStatus['valid']:
		sectorSum += int(roomStatus['sectorId'])

print sectorSum
