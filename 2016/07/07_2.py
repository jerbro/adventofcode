import re

count = 0;
filecontent = open('input.txt','r').read().splitlines()
for line in filecontent:
	SuccessfullLine = False
	prevch = ''
	preprevch = ''
	inBrackets = False
	inBracketsFound = []
	outBracketsFound = []
	for ch in line:
		if ch == preprevch and ch != prevch:
			if inBrackets:
				inBracketsFound.append(str(ch+prevch))
			else:
				outBracketsFound.append(str(prevch+ch))
		if ch == '[':
			inBrackets = True
		if ch == ']':
			inBrackets = False
		preprevch = prevch
		prevch = ch
	for item in inBracketsFound:
		if item in outBracketsFound:
			print item + " - "+line
			SuccessfullLine = True
	if SuccessfullLine:
		count +=1


print count

