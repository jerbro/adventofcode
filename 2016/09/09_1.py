
zippedText = "X(8x2)(3x3)ABCY"
filecontent  = open('input.txt','r').read().splitlines()
zippedText = filecontent[0]
unzippedText = ""
tmpstr = ""

markersizeCounter = 0
markerRepetitions = 0
readingMarkerPart = 0 # 0=none;1=opening '(' 2 = size;3='x';4=repetitions; 5=closing ')' ;6=textToRepeat
markerSize = 0
for ch in zippedText:
	if ch == " ":
		continue
	if readingMarkerPart == 0 and ch == "(":
		readingMarkerPart = 1

	if (readingMarkerPart == 1 or readingMarkerPart == 2) and ch.isdigit():
		markerSize *= 10
		markerSize += int(ch)
		readingMarkerPart = 2

	if (readingMarkerPart == 2 and ch == 'x'):
		readingMarkerPart = 3

	if (readingMarkerPart == 3 or readingMarkerPart == 4 ) and ch.isdigit():
		readingMarkerPart = 4
		markerRepetitions*=10
		markerRepetitions += int(ch)

	if readingMarkerPart == 0:
		unzippedText += ch

	if readingMarkerPart == 5 or readingMarkerPart == 6:
		readingMarkerPart = 6
		if markersizeCounter > 1:
			tmpstr += ch
			markersizeCounter -=1
		else:
			tmpstr += ch
			markersizeCounter -=1
			for x in range(0,markerRepetitions):
				unzippedText += tmpstr
			tmpstr = ""
			readingMarkerPart = 0
			markerRepetitions = 0
			markerSize = 0


	if readingMarkerPart == 4 and ch == ')':
		readingMarkerPart = 5
		markersizeCounter = markerSize

	print "ch: "+ch+" - part: "+str(readingMarkerPart)

	

print unzippedText
print "size: "+str(len(unzippedText))