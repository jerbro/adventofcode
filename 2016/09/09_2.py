
import re

def parseMarker(text):
	parsed = re.search("\((\d+)x(\d+)\)(.*)",text)

	return {'size': int(parsed.group(1)),
			'repetition': int(parsed.group(2)),
			'othertext': parsed.group(3)}

def getSizeOfText(text,doRecursive):
	size = 0
	textNew = text
	while len(textNew)>0:
		if textNew[0] == " ":
			textNew = textNew[1:]
			continue
		if textNew[0] == "(":
			parsedMarker = parseMarker(textNew)
			if doRecursive:
				size += parsedMarker['repetition'] * getSizeOfText(parsedMarker['othertext'][0:parsedMarker['size']],True)
			else:
				size += parsedMarker['repetition'] * parsedMarker['size']
			textNew = parsedMarker['othertext'][parsedMarker['size']:]
		else:
			size += 1
			textNew = textNew[1:]
	return size



filecontent  = open('input.txt','r').read().splitlines()
line = filecontent[0]# only interested in the first line
line = line.replace(" ", "")
print "Length: " +str(getSizeOfText(line,True))










