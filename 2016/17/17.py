import hashlib

def main():
    print ("17 A: Shorstest path to destination: " + str(get_shortest_path("qtetzkpl")))
    print ("17 B: Longest path to destination: " + str(get_longest_path("qtetzkpl")))



def get_shortest_path(start_code):
    curr_positions=dict()
    curr_positions[start_code]=(0,0)
    steps = 0
    result_found = False
    while not result_found and len(curr_positions)>0:
        steps+=1
        # print(str(steps)+" - " +str(curr_positions))
        curr_positions = get_valid_positions_next_step(curr_positions)
        destination_found = final_destination_in_results(curr_positions)
        if destination_found:
            result_found = True
            return destination_found[len(start_code):]
    print("Not found, but no new results")
    return False

def get_longest_path(start_code):
    curr_positions = dict()
    curr_positions[start_code] = (0, 0)
    steps = 0
    longest_till_now = 0
    while len(curr_positions) > 0:
        steps += 1
        # print(str(steps) + " - " + str(len(curr_positions))+" - "+str(longest_till_now))
        curr_positions = get_valid_positions_next_step(curr_positions)
        destinations_removed = remove_final_destinations_in_results(curr_positions)
        if destinations_removed>0:
            longest_till_now = steps
    return longest_till_now


def remove_final_destinations_in_results(curr_positions):
    results_removed = 0
    results_to_remove = set()
    for pos in curr_positions:
        if curr_positions[pos][0] == 3 and curr_positions[pos][1] ==3:
            results_to_remove.add(pos)
            results_removed +=1
    for rem in results_to_remove:
        curr_positions.pop(rem)
    return len(results_to_remove)


def final_destination_in_results(curr_positions):
    for pos in curr_positions:
        if curr_positions[pos][0] == 3 and curr_positions[pos][1] ==3:
            return pos

    return False


def get_valid_positions_next_step(curr_positions):
    result = dict()
    for curr_pos in curr_positions:
        result.update( get_valid_directions(curr_positions[curr_pos][0],curr_positions[curr_pos][1],curr_pos))
    return result

def get_valid_directions(x,y,positionstring):
    valid_dir_xy = get_valid_directions_by_position(x,y)
    md5sum = hashlib.md5()
    md5sum.update(positionstring.encode("utf-8"))
    valid_dir_md5 = get_valid_directions_of_md5(md5sum.hexdigest())

    result = dict()
    if valid_dir_md5 and valid_dir_xy:
        for direction in valid_dir_md5:
            if direction in valid_dir_xy:
                result[positionstring+direction] = (x+valid_dir_xy[direction][0],y+valid_dir_xy[direction][1])

    return result



# Get valid directions. Assume grid is 4 x 4. left above is 0,0. right down is 3,3
def get_valid_directions_by_position(x,y):
    result = dict()
    if x >= 1:
        result['L'] = (-1,0)
    if x <= 2:
        result['R'] = (1, 0)
    if y >= 1:
        result['U'] = (0, -1)
    if y <= 2:
        result['D'] = (0, 1)
    return result


def get_valid_directions_of_md5(md5sum):
    open_code = set(['b','c', 'd','e','f'])
    result = set()
    if (md5sum[0] in open_code):
        result.add('U')
    if (md5sum[1] in open_code):
        result.add('D')
    if (md5sum[2] in open_code):
        result.add('L')
    if (md5sum[3] in open_code):
        result.add('R')
    return result



main()