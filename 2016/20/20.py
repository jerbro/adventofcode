import re


def main():
    lines = open('input.txt', 'r').read().splitlines()

    blacklist = list()
    for line in lines:
        blacklist_regex = re.search("(\\d+)-(\\d+)", line)
        blacklist.append((int(blacklist_regex.group(1)), int(blacklist_regex.group(2))))

    blacklist.sort()
    print "blacklist: " + str(blacklist)

    allowed_ranges = list()
    allowed_ranges.append((0, 4294967295))

    allowed_ranges = get_allowed_ranges(allowed_ranges, blacklist)
    print allowed_ranges

    print("20 A: Smallest allowed IP: " + str(allowed_ranges[0][0]))
    print("20 B: Total number of allowed IP: " + str(count_allowed_addresses(allowed_ranges)))


def count_allowed_addresses(allowed_addresses):
    count = 0
    for allowed_range in allowed_addresses:
        count += 1 + allowed_range[1] - allowed_range[0]
    return count


def get_allowed_ranges(allowed_range, blacklist):
    new_allowed_ranges = allowed_range
    for blacklist_range in blacklist:
        new_allowed_ranges = do_blacklist_range(new_allowed_ranges, blacklist_range)
    return new_allowed_ranges


def do_blacklist_range(allowed_ranges, blacklist_range):
    allowed_ranges.sort()

    bl_start = blacklist_range[0]
    bl_end = blacklist_range[1]
    new_al_list = list()
    for allowed_range in allowed_ranges:
        range_in_blacklist = False
        al_start = allowed_range[0]
        al_end = allowed_range[1]
        # Before the start of the blacklist, there is a bit allowed range
        if al_end > bl_start >= al_start:
            if al_start <= bl_start - 1:
                new_al_list.append((al_start, bl_start - 1))
            range_in_blacklist = True
        # after the end of the blacklist, there is a bit of allowed range
        if al_end >= bl_end >= al_start:
            if bl_end + 1 <= al_end:
                new_al_list.append((bl_end + 1, al_end))
            range_in_blacklist = True
        # the complete allowed range is blacklisted
        if bl_start <= al_start and bl_end >= al_end:
            range_in_blacklist = True
        # nothing is blacklisted
        if not range_in_blacklist:
            new_al_list.append(allowed_range)

    new_al_list.sort()
    return new_al_list


main()
