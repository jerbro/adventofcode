import re
import numpy


def main():
    nodes = parse_file('input.txt')

    print_map(nodes)

    viable_pairs = get_viable_pairs(nodes)
    print("22A: number of viable pairs: " + str(len(viable_pairs)))

    print("22B: Number of steps to get data:" + str(calc_number_of_steps(nodes)))


def get_viable_pairs(nodes):
    result = set()
    for node_a in nodes:
        for node_b in nodes:
            if not node_a == node_b and pair_is_viable(nodes, node_a, node_b):
                result.add(node_a + "&" + node_b)
    return result


def pair_is_viable(nodes, node_a, node_b):
    if 0 < nodes[node_a]['used'] <= nodes[node_b]['avail']:
        return True
    else:
        return False


def print_map(nodes):
    largest_x = 0
    largest_y = 0
    for node in nodes:
        if nodes[node]['x-pos'] > largest_x:
            largest_x = nodes[node]['x-pos']
        if nodes[node]['y-pos'] > largest_y:
            largest_y = nodes[node]['y-pos']
    map_array = numpy.empty((largest_y + 1, largest_x + 1), dtype=str)
    for x in range(0, largest_x + 1):
        for y in range(0, largest_y + 1):
            node = str(x) + "-" + str(y)
            map_array[y][x] = "."
            if nodes[node]['avail'] > 50:
                map_array[y][x] = "_"

            if nodes[node]['used'] > 100:
                map_array[y][x] = "#"
    map_array[0][0] = 'E'
    map_array[0][largest_x] = 'G'

    for line in list(map_array):
        print(str().join(line))


def calc_number_of_steps(nodes):
    # Calculate the number of steps to move G to the empty slot. Assumptions:
    # G and E are always on same line. E is always left-top. Wall is always
    # continuous (starting at right). Empty-slot is right-below the wall

    # E....G
    # ......
    # ...###
    # ...._.

    # in above puzzle, _ is the empty slot. G needs to be moved to position E, # is a ' wall'
    # To solve this:
    # move empty slot to left of the wall
    # move empty slot up to the line with G
    # move empty slot to the left of G
    # move G to the left, and move empty-slot to left of G (takes 5 steps)
    # Do the last move of G, no need to move the empty slot.
    #
    # number of steps needed for each 'step' are calculated. Result is returned
    important_positions = get_important_positions(nodes)
    left_to_pass_wall = important_positions['empty']['x'] - important_positions['wall']['x'] - 1
    up_to_greatest_line = important_positions['empty']['y'] - important_positions['greatest']['y']
    right_after_pass_wall = important_positions['greatest']['x'] - 1 - important_positions['wall']['x'] - 1
    left_with_greatest_block = 5 * important_positions['greatest']['x'] - 1 - 0
    last_move = 1
    steps = left_to_pass_wall + right_after_pass_wall + up_to_greatest_line + left_with_greatest_block + last_move
    return steps


def print_puzzle_array(map_array):
    for line in list(map_array):
        print("".join(line))


def puzzle_array_to_str(map_array):
    result = ""
    for line in list(map_array):
        result += ("".join(line))
    return result


def get_important_positions(nodes):
    result = dict()
    largest_x = 0
    largest_y = 0
    for node in nodes:
        if nodes[node]['x-pos'] > largest_x:
            largest_x = nodes[node]['x-pos']
        if nodes[node]['y-pos'] > largest_y:
            largest_y = nodes[node]['y-pos']
    for x in range(0, largest_x + 1):
        for y in range(0, largest_y + 1):
            node = str(x) + "-" + str(y)
            if nodes[node]['avail'] > 50:
                result['empty'] = {'x': x, 'y': y}

            # Find the ' left' -position of the wall (the first)
            if nodes[node]['used'] > 100 and 'wall' not in result:
                result['wall'] = {'x': x, 'y': y}

    result['greatest'] = {'x': largest_x, 'y': 0}

    return result


def get_adjacent_nodes(nodes, curr_node):
    result = set()
    curr_xpos = nodes[curr_node]['x-pos']
    curr_ypos = nodes[curr_node]['y-pos']
    add_node_to_set_if_exists(nodes, result, str(curr_xpos + 1) + "-" + str(curr_ypos))
    add_node_to_set_if_exists(nodes, result, str(curr_xpos - 1) + "-" + str(curr_ypos))
    add_node_to_set_if_exists(nodes, result, str(curr_xpos) + "-" + str(curr_ypos + 1))
    add_node_to_set_if_exists(nodes, result, str(curr_xpos) + "-" + str(curr_ypos - 1))

    return result


def add_node_to_set_if_exists(nodes, nodes_set, curr_node):
    if curr_node in nodes:
        nodes_set.add(curr_node)


def parse_file(filename):
    result = dict()
    filecontent = open(filename, 'r').read().splitlines()
    for line in filecontent:
        result.update(parseline(line))
    return result


def parseline(line):
    regex = re.search("/dev/grid/node-x([0-9]+)-y([0-9]+)\s+([0-9]+)T\s+([0-9]+)T\s+([0-9]+)T\s+([0-9]+)%", line)
    result = dict()
    if regex:
        x_pos = regex.group(1)
        y_pos = regex.group(2)
        size = regex.group(3)
        used = regex.group(4)
        avail = regex.group(5)
        result[x_pos + "-" + y_pos] = {'x-pos': int(x_pos), 'y-pos': int(y_pos), 'size': int(size), 'used': int(used),
                                       'avail': int(avail)}
    return result


main()
