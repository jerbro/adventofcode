def main():
    setup_a = {'E': 1,
               'floors': {1: ['G1', 'M1', 'G2', 'G3'], 2: ['M2', 'M3'], 3: ['G4', 'M4', 'G5', 'M5'], 4: []}}
    steps_11_a = solve_elevator_puzzle(setup_a, 100)

    setup_b = {'E': 1, 'floors': {1: ['Gel', 'Mel', 'Gdi', 'Mdi', 'Gth', 'Mth', 'Gpl', 'Gst'], 2: ['Mpl', 'Mst'],
                                  3: ['Gpr', 'Mpr', 'Gru', 'Mru'], 4: []}}
    steps_11_b = solve_elevator_puzzle(setup_b, 100)
    print ("11A: found result in " + str(steps_11_a))
    print ("11B: found result in " + str(steps_11_b))


def solve_elevator_puzzle(curr_setup, max_levels_deep):
    old_states = set()
    sort_objects(curr_setup)
    new_setups = find_possible_new_setups(curr_setup)
    levels_deep = 1
    found_final_destination = False
    while levels_deep < max_levels_deep and not found_final_destination:
        levels_deep += 1
        newer_setups = []
        for nw_setup in new_setups:
            newer_setups += find_possible_new_setups(nw_setup)

        new_setups = []
        copy_new_states_ignore_existing(old_states, newer_setups, new_setups)
        found_final_destination = final_destination_in_setups_list(new_setups)

        print ("Level: " + str(levels_deep) + " Currently " + str(len(newer_setups)) + " valid states of which " + str(
            len(new_setups)) + " were not already found in a previous step. Currently " + str(
            len(old_states)) + " in list of old states")

    if found_final_destination:
        print ("found final destination after " + str(levels_deep) + " steps")
        return levels_deep
    else:
        print ("Not found final destination within " + str(levels_deep) + "steps")
        return -1


def final_destination_in_setups_list(setups):
    for nw_setup in setups:
        if setup_is_final_destination(nw_setup, 4):
            return True
    return False


def copy_new_states_ignore_existing(old_states, newer_setups, new_setups):
    for nw_setup in newer_setups:
        if not (str(nw_setup) in old_states):
            old_states.add(str(nw_setup))
            new_setups.append(nw_setup)


def sort_objects(curr_setup):
    for floor in curr_setup['floors']:
        curr_setup['floors'][floor].sort()


def setup_is_final_destination(curr_setup, highest_floor):
    for floor in curr_setup['floors']:
        if floor < highest_floor and len(curr_setup['floors'][floor]) > 0:
            return False
    return True


def find_possible_new_setups(curr_setup):
    new_valid_setups = []
    elevator_floor = curr_setup['E']

    dirs = []  # Make list with allowed directions
    if elevator_floor - 1 in curr_setup['floors']:
        dirs.append(-1)
    if elevator_floor + 1 in curr_setup['floors']:
        dirs.append(+1)

    # for curr_dir in dirs: # Optimized out. Assuming. always 1 object down. Always 2 objects up
    for i in range(0, len(curr_setup['floors'][elevator_floor])):
        for j in range(0, len(curr_setup['floors'][elevator_floor])):
            new_setup = {}
            if i != j:
                if +1 in dirs:
                    new_setup = move_object_to_other_floor(curr_setup, [i, j], elevator_floor, +1)
            else:
                if -1 in dirs:
                    new_setup = move_object_to_other_floor(curr_setup, [i], elevator_floor, -1)

            if new_setup and not (setup_fries_chip(new_setup)):
                new_valid_setups.append(new_setup)

    return new_valid_setups


def copy_setup(curr_setup):
    new_setup = {'E': curr_setup['E'], 'floors': {}}
    for floor in curr_setup['floors']:
        new_setup['floors'][floor] = []
        for obj in curr_setup['floors'][floor]:
            new_setup['floors'][floor].append(obj)
    return new_setup


def move_object_to_other_floor(curr_setup, object_ids, elevator_floor, elevator_dir):
    # new_setup = copy.deepcopy(setup)
    new_setup = copy_setup(curr_setup)
    object_ids.sort(reverse=True)
    for obj_id in object_ids:
        new_setup['floors'][elevator_floor + elevator_dir].append(new_setup['floors'][elevator_floor][obj_id])
        new_setup['floors'][elevator_floor].pop(obj_id)

    new_setup['floors'][elevator_floor + elevator_dir].sort()
    new_setup['E'] += elevator_dir
    return new_setup


def get_floor_with_elevator(curr_setup):
    for floor in curr_setup:
        if 'E' in curr_setup[floor]:
            return floor
    print("No floor with elevator")
    raise SystemExit


def setup_fries_chip(curr_setup):
    for floor in curr_setup['floors']:
        if floor_fries_chip(curr_setup['floors'][floor]):
            return True
    return False


def floor_fries_chip(floor):
    floor_has_generator = False
    for curr_object in floor:
        if curr_object[:1] == 'G':  # objects on a floor are sorted alphabetically. So all 'G's are found before 'M's
            floor_has_generator = True
        if curr_object[:1] == 'M':
            if floor_has_generator and not ('G' + curr_object[1:]) in floor:
                return True
    return False


main()
