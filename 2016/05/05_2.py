import md5


#string = open('input.txt','r').read()
string = 'wtnhxymk'
#string = "abc"
result = '________'


numberFound = False
testNumber = 0
while numberFound ==False:
	testNumber +=1
	md5result = md5.new(string+str(testNumber)).hexdigest()
	if md5result[:5]=="00000":
		position = str((md5result[5:6]))
		code = md5result[6:7]
		if str.isdigit(position) and int(position) < 8:
			if result[int(position):int(position)+1] == '_':
				result = result[0:int(position)]+code+result[int(position)+1:7]
		if result.find('_') == -1:
			numberFound = True
		print "found digit: %d. (at iteration: %d, with MD5sum: %s) Code till now is: %s" %(numberFound,testNumber,md5result,result)

print "For string: '%s', the answer is: %s" %(string,result)