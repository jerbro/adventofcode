import re

def main():
    discs_a = get_disc_config('input.txt')
    time_a = get_fallthrough_position(discs_a)
    print("15A. Position found at time = "+str(time_a-1))


    discs_b = get_disc_config('input_b.txt')
    time_b = get_fallthrough_position(discs_b)
    print("15B. Position found at time = " + str(time_b - 1))


def get_disc_config(filename):
    filecontent = open(filename, 'r').read().splitlines()

    discs = []
    for line in filecontent:
        discs.append(parse_line(line))
    return discs

def get_fallthrough_position(discs):
    time = 0
    position_found = False
    while not position_found:
        if all_discs_in_position(discs, time):
            position_found = True
        time += 1
    return time-1

def all_discs_in_position(discs,time):
    for disc in discs:
        if disc_position(disc,time) != 0:
            return False
    return True

def disc_position(disc,time):
    time_since_start = time+disc['disc_id']-disc['start_time']
    return (time_since_start+disc['start_position'] ) % disc['positions']



def parse_line(line):
    result = {}
    regex_result = re.search("Disc #(\\d+) has (\\d+) positions; at time=(\\d+), it is at position (\\d+).",line)
    result['disc_id'] = int(regex_result.group(1))
    result['positions'] = int(regex_result.group(2))
    result['start_time'] = int(regex_result.group(3))
    result['start_position'] = int(regex_result.group(4))
    return result
main()