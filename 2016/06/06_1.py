
filecontent = open('input.txt','r').read().splitlines()
charsAtPos = []

for i in range ( 0 , len(filecontent[0])):
	charsAtPos.append({})


for line in filecontent:
	pos = 0
	for ch in line:
		if ch in charsAtPos[pos]:
			charsAtPos[pos][ch] +=1
		else:
			charsAtPos[pos][ch] = 1
		pos +=1

resultstr_ptA = ""
resultstr_ptB = ""
for chars in charsAtPos:
	listOfChars = list(chars.items())
	listOfChars.sort(key = lambda x:x[1], reverse = True)
	resultstr_ptA+=(listOfChars[0][0])
	listOfChars.sort(key = lambda x:x[1], reverse = False)
	resultstr_ptB+=(listOfChars[0][0])


print "The message for part a is: "+resultstr_ptA
print "The message for part b is: "+resultstr_ptB