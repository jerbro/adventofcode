import re
width = 50
height = 6
img = []
for y in range(0,height):
	img.append([])
	for x in range(0,width):
		img[y].append(' ')


def drawrect(img,xorig,yorig,w,h):
	for x in range(0,w):
		for y in range(0,h):
			img[yorig+y][xorig+x] = '*'	
	return img

def printimg(img):
	for line in img:
		print ''.join(line)

def rotaterow(img,row,rotations):
	imgHeight = len(img)
	imgWidth = len(img[0])
	newImg = []
	for y in range(0,imgHeight):
		newImgLine = []
		for x in range(0,imgWidth):
			if y == row:
				newImgLine.append(img[y][(x-rotations)%imgWidth])
			else:
				newImgLine.append(img[y][x])
		newImg .append(newImgLine)
	return newImg

def rotatecol(img,col,rotations):
	imgHeight = len(img)
	imgWidth = len(img[0])
	newImg = []
	for y in range(0,imgHeight):
		newImgLine = []
		for x in range(0,imgWidth):
			if x == col:
				newImgLine.append(img[(y-rotations)%imgHeight][x])
			else:
				newImgLine.append(img[y][x])
		newImg .append(newImgLine)
	return newImg

def countChars(img,char):
	imgHeight = len(img)
	imgWidth = len(img[0])
	count = 0
	for y in range(0,imgHeight):
		for x in range(0,imgWidth):
			if img[y][x] == char:
				count +=1			
	return count	

def executeLine(line,img):
	regexrotcol = re.search('rotate column x=([0-9]*) by ([0-9]*)',line)
	regexrotrow = re.search('rotate row y=([0-9]*) by ([0-9]*)',line)
	regexrotrect = re.search('rect ([0-9]*)x([0-9]*)',line)
	newImg = img
	if regexrotcol:
		newImg = rotatecol(img,int(regexrotcol.group(1)),int(regexrotcol.group(2)))
	
	if regexrotrow:
		newImg = rotaterow(img,int(regexrotrow.group(1)),int(regexrotrow.group(2)))	
	
	if regexrotrect:
		newImg = drawrect(img,0,0,int(regexrotrect.group(1)),int(regexrotrect.group(2)))	
	return newImg	

filecontent = open('input.txt', 'r').read().splitlines()
for line in filecontent:
	img = executeLine(line,img)

print "8A: Number of on-pizels: "+ str(countChars(img,'*'))
print "8B: see the display-code below"
printimg(img)
