import re
filecontent = open('input.txt','r').read().splitlines()

validTriangles = 0
invalidTriangles = 0

for line in filecontent:
	parts = re.search('[ ]+([0-9]*)[ ]+([0-9]*)[ ]+([0-9]*)',line)
	nrs = []
	nrs.append(int(parts.group(1)))
	nrs.append(int(parts.group(2)))
	nrs.append(int(parts.group(3)))
	nrs.sort()

	if (nrs[0]+nrs[1] > nrs[2]):
		validTriangles +=1
	else:
		invalidTriangles +=1
		

print "Valid triangles: "+str(validTriangles)
print "Invalid triangles: "+str(invalidTriangles)


	
	