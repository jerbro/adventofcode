import re
filecontent = open('input.txt','r').read().splitlines()

validTriangles = 0
invalidTriangles = 0
nrsC1 = [] #numbers of column 1
nrsC2 = [] #numbers of column 2
nrsC3 = [] #numbers of column 3

for line in filecontent:
	parts = re.search('[ ]+([0-9]*)[ ]+([0-9]*)[ ]+([0-9]*)',line)
	nrsC1.append(int(parts.group(1))) 
	nrsC2.append(int(parts.group(2)))
	nrsC3.append(int(parts.group(3)))
	
nrs = nrsC1+nrsC2+nrsC3 #Make one list with the 3 culumns after each other

for i in range(0,(len(nrs)/3)):

	tmpNrs = nrs[i*3:i*3+3]	#get 3 numbers
	print str(tmpNrs)
	tmpNrs.sort()

	if (tmpNrs[0]+tmpNrs[1] > tmpNrs[2]):
		validTriangles +=1
	else:
		invalidTriangles +=1
		

print "Valid triangles: "+str(validTriangles)
print "Invalid triangles: "+str(invalidTriangles)


	
	