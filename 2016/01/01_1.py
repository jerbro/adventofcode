import re

def nextDir(currDir, changeDir):
	dirs = "NESW"
	if changeDir == 'R':
		next = +1
	else:
		next = -1

	currIndex = dirs.index(currDir)
	nextDir = currIndex+next
	nextDir %= 4
	return dirs[nextDir]





inputFile = open("source.txt","r")
fileContent = inputFile.read()
listOfInstructions = fileContent.split(",")

dircounts = { 'N':0,'E':0,'S':0,'W':0,}

currDir = 'N'
for item in listOfInstructions:
	m = re.search(' ?([RL])([0-9]*)',item)
	currDir = nextDir(currDir,m.group(1))
	dircounts[currDir] = dircounts[currDir]+int(m.group(2))



for direction in dircounts:
	print direction +" "+ str(dircounts[direction])

northDir = dircounts['N']-dircounts['S']
eastDir = dircounts['E']-dircounts['W']
totalDistance = abs(eastDir) + abs(northDir)
print "Travelled: "+str(northDir)+ " north, and "+str(eastDir)+" east."
print "Total distance travveled: "+str(totalDistance)



	
