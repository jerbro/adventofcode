def main():
    data_length = 272
    start_data = "01111010110010011"
    data = create_data(start_data, data_length)
    print("16 A. Checksum for length " + str(data_length) + ": " + calc_checksum(data[0:data_length]))

    data_length = 35651584
    data = create_data(start_data, data_length)
    print("16 B. Checksum for length " + str(data_length) + ": " + calc_checksum(data[0:data_length]))


def calc_checksum(txt):
    checksum_str = calc_checksum_string(txt)
    while len(checksum_str) % 2 == 0:
        checksum_str = calc_checksum_string(checksum_str)
    return checksum_str


def calc_checksum_string(txt):
    result = ""
    for i in range(0, len(txt) - 1, 2):
        if txt[i] == txt[i + 1]:
            result += "1"
        else:
            result += "0"
    return result


def create_data(start, min_size):
    txt = start
    while len(txt) < min_size:
        extra_txt = reverse_string(txt)
        extra_txt = extra_txt.replace('0', '2')
        extra_txt = extra_txt.replace('1', '0')
        extra_txt = extra_txt.replace('2', '1')
        txt += "0"
        txt += extra_txt
    return txt


def swap_ones_and_zeros(txt):
    txt = txt.replace("0", "2")
    txt = txt.replace('1', '0')
    txt = txt.replace('2', '1')
    return txt


def reverse_string(txt):
    return txt[::-1]


main()
