fileContent = open('input.txt','r').read().splitlines()

keyBoardMatrix = [['0','0','1','0','0'],['0','2','3','4','0'],['5','6','7','8','9'],['0','A','B','C','0'],['0','0','D','0','0']]

x=0
y=2
resultstr = ""

for line in fileContent:
	print "  "
#	print line
	for ch in line:
		yold = y
		xold = x
		if ch == 'U':
			y-=1
		if ch == 'D':
			y+=1
		if ch == 'L':
			x-=1
		if ch == 'R':
			x+=1

		if x < 0:
			x = 0
		if x > 4:
			x = 4
		if y < 0:
			y = 0
		if y > 4:
			y = 4

		if (keyBoardMatrix[y][x]=='0'):
			x = xold
			y = yold			

	print "x: "+str(x)+" y: "+str(y)
	print keyBoardMatrix[y][x]
	resultstr += str(keyBoardMatrix[y][x])

print "code: "+resultstr
