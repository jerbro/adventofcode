fileContent = open('input.txt','r').read().splitlines()

keyBoardMatrix = [[1,2,3],[4,5,6],[7,8,9]]
#print str(keyBoardMatrix[0][2])

x=1
y=1
resultstr = ""

for line in fileContent:
	print "  "
#	print line
	for ch in line:
		if ch == 'U':
			y-=1
		if ch == 'D':
			y+=1
		if ch == 'L':
			x-=1
		if ch == 'R':
			x+=1

		if x < 0:
			x = 0
		if x > 2:
			x = 2
		if y < 0:
			y = 0
		if y > 2:
			y = 2

	print "x: "+str(x)+" y: "+str(y)
	print keyBoardMatrix[y][x]
	resultstr += str(keyBoardMatrix[y][x])

print "code: "+resultstr
