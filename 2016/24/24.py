# - parse the map, put in in an array (array, [x][y]{'nr:'' or 0-7; -1 = not a number, 'wall': 0(not wall), 1(wall)}
# get the positions of the number-points
# get the number of steps from every point to every other point
# get the number of steps for each of the permutations of the number-points
# print the result

import copy
import itertools


def main():
    matrix_array = parse_inputfile('input.txt')
    number_positions = get_number_positions(matrix_array)

    distances = get_distances_between_numbers(number_positions, matrix_array)
    shortest_24a = get_shortest_path_all_pois(distances)
    print("24 A: Shortest path to all POI's: " + str(shortest_24a) + " steps")

    shortest_24b = get_shortest_path_all_pois_return_at_end(distances)
    print("24 A: Shortest path to all POI's: " + str(shortest_24b) + " steps")


def get_shortest_path_all_pois(distances):
    shortest = -1
    for order in itertools.permutations(list(distances.keys())):
        if order[0] == 0:
            currlen = get_total_distance(distances, order)
            if shortest == -1 or shortest > currlen:
                shortest = currlen
    return shortest


def get_shortest_path_all_pois_return_at_end(distances):
    shortest = -1
    for order in itertools.permutations(list(distances.keys())):
        if order[0] == 0:
            currlen = get_total_distance_return_at_end(distances, order)
            if shortest == -1 or shortest > currlen:
                shortest = currlen
    return shortest


def get_total_distance(distances, order):
    dist = 0
    prev = ''
    first = True
    for curr in order:
        if first:
            first = False
            prev = curr
        else:
            dist += distances[prev][curr]
            prev = curr
    return dist


def get_total_distance_return_at_end(distances, order):
    dist = 0
    prev = ''
    first = True
    for curr in order:
        if first:
            first = False
            prev = curr
        else:
            dist += distances[prev][curr]
            prev = curr

    dist += distances[prev][0]
    return dist


def get_distances_between_numbers(number_positions, matrix_array):
    distances = {}
    for number in number_positions:
        distances[number] = get_number_of_steps_to_other_cells_from_number(number, number_positions, matrix_array)
    return distances


def get_number_of_steps_to_other_cells_from_number(number, number_positions, matrix_array):
    matrix_array_tmp = copy.deepcopy(matrix_array)
    result = {number: 0}
    iteration = 0
    cells_next_iteration = list()
    cells_next_iteration.append(number_positions[number])
    while len(result) < len(number_positions):
        cells_this_iteration = []
        for curr_cell in cells_next_iteration:
            x = curr_cell['x']
            y = curr_cell['y']
            if not matrix_array_tmp[y][x]['already_visited']:
                matrix_array_tmp[y][x]['already_visited'] = True
                cells_this_iteration.append(curr_cell)

        cells_next_iteration = get_cells_next_iteration(cells_this_iteration, matrix_array_tmp)
        iteration += 1

        for curr_cell in cells_next_iteration:
            if cell_is_numberpoint(curr_cell, matrix_array_tmp):
                x = curr_cell['x']
                y = curr_cell['y']
                nr = matrix_array_tmp[y][x]['nr']
                result[nr] = iteration

    return result


def get_cells_next_iteration(cells_prev_iteration, matrix_array):
    cells_this_iteration = []
    for cell in cells_prev_iteration:
        new_cells = get_valid_new_cells(cell, matrix_array)
        cells_this_iteration += new_cells
    return cells_this_iteration


def get_valid_new_cells(cell, matrix_array):
    possible_cells = get_surrounding_cells(cell, matrix_array)
    valid_cells = []
    for cell in possible_cells:
        if not cell_is_already_visited(cell, matrix_array) and not (cell_is_wall(cell, matrix_array)):
            valid_cells.append(cell)

    return valid_cells


def cell_is_numberpoint(curr_cell, matrix_array):
    x = curr_cell['x']
    y = curr_cell['y']
    return matrix_array[y][x]['nr'] != -1


def cell_is_already_visited(curr_cell, matrix_array):
    x = curr_cell['x']
    y = curr_cell['y']
    return matrix_array[y][x]['already_visited']


def cell_is_wall(curr_cell, matrix_array):
    x = curr_cell['x']
    y = curr_cell['y']
    return matrix_array[y][x]['wall']


def get_surrounding_cells(curr_cell, matrix_array):
    result = []
    x = curr_cell['x']
    y = curr_cell['y']

    if x - 1 >= 0:  # left of current cell
        result.append({'x': x - 1, 'y': y})
    if x + 1 < len(matrix_array[y]):  # right of current cell
        result.append({'x': x + 1, 'y': y})

    if y - 1 >= 0:  # above current cell
        result.append({'x': x, 'y': y - 1})
    if y + 1 < len(matrix_array):  # below current cell
        result.append({'x': x, 'y': y + 1})

    return result


def get_number_positions(matrix_array):
    number_positions = dict()
    for y in range(0, len(matrix_array)):
        for x in range(0, len(matrix_array[y])):
            if matrix_array[y][x]['nr'] != -1:
                number_positions[matrix_array[y][x]['nr']] = {'x': x, 'y': y}
    return number_positions


def parse_inputfile(filename):
    lines = open(filename, 'r').read().splitlines()
    result_arr = []
    for line in lines:
        line_arr = []
        for x in range(0, len(line) - 1):
            curr_pos = dict()
            if line[x] == '#':
                curr_pos['wall'] = True
            else:
                curr_pos['wall'] = False
            if line[x].isdigit():
                curr_pos['nr'] = int(line[x])
            else:
                curr_pos['nr'] = -1

            curr_pos['already_visited'] = False

            line_arr.append(curr_pos)
        result_arr.append(line_arr)
    return result_arr


main()
