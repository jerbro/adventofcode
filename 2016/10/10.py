import re

def makeNewBot():
	return  {'lowTo': -1,
			'highTo': -1,
			'val0': -1, 
			'val1': -1,
			'inputBuffer': [],
			'botnr': -1}

def addValTo(bot,val):
	if bot['val0'] == -1:
		bot['val0'] = val
	else:
		if bot['val1'] == -1:
			bot['val1'] = val
		else: 
			bot['inputBuffer'].append(val)


def addValToBot(botsList,botval,botnr):
	if not botnr in botsList:
		botsList[botnr] = makeNewBot()
	addValTo(botsList[botnr],botval)


def setLowHeigh(botsList,botnr,lowval,highval,lowoutdir, highoutdir):
	if not botnr in botsList:
		botsList[botnr] = makeNewBot()

	botsList[botnr]['lowTo']=lowval
	botsList[botnr]['highTo']=highval
	botsList[botnr]['lowOutDir']=lowoutdir
	botsList[botnr]['highOutDir']=highoutdir

def parseLine(line,botsList):
	valto = re.search('value ([\d]+) goes to bot ([\d]+)',line)
	lowhigh = re.search('bot ([\d]+) gives low to (output|bot) ([\d]+) and high to (output|bot) ([\d]+)',line)
	if valto:
		addValToBot(botsList,int(valto.group(1)),int(valto.group(2)))
	if lowhigh:
		setLowHeigh(botsList,int(lowhigh.group(1)),int(lowhigh.group(3)),int(lowhigh.group(5)),lowhigh.group(2),lowhigh.group(4))

def executeBot(botnr, botsList,outputslist):
	val0 = botsList[botnr]['val0']
	val1 = botsList[botnr]['val1']
	lowTo = botsList[botnr]['lowTo']
	highTo = botsList[botnr]['highTo']
	lowDir = botsList[botnr]['lowOutDir']
	highDir = botsList[botnr]['highOutDir']
	retval = 0
	if val0 > -1 and val1 > -1:
		retval = 1
	#	print "Bot: "+str(botnr)+" Has values: "+str(val0)+" and: "+str(val1) + "  Which will go to (lowest): "+str(botsList[botnr]['lowTo'] )+ " and highest: "+str(botsList[botnr]['highTo'])
		if val0 > val1:
				lowest = val1
				highest = val0
		else:
				lowest = val0
				highest = val1

		if lowDir == "bot":
			addValTo(botsList[lowTo],lowest)
		else:
			outputsList[lowTo] = lowest	

		if highDir == "bot":
			addValTo(botsList[highTo],highest)
		else:
			outputsList[highTo] = highest	

		botsList[botnr]['val0']= -1
		botsList[botnr]['val1']= -1
	return retval

def checkIfBotHas61_17(botnr,botsList):
	val0 =  botsList[botnr]['val0']
	val1 =  botsList[botnr]['val1']
	if (val1 == 61 and val0 == 17) or (val1 == 17 and val0 == 61):
		return True
	return False



Globbotslist = {}
outputsList = {}
fileContent = open('input.txt','r').read().splitlines()
for line in fileContent:
	parseLine(line,Globbotslist)

found = False
iteration = 0
maxiterations =1000
changesThisIteration = 1
while (iteration < maxiterations and changesThisIteration>0):
	changesThisIteration = 0
	iteration +=1
	for bot in Globbotslist:
		if checkIfBotHas61_17(bot,Globbotslist):
			print "10 A: Bot comparing value 61 and 17 is: "+str(bot)
		changesThisIteration += executeBot(bot,Globbotslist,outputsList)

print "10 B: outputs 0,1,2 multiplied gives: "+str(outputsList[0]) +"x"+str(outputsList[1]) +"x"+str(outputsList[2]) +"="+str(outputsList[0]*outputsList[1]*outputsList[2])
