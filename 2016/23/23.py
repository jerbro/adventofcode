import re


def main():
    lines = open('input.txt', 'r').read().splitlines()

    parsed_lines = []
    for line in lines:
        parsed_lines.append(parse_instruction(line))

    registers = {'a': 7, 'b': 0, 'c': 0, 'd': 0}
    run_program(parsed_lines, registers)

    print "After executing, registers contain: " + str(registers)
    print "23A. Register 'a' contains: " + str(registers['a'])

    print " now with register a initialized as 12. executing... "
    registers = {'a': 12, 'b': 0, 'c': 0, 'd': 0}
    for instruction in parsed_lines:
        instruction['command_to_use'] = 0
    run_program(parsed_lines, registers)
    print "After executing, registers contain: " + str(registers)
    print "23B. Register 'a' contains: " + str(registers['a'])


def run_program(program, registers):
    pc = 0  # Reset program counter
    final_instruction = len(program)
    cnt = 0
    while pc < final_instruction:
        pc += execute(program[pc], registers, program, pc)



def execute(instruction, registers, program, pc):
    useAlt = instruction['command_to_use']
    if instruction['command'][useAlt] == 'jnz':
        return instr_jnz(instruction, registers)
    elif instruction['command'][useAlt] == 'inc':
        return instr_inc(instruction, registers)
    elif instruction['command'][useAlt] == 'dec':
        return instr_dec(instruction, registers)
    elif instruction['command'][useAlt] == 'cpy':
        return instr_cpy(instruction, registers)
    elif instruction['command'][useAlt] == 'tgl':
        return instr_tgl(instruction, registers, program, pc)
    else:
        return 999999


def is_int(value):
    try:
        int(value)
        return True
    except ValueError:
        return False


def instr_tgl(parsed_instruction, registers, program, pc):
    if parsed_instruction['arg1_is_value']:
        offset = parsed_instruction['arg1']
    else:
        offset = registers[parsed_instruction['arg1']]
    if pc + offset < len(program):
        if program[pc + offset]['command_to_use'] == 1:
            program[pc + offset]['command_to_use'] = 0
        else:
            program[pc + offset]['command_to_use'] = 1

    return 1


def instr_cpy(parsed_instruction, registers):
    if not parsed_instruction['arg2_is_value']:
        if parsed_instruction['arg1_is_value']:
            registers[parsed_instruction['arg2']] = parsed_instruction['arg1']
        else:
            registers[parsed_instruction['arg2']] = registers[parsed_instruction['arg1']]
    return 1


def instr_inc(parsed_instruction, registers):
    registers[parsed_instruction['arg1']] += 1
    return 1


def instr_dec(parsed_instruction, registers):
    registers[parsed_instruction['arg1']] -= 1
    return 1


def instr_jnz(parsed_instruction, registers):
    if (parsed_instruction['arg1_is_value']) and parsed_instruction['arg1'] == 0:
        return 1
    elif not (parsed_instruction['arg1_is_value']) and registers[parsed_instruction['arg1']] == 0:
        return 1
    else:
        if parsed_instruction['arg2_is_value']:
            return parsed_instruction['arg2']
        else:
            return registers[parsed_instruction['arg2']]


def add_toggled_instruction(parsed_instruction):
    orig_command = parsed_instruction['command']
    if parsed_instruction['arg2'] == '':
        if parsed_instruction['command'] == 'inc':
            tgl_command = 'dec'
        else:
            tgl_command = 'inc'
    else:
        if parsed_instruction['command'] == 'jnz':
            tgl_command = 'cpy'
        else:
            tgl_command = 'jnz'

    parsed_instruction['command'] = {0: orig_command, 1: tgl_command}
    parsed_instruction['command_to_use'] = 0
    return parsed_instruction


def parse_instruction(instruction):
    instruction_parts = re.search("([a-z]*) ([-0-9a-z]*)( )?([-0-9a-z]*)?", instruction)
    parsed_instruction = {}
    if instruction_parts:
        parsed_instruction['command'] = instruction_parts.group(1)
        parsed_instruction['arg1_is_value'] = is_int(instruction_parts.group(2))
        if parsed_instruction['arg1_is_value']:
            parsed_instruction['arg1'] = int(instruction_parts.group(2))
        else:
            parsed_instruction['arg1'] = instruction_parts.group(2)

        parsed_instruction['arg2_is_value'] = is_int(instruction_parts.group(4))
        if parsed_instruction['arg2_is_value']:
            parsed_instruction['arg2'] = int(instruction_parts.group(4))
        else:
            parsed_instruction['arg2'] = instruction_parts.group(4)

    return add_toggled_instruction(parsed_instruction)


main()
