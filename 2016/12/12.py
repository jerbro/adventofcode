import re


def main():
    lines = open('input.txt', 'r').read().splitlines()

    parsed_lines = []
    for line in lines:
        parsed_lines.append(parse_instruction(line))

    registers = {'a': 0, 'b': 0, 'c': 0, 'd': 0}
    run_program(parsed_lines, registers)

    print "After executing, registers contain: " + str(registers)
    print "12A. Register 'a' contains: " + str(registers['a'])

    print " now with register c initialized as 1. executing... "
    registers = {'a': 0, 'b': 0, 'c': 1, 'd': 0}
    run_program(parsed_lines, registers)
    print "After executing, registers contain: " + str(registers)
    print "12B. Register 'a' contains: " + str(registers['a'])


def run_program(program, registers):
    pc = 0  # Reset program counter
    final_instruction = len(program)
    while pc < final_instruction:
        pc += execute(program[pc], registers)


def execute(instruction, registers):
    if instruction['command'] == 'jnz':
        return instr_jnz(instruction, registers)
    elif instruction['command'] == 'inc':
        return instr_inc(instruction, registers)
    elif instruction['command'] == 'dec':
        return instr_dec(instruction, registers)
    elif instruction['command'] == 'cpy':
        return instr_cpy(instruction, registers)
    else:
        return 999999


def is_int(value):
    try:
        int(value)
        return True
    except ValueError:
        return False


def instr_cpy(parsed_instruction, registers):
    if parsed_instruction['arg1_is_value']:
        registers[parsed_instruction['arg2']] = parsed_instruction['arg1']
    else:
        registers[parsed_instruction['arg2']] = registers[parsed_instruction['arg1']]
    return 1


def instr_inc(parsed_instruction, registers):
    registers[parsed_instruction['arg1']] += 1
    return 1


def instr_dec(parsed_instruction, registers):
    registers[parsed_instruction['arg1']] -= 1
    return 1


def instr_jnz(parsed_instruction, registers):
    if (parsed_instruction['arg1_is_value']) and parsed_instruction['arg1'] == 0:
        return 1
    elif not (parsed_instruction['arg1_is_value']) and registers[parsed_instruction['arg1']] == 0:
        return 1
    else:
        return parsed_instruction['arg2']


def parse_instruction(instruction):
    instruction_parts = re.search("([a-z]*) ([0-9a-z]*)( )?([-0-9a-z]*)?", instruction)
    parsed_instruction = {}
    if instruction_parts:
        parsed_instruction['command'] = instruction_parts.group(1)
        parsed_instruction['arg1_is_value'] = is_int(instruction_parts.group(2))
        if parsed_instruction['arg1_is_value']:
            parsed_instruction['arg1'] = int(instruction_parts.group(2))
        else:
            parsed_instruction['arg1'] = instruction_parts.group(2)

        parsed_instruction['arg2_is_value'] = is_int(instruction_parts.group(4))
        if parsed_instruction['arg2_is_value']:
            parsed_instruction['arg2'] = int(instruction_parts.group(4))
        else:
            parsed_instruction['arg2'] = instruction_parts.group(4)

    return parsed_instruction


main()
