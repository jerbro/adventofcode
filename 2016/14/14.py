import hashlib
import re


def main():
    print("14 A: id of 64th hash: " + str(get_id_of_64th_hash("ihaygndm", 1)))
    print("14 B: id of 64th hash: " + str(get_id_of_64th_hash("ihaygndm", 2017)))


def get_id_of_64th_hash(base_hash, hash_count):
    saved_hashes = {}
    saved_results_three = {}
    saved_results_five = {}
    hash_64_found = False
    numbers_found = 0
    curr_id = 0
    while not hash_64_found:
        three_of_a_kind = get_three_of_a_kind_char(base_hash, curr_id, saved_results_three, saved_hashes, hash_count)
        if three_of_a_kind:
            identical_five_found = False
            for i in range(1, 1000):
                if not identical_five_found:
                    five_of_a_kinds = get_five_of_a_kind_chars(base_hash, curr_id + i, saved_results_five, saved_hashes,
                                                               hash_count)
                    if five_of_a_kinds and three_of_a_kind in five_of_a_kinds:
                        numbers_found += 1
                        # print(str(numbers_found)+"  - found with id: "+str(curr_id)+
                        #       " and matching hex-sign: "+str(three_of_a_kind))
                        identical_five_found = True
                        if numbers_found > 63:
                            hash_64_found = True
        curr_id += 1
    return curr_id - 1


def get_three_of_a_kind_char(base_id, hash_id, saved_results, saved_hashes, hash_depth):
    if not (hash_id in saved_results):
        txt = get_stretched_hash(base_id, hash_id, saved_hashes, hash_depth)
        result = re.search("(.)\\1{2}", txt)
        result_txt = ""
        if result:
            result_txt = result.group(1)
        saved_results[hash_id] = result_txt
        return result_txt
    else:
        return saved_results[hash_id]


def get_five_of_a_kind_chars(base_id, hash_id, saved_results, saved_hashes, hash_depth):
    if not (hash_id in saved_results):
        txt = get_stretched_hash(base_id, hash_id, saved_hashes, hash_depth)
        result = re.findall("(.)\\1{4}", txt)
        saved_results[hash_id] = result
        return result
    else:
        return saved_results[hash_id]


def get_stretched_hash(base_id, hash_id, saved_hashes, hash_depth):
    if not (hash_id in saved_hashes):
        txt = (base_id + str(hash_id)).encode("utf-8")
        for i in range(0, hash_depth):
            md5_sum = hashlib.md5()
            md5_sum.update(txt)
            txt = str(md5_sum.hexdigest()).encode("utf-8")
        saved_hashes[hash_id] = str(txt)
        return str(txt)
    else:
        return saved_hashes[hash_id]


main()
