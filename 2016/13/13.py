def main():
    cell = [1, 1]
    cells = list()
    cells.append(cell)
    cell_found = False

    loop_counter = 0
    old_cells = set()
    old_cells.add("1_1")
    cells_reachable = 1
    cells_reachable_in_50_steps = 0
    while not cell_found and loop_counter < 1000:
        loop_counter += 1
        new_cells = find_all_valid_cells_next_step(cells, old_cells, 1362)
        cell_found = set_has_cells_with_position(new_cells, 31, 39)
        cells = new_cells
        cells_reachable += len(new_cells)
        if loop_counter == 50:
            cells_reachable_in_50_steps = cells_reachable
        print("Current step: " + str(loop_counter) +
              " current cells reachable this step: " + str(len(new_cells)) +
              " cells in old_cells: " + str(len(old_cells)) +
              " currently: " + str(cells_reachable) + " cells reachable")

    if cell_found:
        print ("13 A Found position (31,39) in " + str(loop_counter) + " steps")
    else:
        print("Cell not found in " + str(loop_counter) + " steps")

    print ("13 B positions reachable in 50 steps: "+str(cells_reachable_in_50_steps))




def set_has_cells_with_position(cells, x, y):
    for cell in cells:
        if cell[0] == x and cell[1] == y:
            return True
        else:
            return False


def find_all_valid_cells_next_step(cells_curr_step, old_cells, offset):
    new_cells = []
    for cell in cells_curr_step:
        new_cells += get_valid_surrounding_cells(cell, old_cells, offset)
    return new_cells


def get_valid_surrounding_cells(cell, old_cells, offset):
    surrounding_cells = get_surrounding_cells(cell)
    valid_surrounding_cells = []
    for curr_cell in surrounding_cells:
        cell_str = str(curr_cell[0]) + "_" + str(curr_cell[1])
        if not (cell_str in old_cells):
            if not is_wall(curr_cell[0], curr_cell[1], offset):
                valid_surrounding_cells.append(curr_cell)
        old_cells.add(cell_str)
    return valid_surrounding_cells


def get_surrounding_cells(cell):
    surrounding_cells = set()
    surrounding_cells.add(((cell[0]), cell[1] + 1))
    surrounding_cells.add(((cell[0] + 1), cell[1]))

    if cell[0] > 0:
        surrounding_cells.add((cell[0] - 1, cell[1]))

    if cell[1] > 0:
        surrounding_cells.add((cell[0], cell[1] - 1))

    return surrounding_cells


def is_wall(x, y, offset):
    value = x * x + 3 * x + 2 * x * y + y + y * y + offset
    return bin(value).count('1') % 2 == 1


main()
