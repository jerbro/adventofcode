import functools
import re
import itertools


def main():
    commands = parsefile()
    password = "abcdefgh"
    print("21A: Scramblind the password " + password + " gives: " + scramble_password(commands, password))
    scrambled_password = "fbgdceah"
    print("21B: Password based on scrambled password " + scrambled_password + " is: " + find_password_from_scramble(
        commands, scrambled_password))


def find_password_from_scramble(commands, scrambled_password):
    for curr_password in itertools.permutations(scrambled_password, len(scrambled_password)):
        curr_str_password = "".join(curr_password)
        if scramble_password(commands, curr_str_password) == scrambled_password:
            return curr_str_password


def scramble_password(commands, password):
    result = password
    for command in commands:
        result = command(result)
    return result


def parsefile():
    commands = list()
    lines = open("input.txt", 'r').read().splitlines()
    for line in lines:
        commands.append(parseline(line))
    return commands


def parseline(line):
    swp_pos = re.search("swap position (\d+) with position (\d+)", line)
    swp_ltr = re.search("swap letter ([a-zA-Z]) with letter ([a-zA-Z])", line)
    rot_steps = re.search("rotate (left|right) (\d+) steps?", line)
    rot_char = re.search("rotate based on position of letter ([a-zA-Z])", line)
    rev_pos = re.search("reverse positions (\d+) through (\d+)", line)
    mov_pos = re.search("move position (\d+) to position (\d+)", line)
    if swp_pos:
        return functools.partial(swap_pos, int(swp_pos.group(1)), int(swp_pos.group(2)))
    if swp_ltr:
        return functools.partial(swap_char, (swp_ltr.group(1)), (swp_ltr.group(2)))
    if rot_steps:
        if rot_steps.group(1) == "left":
            return functools.partial(rotate_steps, -1 * int(rot_steps.group(2)))
        else:
            return functools.partial(rotate_steps, int(rot_steps.group(2)))
    if rot_char:
        return functools.partial(rotate_character, (rot_char.group(1)))
    if rev_pos:
        return functools.partial(reverse_positions, int(rev_pos.group(1)), int(rev_pos.group(2)))
    if mov_pos:
        return functools.partial(move_position, int(mov_pos.group(1)), int(mov_pos.group(2)))


def test():
    command = functools.partial(swap_pos, 4, 0)
    result = command("abcde")
    print(result)

    command = functools.partial(swap_char, "b", "d")
    result = command(result)
    print(result)

    command = functools.partial(reverse_positions, 0, 4)
    result = command(result)
    print(result)

    command = functools.partial(rotate_steps, -1)
    result = command(result)
    print(result)

    command = functools.partial(move_position, 1, 4)
    result = command(result)
    print(result)

    command = functools.partial(move_position, 3, 0)
    result = command(result)
    print(result)

    command = functools.partial(rotate_character, "b")
    result = command(result)
    print(result)

    command = functools.partial(rotate_character, "d")
    result = command(result)
    print(result)


def rotate_character(ch1, source):
    pos_to_rotate = 1
    pos_of_ch1 = source.find(ch1)
    pos_to_rotate += pos_of_ch1
    if pos_of_ch1 >= 4:
        pos_to_rotate += 1
    return rotate_steps(pos_to_rotate, source)


def move_position(pos1, pos2, source):
    char = source[pos1:pos1 + 1]
    source_without_pos1 = source[:pos1] + source[pos1 + 1:]
    result = "" + source_without_pos1[:pos2] + char + source_without_pos1[pos2:]
    return result


def reverse_positions(sapos, endpos, source):
    result = source[:sapos]
    part_str = source[sapos:endpos + 1]
    result += part_str[::-1]
    result += source[endpos + 1:]
    return result


def rotate_steps(steps, source):
    txt = list(source)
    result = ""
    for i in range(0, len(txt)):
        result += (txt[(i - steps) % len(txt)])
    return result


def swap_char(ch1, ch2, source):
    txt = list(source)
    result = ""
    for ch in txt:
        if ch == ch1:
            result += ch2
        elif ch == ch2:
            result += ch1
        else:
            result += ch
    return result


def swap_pos(pos1, pos2, source):
    txt = list(source)
    txt[pos1] = source[pos2]
    txt[pos2] = source[pos1]
    return "".join(txt)


def func1(arg1, arg2):
    return "func1. arg1: " + str(arg1) + " arg2: " + str(arg2)


def func2():
    return "func1"


main()
