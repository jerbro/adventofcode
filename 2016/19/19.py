import collections


def main():
    print("19 A: the elf that got anything is: " + str(get_elf_which_get_it_all(3014603)))

    elf_list = collections.deque()
    for i in range(1, 3014603 + 1):
        elf_list.append(i)
    print "Finished filling"

    counter = 0
    for i in range(0, 3014603 - 1):

        counter += 1
        # print "before remove"+str(elf_list)
        rotations = len(elf_list) / 2
        elf_list.rotate(-1 * rotations)
        # print "after_rotating" + str(elf_list)
        elf_list.popleft()
        elf_list.rotate(rotations - 1)
        # print "after pop and extra rotating" + str(elf_list)
        if counter % 100 == 0:
            print counter
    print ("19 B: " + str(elf_list))
    # counter = 0
    # while len(elf_list)>10:
    #     counter +=1
    #     del elf_list[3]
    #     if counter % 100 == 0:
    #         print counter
    # print elf_list


def get_number_of_opposite_chair(chair_count, chairs_before_current):
    result = (chair_count / 2 + chairs_before_current)
    if result > chair_count:
        result -= chair_count
    return result


def get_elf_which_get_it_all(number_of_elves):
    keep = True
    elf_list = list()

    for i in range(1, number_of_elves + 1):
        elf_list.append(i)

    while len(elf_list) > 1:
        (elf_list, keep) = cycle_list(elf_list, keep)
        # print(str(elf_list) + " -  " + str(keep))
    return elf_list[0]


def cycle_list(elf_list, keep_init):
    keep = keep_init
    new_list = list()
    for item in elf_list:
        if keep:
            new_list.append(item)
            keep = False
        else:
            keep = True
    return_value = (new_list, keep)
    return return_value


main()
