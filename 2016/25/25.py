import re


def main():
    lines = open('input.txt', 'r').read().splitlines()

    parsed_lines = []
    for line in lines:
        parsed_lines.append(parse_instruction(line))

    print("25A. first 1000 outputs are correct(so assume this goes on forever) for startvalue: " + str(
        get_startvalue_for_good_output(parsed_lines)))


def get_startvalue_for_good_output(parsed_lines):
    found = False
    nr = -1
    while not found:
        nr += 1
        found = output_of_run_program_is_valid(parsed_lines, nr, 1000)

    return nr


def output_of_run_program_is_valid(program, startvalue, number_of_outputs):
    outcounter = 0
    itercounter = 0
    last_out = -1
    pc = 0
    final_instruction = len(program)
    registers = {'a': startvalue, 'b': 0, 'c': 0, 'd': 0}
    while pc < final_instruction and outcounter < number_of_outputs:
        itercounter += 1
        result = execute(program[pc], registers, program, pc)
        pc += result['stp']
        if result['out'] != '':
            outcounter += 1
            if last_out != -1:
                if result['out'] == '1' and last_out == '1':
                    return False
                if result['out'] == '0' and last_out == '0':
                    return False
                if result['out'] != '0' and result['out'] != '1':
                    return False
                last_out = result['out']
            else:
                last_out = result['out']
    return True


def execute(instruction, registers, program, pc):
    use_alt = instruction['command_to_use']
    if instruction['command'][use_alt] == 'jnz':
        return instr_jnz(instruction, registers)
    elif instruction['command'][use_alt] == 'inc':
        return instr_inc(instruction, registers)
    elif instruction['command'][use_alt] == 'dec':
        return instr_dec(instruction, registers)
    elif instruction['command'][use_alt] == 'cpy':
        return instr_cpy(instruction, registers)
    elif instruction['command'][use_alt] == 'tgl':
        return instr_tgl(instruction, registers, program, pc)
    elif instruction['command'][use_alt] == 'out':
        return instr_out(instruction, registers)
    else:
        return {999999, ''}


def is_int(value):
    try:
        int(value)
        return True
    except ValueError:
        return False


def instr_out(parsed_instruction, registers):
    if parsed_instruction['arg1_is_value']:
        out = str(parsed_instruction['arg1'])
    else:
        out = str(registers[parsed_instruction['arg1']])
    return {'stp': 1, 'out': out}


def instr_tgl(parsed_instruction, registers, program, pc):
    if parsed_instruction['arg1_is_value']:
        offset = parsed_instruction['arg1']
    else:
        offset = registers[parsed_instruction['arg1']]
    if pc + offset < len(program):
        if program[pc + offset]['command_to_use'] == 1:
            program[pc + offset]['command_to_use'] = 0
        else:
            program[pc + offset]['command_to_use'] = 1

    return {'stp': 1, 'out': ''}


def instr_cpy(parsed_instruction, registers):
    if not parsed_instruction['arg2_is_value']:
        if parsed_instruction['arg1_is_value']:
            registers[parsed_instruction['arg2']] = parsed_instruction['arg1']
        else:
            registers[parsed_instruction['arg2']] = registers[parsed_instruction['arg1']]
    return {'stp': 1, 'out': ''}


def instr_inc(parsed_instruction, registers):
    registers[parsed_instruction['arg1']] += 1
    return {'stp': 1, 'out': ''}


def instr_dec(parsed_instruction, registers):
    registers[parsed_instruction['arg1']] -= 1
    return {'stp': 1, 'out': ''}


def instr_jnz(parsed_instruction, registers):
    if (parsed_instruction['arg1_is_value']) and parsed_instruction['arg1'] == 0:
        return {'stp': 1, 'out': ''}
    elif not (parsed_instruction['arg1_is_value']) and registers[parsed_instruction['arg1']] == 0:
        return {'stp': 1, 'out': ''}
    else:
        if parsed_instruction['arg2_is_value']:
            return {'stp': parsed_instruction['arg2'], 'out': ''}
        else:
            return {'stp': registers[parsed_instruction['arg2']], 'out': ''}


def add_toggled_instruction(parsed_instruction):
    orig_command = parsed_instruction['command']
    if parsed_instruction['arg2'] == '':
        if parsed_instruction['command'] == 'inc':
            tgl_command = 'dec'
        else:
            tgl_command = 'inc'
    else:
        if parsed_instruction['command'] == 'jnz':
            tgl_command = 'cpy'
        else:
            tgl_command = 'jnz'

    parsed_instruction['command'] = {0: orig_command, 1: tgl_command}
    parsed_instruction['command_to_use'] = 0
    return parsed_instruction


def parse_instruction(instruction):
    instruction_parts = re.search("([a-z]*) ([-0-9a-z]*)( )?([-0-9a-z]*)?", instruction)
    parsed_instruction = {}
    if instruction_parts:
        parsed_instruction['command'] = instruction_parts.group(1)
        parsed_instruction['arg1_is_value'] = is_int(instruction_parts.group(2))
        if parsed_instruction['arg1_is_value']:
            parsed_instruction['arg1'] = int(instruction_parts.group(2))
        else:
            parsed_instruction['arg1'] = instruction_parts.group(2)

        parsed_instruction['arg2_is_value'] = is_int(instruction_parts.group(4))
        if parsed_instruction['arg2_is_value']:
            parsed_instruction['arg2'] = int(instruction_parts.group(4))
        else:
            parsed_instruction['arg2'] = instruction_parts.group(4)

    return add_toggled_instruction(parsed_instruction)


main()
